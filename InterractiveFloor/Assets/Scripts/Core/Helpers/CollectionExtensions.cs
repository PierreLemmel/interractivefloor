﻿using System.Collections.Generic;

namespace InterractiveFloor
{
    public static class CollectionExtensions
    {
        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> sequence)
        {
            Guard.NotNull(collection, nameof(collection));
            Guard.NotNull(sequence, nameof(sequence));

            foreach (T item in sequence)
                collection.Add(item);
        }

        public static void AddRange<T>(this  ICollection<T> collection, params T[] items)
        {
            IEnumerable<T> itemsAsSequence = items;
            collection.AddRange(itemsAsSequence);
        }
    }
}