﻿using System;
using System.Linq;

namespace InterractiveFloor
{
    public static class EnumExtensions
    {
        public static bool IsOneOf<TEnum>(this TEnum input, params TEnum[] values) where TEnum : Enum => values.Any(value => value.Equals(input));
    }
}