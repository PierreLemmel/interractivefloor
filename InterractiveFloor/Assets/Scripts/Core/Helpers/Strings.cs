﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace InterractiveFloor
{
    public static class Strings
    {
        public static string UncapitalizeFirstLetter(this string input)
        {
            char[] chars = input.ToCharArray();
            chars[0] = char.ToLower(chars[0]);

            return new string(chars);
        }

        public static string RemoveDiacritics(this string text)
        {
            string normalized = text.Normalize(NormalizationForm.FormD);
            StringBuilder stringBuilder = new StringBuilder();

            char[] chars = normalized
                .Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                .ToArray();

            string result = new string(chars).Normalize(NormalizationForm.FormC);

            return result;
        }

        public static string TrimPunctuation(this string text)
        {
            int length = text.Length;
            
            int start = 0;
            for (int i = 0; i < length; i++)
            {
                char c = text[i];
                if (char.IsPunctuation(c) || char.IsWhiteSpace(c))
                    start++;
                else
                    break;
            }

            int end = 0;
            for (int i = length-1; i >= 0; i--)
            {
                char c = text[i];
                if (char.IsPunctuation(c) || char.IsWhiteSpace(c))
                    end++;
                else
                    break;
            }

            return text.Substring(start, length - (end + start));
        }

        public static bool IsNullOrEmpty(this string input) => string.IsNullOrEmpty(input);
        public static bool IsNullOrWhiteSpace(this string input) => string.IsNullOrWhiteSpace(input);
    }
}