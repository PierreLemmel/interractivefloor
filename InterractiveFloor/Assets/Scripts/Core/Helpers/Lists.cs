﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InterractiveFloor
{
    public static class Lists
    {
        public static T Last<T>(this IList<T> list)
        {
            if (list.IsEmpty())
                throw new InvalidOperationException("Can't take last element of an empty list");
            else
                return list[list.Count - 1];
        }

        public static IEnumerable<T> NLasts<T>(this IList<T> list, int n)
        {
            int count = list.Count;
            if (list.Count < n)
                throw new InvalidOperationException($"Can't take that many elements ({n}) : the list is too small ({list.Count}");

            return Enumerable.Range(count - n, n).Select(i => list[i]);
        }

        public static List<T> Copy<T>(this List<T> list) => list.ToList();
    }
}
