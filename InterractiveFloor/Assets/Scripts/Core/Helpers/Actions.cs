﻿using System;

namespace InterractiveFloor
{
    public static class Actions
    {
        public static Action<T> Empty<T>() => t => { };
    }
}