﻿using System;
using UnityEngine;

using UObject = UnityEngine.Object;

namespace InterractiveFloor
{
    public static class Guard
    {
        public static void NotNull(object value, string paramName)
        {
            if (value == null)
                throw new ArgumentNullException(paramName);
        }

        /// <summary>
        /// Override for Unity Objects.
        /// </summary>
        /// <remarks>
        /// Internally <c>UnityEngine.Object</c> overrides <c>System.Object</c>
        /// equal operator and causes issues with null comparison.
        /// </remarks>
        public static void NotNull<TUnityObject>(TUnityObject unityObject, string paramName) where TUnityObject : UObject
        {
            if (unityObject == null)
                throw new ArgumentNullException(paramName);
        }

        public static void StrictlyPositive(int param, string paramName)
        {
            if (param <= 0)
                throw new ArgumentException($"The {paramName} parameter must be positive.", paramName);
        }
    }
}
