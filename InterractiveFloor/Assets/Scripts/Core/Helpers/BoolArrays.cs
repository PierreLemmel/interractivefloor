﻿using System;

namespace InterractiveFloor
{
    public static class BoolArrays
    {
        public static void Invert(this bool[] boolSeq) => boolSeq.Set(val => !val);
    }
}