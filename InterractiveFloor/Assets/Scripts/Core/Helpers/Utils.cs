﻿namespace InterractiveFloor
{
    public static class Utils
    {
        public static void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        public static void Swap<T>(ref T elt1, ref T elt2, ref T elt3)
        {
            T temp = elt1;
            elt1 = elt2;
            elt2 = elt3;
            elt3 = temp;
        }
    }
}