﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace InterractiveFloor
{
    public static class MoreExpressions
    {
        [Obsolete("Use this method only for debugging purposes", error: false)]
        public static string DebugView(this Expression expression) => (string)typeof(Expression).GetProperty("DebugView", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(expression);
    }
}