﻿namespace InterractiveFloor.Unity
{
    public interface IServiceProvider<TService>
    {
        TService GetInstance();
    }
}