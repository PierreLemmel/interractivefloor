﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InterractiveFloor
{
    public class Grid<T> : IEnumerable<GridCell<T>>
    {
        public delegate T ValueFunction(int row, int col);

        private GridCell<T>[,] cells;

        public int Width => cells.GetLength(1);
        public int Height => cells.GetLength(0);
        public int CellCount => Width * Height;

        public GridRowCollection<T> Rows { get; }
        public GridColumnCollection<T> Columns { get; }

        public Grid(T[,] values)
        {
            Guard.NotNull(values, nameof(values));

            int width = values.GetLength(1);
            int height = values.GetLength(0);

            Guard.StrictlyPositive(width, nameof(width));
            Guard.StrictlyPositive(height, nameof(height));

            CreateCells(width, height);
            InitializeCells(values);

            Rows = new GridRowCollection<T>(this);
            Columns = new GridColumnCollection<T>(this);
        }

        private Grid(GridCell<T>[,] cells)
        {
            int width = cells.GetLength(1);
            int height = cells.GetLength(0);

            CreateCells(width, height);
            InitializeCells(cells);

            Rows = new GridRowCollection<T>(this);
            Columns = new GridColumnCollection<T>(this);
        }

        public Grid(int width, int height, T defaultValue)
        {
            Guard.StrictlyPositive(width, nameof(width));
            Guard.StrictlyPositive(height, nameof(height));

            CreateCells(width, height);
            InitializeCells(defaultValue);

            Rows = new GridRowCollection<T>(this);
            Columns = new GridColumnCollection<T>(this);
        }

        public Grid(int width, int height, ValueFunction valueFunc)
        {
            Guard.StrictlyPositive(width, nameof(width));
            Guard.StrictlyPositive(height, nameof(height));

            CreateCells(width, height);
            InitializeCells(valueFunc);

            Rows = new GridRowCollection<T>(this);
            Columns = new GridColumnCollection<T>(this);
        }

        public Grid(int width, int height) : this(width, height, default(T)) { }
        
        public Grid(int size, T defaultValue) : this(size, size, defaultValue) { }

        public Grid(int size) : this(size, default(T)) { }

        public GridCell<T> this[int row, int col] => cells[row, col];

        private void CreateCells(int width, int height)
        {
            cells = new GridCell<T>[height, width];

            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    cells[row, col] = new GridCell<T>(this, row, col);
                }
            }
        }

        private void InitializeCells(T[,] values)
        {
            int width = this.Width;
            int height = this.Height;

            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    GridCell<T> cell = this[row, col];
                    cell.Value = values[row, col];
                }
            }
        }

        private void InitializeCells(GridCell<T>[,] otherCells)
        {
            int width = this.Width;
            int height = this.Height;

            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    GridCell<T> cell = this[row, col];
                    cell.Value = otherCells[row, col];
                }
            }
        }

        private void InitializeCells(T value)
        {
            foreach (GridCell<T> cell in this)
                cell.Value = value;
        }

        private void InitializeCells(ValueFunction valueFunc)
        {
            foreach (GridCell<T> cell in this)
                cell.Value = valueFunc(cell.RowIndex, cell.ColumnIndex);
        }

        public IEnumerator<GridCell<T>> GetEnumerator()
        {
            int width = this.Width;
            int height = this.Height;

            for (int row = 0; row < height; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    yield return cells[row, col];
                }
            }
        }

        public Grid<T> Copy() => new Grid<T>(cells);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}