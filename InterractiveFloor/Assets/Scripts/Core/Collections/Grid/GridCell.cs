﻿using System;
using System.Collections.Generic;

namespace InterractiveFloor
{
    public sealed class GridCell<T>
    {
        public int RowIndex { get; }
        public int ColumnIndex { get; }

        public GridRow<T> Row { get { return Grid.Rows[RowIndex]; } }
        public GridColumn<T> Column { get { return Grid.Columns[ColumnIndex]; } }

        public Grid<T> Grid { get; }
        public T Value { get; set; }

        internal GridCell(Grid<T> grid, int row, int column, T value)
        {
            Guard.NotNull(grid, nameof(grid));
            Grid = grid;

            RowIndex = row;
            ColumnIndex = column;
            
            Value = value;
        }

        internal GridCell(Grid<T> grid, int row, int column) : this (grid, row, column, default) { }

        public static implicit operator T(GridCell<T> cell) => cell.Value;

        private GridCell<T>[] neighbours;
        public GridCell<T>[] Neighbours => neighbours = neighbours ?? GetNeighbours();

        private GridCell<T>[] GetNeighbours()
        {
            int width = Grid.Width;
            int height = Grid.Height;

            if (RowIndex == 0)
            {
                if (ColumnIndex == 0)
                {
                    return new GridCell<T>[]
                    {
                        Grid[0, 1],
                        Grid[1, 1],
                        Grid[1, 0]
                    };
                }
                else if (ColumnIndex == width - 1)
                {
                    return new GridCell<T>[]
                    {
                        Grid[0, width - 2],
                        Grid[1, width - 2],
                        Grid[1, width - 1]
                    };
                }
                else
                {
                    return new GridCell<T>[]
                    {
                        Grid[0, ColumnIndex - 1],
                        Grid[1, ColumnIndex - 1],
                        Grid[1, ColumnIndex],
                        Grid[1, ColumnIndex + 1],
                        Grid[0, ColumnIndex + 1]
                    };
                }
            }
            else if (RowIndex == height - 1)
            {
                if (ColumnIndex == 0)
                {
                    return new GridCell<T>[]
                    {
                        Grid[height - 1, 1],
                        Grid[height - 2, 1],
                        Grid[height - 2, 0]
                    };
                }
                else if (ColumnIndex == width - 1)
                {
                    return new GridCell<T>[]
                    {
                        Grid[height - 1, width - 2],
                        Grid[height - 2, width - 2],
                        Grid[height - 2, width - 1]
                    };
                }
                else
                {
                    return new GridCell<T>[]
                    {
                        Grid[height - 1, ColumnIndex - 1],
                        Grid[height - 2, ColumnIndex - 1],
                        Grid[height - 2, ColumnIndex],
                        Grid[height - 2, ColumnIndex + 1],
                        Grid[height - 1, ColumnIndex + 1]
                    };
                }
            }
            else
            {
                if (ColumnIndex == 0)
                {
                    return new GridCell<T>[]
                    {
                        Grid[RowIndex - 1, 0],
                        Grid[RowIndex - 1, 1],
                        Grid[RowIndex, 1],
                        Grid[RowIndex + 1, 1],
                        Grid[RowIndex + 1, 0]
                    };
                }
                else if (ColumnIndex == width - 1)
                {
                    return new GridCell<T>[]
                    {
                        Grid[RowIndex - 1, width - 1],
                        Grid[RowIndex - 1, width - 2],
                        Grid[RowIndex, width - 2],
                        Grid[RowIndex + 1, width - 2],
                        Grid[RowIndex + 1, width - 1]
                    };
                }
                else
                {
                    return new GridCell<T>[]
                    {
                        Grid[RowIndex - 1, ColumnIndex - 1],
                        Grid[RowIndex - 1, ColumnIndex    ],
                        Grid[RowIndex - 1, ColumnIndex + 1],
                        Grid[RowIndex    , ColumnIndex + 1],
                        Grid[RowIndex + 1, ColumnIndex + 1],
                        Grid[RowIndex + 1, ColumnIndex    ],
                        Grid[RowIndex + 1, ColumnIndex - 1],
                        Grid[RowIndex    , ColumnIndex - 1],
                    };
                }
            }
        }
    }
}