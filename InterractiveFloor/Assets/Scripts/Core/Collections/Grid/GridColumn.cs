﻿using System.Collections;
using System.Collections.Generic;

namespace InterractiveFloor
{
    public sealed class GridColumn<T> : IEnumerable<GridCell<T>>
    {
        public int Column { get; }
        public Grid<T> Grid { get; }

        internal GridColumn(Grid<T> grid, int column)
        {
            Guard.NotNull(grid, nameof(grid));
            Grid = grid;
            Column = column;
        }

        public GridCell<T> this[int row] => Grid[row, Column];

        public IEnumerator<GridCell<T>> GetEnumerator()
        {
            int height = Grid.Height;
            for (int row = 0; row < height; row++)
                yield return this[row];
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
