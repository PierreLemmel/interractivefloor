﻿namespace InterractiveFloor
{
    public static class FactoriesExtensions
    {
        public static TService Create<TService>(this IFactory<TService> factory) where TService : class
            => factory.Create<object>(null);

        public static TService Create<TService, TArg>(this IFactory<TService, TArg> factory, TArg arg) where TService : class
            => factory.Create<object>(null, arg);
    }
}