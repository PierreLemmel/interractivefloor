﻿namespace InterractiveFloor
{
    public interface IFactory<TService> where TService : class
    {
        TService Create<TContext>(TContext context);
    }

    public interface IFactory<TService, TArg> where TService : class
    {
        TService Create<TContext>(TContext context, TArg arg);
    }
}