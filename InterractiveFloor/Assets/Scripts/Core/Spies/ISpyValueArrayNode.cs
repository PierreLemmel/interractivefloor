﻿using InterractiveFloor.Spies;

namespace InterractiveFloor.Spies
{
    public interface ISpyValueArrayNode<TStruct> where TStruct : struct
    {
        ISpySetupActionNode<TStruct[]> HasChanged();
    }
}