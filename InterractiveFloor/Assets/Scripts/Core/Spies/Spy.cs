﻿using InterractiveFloor;
using InterractiveFloor.Spies;
using System;

namespace InterractiveFloor
{
    public static class Spy
    {
        public static ISpy CreateNew() => new SpyImpl();
    }
}