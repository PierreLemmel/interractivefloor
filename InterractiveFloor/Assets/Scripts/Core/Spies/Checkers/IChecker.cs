﻿namespace InterractiveFloor.Spies.Checkers
{
    internal interface IChecker<TModel>
    {
        bool CheckForChanges(TModel model);
    }
}