﻿using System;

namespace InterractiveFloor.Spies
{
    public interface ISpySetupActionNode<TModel>
    {
        ISpyConfig Do(Action<TModel> action);
    }
}