﻿using InterractiveFloor.Spies;

namespace InterractiveFloor
{
    public interface ISpy : ISpyConfig
    {
        void DetectChanges();
    }
}