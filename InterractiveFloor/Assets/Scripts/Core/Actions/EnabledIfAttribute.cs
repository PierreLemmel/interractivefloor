﻿using System;

namespace InterractiveFloor
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class EnabledIfAttribute : Attribute
    {
        public string OtherMethod { get; }

        public EnabledIfAttribute(string otherMethod)
        {
            OtherMethod = otherMethod;
        }
    }
}