﻿using System;

namespace InterractiveFloor
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class UserActionAttribute : Attribute
    {
        public string ActionName { get; }

        public UserActionAttribute(string actionName)
        {
            ActionName = actionName;
        }

        public UserActionAttribute() : this(null) { }
    }
}