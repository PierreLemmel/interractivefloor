﻿using System;

namespace InterractiveFloor
{
    public abstract class Builder<T> : IBuilder
    {
        public abstract T Build();

        protected TField MissingField<TField>(string fieldName)
            => throw new InvalidOperationException($"The following field has not been initialized by the {GetType().Name}: {fieldName}");

        public static implicit operator T(Builder<T> builder) => builder.Build();
    }
}