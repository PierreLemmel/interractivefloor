﻿namespace InterractiveFloor
{
    public enum DisplayCondition
    {
        IsTrue,
        IsFalse,
        IsEqualTo
    }
}