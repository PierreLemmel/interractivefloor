﻿using System;

namespace InterractiveFloor
{
    public sealed class InRangeAttribute : AnnotationAttribute
    {
        public float Min { get; }
        public float Max { get; }

        public InRangeAttribute(float min, float max)
        {
            Min = min;
            Max = max;
        }

        public InRangeAttribute(int min, int max)
        {
            Min = min;
            Max = max;
        }
    }
}
