﻿namespace InterractiveFloor
{
    public sealed class NotEditableAttribute : AnnotationAttribute { }
}