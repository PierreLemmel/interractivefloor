﻿using System;

namespace InterractiveFloor
{
    public class ImplementsAttribute : TypeConstraintAttribute
    {
        public Type InterfaceType { get; }
        public string InterfaceSource { get; }

        public ImplementsAttribute(Type interfaceType)
        {
            InterfaceType = interfaceType;
        }

        public ImplementsAttribute(string interfaceSource)
        {
            InterfaceSource = interfaceSource;
        }
    }
}