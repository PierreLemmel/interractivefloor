﻿using System;

namespace InterractiveFloor
{
    public class GreaterThanAttribute : AnnotationAttribute
    {
        public float? Value { get; }
        public string ValueProperty { get; }
        public float Epsilon { get; }

        public GreaterThanAttribute(float value)
        {
            Value = value;
            ValueProperty = null;
            Epsilon = 0.0f;
        }

        public GreaterThanAttribute(string property, float epsilon = 0.1f)
        {
            Value = null;
            ValueProperty = property;
            Epsilon = epsilon;
        }
    }
}