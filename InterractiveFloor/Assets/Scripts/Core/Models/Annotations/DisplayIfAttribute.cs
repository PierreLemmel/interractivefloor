﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InterractiveFloor
{
    public sealed class DisplayIfAttribute : AnnotationAttribute
    {
        public string OtherProperty { get; }
        public DisplayCondition Condition { get; }
        public IReadOnlyCollection<object> Parameters { get; }

        public DisplayIfAttribute(string otherProperty, DisplayCondition condition, params object[] parameters)
        {
            OtherProperty = otherProperty;
            Condition = condition;
            Parameters = parameters?.ToList() ?? new List<object>(0);
        }
    }
}