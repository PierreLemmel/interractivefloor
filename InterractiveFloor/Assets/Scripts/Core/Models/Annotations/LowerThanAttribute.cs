﻿using System;

namespace InterractiveFloor
{
    public class LowerThanAttribute : AnnotationAttribute
    {
        public float? Value { get; }
        public string ValueProperty { get; }
        public float Epsilon { get; }

        public LowerThanAttribute(float value)
        {
            Value = value;
            ValueProperty = null;
            Epsilon = 0.0f;
        }

        public LowerThanAttribute(string property, float epsilon = 0.1f)
        {
            Value = null;
            ValueProperty = property;
            Epsilon = epsilon;
        }
    }
}