﻿using System;

namespace InterractiveFloor
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public abstract class AnnotationAttribute : Attribute
    {
    }
}