﻿using System;

namespace InterractiveFloor
{
    public class ModelValidationException : ArgumentException
    {
        public ModelValidationException(string message, string paramName) : base(message, paramName) { }
    }
}