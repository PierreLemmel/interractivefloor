﻿using System;
using UnityEngine;

namespace InterractiveFloor.Internal
{
    public class __LowerThanAttribute : PropertyAttribute
    {
        public float? Value { get; }
        public string ValueProperty { get; }
        public float Epsilon { get; }


        public __LowerThanAttribute(float value)
        {
            Value = value;
            ValueProperty = null;
            Epsilon = 0.0f;
        }

        public __LowerThanAttribute(string property, float epsilon)
        {
            Value = null;
            ValueProperty = property;
            Epsilon = epsilon;
        }
    }
}