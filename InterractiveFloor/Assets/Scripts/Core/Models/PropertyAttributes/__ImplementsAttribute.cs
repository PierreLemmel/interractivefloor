﻿using System;
using UnityEngine;

namespace InterractiveFloor.Internal
{
    public class __ImplementsAttribute : PropertyAttribute
    {
        public Type InterfaceType { get; }
        public string InterfaceSource { get; }

        public __ImplementsAttribute(Type interfaceType, string interfaceSource)
        {
            InterfaceType = interfaceType;
            InterfaceSource = interfaceSource;
        }
    }
}