﻿using UnityEngine;

namespace InterractiveFloor.Internal
{
    public sealed class __EditTimeOnlyAttribute : PropertyAttribute { }
}