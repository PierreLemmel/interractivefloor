﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.Internal
{
    public sealed class __DisplayIfAttribute : PropertyAttribute
    {
        public string OtherProperty { get; }
        public DisplayCondition Condition { get; }
        public IReadOnlyCollection<object> Parameters { get; }

        public __DisplayIfAttribute(string otherProperty, DisplayCondition condition, params object[] parameters)
        {
            OtherProperty = otherProperty;
            Condition = condition;
            Parameters = parameters?.ToList() ?? new List<object>(0);

            order = 1000;
        }
    }
}