﻿using System;
using UnityEngine;

namespace InterractiveFloor.Internal
{
    public sealed class __ContentBundleSourceAttribute : PropertyAttribute { }
}