﻿using System;
using UnityEngine;

namespace InterractiveFloor.Internal
{
    public class __GreaterThanAttribute : PropertyAttribute
    {
        public float? Value { get; }
        public string ValueProperty { get; }
        public float Epsilon { get; }

        public __GreaterThanAttribute(float value)
        {
            Value = value;
            ValueProperty = null;
            Epsilon = 0.0f;
        }

        public __GreaterThanAttribute(string property, float epsilon)
        {
            Value = null;
            ValueProperty = property;
            Epsilon = epsilon;
        }
    }
}