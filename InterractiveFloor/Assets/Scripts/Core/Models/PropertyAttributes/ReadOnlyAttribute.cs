﻿using UnityEngine;

namespace InterractiveFloor
{
    public sealed class ReadOnlyAttribute : PropertyAttribute { }
}