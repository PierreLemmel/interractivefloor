﻿using System;

namespace InterractiveFloor
{
    public class NormalyDistributedSequenceBuilder : Builder<float[]>
    {
        private int length;
        private float mean = 0.0f;
        private float sigma = 1.0f;

        private float minValue = float.MinValue;
        private float maxValue = float.MaxValue;

        private float? totalValue = null;

        internal NormalyDistributedSequenceBuilder(int length) => this.length = length;

        public NormalyDistributedSequenceBuilder Mean(float mean)
        {
            this.mean = mean;
            return this;
        }

        public NormalyDistributedSequenceBuilder Sigma(float sigma)
        {
            this.sigma = sigma;
            return this;
        }

        public NormalyDistributedSequenceBuilder MinValue(float minValue)
        {
            this.minValue = minValue;
            return this;
        }

        public NormalyDistributedSequenceBuilder Positive() => MinValue(0.0f);

        public NormalyDistributedSequenceBuilder MaxValue(float maxValue)
        {
            this.maxValue = maxValue;
            return this;
        }

        public NormalyDistributedSequenceBuilder Negative() => MaxValue(0.0f);

        public NormalyDistributedSequenceBuilder EnsureTotalValue(float totalValue)
        {
            this.totalValue = totalValue;
            return this;
        }

        public override float[] Build()
        {
            float[] results = new float[length];

            float total = 0.0f;

            for (int i = 0; i < length; i++)
            {
                float result = MoreRandom.NormalDistribution(mean, sigma, minValue, maxValue);

                total += result;

                results[i] = result;
            }

            if (totalValue.HasValue)
            {
                float coef = totalValue.Value / total;

                for (int i = 0; i < length; i++)
                    results[i] *= coef;
            }

            return results;
        }
    }
}