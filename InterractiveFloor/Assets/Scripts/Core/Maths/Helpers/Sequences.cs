﻿namespace InterractiveFloor
{
    public static class Sequences
    {
        public static NormalyDistributedSequenceBuilder NormalyDistributed(int length) => new NormalyDistributedSequenceBuilder(length);
    }
}