﻿using System;
using UnityEngine;

using URandom = UnityEngine.Random;

namespace InterractiveFloor
{
    public static class MoreRandom
    {
        public static bool BoolValue => URandom.value > 0.5f;

        public static float NormalDistribution()
        {
            float u, v, S;

            do
            {
                u = URandom.Range(-1.0f, 1.0f);
                v = URandom.Range(-1.0f, 1.0f);
                S = u * u + v * v;
            }
            while (S >= 1.0);

            float fac = Mathf.Sqrt(-2.0f * Mathf.Log(S) / S);
            return Mathf.Clamp(u * fac, -3.0f, 3.0f);
        }

        public static float NormalDistribution(float mean, float sigma) => mean + NormalDistribution() * sigma;
        public static float NormalDistribution(float mean, float sigma, float minValue, float maxValue) => Mathf.Clamp(NormalDistribution(mean, sigma), minValue, maxValue);

        public static void Randomize(this bool[] boolSeq) => boolSeq.Set(() => BoolValue);
    }
}