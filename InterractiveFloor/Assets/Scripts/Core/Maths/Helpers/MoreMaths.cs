﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class MoreMaths
    {
        public static bool IsInRange(this float value, float min, float max) => value >= min && value <= max;

        public static float InRange(this float value, float min, float max) => value >= min ? (value <= max ? value : max) : min;

        public static float GreaterThan(this float value, float min) => value >= min ? value : min;
        public static float LowerThan(this float value, float max) => value <= max ? value : max;

        public static float Positive(this float value) => value.GreaterThan(0.0f);
        public static float Negative(this float value) => value.LowerThan(0.0f);
    }
}