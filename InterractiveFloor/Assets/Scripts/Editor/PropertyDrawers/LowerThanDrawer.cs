﻿using InterractiveFloor.Internal;
using UnityEditor;
using UnityEngine;

namespace InterractiveFloor.Editor
{
    [CustomPropertyDrawer(typeof(__LowerThanAttribute))]
    public class LowerThanDrawer : PropertyDrawer
    {
        private delegate float ReferenceValueFunction(SerializedProperty property);

        private float epsilon;
        private ReferenceValueFunction referenceValueFunc;

        private bool initialized = false;
        private void Initialize()
        {
            __LowerThanAttribute gta = (__LowerThanAttribute)attribute;

            if (gta.Value.HasValue)
                referenceValueFunc = _ => gta.Value.Value;
            else
            {

                referenceValueFunc = (prop) =>
                {
                    string otherPropName = "_" + gta.ValueProperty.UncapitalizeFirstLetter();
                    string currentPath = prop.propertyPath;
                    string otherpropPath = currentPath.Substring(0, currentPath.LastIndexOf('.')) + '.' + otherPropName;

                    return prop.serializedObject.FindProperty(otherpropPath).floatValue;
                };
            }

            epsilon = gta.Epsilon;

            initialized = true;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!initialized) Initialize();

            float oldValue = property.floatValue;
            float value = EditorGUI.FloatField(position, label, oldValue);

            float referenceValue = referenceValueFunc(property);
            value = Mathf.Min(value, referenceValue - epsilon);

            if (value != oldValue)
                property.floatValue = value;
        }
    }
}