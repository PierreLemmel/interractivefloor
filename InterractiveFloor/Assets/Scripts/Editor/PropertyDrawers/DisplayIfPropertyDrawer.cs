﻿using InterractiveFloor.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace InterractiveFloor.Editor
{
    [CustomPropertyDrawer(typeof(__DisplayIfAttribute))]
    public class DisplayIfPropertyDrawer : OverlayPropertyDrawer<__DisplayIfAttribute>
    {
        private delegate bool DisplayCheckFunction(SerializedProperty otherProperty, IReadOnlyCollection<object> parameters);

        private DisplayCheckFunction displayCheck;
        private string otherPropName;
        private IReadOnlyCollection<object> parameters;

        private static readonly IReadOnlyDictionary<DisplayCondition, DisplayCheckFunction> displayCheckFunctions = new Dictionary<DisplayCondition, DisplayCheckFunction>
        {
            [DisplayCondition.IsTrue] = IsTrueFunction,
            [DisplayCondition.IsFalse] = IsFalseFunction,
            [DisplayCondition.IsEqualTo] = IsEqualToFunction,
        };

        private static bool IsTrueFunction(SerializedProperty otherProperty, IReadOnlyCollection<object> parameters) => otherProperty.boolValue == true;
        private static bool IsFalseFunction(SerializedProperty otherProperty, IReadOnlyCollection<object> parameters) => otherProperty.boolValue == false;
        private static bool IsEqualToFunction(SerializedProperty otherProperty, IReadOnlyCollection<object> parameters)
        {
            object otherValue = parameters.Single();

            if (otherValue.GetType().IsEnum)
            {
                return otherProperty.intValue == (int)otherValue;
            }
            else
                throw new NotSupportedException("IsEqualTo only supports comparison with Enum types.");
        }

        private bool display = true;

        private bool initialized = false;
        private void Initialize()
        {
            __DisplayIfAttribute displayIfAttribute = (__DisplayIfAttribute)attribute;

            otherPropName = '_' + displayIfAttribute.OtherProperty.UncapitalizeFirstLetter();
            parameters = displayIfAttribute.Parameters;
            
            DisplayCondition condition = displayIfAttribute.Condition;

            if (displayCheckFunctions.TryGetValue(condition, out DisplayCheckFunction check))
                displayCheck = check;
            else
                throw new NotImplementedException($"Missing implementation for display condition '{condition}'");
        }

        private void CheckForDisplay(SerializedProperty property)
        {
            if (!initialized)
            {
                Initialize();
                initialized = true;
            }

            string currentPath = property.propertyPath;
            string otherPropPath = currentPath.Substring(0, currentPath.LastIndexOf('.')) + '.' + otherPropName;

            SerializedProperty otherProperty = property.serializedObject.FindProperty(otherPropPath);

            display = displayCheck(otherProperty, parameters);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            CheckForDisplay(property);

            return display ? GetHeight(property, label) : 0.0f;
        }

        private float GetHeight(SerializedProperty property, GUIContent label)
        {
            if (InnerDrawer != null)
                return InnerDrawer.GetPropertyHeight(property, label);
            else
                return EditorGUI.GetPropertyHeight(property);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (display)
                DisplayProperty(position, property, label);
        }

        private void DisplayProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            if (InnerDrawer != null)
                InnerDrawer.OnGUI(position, property, label);
            else
                EditorGUI.PropertyField(position, property, true);
        }
    }
}