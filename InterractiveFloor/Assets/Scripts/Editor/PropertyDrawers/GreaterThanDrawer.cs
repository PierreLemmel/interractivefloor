﻿using InterractiveFloor.Internal;
using System;
using UnityEditor;
using UnityEngine;

namespace InterractiveFloor.Editor
{
    [CustomPropertyDrawer(typeof(__GreaterThanAttribute))]
    public class GreaterThanDrawer : PropertyDrawer
    {
        private InnerDrawer innerDrawer;

        private bool initialized = false;
        private void InitializeDrawer()
        {
            __GreaterThanAttribute gta = (__GreaterThanAttribute)attribute;

            if (fieldInfo.FieldType == typeof(float))
                innerDrawer = new FloatDrawer(gta);
            else if (fieldInfo.FieldType == typeof(int))
                innerDrawer = new IntDrawer(gta);
            else
                throw new InvalidOperationException("");
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!initialized) InitializeDrawer();

            innerDrawer.DrawGUI(position, property, label);
        }

        private abstract class InnerDrawer
        {
            protected readonly __GreaterThanAttribute gta;

            public InnerDrawer(__GreaterThanAttribute gta) => this.gta = gta;

            public abstract void DrawGUI(Rect position, SerializedProperty property, GUIContent label);
        }

        private class FloatDrawer : InnerDrawer
        {
            private delegate float ReferenceValueFunction(SerializedProperty property);

            private float epsilon;
            private ReferenceValueFunction referenceValueFunc;

            private bool initialized = false;

            public FloatDrawer(__GreaterThanAttribute gta) : base(gta) { }

            private void Initialize()
            {
                if (gta.Value.HasValue)
                    referenceValueFunc = _ => gta.Value.Value;
                else
                {

                    referenceValueFunc = (prop) =>
                    {
                        string otherPropName = "_" + gta.ValueProperty.UncapitalizeFirstLetter();
                        string currentPath = prop.propertyPath;
                        string otherpropPath = currentPath.Substring(0, currentPath.LastIndexOf('.')) + '.' + otherPropName;

                        return prop.serializedObject.FindProperty(otherpropPath).floatValue;
                    };
                }

                epsilon = gta.Epsilon;

                initialized = true;
            }

            public override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
            {
                if (!initialized) Initialize();

                float oldValue = property.floatValue;
                float value = EditorGUI.FloatField(position, label, oldValue);

                float referenceValue = referenceValueFunc(property);
                value = Mathf.Max(value, referenceValue + epsilon);

                if (value != oldValue)
                    property.floatValue = value;
            }
        }

        private class IntDrawer : InnerDrawer
        {
            private delegate int ReferenceValueFunction(SerializedProperty property);
            private ReferenceValueFunction referenceValueFunc;

            private bool initialized = false;

            public IntDrawer(__GreaterThanAttribute gta) : base(gta) { }

            private void Initialize()
            {
                if (gta.Value.HasValue)
                    referenceValueFunc = _ => (int)gta.Value.Value;
                else
                {

                    referenceValueFunc = (prop) =>
                    {
                        string otherPropName = "_" + gta.ValueProperty.UncapitalizeFirstLetter();
                        string currentPath = prop.propertyPath;
                        string otherpropPath = currentPath.Substring(0, currentPath.LastIndexOf('.')) + '.' + otherPropName;

                        return prop.serializedObject.FindProperty(otherpropPath).intValue;
                    };
                }

                initialized = true;
            }

            public override void DrawGUI(Rect position, SerializedProperty property, GUIContent label)
            {
                if (!initialized) Initialize();

                int oldValue = property.intValue;
                int value = EditorGUI.IntField(position, label, oldValue);

                int referenceValue = referenceValueFunc(property);
                value = Mathf.Max(value, referenceValue);

                if (value != oldValue)
                    property.intValue = value;
            }
        }
    }
}