﻿using InterractiveFloor.Internal;
using InterractiveFloor.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace InterractiveFloor.Editor
{
	[CustomPropertyDrawer(typeof(__IsClassAttribute))]
	[CustomPropertyDrawer(typeof(__IsInterfaceAttribute))]
	[CustomPropertyDrawer(typeof(__ImplementsAttribute))]
	[CustomPropertyDrawer(typeof(TypeReference))]
    public sealed class TypeReferencePropertyDrawer : PropertyDrawer
    {
		private const string DisplayForNull = "None";
		private static List<Type> ProjectTypes = AppDomain
			.CurrentDomain
			.GetAssemblies()
			.Where(assembly => assembly.FullName.Contains("InterractiveFloor.")
				&& !assembly.FullName.Contains(".Tests")
			    && !assembly.FullName.Contains(".Editor")
				&& !assembly.FullName.Contains(".PostProcessing"))
			.SelectMany(assembly => assembly.GetTypes())
			.Where(type => !(type.IsClass && type.IsAbstract))
			.Where(type => !type.IsCaptureClass())
			.Where(type => type.IsPublic)
			.Where(type => !type.Name.StartsWith("__"))
			.Where(type => !type.IsGenericTypeDefinition)
			.ToList();

		public TypeReferencePropertyDrawer()
		{
			controlHint = GetType().GetHashCode();
		}

		private int controlHint;

		private int? lastcontrolId = null;

		private Type currentValue = null;
		private Type lastValue = null;

		private bool isClass;
		private bool isInterface;

		private bool implementsFixedInterface;
		private Type fixedInterface;

		private bool implementsInterfaceSource;
		private string implementsPropertyName;
		private Type sourceType;

		private Type[] validTypes;

		private bool initialized = false;
		private void Initialize(SerializedProperty property)
		{
			isClass = fieldInfo.HasCustomAttribute<__IsClassAttribute>();
			isInterface = fieldInfo.HasCustomAttribute<__IsInterfaceAttribute>();

			if (fieldInfo.TryGetCustomAttribute(out __ImplementsAttribute implements))
			{
				implementsFixedInterface = implements.InterfaceType != null;
				fixedInterface = implements.InterfaceType;

				implementsInterfaceSource = !implements.InterfaceSource.IsNullOrEmpty();
				implementsPropertyName = implements.InterfaceSource;
			}
			else
			{
				implementsFixedInterface = false;
				implementsInterfaceSource = false;
			}

			if (implementsInterfaceSource)
			{
				sourceType = GetSourceType(property);
			}

			SetupValidTypes();

			SetupValue(property);	

			initialized = true;
		}

		private void SetupValue(SerializedProperty property)
		{
			SerializedProperty aqnProperty = GetAssemblyQualifiedNameProperty(property);
			string aqn = aqnProperty.stringValue;

			Type referencedType = validTypes.FirstOrDefault(type => type.AssemblyQualifiedName == aqn);

			currentValue = referencedType;

			if (currentValue != lastValue)
			{
				lastValue = currentValue;
				SetValue(property);
			}
		}

		private void SetupValidTypes()
		{
			IEnumerable<Type> types = ProjectTypes;

			if (isClass) types = types.Where(type => type.IsClass);
			if (isInterface) types = types.Where(type => type.IsInterface);
			if (implementsFixedInterface) types = types.Where(type => type.Implements(fixedInterface));
			if (implementsInterfaceSource && sourceType != null) types = types.Where(type => type.Implements(sourceType));

			validTypes = types.ToArray();
		}
		
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (!initialized)
				Initialize(property);


			position = EditorGUI.PrefixLabel(position, label);

			int controlId = GUIUtility.GetControlID(controlHint, FocusType.Keyboard, position);
			if (controlId != lastcontrolId)
			{
				if (implementsInterfaceSource)
				{
					sourceType = GetSourceType(property);

					SetupValidTypes();
					SetupValue(property);
				}
			}

			bool triggerDropDown = false;

			Event currentEvent = Event.current;
			EventType eventType = currentEvent.GetTypeForControl(controlId);

			switch (eventType)
			{
				case EventType.MouseDown when GUI.enabled && position.Contains(currentEvent.mousePosition):
					GUIUtility.keyboardControl = controlId;
					triggerDropDown = true;
					currentEvent.Use();
					break;

				case EventType.KeyDown when GUI.enabled && GUIUtility.keyboardControl == controlId:
					if (currentEvent.keyCode.IsOneOf(KeyCode.Return, KeyCode.Tab))
					{
						triggerDropDown = true;
						currentEvent.Use();
					}
					break;

				case EventType.Repaint:
					SerializedProperty aqnProperty = GetAssemblyQualifiedNameProperty(property);
					string aqn = aqnProperty.stringValue;
					string typeName = GetTypeNameFromAssemblyQualifiedName(aqn);
					EditorStyles.popup.Draw(position, new GUIContent(typeName), controlId);
					break;
			}

			if (triggerDropDown)
			{
				GenericMenu menu = new GenericMenu();
				menu.AddItem(new GUIContent(DisplayForNull), false, CreateMenuFunction(null));
				menu.AddSeparator("");

				foreach (Type type in validTypes)
					menu.AddItem(new GUIContent(type.FullName), false, CreateMenuFunction(type));

				menu.DropDown(position);

				GenericMenu.MenuFunction CreateMenuFunction(Type type)
				{
					return () =>
					{
						currentValue = type;
						if (currentValue != lastValue)
						{
							lastValue = currentValue;
							SetValue(property);
						}
					};
				}
			}
		}

		private void SetValue(SerializedProperty property)
		{
			SerializedProperty aqnProperty = GetAssemblyQualifiedNameProperty(property);
			aqnProperty.stringValue = currentValue?.AssemblyQualifiedName;
			property.serializedObject.ApplyModifiedProperties();
		}

		private Type GetSourceType(SerializedProperty property)
		{
			string currentPath = property.propertyPath;

			string otherPropName = "_" + implementsPropertyName.UncapitalizeFirstLetter();
			string otherPropPath = currentPath.Substring(0, currentPath.LastIndexOf('.')) + '.' + otherPropName;

			SerializedProperty otherProperty = property.serializedObject.FindProperty(otherPropPath);
			SerializedProperty otherAqnProperty = GetAssemblyQualifiedNameProperty(otherProperty);

			string aqn = otherAqnProperty.stringValue;
			return Type.GetType(aqn);
		}

		private SerializedProperty GetAssemblyQualifiedNameProperty(SerializedProperty typeRefProperty)
		{
			string path = "_" + nameof(TypeReference.AssemblyQualifiedName).UncapitalizeFirstLetter();
			return typeRefProperty.FindPropertyRelative(path);
		}

		private static string GetTypeNameFromAssemblyQualifiedName(string aqn)
		{
			if (aqn.IsNullOrEmpty())
				return DisplayForNull;
			return aqn?.Substring(0, aqn.IndexOf(',')) ?? DisplayForNull;
		}
	}
}