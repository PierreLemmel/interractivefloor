﻿using InterractiveFloor.Internal;
using InterractiveFloor.Unity.Application.Content;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace InterractiveFloor.Editor
{
    [CustomPropertyDrawer(typeof(__ContentBundleSourceAttribute))]
    public sealed class ContentBundleSourceDrawer : PropertyDrawer
    {
        private const string DisplayValueForNull = "--";

        private bool initialized = false;
        private readonly string[] bundles;
        private readonly GUIContent[] displayOptions;
        private int index = 0;

        public ContentBundleSourceDrawer()
        {
            bundles = ContentBundles
                .AllBundles
                .Prepend(null)
                .ToArray();

            displayOptions = bundles
                .Select(bundle => new GUIContent(bundle ?? DisplayValueForNull));
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!initialized)
            {
                index = Mathf.Max(bundles.IndexOf(property.stringValue), 0);
                initialized = true;
            }

            EditorGUI.BeginProperty(position, label, property);
            index = EditorGUI.Popup(position, label, index, displayOptions);
            property.stringValue = bundles[index];
            EditorGUI.EndProperty();
        }
    }
}