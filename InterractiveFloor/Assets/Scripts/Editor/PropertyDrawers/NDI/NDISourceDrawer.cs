﻿using InterractiveFloor;
using InterractiveFloor.Internal;
using Klak.Ndi;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Editor.PropertyDrawers.NDI
{
    [CustomPropertyDrawer(typeof(__NDISourceAttribute))]
    public sealed class NDISourceDrawer : PropertyDrawer
    {
        private const string DisplayValueForNull = "--";
        private const float SourceUpdateInterval = 1.0f;

        private string[] sources;
        private GUIContent[] displayOptions;
        private int index = 0;

        private DateTime lastSetup = DateTime.MinValue;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            
            if (DateTime.Now - lastSetup > TimeSpan.FromSeconds(SourceUpdateInterval))
            {
                sources = NdiManager.GetSourceNames()
                    .Prepend(null)
                    .ToArray();

                displayOptions = sources
                    .Select(src => new GUIContent(src ?? DisplayValueForNull));

                index = Mathf.Max(sources.IndexOf(property.stringValue), 0);

                lastSetup = DateTime.Now;
            }

            EditorGUI.BeginProperty(position, label, property);
            index = EditorGUI.Popup(position, label, index, displayOptions);
            property.stringValue = sources[index];
            EditorGUI.EndProperty();
        }
    }
}