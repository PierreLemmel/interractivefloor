﻿using CSCore.CoreAudioAPI;
using InterractiveFloor.Internal;
using UnityEditor;

namespace InterractiveFloor.Editor
{
    [CustomPropertyDrawer(typeof(__AudioDeviceSourceAttribute))]
    public sealed class AudioDeviceSourceDrawer : AbstractAudioDeviceSourceDrawer
    {
        protected override DataFlow DataFlow => DataFlow.All;
    }
}