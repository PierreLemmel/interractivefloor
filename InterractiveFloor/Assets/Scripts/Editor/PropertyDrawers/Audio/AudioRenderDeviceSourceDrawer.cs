﻿using CSCore.CoreAudioAPI;
using InterractiveFloor.Internal;
using UnityEditor;

namespace InterractiveFloor.Editor
{
    [CustomPropertyDrawer(typeof(__AudioRenderDeviceSourceAttribute))]
    public sealed class AudioRenderDeviceSourceDrawer : AbstractAudioDeviceSourceDrawer
    {
        protected override DataFlow DataFlow => DataFlow.Render;
    }
}