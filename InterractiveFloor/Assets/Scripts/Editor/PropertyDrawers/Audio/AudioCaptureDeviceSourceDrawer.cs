﻿using CSCore.CoreAudioAPI;
using InterractiveFloor.Internal;
using UnityEditor;

namespace InterractiveFloor.Editor
{
    [CustomPropertyDrawer(typeof(__AudioCaptureDeviceSourceAttribute))]
    public sealed class AudioCaptureDeviceSourceDrawer : AbstractAudioDeviceSourceDrawer
    {
        protected override DataFlow DataFlow => DataFlow.Capture;
    }
}