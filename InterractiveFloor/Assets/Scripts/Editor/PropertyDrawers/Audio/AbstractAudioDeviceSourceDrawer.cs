﻿using CSCore.CoreAudioAPI;
using InterractiveFloor.Unity.Audio;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace InterractiveFloor.Editor
{
    public abstract class AbstractAudioDeviceSourceDrawer : PropertyDrawer
    {
        private const string DisplayValueForNull = "--";

        private bool initialized = false;
        private string[] deviceIds;
        private GUIContent[] displayOptions;
        private readonly MMDeviceEnumerator deviceEnumerator;
        private readonly MMNotificationClient notificationClient;
        private int index = 0;

        protected abstract DataFlow DataFlow { get; }

        protected AbstractAudioDeviceSourceDrawer()
        {
            deviceEnumerator = new MMDeviceEnumerator();
            notificationClient = new MMNotificationClient(deviceEnumerator);
            notificationClient.HandleAnyChange(Setup);

            Setup();
        }

        ~AbstractAudioDeviceSourceDrawer()
        {
            notificationClient?.Dispose();
            deviceEnumerator?.Dispose();
        }

        private void Setup()
        {
            MMDevice[] devices = deviceEnumerator
                .EnumAudioEndpoints(DataFlow, DeviceState.Active)
                .Prepend(null)
                .ToArray();

            deviceIds = devices.Select(device => device?.DeviceID);

            displayOptions = devices
                .Select(device => new GUIContent(device?.FriendlyName ?? DisplayValueForNull));

            initialized = false;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!initialized)
            {
                index = Mathf.Max(deviceIds.IndexOf(property.stringValue), 0);
                initialized = true;
            }

            EditorGUI.BeginProperty(position, label, property);
            index = EditorGUI.Popup(position, label, index, displayOptions);
            property.stringValue = deviceIds[index];
            EditorGUI.EndProperty();
        }
    }
}
