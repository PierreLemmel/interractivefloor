﻿using InterractiveFloor.Internal;
using System;
using UnityEditor;
using UnityEngine;

namespace InterractiveFloor.Editor
{
    [CustomPropertyDrawer(typeof(__PositiveAttribute))]
    public class PositiveDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            switch(property.propertyType)
            {
                case SerializedPropertyType.Float:
                    OnGUI_Float(position, property, label);
                    break;
                case SerializedPropertyType.Integer:
                    OnGUI_Int(position, property, label);
                    break;
                default:
                    throw new InvalidOperationException($"Unexpected property type: {property.propertyType}");
            }
        }

        private void OnGUI_Float(Rect position, SerializedProperty property, GUIContent label)
        {
            float oldValue = property.floatValue;
            float value = EditorGUI.FloatField(position, label, oldValue);

            value = Mathf.Max(value, 0.0f);

            if (value != oldValue)
                property.floatValue = value;
        }

        private void OnGUI_Int(Rect position, SerializedProperty property, GUIContent label)
        {
            int oldValue = property.intValue;
            int value = EditorGUI.IntField(position, label, oldValue);

            value = Mathf.Max(value, 0);

            if (value != oldValue)
                property.intValue = value;
        }
    }
}