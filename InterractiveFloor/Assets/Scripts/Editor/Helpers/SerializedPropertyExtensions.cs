﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

using UObject = UnityEngine.Object;

namespace InterractiveFloor.Editor
{
    public static class SerializedPropertyExtensions
    {
        public static T GetValue<T>(this SerializedProperty property) where T : class
        {
            object current = property.serializedObject.targetObject;

            IEnumerable<string> pathElts = property.propertyPath.Split('.');
            foreach(string pathElt in pathElts)
            {
                if (current == null)
                    return null;

                FieldInfo field = current.GetType().GetField(pathElt, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                current = field.GetValue(current);
            }

            return (T)current;
        }
    }
}
