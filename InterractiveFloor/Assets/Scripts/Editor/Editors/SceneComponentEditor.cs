﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using UEditor = UnityEditor.Editor;
using System.Reflection;
using System.Linq;
using System;
using InterractiveFloor.Unity;

namespace InterractiveFloor.Editor
{
    [CustomEditor(typeof(SceneComponent), editorForChildClasses: true, isFallback = true)]
    public class SceneComponentEditor : UEditor
    {
        private IReadOnlyCollection<NamedAction> userActions;

        private void Initialize()
        {
            userActions = target.GetType()
                .GetMethods(BindingFlags.Instance | BindingFlags.Public)
                .Where(method => method.HasCustomAttribute<UserActionAttribute>())
                .Select(method =>
                {
                    string actionName = method.GetCustomAttribute<UserActionAttribute>().ActionName ?? method.Name;
                    Action action = method.CreateDelegate<Action>(target);

                    Func<bool> enableFunction = null;
                    if (method.TryGetCustomAttribute(out EnabledIfAttribute enabledIf))
                    {
                        MethodInfo otherMethod = method.DeclaringType.GetMethod(enabledIf.OtherMethod, BindingFlags.Instance | BindingFlags.Public);
                        enableFunction = otherMethod.CreateDelegate<Func<bool>>(target);
                    }
                    return new NamedAction(actionName, action, enableFunction);
                })
                .ToList();
        }

        public override void OnInspectorGUI()
        {
            if (userActions is null) Initialize();

            if (userActions.Any())
            {
                foreach (NamedAction userAction in userActions)
                {
                    bool enabled = EditorApplication.isPlaying && (userAction.EnableFunction?.Invoke() ?? true);
                    
                    EditorGUI.BeginDisabledGroup(!enabled);
                    if (GUILayout.Button(userAction.Name))
                    {
                        userAction.Action();
                    }
                    EditorGUI.EndDisabledGroup();
                }

                GUILayout.Space(15.0f);
            } 

            base.OnInspectorGUI();
        }

        private class NamedAction
        {
            public NamedAction(string name, Action action, Func<bool> enableFunction)
            {
                Name = name;
                Action = action;
                EnableFunction = enableFunction;
            }

            public string Name { get; }
            public Action Action { get; }
            public Func<bool> EnableFunction { get; }
        }
    }
}