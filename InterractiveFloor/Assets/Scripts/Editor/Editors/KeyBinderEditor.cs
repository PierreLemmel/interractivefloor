﻿using UnityEngine;
using UnityEditor;

using UEditor = UnityEditor.Editor;
using InterractiveFloor.Unity.Inputs;

namespace InterractiveFloor.Unity.Editor
{
    [CustomEditor(typeof(KeyBinder))]
    public class KeyBinderEditor : UEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            KeyBinder keyBinder = (KeyBinder)target;

            EditorGUI.BeginDisabledGroup(true);
            
            foreach(KeyBinding binding in keyBinder.Bindings)
            {
                KeyCode key = binding.Key;
                string name = binding.Event.GetPersistentMethodName(0) ?? "NULL";

                EditorGUILayout.TextField(key.ToString(), name);
            }

            EditorGUI.EndDisabledGroup();
        }
    }
}
