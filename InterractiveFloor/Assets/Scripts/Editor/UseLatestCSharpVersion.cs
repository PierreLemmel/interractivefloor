﻿#if ENABLE_VSTU
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using UnityEngine;
using UnityEditor;

using SyntaxTree.VisualStudio.Unity.Bridge;
using System.Text.RegularExpressions;

namespace InterractiveFloor.Editor
{
    [InitializeOnLoad]
    public class UseLatestCSharpVersion
    {
        private class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding => Encoding.UTF8;
        }

        static UseLatestCSharpVersion()
        {
            ProjectFilesGenerator.ProjectFileGeneration += (string name, string content) =>
            {
                const string interractiveFloorCsprojPattern = @"InterractiveFloor(?:\.[a-zA-Z]+)*\.csproj";

                if (Regex.IsMatch(name, interractiveFloorCsprojPattern))
                {
                    XDocument document = XDocument.Parse(content);

                    XElement langVerElt = document.Root.Descendants().Single(elt => elt.Name.LocalName == "LangVersion");
                    langVerElt.SetValue("latest");

                    TextWriter strWriter = new Utf8StringWriter();
                    document.Save(strWriter);

                    return strWriter.ToString();
                }
                else
                    return content;
            };
        }
    }
} 
#endif