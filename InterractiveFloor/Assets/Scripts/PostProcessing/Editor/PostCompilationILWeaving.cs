﻿using InterractiveFloor.PostProcessing.ILWeaving;
using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEditor.Compilation;

using UDebug = UnityEngine.Debug;

namespace InterractiveFloor.PostProcessing.Editor
{
    [InitializeOnLoad]
    public static class PostCompilationILWeaving
    {
        static PostCompilationILWeaving()
        {
            CompilationPipeline.assemblyCompilationFinished += (assembly, _) => WeaveAssembly(assembly);
        }

        [MenuItem("Scripts/Weave assemblies")]
        public static void ReweaveAssemblies()
        {
            Assembly[] assemblies = CompilationPipeline.GetAssemblies(AssembliesType.PlayerWithoutTestAssemblies);
            foreach (Assembly assembly in assemblies)
                WeaveAssembly(assembly.outputPath);
        }

        private static void WeaveAssembly(string assembly)
        {
            if (assembly.Contains(".Tests") || !assembly.Contains("InterractiveFloor.Unity")) return;

            Stopwatch watch = Stopwatch.StartNew();
            IAssemblyWeaver weaver = new ModelReweaver();

            string tempPath = assembly.Substring(0, assembly.Length - ".dll".Length) + ".Weaved.dll";
            weaver.WeaveAssembly(assembly, tempPath);

            File.Delete(assembly);
            File.Move(tempPath, assembly);

            UDebug.Log($"Assembly '{assembly}' weaved in {watch.ElapsedMilliseconds} ms");
        }
    }
}