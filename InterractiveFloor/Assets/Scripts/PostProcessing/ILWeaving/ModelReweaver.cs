﻿using InterractiveFloor.Internal;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Rocks;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using ReflectionConstructor = System.Reflection.ConstructorInfo;
using UObject = UnityEngine.Object;
using MCTypeReference = Mono.Cecil.TypeReference;

namespace InterractiveFloor.PostProcessing.ILWeaving
{
    public class ModelReweaver : IAssemblyWeaver
    {
        private delegate void PropertyTransformation(ModuleDefinition module, TypeDefinition type, PropertyDefinition property, FieldDefinition backingField);

        private readonly IReadOnlyCollection<PropertyTransformation> propertyTransformations;

        private MethodReference mvexConstructor;
        private MethodReference rangeAttrConstructor;
        private MethodReference readOnlyAttrConstructor;
        private MethodReference internalEditTimeOnlyAttrConstructor;
        private MethodReference textAreaAttrConstructor;
        private MethodReference internalBundleSourceAttrConstructor;
        private MethodReference internalDisplayIfAttrConstructor;
        private MethodReference internalAudioDeviceSourceAttributeConstructor;
        private MethodReference internalAudioCaptureDeviceSourceAttributeConstructor;
        private MethodReference internalAudioRenderDeviceSourceAttributeConstructor;
        private MethodReference internalIsInterfaceAttributeconstructor;
        private MethodReference internalIsClassAttributeconstructor;
        private MethodReference internalImplementsAttributeConstructor;
        private MethodReference internalGreaterThanAttributeConstructor_floatArg;
        private MethodReference internalGreaterThanAttributeConstructor_stringFloatArg;
        private MethodReference internalLowerThanAttributeConstructor_floatArg;
        private MethodReference internalLowerThanAttributeConstructor_stringFloatArg;
        private MethodReference internalPositiveAttributeConstructor;
        private MethodReference internalNDISourceAttributeConstructor;

        private MCTypeReference boolTypeRef;
        private MCTypeReference floatTypeRef;
        private MCTypeReference nullableFloatTypeRef;
        private MCTypeReference intTypeRef;
        private MCTypeReference stringTypeRef;
        private MCTypeReference systemTypeTypeRef;
        private MCTypeReference displayConditionTypeRef;
        private MCTypeReference customAttributesArgArrayTypeRef;

        public ModelReweaver()
        {
            propertyTransformations = new List<PropertyTransformation>()
            {
                InRangeTransformation,
                NotEditableTransformation,
                EditTimeOnlyTransformation,
                MultilineTransformation,
                ContentBundleSourceTransformation,
                DisplayIfTransformation,
                AudioDeviceSourceTransformation,
                AudioRenderDeviceSourceTransformation,
                AudioCaptureDeviceSourceTransformation,
                IsClassTransformation,
                IsInterfaceTransformation,
                ImplementsTransformation,
                GreaterThanTransformation,
                LowerThanTransformation,
                PositiveTransformation,
                NDISourceTransformation
            };
        }

        private void CacheReferences(ModuleDefinition module)
        {
            mvexConstructor = module.ImportConstructor<ModelValidationException>(typeof(string), typeof(string));
            rangeAttrConstructor = module.ImportConstructor<RangeAttribute>(typeof(float), typeof(float));
            readOnlyAttrConstructor = module.ImportConstructor<ReadOnlyAttribute>();
            internalEditTimeOnlyAttrConstructor = module.ImportConstructor<__EditTimeOnlyAttribute>();
            textAreaAttrConstructor = module.ImportConstructor<TextAreaAttribute>();
            internalBundleSourceAttrConstructor = module.ImportConstructor<__ContentBundleSourceAttribute>();
            internalDisplayIfAttrConstructor = module.ImportConstructor<__DisplayIfAttribute>(typeof(string), typeof(DisplayCondition), typeof(object[]));
            internalAudioDeviceSourceAttributeConstructor = module.ImportConstructor<__AudioDeviceSourceAttribute>();
            internalAudioCaptureDeviceSourceAttributeConstructor = module.ImportConstructor<__AudioCaptureDeviceSourceAttribute>();
            internalAudioRenderDeviceSourceAttributeConstructor = module.ImportConstructor<__AudioRenderDeviceSourceAttribute>();
            internalIsInterfaceAttributeconstructor = module.ImportConstructor<__IsInterfaceAttribute>();
            internalIsClassAttributeconstructor = module.ImportConstructor<__IsClassAttribute>();
            internalImplementsAttributeConstructor = module.ImportConstructor<__ImplementsAttribute>(typeof(Type), typeof(string));
            internalGreaterThanAttributeConstructor_floatArg = module.ImportConstructor<__GreaterThanAttribute>(typeof(float));
            internalGreaterThanAttributeConstructor_stringFloatArg = module.ImportConstructor<__GreaterThanAttribute>(typeof(string), typeof(float)); ;
            internalLowerThanAttributeConstructor_floatArg = module.ImportConstructor<__LowerThanAttribute>(typeof(float));
            internalLowerThanAttributeConstructor_stringFloatArg = module.ImportConstructor<__LowerThanAttribute>(typeof(string), typeof(float));
            internalPositiveAttributeConstructor = module.ImportConstructor<__PositiveAttribute>();
            internalNDISourceAttributeConstructor = module.ImportConstructor<__NDISourceAttribute>();

            boolTypeRef = module.ImportType<bool>();
            floatTypeRef = module.ImportType<float>();
            nullableFloatTypeRef = module.ImportType<float?>();
            intTypeRef = module.ImportType<int>();
            stringTypeRef = module.ImportType<string>();
            systemTypeTypeRef = module.ImportType<Type>();
            displayConditionTypeRef = module.ImportType<DisplayCondition>();
            customAttributesArgArrayTypeRef = module.ImportType<CustomAttributeArgument[]>();
        }

        public void WeaveAssembly(string assemblyPath, string newPath)
        {
            ReaderParameters readerParameters = new ReaderParameters
            {
                AssemblyResolver = new UnityAssemblyResolver()
            };

            using (AssemblyDefinition assemblyDefinition = AssemblyDefinition.ReadAssembly(assemblyPath, readerParameters))
            {
                ModuleDefinition module = assemblyDefinition
                    .Modules
                    .Single();

                if (assemblyDefinition.HasCustomAttribute<AlreadyWeavedAttribute>())
                    return;

                CacheReferences(module);

                IEnumerable<TypeDefinition> typesToWeave = module.GetTypes()
                    .Where(type => !type.IsAbstract)
                    .Where(type => type.InheritsFrom<Model>() || type.InheritsFrom<UObject>());

                foreach (TypeDefinition typeToWeave in typesToWeave)
                {
                    WeaveModelType(module, typeToWeave);
                }

                assemblyDefinition.AddCustomAttribute<AlreadyWeavedAttribute>(module);
                assemblyDefinition.Write(newPath);
            }
        }

        private void WeaveModelType(ModuleDefinition module, TypeDefinition typeToWeave)
        {
            typeToWeave.MakeSerializable();

            IEnumerable<PropertyDefinition> propertiesToWeave = typeToWeave
                .Properties
                .Where(property => property.IsAutoProperty() && property.IsGetSetProperty());

            foreach (PropertyDefinition property in propertiesToWeave)
            {
                FieldDefinition backingField = property.GetBackingField();
                backingField.Name = "_" + property.Name.UncapitalizeFirstLetter();
                backingField.AddCustomAttribute<SerializeField>(module);

                foreach (PropertyTransformation transformation in propertyTransformations)
                    transformation.Invoke(module, typeToWeave, property, backingField);
            }
        }

        private void InRangeTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            CustomAttribute inRange = property.GetCustomAttribute<InRangeAttribute>();
            if (inRange != null)
            {
                if (property.PropertyType.IsReferenceTo<float>())
                {
                    float minf = (float)inRange.ConstructorArguments[0].Value;
                    float maxf = (float)inRange.ConstructorArguments[1].Value;
                    
                    CustomAttribute unityRangeAttribute = new CustomAttribute(rangeAttrConstructor);
                    unityRangeAttribute.ConstructorArguments.Add(new CustomAttributeArgument(floatTypeRef, minf));
                    unityRangeAttribute.ConstructorArguments.Add(new CustomAttributeArgument(floatTypeRef, maxf));
                    backingField.CustomAttributes.Add(unityRangeAttribute);

                    MethodBody setMethodBody = property.SetMethod.Body;
                    setMethodBody.Variables.Insert(0, new VariableDefinition(boolTypeRef));

                    Instruction[] existingInstructions = setMethodBody.Instructions.ToArray();
                    setMethodBody.Instructions.Clear();
                    ILProcessor il = setMethodBody.GetILProcessor();

                    Instruction shortCircuitAndInstr = il.Create(OpCodes.Ldc_I4_1);
                    Instruction assignCgtResultInstr = il.Create(OpCodes.Stloc_0);
                    Instruction[] andOpInstrs = new Instruction[]
                    {
                        il.Create(OpCodes.Ldarg_1),
                        il.Create(OpCodes.Ldc_R4, minf),
                        il.Create(OpCodes.Blt, shortCircuitAndInstr),

                        il.Create(OpCodes.Ldarg_1),
                        il.Create(OpCodes.Ldc_R4, maxf),
                        il.Create(OpCodes.Cgt),
                        il.Create(OpCodes.Br, assignCgtResultInstr),
                    };

                    Instruction[] throwIfLoc0IsFalseInstrs = new Instruction[]
                    {
                        il.Create(OpCodes.Ldloc_0),
                        il.Create(OpCodes.Brfalse, existingInstructions[0]),
                        il.Create(OpCodes.Ldstr, $"The argument must be in range [{minf}, {maxf}]"),
                        il.Create(OpCodes.Ldstr, property.Name),
                        il.Create(OpCodes.Newobj, mvexConstructor),
                        il.Create(OpCodes.Throw),
                    };

                    il.Append(andOpInstrs);
                    il.Append(shortCircuitAndInstr);
                    il.Append(assignCgtResultInstr);
                    il.Append(throwIfLoc0IsFalseInstrs);
                    il.Append(existingInstructions);

                    setMethodBody.OptimizeMacros();
                }
                else if (property.PropertyType.IsReferenceTo<int>())
                {
                    int min = (int)inRange.ConstructorArguments[0].Value;
                    int max = (int)inRange.ConstructorArguments[1].Value;

                    CustomAttribute unityRangeAttribute = new CustomAttribute(rangeAttrConstructor);
                    unityRangeAttribute.ConstructorArguments.Add(new CustomAttributeArgument(floatTypeRef, (float)min));
                    unityRangeAttribute.ConstructorArguments.Add(new CustomAttributeArgument(floatTypeRef, (float)max));
                    backingField.CustomAttributes.Add(unityRangeAttribute);

                    MethodBody setMethodBody = property.SetMethod.Body;
                    setMethodBody.Variables.Insert(0, new VariableDefinition(boolTypeRef));

                    Instruction[] existingInstructions = setMethodBody.Instructions.ToArray();
                    setMethodBody.Instructions.Clear();
                    ILProcessor il = setMethodBody.GetILProcessor();

                    Instruction shortCircuitAndInstr = il.Create(OpCodes.Ldc_I4_1);
                    Instruction assignCgtResultInstr = il.Create(OpCodes.Stloc_0);
                    Instruction[] andOpInstrs = new Instruction[]
                    {
                        il.Create(OpCodes.Ldarg_1),
                        il.Create(OpCodes.Ldc_I4, min),
                        il.Create(OpCodes.Blt, shortCircuitAndInstr),

                        il.Create(OpCodes.Ldarg_1),
                        il.Create(OpCodes.Ldc_I4, max),
                        il.Create(OpCodes.Cgt),
                        il.Create(OpCodes.Br, assignCgtResultInstr),
                    };

                    Instruction[] throwIfLoc0IsFalseInstrs = new Instruction[]
                    {
                        il.Create(OpCodes.Ldloc_0),
                        il.Create(OpCodes.Brfalse, existingInstructions[0]),
                        il.Create(OpCodes.Ldstr, $"The argument must be in range [{min}, {max}]"),
                        il.Create(OpCodes.Ldstr, property.Name),
                        il.Create(OpCodes.Newobj, mvexConstructor),
                        il.Create(OpCodes.Throw),
                    };

                    il.Append(andOpInstrs);
                    il.Append(shortCircuitAndInstr);
                    il.Append(assignCgtResultInstr);
                    il.Append(throwIfLoc0IsFalseInstrs);
                    il.Append(existingInstructions);

                    setMethodBody.OptimizeMacros();
                }
                else
                    Debug.LogWarning($"InRange attribute on unexpected type '{property.PropertyType.FullName}' on property '{property.FullName}'");
            }
        }

        private void NotEditableTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<NotEditableAttribute>())
            {
                CustomAttribute readOnlyAttribute = new CustomAttribute(readOnlyAttrConstructor);
                backingField.CustomAttributes.Add(readOnlyAttribute);
            }
        }

        private void EditTimeOnlyTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<EditTimeOnlyAttribute>())
            {
                CustomAttribute editTimeOnly = new CustomAttribute(internalEditTimeOnlyAttrConstructor);
                backingField.CustomAttributes.Add(editTimeOnly);
            }
        }

        private void MultilineTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<MultilineAttribute>())
            {
                CustomAttribute multilineAttribute = new CustomAttribute(textAreaAttrConstructor);
                backingField.CustomAttributes.Add(multilineAttribute);
            }
        }

        private void ContentBundleSourceTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<ContentBundleSourceAttribute>())
            {
                CustomAttribute internalBundleSourceAttribute = new CustomAttribute(internalBundleSourceAttrConstructor);
                backingField.CustomAttributes.Add(internalBundleSourceAttribute);
            }
        }

        private void DisplayIfTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            CustomAttribute displayIfAttribute = property.GetCustomAttribute<DisplayIfAttribute>();
            if (displayIfAttribute != null)
            {
                string otherProperty = (string)displayIfAttribute.ConstructorArguments[0].Value;
                DisplayCondition condition = (DisplayCondition)displayIfAttribute.ConstructorArguments[1].Value;
                CustomAttributeArgument[] paramsArg = (CustomAttributeArgument[])displayIfAttribute.ConstructorArguments[2].Value;

                CustomAttribute propertyAttribute = new CustomAttribute(internalDisplayIfAttrConstructor);
                propertyAttribute.ConstructorArguments.Add(new CustomAttributeArgument(stringTypeRef, otherProperty));
                propertyAttribute.ConstructorArguments.Add(new CustomAttributeArgument(displayConditionTypeRef, condition));
                propertyAttribute.ConstructorArguments.Add(new CustomAttributeArgument(customAttributesArgArrayTypeRef, paramsArg));

                backingField.CustomAttributes.Add(propertyAttribute);
            }
        }

        private void AudioDeviceSourceTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<AudioDeviceSourceAttribute>())
            {
                CustomAttribute internalAudioDeviceSourceAttribute = new CustomAttribute(internalAudioDeviceSourceAttributeConstructor);
                backingField.CustomAttributes.Add(internalAudioDeviceSourceAttribute);
            }
        }

        private void AudioCaptureDeviceSourceTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<AudioCaptureDeviceSourceAttribute>())
            {
                CustomAttribute internalAudioCaptureDeviceSourceAttribute = new CustomAttribute(internalAudioCaptureDeviceSourceAttributeConstructor);
                backingField.CustomAttributes.Add(internalAudioCaptureDeviceSourceAttribute);
            }
        }

        private void AudioRenderDeviceSourceTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<AudioRenderDeviceSourceAttribute>())
            {
                CustomAttribute internalAudioRenderDeviceSourceAttribute = new CustomAttribute(internalAudioRenderDeviceSourceAttributeConstructor);
                backingField.CustomAttributes.Add(internalAudioRenderDeviceSourceAttribute);
            }
        }

        private void IsClassTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<IsClassAttribute>())
            {
                CustomAttribute internalIsClassAttribute = new CustomAttribute(internalIsClassAttributeconstructor);
                backingField.CustomAttributes.Add(internalIsClassAttribute);
            }
        }

        private void IsInterfaceTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<IsInterfaceAttribute>())
            {
                CustomAttribute internalIsInterfaceAttribute = new CustomAttribute(internalIsInterfaceAttributeconstructor);
                backingField.CustomAttributes.Add(internalIsInterfaceAttribute);
            }
        }

        private void ImplementsTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            CustomAttribute implementsAttribute = property.GetCustomAttribute<ImplementsAttribute>();
            if (implementsAttribute != null)
            {
                object constructorArgument = implementsAttribute.ConstructorArguments[0].Value;

                Type interfaceType = null;
                string interfaceSource = null;

                if (constructorArgument is Type type)
                    interfaceType = type;
                if (constructorArgument is string str)
                    interfaceSource = str;

                CustomAttribute internalImplementsAttribute = new CustomAttribute(internalImplementsAttributeConstructor);
                internalImplementsAttribute.ConstructorArguments.Add(new CustomAttributeArgument(systemTypeTypeRef, interfaceType));
                internalImplementsAttribute.ConstructorArguments.Add(new CustomAttributeArgument(stringTypeRef, interfaceSource));

                backingField.CustomAttributes.Add(internalImplementsAttribute);
            }
        }

        private void GreaterThanTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            CustomAttribute greaterThanAttribute = property.GetCustomAttribute<GreaterThanAttribute>();
            if (greaterThanAttribute != null)
            {
                object[] arguments = greaterThanAttribute.ConstructorArguments.Select(ca => ca.Value).ToArray();

                CustomAttribute internalGreaterThanAttribute;
                if (arguments.Length == 1)
                {
                    float value = (float)arguments[0];

                    internalGreaterThanAttribute = new CustomAttribute(internalGreaterThanAttributeConstructor_floatArg);
                    internalGreaterThanAttribute.ConstructorArguments.Add(new CustomAttributeArgument(nullableFloatTypeRef, value));
                }
                else if (arguments.Length == 2)
                {
                    string valueProperty = (string)arguments[0];
                    float epsilon = (float)arguments[1];

                    internalGreaterThanAttribute = new CustomAttribute(internalGreaterThanAttributeConstructor_stringFloatArg);
                    internalGreaterThanAttribute.ConstructorArguments.Add(new CustomAttributeArgument(stringTypeRef, valueProperty));
                    internalGreaterThanAttribute.ConstructorArguments.Add(new CustomAttributeArgument(floatTypeRef, epsilon));
                }
                else
                    throw new InvalidOperationException($"Unknown constructor for type {nameof(GreaterThanAttribute)}");

                backingField.CustomAttributes.Add(internalGreaterThanAttribute);
            }
        }

        private void LowerThanTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            CustomAttribute lowerThanAttribute = property.GetCustomAttribute<LowerThanAttribute>();
            if (lowerThanAttribute != null)
            {
                object[] arguments = lowerThanAttribute.ConstructorArguments.Select(ca => ca.Value).ToArray();

                CustomAttribute internalLowerThanAttribute;
                if (arguments.Length == 1)
                {
                    float value = (float)arguments[0];

                    internalLowerThanAttribute = new CustomAttribute(internalLowerThanAttributeConstructor_floatArg);
                    internalLowerThanAttribute.ConstructorArguments.Add(new CustomAttributeArgument(nullableFloatTypeRef, value));
                }
                else if (arguments.Length == 2)
                {
                    string valueProperty = (string)arguments[0];
                    float epsilon = (float)arguments[1];

                    internalLowerThanAttribute = new CustomAttribute(internalLowerThanAttributeConstructor_stringFloatArg);
                    internalLowerThanAttribute.ConstructorArguments.Add(new CustomAttributeArgument(stringTypeRef, valueProperty));
                    internalLowerThanAttribute.ConstructorArguments.Add(new CustomAttributeArgument(floatTypeRef, epsilon));
                }
                else
                    throw new InvalidOperationException($"Unknown constructor for type {nameof(LowerThanAttribute)}");

                backingField.CustomAttributes.Add(internalLowerThanAttribute);
            }
        }

        private void PositiveTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<PositiveAttribute>())
            {
                CustomAttribute internalPositiveAttribute = new CustomAttribute(internalPositiveAttributeConstructor);
                backingField.CustomAttributes.Add(internalPositiveAttribute);
            }
        }

        private void NDISourceTransformation(ModuleDefinition module, TypeDefinition typeToWeave, PropertyDefinition property, FieldDefinition backingField)
        {
            if (property.HasCustomAttribute<NDISourceAttribute>())
            {
                CustomAttribute internalNDISourceAttribute = new CustomAttribute(internalNDISourceAttributeConstructor);
                backingField.CustomAttributes.Add(internalNDISourceAttribute);
            }
        }
    }
}