﻿namespace InterractiveFloor.PostProcessing.ILWeaving
{
    public interface IAssemblyWeaver
    {
        void WeaveAssembly(string assemblyPath, string newPath);
    }
}