﻿using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace InterractiveFloor.PostProcessing.ILWeaving
{
    internal sealed class UnityAssemblyResolver : BaseAssemblyResolver
    {
        private readonly IDictionary<string, AssemblyDefinition> cache = new Dictionary<string, AssemblyDefinition>();
        private readonly IDictionary<string, string> appDomainLocations = new Dictionary<string, string>();

        public UnityAssemblyResolver()
        {
            IEnumerable<Assembly> appDomainAssemblies = AppDomain
                .CurrentDomain
                .GetAssemblies()
                .Where(assembly => !assembly.IsDynamic);

            foreach (Assembly appDomainAssembly in appDomainAssemblies)
            {
                appDomainLocations.Add(appDomainAssembly.FullName, appDomainAssembly.Location);
                string appDomainDirectory = Path.GetDirectoryName(appDomainAssembly.Location);
                AddSearchDirectory(appDomainDirectory);
            }
        }

        public override AssemblyDefinition Resolve(AssemblyNameReference name)
        {
            string fullName = name.FullName;

            if (cache.TryGetValue(fullName, out AssemblyDefinition result))
                return result;

            AssemblyDefinition asmdef;
            if (appDomainLocations.TryGetValue(fullName, out string location))
                asmdef = AssemblyDefinition.ReadAssembly(location);
            else
                asmdef = base.Resolve(name);

            cache.Add(fullName, asmdef);
            return asmdef;
        }

        public override AssemblyDefinition Resolve(AssemblyNameReference name, ReaderParameters parameters)
        {
            string fullName = name.FullName;

            if (cache.TryGetValue(fullName, out AssemblyDefinition result))
                return result;

            AssemblyDefinition asmdef;
            if (appDomainLocations.TryGetValue(fullName, out string location))
                asmdef = AssemblyDefinition.ReadAssembly(location, parameters);
            else
                asmdef = base.Resolve(name, parameters);

            cache.Add(fullName, asmdef);
            return asmdef;
        }
    }
}