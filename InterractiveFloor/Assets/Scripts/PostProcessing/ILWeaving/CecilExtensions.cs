﻿using Mono.Cecil;
using System;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.PostProcessing.ILWeaving
{
    internal static class CecilExtensions
    {
        public static Mono.Cecil.TypeReference ImportType<T>(this ModuleDefinition module) => module.ImportReference(typeof(T));
        public static MethodReference ImportConstructor<T>(this ModuleDefinition module, params Type[] types) => module.ImportReference(typeof(T).GetConstructor(types));

        public static CustomAttribute GetCustomAttribute<TAttribute>(this ICustomAttributeProvider customAttributeProvider)
            => customAttributeProvider.CustomAttributes.FirstOrDefault(cattr => cattr.AttributeType.IsReferenceTo<TAttribute>());

        public static bool HasCustomAttribute<TAttribute>(this ICustomAttributeProvider customAttributeProvider)
            => customAttributeProvider.GetCustomAttribute<TAttribute>() != null;

        public static void AddCustomAttribute<TAttribute>(this ICustomAttributeProvider customAttributeProvider, ModuleDefinition module)
            where TAttribute : Attribute, new()
        {
            MethodReference attributeConstructor = module.ImportConstructor<TAttribute>();

            CustomAttribute customAttribute = new CustomAttribute(attributeConstructor);
            customAttributeProvider.CustomAttributes.Add(customAttribute);
        }

        public static bool IsReferenceTo<T>(this Mono.Cecil.TypeReference typeRef)
        {
            ModuleDefinition module = typeRef.Module;
            Mono.Cecil.TypeReference otherTypeRef = module.ImportReference(typeof(T));

            return typeRef.FullName == otherTypeRef.FullName;
        }

        public static bool InheritsFrom<TClass>(this TypeDefinition typeDef)
            where TClass : class
        {
            if (!typeof(TClass).IsClass) throw new ArgumentException($"The type '{typeof(TClass).FullName}' is not a class type.");

            Mono.Cecil.TypeReference baseType = typeDef.BaseType;
            if (baseType is null || baseType.IsReferenceTo<object>())
                return false;
            else if (baseType.IsReferenceTo<TClass>())
                return true;
            else
                return InheritsFrom<TClass>(typeDef.BaseType.Resolve());
        }

        public static bool IsGetSetProperty(this PropertyDefinition propDef)
            => propDef.GetMethod != null && propDef.SetMethod != null;

        public static bool IsAutoProperty(this PropertyDefinition propDef)
            => propDef.GetBackingField() != null;

        public static FieldDefinition GetBackingField(this PropertyDefinition propDef)
        {
            string backingFieldName = $"<{propDef.Name}>k__BackingField";
            return propDef.DeclaringType.GetField(backingFieldName);
        }

        public static FieldDefinition GetField(this TypeDefinition typeDef, string fieldName)
            => typeDef.Fields.FirstOrDefault(field => field.Name == fieldName);

        public static void MakeSerializable(this TypeDefinition typeDef)
            => typeDef.Attributes = typeDef.Attributes | TypeAttributes.Serializable;
    }
}