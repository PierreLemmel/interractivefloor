﻿using Mono.Cecil.Cil;

namespace InterractiveFloor.PostProcessing.ILWeaving
{
    public static class ILProcessorExtensions
    {
        public static void Append(this ILProcessor il, Instruction[] instructions)
        {
            foreach (Instruction instruction in instructions)
                il.Append(instruction);
        }
    }
}