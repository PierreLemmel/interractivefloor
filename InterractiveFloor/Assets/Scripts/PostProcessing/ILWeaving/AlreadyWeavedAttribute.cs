﻿using System;

namespace InterractiveFloor.PostProcessing.ILWeaving
{
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
    public class AlreadyWeavedAttribute : Attribute
    {
    }
}