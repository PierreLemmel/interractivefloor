﻿

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.Unity.Inputs
{
    [RequireService(typeof(IKeySource))]
    public class KeyBinder : SceneComponent
    {
        [EditTimeOnly]
        public List<KeyBinding> Bindings { get; set; }

        private IKeySource keySource;

        protected override void Bootstrap(ISpyConfig spy)
        {
            keySource = Resolve<IKeySource>();
        }

        protected override void Tick()
        {
            if (keySource.TryGetNextResult(out KeySourceResult result))
            {
                IReadOnlyDictionary<KeyCode, KeyResult> results = result.KeyResults;
                foreach(KeyBinding binding in Bindings)
                {
                    if (results[binding.Key].IsDown())
                        binding.Event.Invoke();
                }
            }
        }

        protected override void UponActivation()
        {
            KeySourceSettings settings = KeySourceSettings.CreateDefault();
            settings.KeyCodes = Bindings.Select(binding => binding.Key).ToList();

            keySource.Start(settings);
        }

        protected override void UponDeactivation()
        {
            keySource.Stop();
        }
    }
}
