﻿using System.Collections.Generic;
using UnityEngine;

namespace InterractiveFloor.Unity.Inputs
{
	public class KeyResult : Result
	{
		public KeyStatus Status { get; }

		public bool IsDown() => Status == KeyStatus.Down;
		public bool IsPressed() => Status.IsOneOf(KeyStatus.Pressed, KeyStatus.Down);
		public bool IsUp() => Status == KeyStatus.Up;
		public bool IsFree() => Status.IsOneOf(KeyStatus.Free, KeyStatus.Up);

		public KeyResult(KeyStatus status)
		{
			Status = status;
		}
	} 
}