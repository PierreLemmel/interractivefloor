﻿using UnityEngine;

namespace InterractiveFloor.Unity.Inputs
{
    public class ScreenPosition : Result
    {
        public Vector2 AbsolutePosition { get; }
        public Vector2 ScreenSize { get; }
        public Vector2 RelativePosition => new Vector2(AbsolutePosition.x / ScreenSize.x, AbsolutePosition.y / ScreenSize.y);

        public float Left => AbsolutePosition.x;
        public float Right => ScreenSize.x - Left;

        public float Bottom => AbsolutePosition.y;
        public float Top => ScreenSize.y - Bottom;

        public float RelativeLeft => AbsolutePosition.x / ScreenSize.x;
        public float RelativeRight => 1.0f - RelativeLeft;

        public float RelativeBottom => AbsolutePosition.y / ScreenSize.y;
        public float RelativeTop => 1.0f - RelativeBottom;

        public ScreenPosition(Vector2 absolute, Vector2 screenSize)
        {
            AbsolutePosition = absolute;
            ScreenSize = screenSize;
        }
    }
}
