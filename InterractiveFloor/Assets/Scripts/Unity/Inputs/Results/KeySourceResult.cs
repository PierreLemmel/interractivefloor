﻿using System.Collections.Generic;
using UnityEngine;

namespace InterractiveFloor.Unity.Inputs
{
	public class KeySourceResult : Result
	{
		public IReadOnlyDictionary<KeyCode, KeyResult> KeyResults { get; }

		public KeySourceResult(IReadOnlyDictionary<KeyCode, KeyResult> keyResults)
		{
			KeyResults = keyResults;
		}
	}
}