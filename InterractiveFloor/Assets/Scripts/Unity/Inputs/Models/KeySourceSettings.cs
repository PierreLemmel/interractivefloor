﻿using System.Collections.Generic;
using UnityEngine;

namespace InterractiveFloor.Unity.Inputs
{
    public class KeySourceSettings : Settings<KeySourceSettings>
    {
        public List<KeyCode> KeyCodes { get; set; } = new List<KeyCode>();
    }
}