﻿using System;

namespace InterractiveFloor.Unity.Inputs
{
    public enum KeyStatus
    {
        Free = 0,
        Up = 1,
        Pressed = 2,
        Down = 3
    }
}