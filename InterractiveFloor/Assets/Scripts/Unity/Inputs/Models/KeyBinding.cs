﻿using UnityEngine;
using UnityEngine.Events;

namespace InterractiveFloor.Unity.Inputs
{
    public class KeyBinding : Model
    {
        public KeyCode Key { get; set; }
        public UnityEvent Event { get; set; }
    }
}
