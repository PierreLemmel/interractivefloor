﻿using System;

namespace InterractiveFloor.Unity.Inputs
{
    public interface IClickSource : IAsyncResultStream<ScreenPosition>
    {
    }
}
