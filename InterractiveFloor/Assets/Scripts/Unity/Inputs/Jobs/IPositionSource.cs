﻿namespace InterractiveFloor.Unity.Inputs
{
    public interface IPositionSource : IAsyncResultStream<ScreenPosition>
    {
        
    }
}