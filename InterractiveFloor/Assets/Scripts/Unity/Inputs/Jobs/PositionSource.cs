﻿using UnityEngine;

namespace InterractiveFloor.Unity.Inputs
{
    public class PositionSource : IPositionSource
    {
        private static readonly Vector2 Unitialized = new Vector2(-1.0f, -1.0f);

        private Vector2 lastPosition = Unitialized;
        private Vector2 lastSize = Unitialized;

        public bool TryGetNextResult(out ScreenPosition result)
        {
            Vector3 uPos = Input.mousePosition;
            Vector2 position = new Vector2(uPos.x, uPos.y);
            Vector2 screenSize = new Vector2(Screen.width, Screen.height);
            
            if (position != lastPosition || screenSize != lastSize)
            {
                lastPosition = position;
                lastSize = screenSize;

                result = new ScreenPosition(position, screenSize);
                return true;
            }
            else
            {
                result = null;
                return false;
            }
        }
    }
}