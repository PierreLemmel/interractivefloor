﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.Unity.Inputs
{
    public class KeySource : IKeySource
    {
        private IReadOnlyCollection<KeyCode> keys;

        public StartStopState State { get; private set; }

        public void Start(KeySourceSettings settings)
        {
            if (State == StartStopState.Started)
                throw new InvalidOperationException("Impossible to start KeySource : it has already been started yet");

            keys = settings.KeyCodes.Copy();
            State = StartStopState.Started;
        }

        public void Stop()
        {
            if (State != StartStopState.Started)
                throw new InvalidOperationException("Impossible to stop KeySource : it has not been started yet");

            keys = null;
            State = StartStopState.Stopped;
        }

        public bool TryGetNextResult(out KeySourceResult result)
        {
            if (State != StartStopState.Started)
                throw new InvalidOperationException("KeySource has not been started yet");

            IReadOnlyDictionary<KeyCode, KeyResult> keyResults = keys.ToDictionary(
                key => key,
                key =>
                {
                    KeyStatus status;
                    if (Input.GetKeyDown(key))
                        status = KeyStatus.Down;
                    else if (Input.GetKeyUp(key))
                        status = KeyStatus.Up;
                    else if (Input.GetKey(key))
                        status = KeyStatus.Pressed;
                    else
                        status = KeyStatus.Free;

                    return new KeyResult(status);
                });

            result = new KeySourceResult(keyResults);
            return true;
        }
    }
}