﻿namespace InterractiveFloor.Unity.Inputs
{
    public interface IKeySource : IAsyncResultStream<KeySourceResult>, IStartable<KeySourceSettings>, IStopable, IStateMachine<StartStopState>
    {
        
    } 
}