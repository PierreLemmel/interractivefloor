﻿using UnityEngine;

namespace InterractiveFloor.Unity.Inputs
{
    public class ClickSource : IClickSource
    {
        public bool TryGetNextResult(out ScreenPosition result)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 uPos = Input.mousePosition;
                Vector2 position = new Vector2(uPos.x, uPos.y);
                Vector2 screenSize = new Vector2(Screen.width, Screen.height);

                result = new ScreenPosition(position, screenSize);
                return true;
            }
            else
            {
                result = null;
                return false;
            }
        }
    }
}