﻿namespace InterractiveFloor.Unity.Inputs
{
    public class KeyBinderServiceProvider : SceneComponent, IServiceProvider<IKeySource>
    {
        IKeySource IServiceProvider<IKeySource>.GetInstance() => new KeySource();
    }
}
