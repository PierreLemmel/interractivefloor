﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public class BridgeSettings : Model
    {
        public float RefreshRate { get; set; } = 60.0f;
    }
}