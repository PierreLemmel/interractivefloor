﻿using System;

namespace InterractiveFloor.Unity.Vision
{
    public class LiveViewResult : Result
    {
        public IntPtr ImgPtr { get; }
        public int Length { get; }
        public LiveViewMetadata Metadata { get; }

        public LiveViewResult(IntPtr imgPtr, int length, LiveViewMetadata metadata)
        {
            ImgPtr = imgPtr;
            Length = length;
            Metadata = metadata;
        }
    }
}