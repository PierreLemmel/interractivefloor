﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity.Vision
{
    public class LiveViewMetadata
    {
        public LiveViewMetadata(float zoomLevel, Vector2Int imagePosition, RectInt zoomRect, Vector2Int coordinateSystem)
        {
            ZoomLevel = zoomLevel;
            ImagePosition = imagePosition;
            ZoomRect = zoomRect;
            CoordinateSystem = coordinateSystem;
        }

        public float ZoomLevel { get; }
        public Vector2Int ImagePosition { get; }
        public RectInt ZoomRect { get; }
        public Vector2Int CoordinateSystem { get; }
    }
}