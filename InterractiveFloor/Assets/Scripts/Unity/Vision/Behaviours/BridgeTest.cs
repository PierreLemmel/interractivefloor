﻿using InterractiveFloor.Unity.Vision.Canon;
using System;
using UnityEngine;

namespace InterractiveFloor.Unity.Vision
{
    [RequireService(typeof(ICanonCameraBridge))]
    public class BridgeTest : SceneComponent
    {
        private ICanonCameraBridge bridge;

        public Material LiveViewMaterial { get; set; }

        public BridgeSettings BridgeSettings { get; set; }

        public CanonCameraProperties CameraProperties { get; set; }
        public CanonCaptureProperties CaptureProperties { get; set; }
        public EvfProperties EvfProperties { get; set; }

        private Texture2D targetTexture;

        protected override void Bootstrap(ISpyConfig spy)
        {
            bridge = new CanonCameraBridge();

            targetTexture = new Texture2D(1, 1);
        }

        protected override void Initialize()
        {
            bridge.Initialize(BridgeSettings);
            CameraProperties = bridge.CameraProperties;
            CaptureProperties = bridge.CaptureProperties;
            EvfProperties = bridge.EvfProperties;
        }

        protected override void Tick()
        {
            if (bridge.TryGetNextResult(out LiveViewResult result))
            {
                Debug.Log("Yep");
                IntPtr imgPtr = result.ImgPtr;
                int length = result.Length;

                LiveViewMetadata metadata = result.Metadata;

                Debug.Log($"ImgPtr: {imgPtr}");
                Debug.Log($"Length: {length}");

                Debug.Log($"Metadata zoomlvl: {metadata.ZoomLevel}");
                Debug.Log($"Metadata zoomrect: {metadata.ZoomRect}");
                Debug.Log($"Metadata imgPosition: {metadata.ImagePosition}");
                Debug.Log($"Metadata coordsyst: {metadata.CoordinateSystem}");

                targetTexture.LoadRawTextureData(imgPtr, length);
                Debug.Log("Yep2");
            }
        }

        [UserAction]
        public void StartLiveView() => bridge.Start();

        [UserAction]
        public void StopLiveView() => bridge.Stop();

        protected override void OnDisposed() => bridge.Terminate();
    }
}
