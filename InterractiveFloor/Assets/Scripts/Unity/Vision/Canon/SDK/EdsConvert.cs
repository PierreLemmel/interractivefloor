﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity.Vision.Canon
{
    public static class EdsConvert
    {
        public static Vector2Int ToVector2Int(this EdsPoint pt) => new Vector2Int(pt.x, pt.y);
        public static Vector2Int ToVector2Int(this EdsSize size) => new Vector2Int(size.width, size.height);

        public static RectInt ToRectInt(this EdsRect rect) => new RectInt(rect.x, rect.y, rect.width, rect.height);

        public static float ToFloat(this EvfZoom zoom)
        {
            switch (zoom)
            {
                case EvfZoom.Fit: return 1.0f;
                case EvfZoom.Zoom_x5: return 5.0f;
                case EvfZoom.Zoom_x10: return 10.0f;
                default: throw new InvalidOperationException($"Unexpected zoom: {zoom} ({(int)zoom})");
            };
        }
    }
}