﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum PictureStyle : uint
    {
        UNINITIALIZED = 0x0000,

        Standard = 0x0081,
        Portrait = 0x0082,
        Landscape = 0x0083,
        Neutral = 0x0084,
        Faithful = 0x0085,
        Monochrome = 0x0086,
        Auto = 0x0087,
        FineDetail = 0x0088,
        ComputerSetting1 = 0x0041,
        ComputerSetting2 = 0x0042,
        ComputerSetting3 = 0x0043,
    }
}