﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum AFMode : uint
    {
        OneShotAF = 0,
        AIServoAF = 1,
        AIFocusAF = 2,
        ManualFocus = 3,
        INVALID = 0xffffffff,
    }
}