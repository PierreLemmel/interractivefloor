﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum DriveMode : uint
    {
        SingleShooting = 0x00000000,
        ContinuousShooting = 0x00000001,
        Video = 0x00000002,
        HighSpeedContinuous = 0x00000004,
        LowSpeedContinuous = 0x00000005,
        SingleSilentShooting = 0x00000006,
        SelfTimerContinuous = 0x00000007,
        SelfTimer10Sec = 0x00000010,
        SelfTimer2Sec = 0x00000011,
        SuperHighSpeed_14FPS = 0x00000012,
        SilentSingleShooting = 0x00000013,
        SilentContinShooting = 0x00000014,
        SilentHSContinuous = 0x00000015,
        SilentLSContinuous = 0x00000016,
    }
}