﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum Bracket : uint
    {
        UNITIALIZED = 0,

        AEBracket = 0x01,
        ISOBracket = 0x02,
        WBBracket = 0x04,
        FEBracket = 0x08,

        BracketOff = 0xffffffff,
    }
}