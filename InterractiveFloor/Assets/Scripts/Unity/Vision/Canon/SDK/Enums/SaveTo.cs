﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum SaveTo : uint
    {
        UNINITIALIZED = 0,

        MemoryCard = 1,
        DownloadToHost = 2,
        BothWays = 3
    }
}