﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum ISOSpeed
    {
        ISOAuto = 0x00000000,
        ISO6 = 0x00000028,
        ISO12 = 0x00000030,
        ISO25 = 0x00000038,
        ISO50 = 0x00000040,
        ISO100 = 0x00000048,
        ISO125 = 0x0000004b,
        ISO160 = 0x0000004d,
    }
}
