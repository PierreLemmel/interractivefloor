﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum MeteringMode : uint
    {
        UNITIALIZED = 0,

        SpotMetering = 1,
        EvaluativeMetering = 3,
        PartialMetering = 4,
        CenterWeightedAveragingMatering = 5,

        INVALID = 0xffffffff,
    }
}