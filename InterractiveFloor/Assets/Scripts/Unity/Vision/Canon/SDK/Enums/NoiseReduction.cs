﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum NoiseReduction : uint
    {
        Off = 0,
        On1 = 1,
        On2 = 2,
        On = 3,

        INVALID = 0xffffffff,
    }
}