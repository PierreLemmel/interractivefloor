﻿using System;

namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EvfZoom : uint
    {
        UNINITIALIZED = 0,

        Fit = 1,
        Zoom_x5 = 5,
        Zoom_x10 = 10
    }
}