﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum RedEye : uint
    {
        Off = 0,
        On = 1,
        INVALID = 0xffffffff,
    }
}