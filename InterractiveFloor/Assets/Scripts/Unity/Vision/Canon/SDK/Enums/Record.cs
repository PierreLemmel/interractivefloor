﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum Record : uint
    {
        EndMovieShooting = 0,
        Ready = 3,
        BeginMovieShooting = 4
    }
}