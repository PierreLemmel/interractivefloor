﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsPropertyEvent : uint
    {
        All = 0x00000100,
        PropertyChanged = 0x00000101,
        PropertyDescChanged = 0x00000102
    }
}