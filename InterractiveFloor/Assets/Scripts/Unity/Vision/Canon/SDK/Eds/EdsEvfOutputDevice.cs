﻿using System;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [Flags]
    public enum EdsEvfOutputDevice : uint
    {
        None = 0,
        Camera = 1,
        PC = 2,
        Filming = 3
    }
}