﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsProperty : uint
    {
        Unknown = 0x0000ffff,

        ProductName = 0x00000002,
        BodyIDEx = 0x00000015,
        OwnerName = 0x00000004,
        MakerName = 0x00000005,
        DateTime = 0x00000006,
        FirmwareVersion = 0x00000007,
        BatteryLevel = 0x00000008,
        CFn = 0x00000009,
        SaveTo = 0x0000000b,
        CurrentStorage = 0x0000000c,
        CurrentFolder = 0x0000000d,

        BatteryQuality = 0x00000010,

        // Image Properties
        ImageQuality = 0x00000100,
        Orientation = 0x00000102,
        ICCProfile = 0x00000103,
        FocusInfo = 0x00000104,
        WhiteBalance = 0x00000106,
        ColorTemperature = 0x00000107,
        WhiteBalanceShift = 0x00000108,
        ColorSpace = 0x0000010d,
        PictureStyle = 0x00000114,
        PictureStyleDesc = 0x00000115,
        PictureStyleCaption = 0x00000200,


        // Capture Properties
        AEMode = 0x00000400,
        AEModeSelect = 0x00000436,
        DriveMode = 0x00000401,
        ISOSpeed = 0x00000402,
        MeteringMode = 0x00000403,
        AFMode = 0x00000404,
        Av = 0x00000405,
        Tv = 0x00000406,
        ExposureCompensation = 0x00000407,
        FocalLength = 0x00000409,
        AvailableShots = 0x0000040a,
        Bracket = 0x0000040b,
        WhiteBalanceBracket = 0x0000040c,
        LensName = 0x0000040d,
        AEBracket = 0x0000040e,
        FEBracket = 0x0000040f,
        ISOBracket = 0x00000410,
        NoiseReduction = 0x00000411,
        FlashOn = 0x00000412,
        RedEye = 0x00000413,
        FlashMode = 0x00000414,
        LensStatus = 0x00000416,

        Artist = 0x00000418,
        Copyright = 0x00000419,

        // EVF Properties
        Evf_OutputDevice = 0x00000500,
        Evf_Mode = 0x00000501,
        Evf_WhiteBalance = 0x00000502,
        Evf_ColorTemperature = 0x00000503,
        Evf_DepthOfFieldPreview = 0x00000504,

        // EVF IMAGE DATA Properties
        Evf_Zoom = 0x00000507,
        Evf_ZoomPosition = 0x00000508,
        Evf_ImagePosition = 0x0000050B,
        Evf_HistogramStatus = 0x0000050C,
        Evf_AFMode = 0x0000050E,
        Evf_HistogramY = 0x00000515,
        Evf_HistogramR = 0x00000516,
        Evf_HistogramG = 0x00000517,
        Evf_HistogramB = 0x00000518,

        Evf_CoordinateSystem = 0x00000540,
        Evf_ZoomRect = 0x00000541,

        Record = 0x00000510,

        // Image GPS Properties
        GPSVersionID = 0x00000800,
        GPSLatitudeRef = 0x00000801,
        GPSLatitude = 0x00000802,
        GPSLongitudeRef = 0x00000803,
        GPSLongitude = 0x00000804,
        GPSAltitudeRef = 0x00000805,
        GPSAltitude = 0x00000806,
        GPSTimeStamp = 0x00000807,
        GPSSatellites = 0x00000808,
        GPSStatus = 0x00000809,
        GPSMapDatum = 0x00000812,
        GPSDateStamp = 0x0000081D,

        // DC Properties
        DC_Zoom = 0x00000600,
        DC_Strobe = 0x00000601,
        LensBarrelStatus = 0x00000605,


        TempStatus = 0x01000415,
        Evf_RollingPitching = 0x01000544,
        FixedMovie = 0x01000422,
        MovieParam = 0x01000423,

        Evf_ClickWBCoeffs = 0x01000506,
        ManualWhiteBalanceData = 0x01000204,

        MirrorUpSetting = 0x01000438,
        MirrorLockUpState = 0x01000421,

        UTCTime = 0x01000016,
        TimeZone = 0x01000017,
        SummerTimeSetting = 0x01000018,
    }
}
