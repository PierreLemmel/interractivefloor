﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsCameraCommand : uint
    {
        TakePicture = 0x00000000,
        ExtendShutDownTimer = 0x00000001,
        BulbStart = 0x00000002,
        BulbEnd = 0x00000003,
        DoEvfAf = 0x00000102,
        DriveLensEvf = 0x00000103,
        DoClickWBEvf = 0x00000104,
        MovieSelectSwON = 0x00000107,
        MovieSelectSwOFF = 0x00000108,
        PressShutterButton = 0x00000004,
        SetRemoteShootingMode = 0x0000010f,
        RequestRollPitchLevel = 0x00000109,
    }
}