﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsFileCreateDisposition : uint
    {
        CreateNew = 0,
        CreateAlways,
        OpenExisting,
        OpenAlways,
        TruncateExsisting,
    }
}