﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsAccess : uint
    {
        Read = 0,
        Write,
        ReadWrite,
        Error = 0xFFFFFFFF,
    }
}