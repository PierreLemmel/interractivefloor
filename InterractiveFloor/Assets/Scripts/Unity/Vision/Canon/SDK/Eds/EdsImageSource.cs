﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsImageSource : uint
    {
        FullView = 0,
        Thumbnail,
        Preview,
    }
}