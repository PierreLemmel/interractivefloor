﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsCameraState : uint
    {
        UILock = 0,
        UIUnlock = 1,
        EnterDirectTransfer = 2,
        ExitDirectTransfer = 3,
    }
}