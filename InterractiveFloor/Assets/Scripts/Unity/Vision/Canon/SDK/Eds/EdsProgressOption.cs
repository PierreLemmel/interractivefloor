﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsProgressOption : uint
    {
        NoReport = 0,
        Done,
        Periodically,
    }
}