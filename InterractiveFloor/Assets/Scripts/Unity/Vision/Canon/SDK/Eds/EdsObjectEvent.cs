﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsObjectEvent : uint
    {
        All = 0x00000200,
        VolumeInfoChanged = 0x00000201,
        VolumeUpdateItems = 0x00000202,
        FolderUpdateItems = 0x00000203,
        DirItemCreated = 0x00000204,
        DirItemRemoved = 0x00000205,
        DirItemInfoChanged = 0x00000206,
        DirItemContentChanged = 0x00000207,
        DirItemRequestTransfer = 0x00000208,
        DirItemRequestTransferDT = 0x00000209,
        DirItemCancelTransferDT = 0x0000020a,

        VolumeAdded = 0x0000020c,
        VolumeRemoved = 0x0000020d,
    }
}