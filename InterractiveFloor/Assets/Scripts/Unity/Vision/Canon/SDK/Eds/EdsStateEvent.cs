﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsStateEvent : uint
    {
        All = 0x00000300,
        Shutdown = 0x00000301,
        JobStatusChanged = 0x00000302,
        WillSoonShutDown = 0x00000303,
        ShutDownTimerUpdate = 0x00000304,
        CaptureError = 0x00000305,
        InternalError = 0x00000306,
        AfResult = 0x00000309,
    }
}