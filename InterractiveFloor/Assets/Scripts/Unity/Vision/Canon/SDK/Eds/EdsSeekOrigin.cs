﻿namespace InterractiveFloor.Unity.Vision.Canon
{
    public enum EdsSeekOrigin : uint
    {
        Cur = 0,
        Begin,
        End,
    }
}