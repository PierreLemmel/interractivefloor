﻿using System;

namespace InterractiveFloor.Unity.Vision.Canon
{
    public class EDSDKException : Exception
    {
        public EdsError ErrorCode { get; }

        public EDSDKException(string message) : this(message, EdsError.OK) { }
        public EDSDKException(string message, EdsError errorCode) : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}