﻿using System;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SDKPropertyAttribute : Attribute
    {
        public EdsProperty PropertyId { get; }
        public bool Initialize { get; }

        public SDKPropertyAttribute(EdsProperty property, bool initialize = true)
        {
            PropertyId = property;
            Initialize = initialize;
        }
    }
}