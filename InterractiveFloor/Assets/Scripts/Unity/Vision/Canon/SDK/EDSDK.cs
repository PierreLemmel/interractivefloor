﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace InterractiveFloor.Unity.Vision.Canon
{
    public static unsafe partial class EDSDK
    {
        private const string DllPath = "EDSDK.dll";

        public const int EDS_MAX_NAME = 256;
        public const int EDS_TRANSFER_BLOCK_SIZE = 512;

        public delegate EdsError EdsProgressCallback(uint inPercent, IntPtr ctx, ref bool outCancel);
        public delegate EdsError EdsCameraAddedHandler(IntPtr inContext);
        public delegate EdsError EdsPropertyEventHandler(EdsPropertyEvent inEvent, EdsProperty property, uint param, IntPtr ctx);
        public delegate EdsError EdsObjectEventHandler(EdsObjectEvent inEvent, IntPtr inRef, IntPtr ctx);
        public delegate EdsError EdsStateEventHandler(EdsStateEvent inEvent, uint parameter, IntPtr ctx);

        [DllImport(DllPath)]
        public extern static EdsError EdsInitializeSDK();

        [DllImport(DllPath)]
        public extern static EdsError EdsTerminateSDK();

        [DllImport(DllPath)]
        public extern static EdsError EdsRetain(IntPtr inRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsRelease(IntPtr inRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetChildCount(IntPtr inRef, out int outCount);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetChildAtIndex(IntPtr inRef, int inIndex, out IntPtr outRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetParent(IntPtr inRef, out IntPtr outParentRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetPropertySize(IntPtr inRef, EdsProperty property, int param, out EdsDataType outDataType, out uint outSize);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetPropertyData(IntPtr inRef, EdsProperty property, int param, uint propertySize, void* outPropertyData);

        public static unsafe EdsError EdsGetPropertyData<TData>(IntPtr inRef, EdsProperty property, int param, out TData outPropertyData) where TData : unmanaged
        {
            uint size = (uint)sizeof(TData);

            TData result = default;
            EdsError err = EdsGetPropertyData(inRef, property, param, size, &result);

            outPropertyData = result;
            return err;
        }

        public static EdsError EdsGetPropertyData(IntPtr inRef, EdsProperty property, int param, out string outPropertyData)
        {
            IntPtr ptr = Marshal.AllocHGlobal(256);
            EdsError err = EdsGetPropertyData(inRef, property, param, 256, ptr.ToPointer());

            outPropertyData = Marshal.PtrToStringAnsi(ptr);
            Marshal.FreeHGlobal(ptr);

            return err;
        }

        public static EdsError EdsGetPropertyData<TData>(IntPtr inRef, EdsProperty property, out TData outPropertyData) where TData : unmanaged
            => EdsGetPropertyData(inRef, property, 0, out outPropertyData);

        public static EdsError EdsGetPropertyData(IntPtr inRef, EdsProperty property, out string outPropertyData) => EdsGetPropertyData(inRef, property, 0, out outPropertyData);

        [DllImport(DllPath)]
        public extern static EdsError EdsSetPropertyData(IntPtr inRef, EdsProperty property, int param, uint propertySize, void* inPropertyData);

        public static EdsError EdsSetPropertyData<TData>(IntPtr inRef, EdsProperty property, int param, uint propertySize, TData data) where TData : unmanaged
            => EdsSetPropertyData(inRef, property, param, propertySize, &data);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetPropertyDesc(IntPtr inRef, EdsProperty property, out EdsPropertyDesc outPropertyDesc);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetCameraList(out IntPtr outCameraListRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetDeviceInfo(IntPtr camera, out EdsDeviceInfo outDeviceInfo);

        [DllImport(DllPath)]
        public extern static EdsError EdsOpenSession(IntPtr camera);

        [DllImport(DllPath)]
        public extern static EdsError EdsCloseSession(IntPtr camera);

        [DllImport(DllPath)]
        public extern static EdsError EdsSendCommand(IntPtr camera, EdsCameraCommand command, int param);

        [DllImport(DllPath)]
        public extern static EdsError EdsSendStatusCommand(IntPtr camera, EdsCameraState cameraState, int param);

        [DllImport(DllPath)]
        public extern static EdsError EdsSetCapacity(IntPtr camera, EdsCapacity inCapacity);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetVolumeInfo(IntPtr camera, out EdsVolumeInfo outVolumeInfo);

        [DllImport(DllPath)]
        public extern static EdsError EdsFormatVolume(IntPtr inVolumeRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetDirectoryItemInfo(IntPtr inDirItemRef, out EdsDirectoryItemInfo outDirItemInfo);

        [DllImport(DllPath)]
        public extern static EdsError EdsDeleteDirectoryItem(IntPtr inDirItemRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsDownload(IntPtr inDirItemRef, ulong inReadSize, IntPtr outStream);

        [DllImport(DllPath)]
        public extern static EdsError EdsDownloadCancel(IntPtr inDirItemRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsDownloadComplete(IntPtr inDirItemRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsDownloadThumbnail(IntPtr inDirItemRef, IntPtr outStream);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetAttribute(IntPtr inDirItemRef, out EdsFileAttribute outFileAttribute);

        [DllImport(DllPath)]
        public extern static EdsError EdsSetAttribute(IntPtr inDirItemRef, EdsFileAttribute inFileAttribute);

        [DllImport(DllPath)]
        public extern static EdsError EdsCreateFileStream(string inFileName, EdsFileCreateDisposition inCreateDisposition, EdsAccess inDesiredAccess, out IntPtr outStream);

        [DllImport(DllPath)]
        public extern static EdsError EdsCreateMemoryStream(ulong inBufferSize, out IntPtr outStream);

        [DllImport(DllPath)]
        public extern static EdsError EdsCreateStreamEx(string inFileName, EdsFileCreateDisposition inCreateDisposition, EdsAccess inDesiredAccess, out IntPtr outStream);

        [DllImport(DllPath)]
        public extern static EdsError EdsCreateMemoryStreamFromPointer(IntPtr inUserBuffer, ulong inBufferSize, out IntPtr outStream);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetPointer(IntPtr inStreamRef, out IntPtr outPointer);

        [DllImport(DllPath)]
        public extern static EdsError EdsRead(IntPtr inStreamRef, ulong inReadSize, IntPtr outBuffer, out ulong outReadSize);

        [DllImport(DllPath)]
        public extern static EdsError EdsWrite(IntPtr inStreamRef, ulong inWriteSize, IntPtr inBuffer, out uint outWrittenSize);

        [DllImport(DllPath)]
        public extern static EdsError EdsSeek(IntPtr inStreamRef, long inSeekOffset, EdsSeekOrigin inSeekOrigin);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetPosition(IntPtr inStreamRef, out ulong outPosition);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetLength(IntPtr inStreamRef, out ulong length);

        public static EdsError EdsGetLength(IntPtr inStreamRef, out int length)
        {
            EdsError result = EdsGetLength(inStreamRef, out ulong longLength);
            length = (int)longLength;
            return result;
        }

        [DllImport(DllPath)]
        public extern static EdsError EdsCopyData(IntPtr inStreamRef, ulong inWriteSize, IntPtr outStreamRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsSetProgressCallback(IntPtr inRef, EdsProgressCallback inProgressFunc, EdsProgressOption inProgressOption, IntPtr ctx);

        [DllImport(DllPath)]
        public extern static EdsError EdsCreateImageRef(IntPtr inStreamRef, out IntPtr outImageRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetImageInfo(IntPtr inImageRef, EdsImageSource inImageSource, out EdsImageInfo outImageInfo);

        [DllImport(DllPath)]
        public extern static EdsError EdsGetImage(IntPtr inImageRef, EdsImageSource inImageSource, EdsTargetImageType inImageType, EdsRect inSrcRect, EdsSize inDstSize, IntPtr outStreamRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsSetCameraAddedHandler(EdsCameraAddedHandler inCameraAddedHandler, IntPtr ctx);

        [DllImport(DllPath)]
        public extern static EdsError EdsSetPropertyEventHandler(IntPtr camera, EdsPropertyEvent inEvent, EdsPropertyEventHandler propertyEventHandler, IntPtr ctx);

        [DllImport(DllPath)]
        public extern static EdsError EdsSetObjectEventHandler(IntPtr camera, EdsObjectEvent inEvent, EdsObjectEventHandler inObjectEventHandler, IntPtr ctx);

        [DllImport(DllPath)]
        public extern static EdsError EdsSetCameraStateEventHandler(IntPtr camera, EdsStateEvent inEvent, EdsStateEventHandler inStateEventHandler, IntPtr ctx);

        [DllImport(DllPath)]
        public extern static EdsError EdsCreateEvfImageRef(IntPtr inStreamRef, out IntPtr outEvfImageRef);

        [DllImport(DllPath)]
        public extern static EdsError EdsDownloadEvfImage(IntPtr camera, IntPtr outEvfImageRef);
    }
}