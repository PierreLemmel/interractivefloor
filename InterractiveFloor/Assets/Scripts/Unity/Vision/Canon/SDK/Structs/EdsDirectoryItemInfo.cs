﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsDirectoryItemInfo
    {
        public ulong Size;
        public int isFolder;
        public uint GroupID;
        public uint Option;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = EDSDK.EDS_MAX_NAME)]
        public string szFileName;

        public uint format;
        public uint dateTime;
    }
}