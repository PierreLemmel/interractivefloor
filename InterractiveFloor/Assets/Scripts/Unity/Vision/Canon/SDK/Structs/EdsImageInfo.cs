﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsImageInfo
    {
        public uint Width;
        public uint Height;

        public uint NumOfColorComponents;
        public uint ComponentDepth;

        public EdsRect EffectiveRect;

        public uint reserved1;
        public uint reserved2;
    }
}