﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsManualWBData
    {
        public uint Valid;
        public uint dataSize;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string szCaption;

        [MarshalAs(UnmanagedType.ByValArray)]
        public byte[] data;
    }
}