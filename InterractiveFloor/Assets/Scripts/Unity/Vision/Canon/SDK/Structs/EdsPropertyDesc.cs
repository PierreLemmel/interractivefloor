﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsPropertyDesc
    {
        public int Form;
        public uint Access;
        public int NumElements;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public int[] PropDesc;
    }
}