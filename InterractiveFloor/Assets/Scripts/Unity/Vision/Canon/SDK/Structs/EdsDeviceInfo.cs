﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsDeviceInfo
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = EDSDK.EDS_MAX_NAME)]
        public string szPortName;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = EDSDK.EDS_MAX_NAME)]
        public string szDeviceDescription;

        public uint DeviceSubType;

        public uint reserved;
    }
}