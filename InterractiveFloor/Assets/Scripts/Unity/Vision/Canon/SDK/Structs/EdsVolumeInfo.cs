﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsVolumeInfo
    {
        public uint StorageType;
        public uint Access;
        public ulong MaxCapacity;
        public ulong FreeSpaceInBytes;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = EDSDK.EDS_MAX_NAME)]
        public string szVolumeLabel;
    }
}