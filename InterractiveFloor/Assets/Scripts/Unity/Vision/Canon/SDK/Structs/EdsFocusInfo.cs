﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsFocusInfo
    {
        public EdsRect imageRect;
        public uint pointNumber;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 600)]
        public EdsFocusPoint[] focusPoint;
        public uint executeMode;
    }
}