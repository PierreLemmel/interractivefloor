﻿using System;
using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsSaveImageSetting
    {
        public uint JPEGQuality;
        IntPtr iccProfileStream;
        public uint reserved;
    }
}