﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsCameraPos
    {
        public int status;
        public int position;
        public int rolling;
        public int pitching;
    }
}