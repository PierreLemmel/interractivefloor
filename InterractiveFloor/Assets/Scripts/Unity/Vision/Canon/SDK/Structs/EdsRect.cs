﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsRect
    {
        public int x;
        public int y;
        public int width;
        public int height;
    }
}