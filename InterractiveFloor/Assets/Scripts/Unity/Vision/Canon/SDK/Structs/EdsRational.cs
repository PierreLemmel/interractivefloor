﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsRational
    {
        public int Numerator;
        public uint Denominator;
    }
}