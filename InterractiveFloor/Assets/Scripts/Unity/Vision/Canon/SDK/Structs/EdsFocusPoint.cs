﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsFocusPoint
    {
        public uint valid;
        public uint selected;
        public uint justFocus;
        public EdsRect rect;
        public uint reserved;
    }
}