﻿using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Vision.Canon
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EdsPictureStyleDesc
    {
        public int contrast;
        public uint sharpness;
        public int saturation;
        public int colorTone;
        public uint filterEffect;
        public uint toningEffect;
        public uint sharpFineness;
        public uint sharpThreshold;
    }
}