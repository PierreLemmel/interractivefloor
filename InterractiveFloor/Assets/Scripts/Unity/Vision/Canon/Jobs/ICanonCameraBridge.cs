﻿using System;

namespace InterractiveFloor.Unity.Vision.Canon
{
    public interface ICanonCameraBridge : IStartable, IStopable, IInitializable<BridgeSettings>, ITerminable, IAsyncResultStream<LiveViewResult>, IStateMachine<SourceState>
    {
        CanonCameraProperties CameraProperties { get; }
        CanonCaptureProperties CaptureProperties { get; }
        EvfProperties EvfProperties { get; }
    }
}