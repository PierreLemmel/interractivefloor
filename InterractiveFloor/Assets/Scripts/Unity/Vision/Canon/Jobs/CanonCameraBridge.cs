﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;


namespace InterractiveFloor.Unity.Vision.Canon
{
    public class CanonCameraBridge : ICanonCameraBridge
    {
        private delegate void PropertyChangedFunction(uint param);
        private class PropertyChangedHandler
        {
            public PropertyChangedFunction Function { get; }
            public bool Initialize { get; }

            public PropertyChangedHandler(PropertyChangedFunction function, bool initialize)
            {
                Function = function;
                Initialize = initialize;
            }
        }

        private IntPtr camera;
        private string deviceName;

        private IDictionary<EdsProperty, PropertyChangedHandler> propertyHandlers;
        private IDictionary<EdsProperty, PropertyChangedFunction> additionalHandlers;

        public SourceState State { get; private set; } = SourceState.Uninitialized;

        private BridgeSettings bridgeSettings;

        public CanonCameraProperties CameraProperties { get; }
        public CanonCaptureProperties CaptureProperties { get; }
        public EvfProperties EvfProperties { get; }

        private bool sdkInitialized = false;
        private bool sessionOpened = false;

        private object resultMutex = new object();
        private LiveViewResult lastResult;

        public CanonCameraBridge()
        {
            CameraProperties = new CanonCameraProperties();
            CaptureProperties = new CanonCaptureProperties();
            EvfProperties = new EvfProperties();

            propertyHandlers = new Dictionary<EdsProperty, PropertyChangedHandler>();

            SDKBinder<CanonCameraProperties> cameraPropertiesBinder = new SDKBinder<CanonCameraProperties>();
            foreach ((EdsProperty PropertyId, SDKBinder<CanonCameraProperties>.Binding binding) in cameraPropertiesBinder.GetBindings())
            {
                PropertyChangedFunction function = param => binding.Function.Invoke(camera, CameraProperties, (int)param);
                PropertyChangedHandler handler = new PropertyChangedHandler(function, binding.Initialize);

                propertyHandlers.Add(PropertyId, handler);
            }

            SDKBinder<CanonCaptureProperties> capturePropertiesBinder = new SDKBinder<CanonCaptureProperties>();
            foreach ((EdsProperty PropertyId, SDKBinder<CanonCaptureProperties>.Binding binding) in capturePropertiesBinder.GetBindings())
            {
                PropertyChangedFunction function = param => binding.Function.Invoke(camera, CaptureProperties, (int)param);
                PropertyChangedHandler handler = new PropertyChangedHandler(function, binding.Initialize);

                propertyHandlers.Add(PropertyId, handler);
            }

            SDKBinder<EvfProperties> evfPropertiesBinder = new SDKBinder<EvfProperties>();
            foreach ((EdsProperty PropertyId, SDKBinder<EvfProperties>.Binding binding) in evfPropertiesBinder.GetBindings())
            {
                PropertyChangedFunction function = param => binding.Function.Invoke(camera, EvfProperties, (int)param);
                PropertyChangedHandler handler = new PropertyChangedHandler(function, binding.Initialize);

                propertyHandlers.Add(PropertyId, handler);
            }

            additionalHandlers = new Dictionary<EdsProperty, PropertyChangedFunction>()
            {
                { EdsProperty.Record, _ => StartLiveViewIfPossible() },
                { EdsProperty.Evf_OutputDevice, _ => StartLiveViewIfPossible() }
            };
        }

        public void Initialize(BridgeSettings settings)
        {
            bridgeSettings = settings;

            IntPtr cameraList = IntPtr.Zero;

            try
            {
                EdsError err = EdsError.OK;

                Debug.Log("Initializing Canon SDK");

                err = EDSDK.EdsInitializeSDK();
                CheckEDSDKResult("EdsSDKInitialize", err);
                sdkInitialized = true;

                #region Camera Detection
                Debug.Log("Canon SDK : retrieving camera");

                err = EDSDK.EdsGetCameraList(out cameraList);
                CheckEDSDKResult("EDSGetCameraList", err);

                err = EDSDK.EdsGetChildCount(cameraList, out int count);
                CheckEDSDKResult("EdsGetChildCount", err);

                if (count == 0)
                    err = EdsError.DEVICE_NOT_FOUND;
                CheckEDSDKResult("CameraCountIsNotNull", err);

                err = EDSDK.EdsGetChildAtIndex(cameraList, 0, out camera);
                CheckEDSDKResult("EdsGetChildCount", err);

                err = EDSDK.EdsGetDeviceInfo(camera, out EdsDeviceInfo device);
                CheckEDSDKResult("EdsGetDeviceInfo", err);

                if (camera == null)
                    err = EdsError.DEVICE_NOT_FOUND;
                CheckEDSDKResult("CameraFound", err);

                deviceName = device.szDeviceDescription;
                Debug.Log($"Canon SDK : found camera {deviceName}");

                Debug.Log($"Canon SDK : opening session for camera {deviceName}");
                err = EDSDK.EdsOpenSession(camera);
                CheckEDSDKResult("EdsOpenSession", err);
                sessionOpened = true;
                Debug.Log($"Canon SDK : session for camera {deviceName} successfully opened");
                #endregion

                #region EventListeners
                Debug.Log("Canon SDK : setting up callbacks");

                PropertyEventStaticDispatcher += HandlePropertyEvent;
                err = EDSDK.EdsSetPropertyEventHandler(camera, EdsPropertyEvent.All, DispatchPropertyEvent, IntPtr.Zero);
                CheckEDSDKResult("EdsSetPropertyEventHandler", err);

                ObjectEventStaticDispatcher += HandleObjectEvent;
                err = EDSDK.EdsSetObjectEventHandler(camera, EdsObjectEvent.All, ObjectEventStaticDispatcher, IntPtr.Zero);
                CheckEDSDKResult("EdsSetObjectEventHandler", err);

                StateEventStaticDispatcher += HandleStateEvent;
                err = EDSDK.EdsSetCameraStateEventHandler(camera, EdsStateEvent.All, StateEventStaticDispatcher, IntPtr.Zero);
                CheckEDSDKResult("EdsSetCameraStateEventHandler", err);
                Debug.Log("Canon SDK : callbacks set up");
                #endregion

                #region Initialize Properties
                Debug.Log("Initializing properties");
                IEnumerable<PropertyChangedHandler> initializableBindings = propertyHandlers.Values.Where(b => b.Initialize);
                foreach (PropertyChangedHandler binding in initializableBindings)
                    binding.Function.Invoke(param: 0);
                #endregion

                Debug.Log("Canon SDK initialized");
                State = SourceState.Initialized;
            }
            finally
            {
                Release(ref cameraList);
            }
        }

        public void Start()
        {
            EdsError err = EdsError.OK;

            err = EDSDK.EdsGetPropertySize(camera, EdsProperty.Evf_OutputDevice, 0, out _, out uint size);
            CheckEDSDKResult("EdsGetPropertySize - Evf OutputDevice", err);

            err = EDSDK.EdsSetPropertyData(camera, EdsProperty.Evf_OutputDevice, 0, size, EdsEvfOutputDevice.PC);
            CheckEDSDKResult("EdsSetPropertySize - Evf OutputDevice - PC", err);

            State = SourceState.Started;
        }

        public bool TryGetNextResult(out LiveViewResult result)
        {
            lock (resultMutex)
            {
                if (lastResult != null)
                {
                    result = lastResult;
                    lastResult = null;
                    
                    return true;
                }
                else
                {
                    result = null;

                    return false;
                }
            }
        }

        public void Stop()
        {
            lvCancellation?.Cancel();
            lvCancellation = null;
        }

        public void Terminate()
        {
            if (State == SourceState.Started)
                throw new InvalidOperationException("Impossible to terminate a CanonCameraBridge that has not been terminated yet");

            EdsError err = EdsError.OK;

            if (sessionOpened)
            {
                Debug.Log($"Canon SDK : closing session for camera {deviceName}");
                err = EDSDK.EdsCloseSession(camera);
                CheckEDSDKResult("EdsCloseSession", err);
                Debug.Log($"Canon SDK : session for camera {deviceName} successfully closed"); 
            }
            else
            {
                Debug.LogWarning("Canon SDK : Due to prior failure in opening Camera Session, skipping session closure");
            }

            Release(ref camera);

            if (sdkInitialized)
            {
                Debug.Log("Terminating Canon SDK");
                err = EDSDK.EdsTerminateSDK();
                CheckEDSDKResult("EdsTerminateSDK", err);
                Debug.Log("Canon SDK terminated");

                sdkInitialized = false;
            }
            else
            {
                Debug.LogWarning("Canon SDK : due to prior failure in initializing SDK, skipping SDK termination");
            }

            State = SourceState.Terminated;
        }

        private void StartLiveViewIfPossible()
        {
            Record record = EvfProperties.Record;
            EdsEvfOutputDevice output = EvfProperties.OutputDevice;

            if (output == EdsEvfOutputDevice.PC || (output == EdsEvfOutputDevice.Filming && record == Record.Ready))
            {
                OnLiveviewStarted();
            }
        }

        private Task liveviewTask;
        private CancellationTokenSource lvCancellation;
        private void OnLiveviewStarted()
        {
            lvCancellation = new CancellationTokenSource();
            liveviewTask = Task.Run(async () => await CreateLiveViewTask(lvCancellation));
        }

        private async Task CreateLiveViewTask(CancellationTokenSource cancellation)
        {
            Debug.Log("Canon SDK: Liveview Started");

            IntPtr stream = IntPtr.Zero;

            int delay = (int) (1000.0f / bridgeSettings.RefreshRate);

            try
            {
                CancellationToken token = cancellation.Token;
                EdsError err = EdsError.OK;

                err = EDSDK.EdsCreateMemoryStream(0, out stream);
                CheckEDSDKResult("EdsCreateMemoryStream", err);

                State = SourceState.Started;

                IntPtr image = IntPtr.Zero;
                while(true)
                {
                    token.ThrowIfCancellationRequested();
                    await Task.Delay(delay);

                    try
                    {
                        err = EDSDK.EdsCreateEvfImageRef(stream, out image);
                        CheckEDSDKResult("EdsCreateEvfImageRef", err);

                        err = EDSDK.EdsDownloadEvfImage(camera, image);

                        if (err == EdsError.OBJECT_NOTREADY) continue;

                        CheckEDSDKResult("EdsDownloadEvfImage", err);

                        err = EDSDK.EdsGetPropertyData(image, EdsProperty.Evf_Zoom, 0, out EvfZoom evfZoom);
                        CheckEDSDKResult("EdsGetPropertyData - Image - Zoom", err);

                        err = EDSDK.EdsGetPropertyData(image, EdsProperty.Evf_ImagePosition, out EdsPoint evfImgPosition);
                        CheckEDSDKResult("EdsGetPropertyData - Image - Position", err);

                        err = EDSDK.EdsGetPropertyData(image, EdsProperty.Evf_ZoomRect, out EdsRect evfZoomRect);
                        CheckEDSDKResult("EdsGetPropertyData - Image - ZoomRect", err);

                        err = EDSDK.EdsGetPropertyData(image, EdsProperty.Evf_CoordinateSystem, out EdsSize evfCoordinateSystem);
                        CheckEDSDKResult("EdsGetPropertyData - Image - CoordinateSystem", err);

                        float zoom = evfZoom.ToFloat();
                        Vector2Int imgPosition = evfImgPosition.ToVector2Int();
                        RectInt zoomRect = evfZoomRect.ToRectInt();
                        Vector2Int coordSyst = evfCoordinateSystem.ToVector2Int();

                        LiveViewMetadata metadata = new LiveViewMetadata(zoom, imgPosition, zoomRect, coordSyst);

                        err = EDSDK.EdsGetPointer(stream, out IntPtr streamPtr);
                        CheckEDSDKResult("EdsGetPointer - streamPtr", err);

                        err = EDSDK.EdsGetLength(stream, out int streamLength);
                        CheckEDSDKResult("EdsGetLength - stream", err);

                        lock (resultMutex)
                        {
                            lastResult = new LiveViewResult(streamPtr, streamLength, metadata);
                        }
                    }
                    finally
                    {
                        Release(ref image);
                    }
                }
            }
            catch(TaskCanceledException)
            {
                OnLiveViewStopped();
            }
            finally
            {
                Release(ref stream);
            }
        }

        private void OnLiveViewStopped()
        {
            Debug.Log("Canon SDK: Liveview Stopped");
            State = SourceState.Stopped;
        }

        private EdsError HandlePropertyEvent(EdsPropertyEvent inEvent, EdsProperty property, uint inParam, IntPtr ctx)
        {
            switch (inEvent)
            {
                case EdsPropertyEvent.PropertyChanged:
                    HandlePropertyChanged(property, inParam);
                    break;
                case EdsPropertyEvent.PropertyDescChanged:
                    HandlePropertyDescChanged(property, inParam);
                    break;
                default:
                    throw new EDSDKException($"Unexepected event type: {inEvent}");
            }
            return EdsError.OK;
        }

        private void HandlePropertyChanged(EdsProperty property, uint param)
        {
            if (propertyHandlers.TryGetValue(property, out PropertyChangedHandler handler))
            {
                handler.Function.Invoke(param);
            }
            else
            {
                Debug.LogWarning($"Missing binding for PropertyChanged on property {property}");
            }

            if (additionalHandlers.TryGetValue(property, out var addHandler))
            {
                addHandler.Invoke(param);

            }
        }

        private void HandlePropertyDescChanged(EdsProperty propertyId, uint param)
        {
            Debug.Log($"Handle property desc changed for property '{propertyId}'. Parameter: '{param}'");
        }

        private EdsError HandleObjectEvent(EdsObjectEvent inEvent, IntPtr inRef, IntPtr ctx)
        {
            Debug.Log($"Handle object event '{inEvent}'");
            return EdsError.OK;
        }

        private EdsError HandleStateEvent(EdsStateEvent inEvent, uint inParam, IntPtr ctx)
        {
            Debug.Log($"Handle state event '{inEvent}'. Parameter: '{inParam}'");
            return EdsError.OK;
        }

        private static EDSDK.EdsPropertyEventHandler PropertyEventStaticDispatcher;
        private static EdsError DispatchPropertyEvent(EdsPropertyEvent inEvent, EdsProperty property, uint param, IntPtr ctx)
            => PropertyEventStaticDispatcher?.Invoke(inEvent, property, param, ctx) ?? EdsError.OK;

        private static EDSDK.EdsObjectEventHandler ObjectEventStaticDispatcher;
        private static EdsError DispatchObjectEvent(EdsObjectEvent inEvent, IntPtr inRef, IntPtr ctx)
            => ObjectEventStaticDispatcher?.Invoke(inEvent, inRef, ctx) ?? EdsError.OK;

        private static EDSDK.EdsStateEventHandler StateEventStaticDispatcher;
        private static EdsError DispatchStateEvent(EdsStateEvent inEvent, uint param, IntPtr ctx)
            => StateEventStaticDispatcher?.Invoke(inEvent, param, ctx) ?? EdsError.OK;

        private static void Release(ref IntPtr ptr)
        {
            if (ptr != IntPtr.Zero)
            {
                EDSDK.EdsRelease(ptr);
                ptr = IntPtr.Zero;
            }
        }

        private static void CheckEDSDKResult(string step, EdsError err)
        {
            if (err != EdsError.OK)
                throw new EDSDKException($"An error occured at step '{step}', check error code ({err}) for more information", err);
        }

        private class SDKBinder<TModel> where TModel : Model
        {
            public class Binding
            {
                public bool Initialize { get; }
                public BindingFunction Function { get; }

                public Binding(bool initialize, BindingFunction function)
                {
                    Initialize = initialize;
                    Function = function;
                }
            }

            public delegate void BindingFunction(IntPtr camera, TModel model, int param);

            public IReadOnlyCollection<(EdsProperty PropertyId, Binding Binding)> GetBindings() => typeof(TModel).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .Select(prop => (Property: prop, SDKAttribute: prop.GetCustomAttribute<SDKPropertyAttribute>()))
                    .Where(pa => pa.SDKAttribute != null)
                    .Select(pa => (
                            PropertyId: pa.SDKAttribute.PropertyId,
                            Binding: CreateBinding(pa.Property, pa.SDKAttribute)
                        )
                    ).ToList();

            private Binding CreateBinding(PropertyInfo property, SDKPropertyAttribute sdkAttr)
            {
                Type propertyType = property.PropertyType;
                EdsProperty propertyId = sdkAttr.PropertyId;
                bool initialize = sdkAttr.Initialize;

                BindingFunction function;
                if (propertyType.IsClass)
                    function = CreateBinding_ClassType(property, propertyId, propertyType);
                else
                    function = CreateBinding_ValueType(property, propertyId, propertyType);

                return new Binding(initialize, function);
            }

            private BindingFunction CreateBinding_ClassType(PropertyInfo property, EdsProperty propertyId, Type propertyType)
            {
                ParameterExpression cameraParam = Expression.Parameter(typeof(IntPtr), "camera");
                ParameterExpression modelParam = Expression.Parameter(typeof(TModel), "model");
                ParameterExpression paramParam = Expression.Parameter(typeof(int), "param");

                Type[] edsGetPropertyDataTypes = new Type[] { typeof(IntPtr), typeof(EdsProperty), typeof(int), propertyType.MakeByRefType() };

                MethodInfo getPropertyMethod = typeof(EDSDK).GetMethod(nameof(EDSDK.EdsGetPropertyData), edsGetPropertyDataTypes);
                MethodInfo checkResultMethod = typeof(CanonCameraBridge).GetMethod(nameof(CanonCameraBridge.CheckEDSDKResult), BindingFlags.Static | BindingFlags.NonPublic);

                ParameterExpression outResult = Expression.Parameter(propertyType, "outResult");
                ParameterExpression errVariable = Expression.Parameter(typeof(EdsError), "err");

                Expression body = Expression.Block(
                    new ParameterExpression[]
                    {
                        outResult,
                        errVariable
                    },
                    Expression.Assign(errVariable, Expression.Call(getPropertyMethod, cameraParam, Expression.Constant(propertyId), paramParam, outResult)),
                    Expression.Call(checkResultMethod, Expression.Constant($"GetProperty Binding for property {propertyId}"), errVariable),
                    Expression.Assign(Expression.Property(modelParam, property), outResult)
                );

                Expression<BindingFunction> lambda = Expression.Lambda<BindingFunction>(body, cameraParam, modelParam, paramParam);

                return lambda.Compile();
            }

            private BindingFunction CreateBinding_ValueType(PropertyInfo property, EdsProperty propertyId, Type propertyType)
            {
                // Special case: boolean does not exist in EDSDK
                bool isBoolProperty = propertyType == typeof(bool);

                ParameterExpression cameraParam = Expression.Parameter(typeof(IntPtr), "camera");
                ParameterExpression modelParam = Expression.Parameter(typeof(TModel), "model");
                ParameterExpression paramParam = Expression.Parameter(typeof(int), "param");

                MethodInfo getPropertyMethod_genericDefinition = typeof(EDSDK).GetGenericMethodDefinition(nameof(EDSDK.EdsGetPropertyData), BindingFlags.Public | BindingFlags.Static);
                Type getPropertyType = isBoolProperty ? typeof(uint) : propertyType;
                MethodInfo getPropertyMethod = getPropertyMethod_genericDefinition.MakeGenericMethod(getPropertyType);
                MethodInfo checkResultMethod = typeof(CanonCameraBridge).GetMethod(nameof(CanonCameraBridge.CheckEDSDKResult), BindingFlags.Static | BindingFlags.NonPublic);

                ParameterExpression outResult = Expression.Parameter(getPropertyType, "outResult");
                ParameterExpression errVariable = Expression.Parameter(typeof(EdsError), "err");

                Expression assignResult;
                if (!isBoolProperty)
                    assignResult = Expression.Assign(Expression.Property(modelParam, property), outResult);
                else
                {
                    assignResult = Expression.Assign(Expression.Property(modelParam, property), Expression.GreaterThan(outResult, Expression.Constant(0U)));
                }
                
                Expression body = Expression.Block(
                    new ParameterExpression[]
                    {
                        outResult,
                        errVariable
                    },
                    Expression.Assign(errVariable, Expression.Call(getPropertyMethod, cameraParam, Expression.Constant(propertyId), paramParam, outResult)),
                    Expression.Call(checkResultMethod, Expression.Constant($"GetProperty Binding for property {propertyId}"), errVariable),
                    assignResult
                );

                Expression<BindingFunction> lambda = Expression.Lambda<BindingFunction>(body, cameraParam, modelParam, paramParam);

                return lambda.Compile();
            }
        }
    } 
}