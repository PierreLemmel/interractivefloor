﻿using System;

namespace InterractiveFloor.Unity.Vision.Canon
{
    public class EvfProperties : Model
    {
        [NotEditable]
        [SDKProperty(EdsProperty.Evf_OutputDevice)]
        public EdsEvfOutputDevice OutputDevice { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.PictureStyle)]
        public PictureStyle PictureStyle { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.Evf_AFMode)]
        public AFMode AFMode { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.Record)]
        public Record Record { get; set; }
    }
}