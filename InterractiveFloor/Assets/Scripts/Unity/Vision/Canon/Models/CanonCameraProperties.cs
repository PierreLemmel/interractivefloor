﻿using System;

namespace InterractiveFloor.Unity.Vision.Canon
{
    public class CanonCameraProperties : Model
    {
        [NotEditable]
        [SDKProperty(EdsProperty.ProductName)]
        public string ProductName { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.BodyIDEx)]
        public string SerialNumber { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.OwnerName)]
        public string OwnerName { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.DateTime)]
        public EdsTime DateTime { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.FirmwareVersion)]
        public string FirmwareVersion { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.BatteryLevel)]
        public uint BatteryLevel { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.SaveTo)]
        public SaveTo SaveTo { get; set; }
    }
}
