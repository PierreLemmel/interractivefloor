﻿using System;

namespace InterractiveFloor.Unity.Vision.Canon
{
    public class CanonCaptureProperties : Model
    {
        [NotEditable]
        [SDKProperty(EdsProperty.AEMode)]
        public AEMode AEMode { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.AEModeSelect)]
        public AEMode AEModeSelect { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.DriveMode)]
        public DriveMode DriveMode { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.ISOSpeed)]
        public ISOSpeed ISOSpeed { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.MeteringMode)]
        public MeteringMode MeteringMode { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.AFMode)]
        public AFMode AFMode { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.Av)]
        public ApertureValue ApertureValue { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.Tv)]
        public ShutterSpeed Tv { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.ExposureCompensation)]
        public ExposureCompensation ExposureCompensation { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.AvailableShots)]
        public uint AvailableShots { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.Bracket)]
        public Bracket Bracket { get; set; }

        //int[]
        //[NotEditable]
        //[SDKProperty(EdsProperty.WhiteBalanceBracket)]
        //public uint WhiteBalanceBracket { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.LensName)]
        public string LensName { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.AEBracket)]
        public float AEBracket { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.LensStatus)]
        public bool LensAttached { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.Artist)]
        public string Artist { get; set; }

        [NotEditable]
        [SDKProperty(EdsProperty.Copyright)]
        public string Copyright { get; set; }
    }
}