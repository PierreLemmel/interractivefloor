﻿using Jace;
using Jace.Compilation;

namespace InterractiveFloor.Unity.Maths
{
    public class MathEngine : IMathEngine
    {
        private readonly CalculationEngine jaceEngine = new CalculationEngine();

        public IFormulaBuilder Formula(string text) => jaceEngine.Formula(text);
    }
}