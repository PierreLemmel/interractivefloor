﻿using System;
using System.Threading.Tasks;
using URandom = UnityEngine.Random;

namespace InterractiveFloor.Unity.Maths
{
    public class GameOfLifeAutomaton : IGameOfLifeAutomaton
    {
        private Grid<bool> currentState;
        private Grid<bool> nextState;

        public StartStopState State { get; private set; } = StartStopState.Stopped;

        public Grid<bool> CurrentValue => State == StartStopState.Started
            ? currentState
            : throw new InvalidOperationException("Can't get value when automaton has not been started");

        public bool MoveNext()
        {
            if (State != StartStopState.Started)
                throw new InvalidOperationException($"The automaton has not been started yet");

            int height = currentState.Height;
            int width = currentState.Width;

            Parallel.For(0, height, row =>
            {
                for (int col = 0; col < width; col++)
                {
                    GridCell<bool> cell = currentState[row, col];
                    var neighbours = cell.Neighbours;

                    int c = 0;
                    for (int i = 0; i < neighbours.Length; i++)
                    {
                        if (neighbours[i]) c++;
                    }

                    nextState[row, col].Value = cell ? (c == 2 || c == 3) : c == 3;
                }
            });

            Utils.Swap(ref currentState, ref nextState);

            return true;
        }

        public void Start(GameOfLifeSettings settings)
        {
            currentState = new Grid<bool>(
                settings.Width, settings.Height, (row, col) => URandom.Range(0.0f, 1.0f) <= settings.SpawnRate);
            nextState = currentState.Copy();

            State = StartStopState.Started;
        }

        public void Stop()
        {
            State = StartStopState.Stopped;
        }
    }
}