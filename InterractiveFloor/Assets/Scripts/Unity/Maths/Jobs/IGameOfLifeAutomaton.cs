﻿
namespace InterractiveFloor.Unity.Maths
{
    public interface IGameOfLifeAutomaton : IStartable<GameOfLifeSettings>, IStopable,
        IStateMachine<StartStopState>, IDeterministicSequence<Grid<bool>>
    {

    }
}