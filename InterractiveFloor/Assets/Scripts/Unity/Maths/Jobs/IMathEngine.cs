﻿using Jace.Compilation;

namespace InterractiveFloor.Unity.Maths
{
    public interface IMathEngine
    {
        IFormulaBuilder Formula(string text);
    }
}