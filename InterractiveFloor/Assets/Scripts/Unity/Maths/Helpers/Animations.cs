﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class Animations
    {
        public static float SmoothDamp(float current, float target, ref float velocity, FloatDampSettings dampSettings)
        {
            if (dampSettings.HasMaxSpeed)
                return Mathf.SmoothDamp(current, target, ref velocity, dampSettings.SmoothTime, dampSettings.MaxSpeed);
            else
                return Mathf.SmoothDamp(current, target, ref velocity, dampSettings.SmoothTime);
        }
    }
}