﻿namespace InterractiveFloor.Unity
{
    public class FloatDampSettings : Model
    {
        [InRange(0.01f, 0.5f)]
        public float SmoothTime { get; set; } = 0.1f;
        public bool HasMaxSpeed { get; set; } = false;
        [DisplayIf(nameof(HasMaxSpeed), DisplayCondition.IsTrue)]
        public float MaxSpeed { get; set; } = 100.0f;
    }
}