﻿namespace InterractiveFloor.Unity.Maths
{
    public class GameOfLifeSettings : Model
    {
        [GreaterThan(1)]
        public int Width { get; set; } = 100;

        [GreaterThan(1)]
        public int Height { get; set; } = 100;

        [InRange(0.0f, 1.0f)]
        public float SpawnRate { get; set; } = 0.1f;
    }
}