﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshFilter))]
    public class VideoFragment : SceneComponent, ISharingMesh, ISharingMaterial, ISettingsOwner<VideoFragmentSettings>
    {
        public Mesh SharedMesh { get; set; }
        public Material SharedMaterial { get; set; }

        public Vector3 Direction { get; set; }
        public VideoFragmentSettings Settings { get; set; }

        private float explodingDistance;
        private float initialVelocity;

        private float targetDistance;
        private float distance;
        private float distanceVel;

        private MeshFilter meshFilter;
        private MeshRenderer meshRenderer;


        protected override void Bootstrap(ISpyConfig spy)
        {
            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<MeshRenderer>();

            spy.When(this)
                .HasChangesOn(me => me.SharedMesh)
                .Do(SetupSharedMesh);

            spy.When(this)
                .HasChangesOn(me => me.SharedMaterial)
                .Do(SetupSharedMaterial);

            spy.When(Settings)
                .HasChangesOn(
                    fs => fs.ExplodingDistanceMean,
                    fs => fs.ExplodingDistanceSigma)
                .Do(RegenerateExplodingDistance);

            spy.When(Settings)
                .HasChangesOn(
                    fs => fs.InitialVelocityMean,
                    fs => fs.InitialVelocitySigma)
                .Do(RegenerateInitialVelocity);
        }

        protected override void Tick()
        {
            distance = Mathf.SmoothDamp(distance, targetDistance, ref distanceVel, Settings.SmoothTime);
            transform.localPosition = distance * Direction;
        }

        public void Explode()
        {
            if (Settings.AutoChangeDistance) RegenerateExplodingDistance();
            if (Settings.AutoChangeVelocity) RegenerateInitialVelocity();

            distance = 0.0f;
            targetDistance = explodingDistance;
            distanceVel = initialVelocity;
        }

        public void Gather()
        {
            if (Settings.AutoChangeVelocity) RegenerateInitialVelocity();

            distance = explodingDistance;
            targetDistance = 0.0f;
            distanceVel = -initialVelocity;
        }

        private void SetupSharedMesh() => meshFilter.sharedMesh = SharedMesh;
        private void SetupSharedMaterial() => meshRenderer.sharedMaterial = SharedMaterial;

        private void RegenerateExplodingDistance() => explodingDistance = MoreRandom.NormalDistribution(Settings.ExplodingDistanceMean, Settings.ExplodingDistanceSigma).Positive();
        private void RegenerateInitialVelocity() => initialVelocity = MoreRandom.NormalDistribution(Settings.InitialVelocityMean, Settings.InitialVelocitySigma).Positive();
    }
}