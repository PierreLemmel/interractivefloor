﻿using InterractiveFloor;
using InterractiveFloor.Unity.MeshGen;
using System.Linq;
using UnityEngine;


namespace InterractiveFloor.Unity
{
    [RequireService(typeof(IHollowPolyhedronGenerator))]
    [RequireService(typeof(IHollowPolyhedronTextureGenerator))]
    public class PsycheTunnel : SceneComponent
    {
        private IHollowPolyhedronGenerator meshGenerator;
        private IHollowPolyhedronTextureGenerator textureGenerator;

        public PsycheTunnelSettings Settings { get; set; }
        public HollowPolyhedronParameters TunnelShape { get; set; }
        public HsvCircleSchemeSettings ColorScheme { get; set; }

        private GameObject shapeModel;
        private GameObject spawnerObj;

        private Mesh tunnelMesh;
        private TranslateSettings shapeTranslateSettings;
        private RotateSettings shapeRotateSettings;
        private SpawnerSettings shapeSpawnerSettings;

        protected override void Bootstrap(ISpyConfig spy)
        {
            meshGenerator = new HollowPolyhedronGenerator();
            textureGenerator = new HollowPolyhedronTextureGenerator();

            tunnelMesh = new Mesh() { name = "TunnelMesh" };

            shapeTranslateSettings = TranslateSettings.CreateDefault();
            shapeTranslateSettings.Direction = Vector3.up;
            shapeTranslateSettings.Space = Space.Self;

            shapeRotateSettings = RotateSettings.CreateDefault();
            shapeRotateSettings.Axis = Vector3.up;
            shapeRotateSettings.Space = Space.Self;


            shapeModel = GameObjects
                .CreatePrefab()
                .Named("TunnelShape")
                .Rotating(rotate => rotate
                    .SharingSettings(shapeRotateSettings))
                .Translating(translate => translate
                    .SharingSettings(shapeTranslateSettings))
                .WithComponent<DistanceDestroy>(dd => dd
                    .CreateSettings(DistanceDestroySettings.CreateDefault)
                    .Targetting(gameObject)
                    .MaxDistance(Settings.MaxDistance))
                .WithComponent<MeshRenderer>(renderer => renderer
                    .SharingMaterial(Settings.ShapesMaterial))
                .WithComponent<MeshFilter>(filter => filter
                    .SharingMesh(tunnelMesh));

            shapeSpawnerSettings = ScriptableObject.CreateInstance<SpawnerSettings>();
            shapeSpawnerSettings.ObjectToSpawn = shapeModel;

            spawnerObj = GameObjects
                .CreateNew()
                .Named("Spawner")
                .AsChildOf(this)
                .RotatedBy(90.0f, 0.0f, 0.0f)
                .WithComponent<Spawner>(spawner => spawner
                    .SharingSettings(shapeSpawnerSettings)
                    .Spawning(shapeModel));

            spawnerObj.GetComponent<Spawner>().ForceSpawning(Settings.InitialItems);

            spy.When(Settings)
                .HasChangesOn(s => s.ShapesRotationSpeed)
                .Do(SetupRotate);

            spy.When(Settings)
                .HasChangesOn(s => s.ShapesTranslationSpeed)
                .Do(SetupTranslate);

            spy.When(Settings)
                .HasChangesOn(s => s.SpawnInterval)
                .Do(SetupSpawner);

            spy.When(Settings)
                .HasChanged()
                .Do(SetupChildren);

            spy.When(TunnelShape)
                .HasChangesOn(ts => ts.Sides)
                .Do(SetupMaterialTexture);

            spy.When(ColorScheme)
                .HasChanged()
                .Do(SetupMaterialTexture);

            spy.When(TunnelShape)
                .HasChanged()
                .Do(SetupMesh);
        }

        private void SetupChildren()
        {
            spawnerObj.GetComponent<Spawner>().ResetTimer();
            GameObject[] spawnedObjects = spawnerObj.GetChildren().ToArray();

            for (int i = spawnedObjects.Length - 1, k = 0; i >= 0; i--, k++)
            {
                GameObject child = spawnedObjects[i];

                float equivalentTime = k * Settings.SpawnInterval;

                Vector3 equivalentPosition = equivalentTime * Settings.ShapesTranslationSpeed * Vector3.up;
                float equivalentRotation = equivalentTime * Settings.ShapesRotationSpeed;

                child.transform.localPosition = equivalentPosition;
                child.transform.localRotation = Quaternion.Euler(0.0f, equivalentRotation, 0.0f);
            }
        }

        private void SetupTranslate() => shapeTranslateSettings.Speed = Settings.ShapesTranslationSpeed;
        private void SetupRotate() => shapeRotateSettings.Speed = Settings.ShapesRotationSpeed;
        private void SetupSpawner() => shapeSpawnerSettings.Interval = Settings.SpawnInterval;

        private void SetupMaterialTexture()
        {
            Texture2D texture = textureGenerator.GenerateTexture(ColorScheme, TunnelShape.Sides);
            Settings.ShapesMaterial.mainTexture = texture;
        }

        private void SetupMesh()
        {
            MeshGenerationResult meshGenResult = meshGenerator.GeneratePolyhedron(TunnelShape);

            tunnelMesh.Clear(keepVertexLayout: false);

            tunnelMesh.vertices = meshGenResult.Vertices;
            tunnelMesh.uv = meshGenResult.UVs;
            tunnelMesh.triangles = meshGenResult.Triangles;

            tunnelMesh.RecalculateNormals();
            tunnelMesh.RecalculateBounds();
            tunnelMesh.RecalculateTangents();
        }
    }
}