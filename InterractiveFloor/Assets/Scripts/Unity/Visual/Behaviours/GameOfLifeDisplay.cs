﻿using InterractiveFloor.Unity.Maths;
using InterractiveFloor.Unity.MeshGen;
using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    [RequireService(typeof(IGameOfLifeAutomaton))]
    [RequireService(typeof(IGameOfLifeGridGenerator))]
    public class GameOfLifeDisplay : SceneComponent
    {
        public GameOfLifeSettings AutomatonSettings { get; set; }
        public GameOfLifeDisplaySettings DisplaySettings { get; set; }

        [NotEditable]
        public int Steps { get; set; }

        private IGameOfLifeAutomaton automaton;
        private IGameOfLifeGridGenerator gridGenerator;

        private GameObject gridObject;
        private Mesh gridMesh;

        private int nbOfCols;
        private int nbOfRows;

        private float lastTime;

        protected override void Bootstrap(ISpyConfig spy)
        {
            automaton = Resolve<IGameOfLifeAutomaton>();
            gridGenerator = Resolve<IGameOfLifeGridGenerator>();

            Guard.NotNull(DisplaySettings.AliveMaterial, nameof(DisplaySettings.AliveMaterial));
            Guard.NotNull(DisplaySettings.DeadMaterial, nameof(DisplaySettings.DeadMaterial));

           
            GameOfLifeCellDisplaySettings cellDisplaySettings = GameOfLifeCellDisplaySettings.CreateDefault();
            cellDisplaySettings.AliveMaterial = DisplaySettings.AliveMaterial;
            cellDisplaySettings.DeadMaterial = DisplaySettings.DeadMaterial;

            gridMesh = Meshes.CreateNew()
                .Named("GameOfLifeGrid")
                .UsingInt32()
                .WithSubmeshes(2);

            gridObject = GameObjects.CreateNew()
                .Named("Grid")
                .AsChildOf(this)
                .WithComponent<MeshRenderer>(mr => mr.SharingMaterials(
                    DisplaySettings.AliveMaterial,
                    DisplaySettings.DeadMaterial
                ))
                .WithComponent<MeshFilter>(mf => mf.SharingMesh(gridMesh));

            spy.When(DisplaySettings)
                .HasChangesOn(
                    ds => ds.CellWidth,
                    ds => ds.CellSpacing)
                .Do(
                    SetupGridMesh,
                    SetupCamera
                );
        }

        protected override void Tick()
        {
            if (Time.time - lastTime > DisplaySettings.Step)
            {
                if (automaton.MoveNext())
                {
                    Grid<bool> state = automaton.CurrentValue;
                    gridGenerator.UpdateSubmeshes(gridMesh, state);

                    Steps++;

                    lastTime = Time.time;
                }
                else
                    throw new InvalidOperationException("Unexpected result from Game of Life automaton");
            }
        }

        protected override void UponActivation() => StartDisplay();
        protected override void UponDeactivation() => StopDisplay();

        [UserAction]
        public void Restart()
        {
            StopDisplay();
            StartDisplay();
        }

        private void StartDisplay()
        {
            automaton.Start(AutomatonSettings);
            Steps = 0;
            lastTime = float.MinValue;

            nbOfCols = automaton.CurrentValue.Width;
            nbOfRows = automaton.CurrentValue.Height;

            SetupGridMesh();
            SetupCamera();
        }

        private void StopDisplay() => automaton.Stop();

        private void SetupGridMesh()
        {
            if (automaton.State != StartStopState.Started) return;

            gridGenerator.UpdateVertices(gridMesh, nbOfRows, nbOfCols, DisplaySettings.CellWidth, DisplaySettings.CellSpacing);
        }

        private void SetupCamera()
        {
            float gridHeight = gridMesh.bounds.size.z;

            Camera sceneCamera = SceneRoot.SceneCamera;
            sceneCamera.transform.position = new Vector3(0.0f, 10.0f, 0.0f);
            sceneCamera.LookAt(this);
            sceneCamera.orthographic = true;
            sceneCamera.orthographicSize = gridHeight / 2.0f;
        }
    }
}