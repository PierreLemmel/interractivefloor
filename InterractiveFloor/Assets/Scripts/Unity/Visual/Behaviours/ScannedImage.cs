﻿using InterractiveFloor.Unity.MeshGen;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    [RequireService(typeof(IScannedSquareGenerator))]
    public class ScannedImage : SceneComponent
    {
        private IScannedSquareGenerator squareGenerator;

        public ScannedSquareParameters SquareParameters { get; set; } = new ScannedSquareParameters();
        public ScannedImageSettings ScannedImageSettings { get; set; } = new ScannedImageSettings();

        public List<ScanImageRowData> Rows { get; set; } = new List<ScanImageRowData>();
        public List<ScanImageColumnData> Columns { get; set; } = new List<ScanImageColumnData>();

        private Mesh squareMesh;

        protected override void Bootstrap(ISpyConfig spy)
        {
            squareGenerator = Resolve<IScannedSquareGenerator>();

            squareMesh = new Mesh() { name = "squareMesh" };

            GameObject squareMeshObject = GameObjects.CreateNew()
                .Named("Square Mesh")
                .AsChildOf(this)
                .WithComponent<MeshRenderer>(renderer =>
                    renderer.SharingMaterial(ScannedImageSettings.Material))
                .WithComponent<MeshFilter>(filter =>
                    filter.SharingMesh(squareMesh));
        }

        protected override void Tick()
        {
            UpdatePositions();
            ClearupUnusedLines();
            UpdateMesh();
        }

        private void UpdatePositions()
        {
            foreach(ScanImageRowData row in Rows)
                row.Position += (row.Direction == ScanLineVerticalDirection.Up ? 1.0f : -1.0f) * Time.deltaTime * ScannedImageSettings.RowSpeed;

            foreach (ScanImageColumnData col in Columns)
                col.Position += (col.Direction == ScanLineHorizontalDirection.Right ? 1.0f : -1.0f) * Time.deltaTime * ScannedImageSettings.ColumnSpeed;
        }

        private void ClearupUnusedLines()
        {
            Rows = Rows.Where(row => row.Position.IsInRange(0.0f, SquareParameters.Height)).ToList();
            Columns = Columns.Where(col => col.Position.IsInRange(0.0f, SquareParameters.Width)).ToList();
        }

        private void UpdateMesh()
        {
            MeshGenerationResult meshGenResult = squareGenerator.GenerateScannedSurface(SquareParameters, Rows, Columns);

            squareMesh.Clear(keepVertexLayout: false);

            squareMesh.vertices = meshGenResult.Vertices;
            squareMesh.uv = meshGenResult.UVs;
            squareMesh.triangles = meshGenResult.Triangles;

            squareMesh.RecalculateNormals();
            squareMesh.RecalculateBounds();
            squareMesh.RecalculateTangents();
        }

        [UserAction]
        public void Clear()
        {
            Rows.Clear();
            Columns.Clear();
        }

        [UserAction]
        public void SpawnLeft() => AddLine(ScanLineSpawnPosition.Left);

        [UserAction]
        public void SpawnRight() => AddLine(ScanLineSpawnPosition.Right);

        [UserAction]
        public void SpawnTop() => AddLine(ScanLineSpawnPosition.Top);

        [UserAction]
        public void SpawnBottom() => AddLine(ScanLineSpawnPosition.Bottom);

        [UserAction]
        public void SpawnTopLeft() => AddLine(ScanLineSpawnPosition.TopLeft);

        [UserAction]
        public void SpawnTopRight() => AddLine(ScanLineSpawnPosition.TopRight);

        [UserAction]
        public void SpawnBottomLeft() => AddLine(ScanLineSpawnPosition.BottomLeft);

        [UserAction]
        public void SpawnBottomRight() => AddLine(ScanLineSpawnPosition.BottomRight);

        [UserAction]
        public void SpawnLeftRight() => AddLine(ScanLineSpawnPosition.LeftRight);

        [UserAction]
        public void SpawnTopBottom() => AddLine(ScanLineSpawnPosition.TopBottom);

        [UserAction]
        public void SpawnAll() => AddLine(ScanLineSpawnPosition.AllSides);



        private void AddLine(ScanLineSpawnPosition position)
        {
            if (position.HasFlag(ScanLineSpawnPosition.Left))
                Columns.Add(new ScanImageColumnData(ScanLineHorizontalDirection.Right, 0.0f, ScannedImageSettings.ColumnsThickness));

            if (position.HasFlag(ScanLineSpawnPosition.Right))
                Columns.Add(new ScanImageColumnData(ScanLineHorizontalDirection.Left, SquareParameters.Width, ScannedImageSettings.ColumnsThickness));

            if (position.HasFlag(ScanLineSpawnPosition.Top))
                Rows.Add(new ScanImageRowData(ScanLineVerticalDirection.Down, SquareParameters.Height, ScannedImageSettings.RowsThickness));

            if (position.HasFlag(ScanLineSpawnPosition.Bottom))
                Rows.Add(new ScanImageRowData(ScanLineVerticalDirection.Up, 0.0f, ScannedImageSettings.RowsThickness));
        }

        [Flags]
        public enum ScanLineSpawnPosition
        {
            None = 0,
            Left = 0x0001,
            Right = 0x0002,
            Top = 0x0004,
            Bottom = 0x0008,

            TopLeft = Top | Left,
            TopRight = Top | Right,
            BottomLeft = Bottom | Left,
            BottomRight = Bottom | Right,

            LeftRight = Left | Right,
            TopBottom = Top | Bottom,

            AllSides = Left | Right | Top | Bottom
        }
    }
}