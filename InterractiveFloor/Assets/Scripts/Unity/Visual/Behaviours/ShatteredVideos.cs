﻿using InterractiveFloor.Unity.MeshGen;
using InterractiveFloor.Unity.NDI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    [RequireService(typeof(IFragmentGenerator))]
    public class ShatteredVideos : SceneComponent
    {
        public ShatteredVideosMeshSettings MeshSettings { get; set; }

        public ShatteredVideosMovementSettings MovementSettings { get; set; }

        [NDISource]
        public string NDISource1 { get; set; }

        [NDISource]
        public string NDISource2 { get; set; }

        public bool[] MaterialToggles { get; set; }

        private IFragmentGenerator fragmentGenerator;

        private GameObject fragmentsRoot;

        private VideoFragment[] fragments;
        private Mesh[] fragmentMeshes;
        private Vector3[] directions;

        private Material[] materials;

        private NDIMaterial ndiMaterial1;
        private NDIMaterial ndiMaterial2;

        private VideoFragmentSettings fragmentSettings;

        [UserAction]
        public void Explode() => FragmentsAction(frag => frag.Explode());

        [UserAction]
        public void Gather() => FragmentsAction(frag => frag.Gather());

        [UserAction]
        public void All1() => MaterialToggles.Set(true);

        [UserAction]
        public void All2() => MaterialToggles.Set(false);

        [UserAction]
        public void Alternate() => MaterialToggles.Set(i => i % 2 == 0);

        [UserAction]
        public void Invert() => MaterialToggles.Invert();

        [UserAction]
        public void Randomize() => MaterialToggles.Randomize();

        [UserAction]
        public void ShiftLeft() => MaterialToggles.ShiftLeft();

        [UserAction]
        public void ShiftRight() => MaterialToggles.ShiftRight();

        private void FragmentsAction(Action<VideoFragment, int> action)
        {
            for (int i = 0; i < fragments.Length; i++)
                action.Invoke(fragments[i], i);
        }

        private void FragmentsAction(Action<VideoFragment> action) => FragmentsAction((frag, _) => action(frag));

        protected override void Bootstrap(ISpyConfig spy)
        {
            fragmentGenerator = Resolve<IFragmentGenerator>();

            spy.When(MeshSettings)
                .HasChangesOn(ms => ms.Fragments)
                .Do(SetupFragments);

            spy.When(MeshSettings)
                .HasChanged()
                .Do(
                    RegenerateMeshFragments,
                    UpdateDirections
                );

            spy.When(MeshSettings)
                .HasChangesOn(
                    ms => ms.Width,
                    ms => ms.Height)
                .Do(SetupCamera);

            spy.When(this)
                .HasChangesOn(me => me.NDISource1)
                .Do(SetupSource1);

            spy.When(this)
                .HasChangesOn(me => me.NDISource2)
                .Do(SetupSource2);

            spy.WhenAny(() => MaterialToggles)
                .HasChanged()
                .Do(UpdateMaterials);

            fragmentSettings = VideoFragmentSettings.CreateDefault($"DefaultSettings - {name}");

            spy.Bind(fragmentSettings,
                    fs => fs.SmoothTime,
                    fs => fs.AutoChangeDistance,
                    fs => fs.ExplodingDistanceMean,
                    fs => fs.ExplodingDistanceSigma,
                    fs => fs.AutoChangeVelocity,
                    fs => fs.InitialVelocityMean,
                    fs => fs.InitialVelocitySigma)
                .To(MovementSettings,
                    ms => ms.FragmentSmoothTime,
                    ms => ms.AutoChangeDistance,
                    ms => ms.DistanceMean,
                    ms => ms.DistanceSigma,
                    ms => ms.AutoChangeVelocity,
                    ms => ms.InitialVelocityMean,
                    ms => ms.InitialVelocitySigma);

            materials = new Material[]
            {
                Materials.FromShader(Shaders.Unlit.Texture),
                Materials.FromShader(Shaders.Unlit.Texture)
            };

            GameObject ndiMaterial1Obj = GameObjects.CreateNew()
                .Named("NDIMaterial1")
                .AsChildOf(this)
                .WithComponent<NDIMaterial>(ndiMat => ndiMat
                    .TargetingMaterial(materials[0]));
            ndiMaterial1 = ndiMaterial1Obj.GetComponent<NDIMaterial>();

            GameObject ndiMaterial2Obj = GameObjects.CreateNew()
                .Named("NDIMaterial2")
                .AsChildOf(this)
                .WithComponent<NDIMaterial>(ndiMat => ndiMat
                    .TargetingMaterial(materials[1]));
            ndiMaterial2 = ndiMaterial2Obj.GetComponent<NDIMaterial>();

            fragmentsRoot = GameObjects.CreateNew()
                .Named("FragmentsRoot")
                .AsChildOf(this);
        }

        protected override void OnDisposed()
        {
            CleanupMeshes();
            CleanUpMaterials();

            fragmentSettings.Destroy();
        }

        private void SetupSource1() => ndiMaterial1.SourceName = NDISource1;
        private void SetupSource2() => ndiMaterial2.SourceName = NDISource2;

        private void SetupFragments()
        {
            fragmentsRoot.ClearChildren();
            CleanupMeshes();

            int fragCount = MeshSettings.Fragments;

            fragments = new VideoFragment[fragCount];
            fragmentMeshes = new Mesh[fragCount];
            directions = new Vector3[fragCount];

            MaterialToggles = new bool[fragCount];

            for (int i = 0; i < fragCount; i++)
            {
                Mesh fragMesh = Meshes.CreateNew()
                    .Named($"Fragment-{i}");

                fragmentMeshes[i] = fragMesh;

                GameObject fragmentObj = GameObjects.CreateNew()
                    .Named($"Fragment-{i}")
                    .AsChildOf(fragmentsRoot)
                    .WithComponents<MeshRenderer, MeshFilter>()
                    .WithComponent<VideoFragment>(vf => vf
                        .SharingMesh(fragMesh)
                        .SharingSettings(fragmentSettings));

                VideoFragment fragment = fragmentObj.GetComponent<VideoFragment>();
                fragments[i] = fragment;
            }
        }

        private void SetupCamera()
        {
            Camera sceneCamera = GetComponentInScene<Camera>();

            sceneCamera.transform.localRotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);

            float fov = sceneCamera.fieldOfView;

            float y = MeshSettings.Width / (2.0f * Mathf.Tan(Mathf.Deg2Rad * fov));
            sceneCamera.farClipPlane = 2 * y;

            sceneCamera.transform.localPosition = new Vector3(0.0f, y, 0.0f);
        }

        private void RegenerateMeshFragments() => fragmentGenerator.GenerateFragments(MeshSettings, fragmentMeshes, directions);

        private void UpdateDirections() => FragmentsAction((frag, i) => frag.Direction = directions[i]);

        private void UpdateMaterials() => FragmentsAction((frag, i) => frag.SharedMaterial = materials[MaterialToggles[i] ? 0 : 1]);

        private void CleanupMeshes()
        {
            if (fragmentMeshes == null) return;

            foreach (Mesh mesh in fragmentMeshes)
                mesh.Destroy();
            
            fragmentMeshes = null;
        }

        private void CleanUpMaterials()
        {
            foreach (Material mat in materials)
                mat.Destroy();
        }
    }
}