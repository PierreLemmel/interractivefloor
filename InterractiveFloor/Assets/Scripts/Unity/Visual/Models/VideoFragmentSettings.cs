﻿namespace InterractiveFloor.Unity
{
    public class VideoFragmentSettings : Settings<VideoFragmentSettings>
    {
        public float SmoothTime { get; set; } = 2.0f;

        public bool AutoChangeDistance { get; set; } = true;

        [Positive]
        public float ExplodingDistanceMean { get; set; } = 200.0f;
        [Positive]
        public float ExplodingDistanceSigma { get; set; } = 20.0f;


        public bool AutoChangeVelocity { get; set; } = true;

        [Positive]
        public float InitialVelocityMean { get; set; } = 100.0f;
        [Positive]
        public float InitialVelocitySigma { get; set; } = 10.0f;
    }
}