﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class GameOfLifeDisplaySettings : Model
    {
        [GreaterThan(0.1f)]
        public float Step { get; set; } = 0.5f;

        [Positive]
        public float CellWidth { get; set; } = 1.0f;

        [Positive]
        public float CellSpacing { get; set; } = 0.1f;

        public Material AliveMaterial { get; set; }
        public Material DeadMaterial { get; set; }
    }
}