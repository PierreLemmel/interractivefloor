﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class GameOfLifeCellDisplaySettings : Settings<GameOfLifeCellDisplaySettings>
    {
        public Material AliveMaterial { get; set; }
        public Material DeadMaterial { get; set; }
    }
}