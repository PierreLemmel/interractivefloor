﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class PsycheTunnelSettings : Model
    {
        public Material ShapesMaterial { get; set; }

        public int InitialItems { get; set; } = 200;
        public float MaxDistance { get; set; } = 1000.0f;

        [InRange(-360.0f, 360.0f)]
        public float ShapesRotationSpeed { get; set; } = 60.0f;
        [Positive]
        public float ShapesTranslationSpeed { get; set; } = 3.0f;
        [InRange(0.2f, 3.0f)]
        public float SpawnInterval { get; set; } = 1.0f;
    }
}