﻿namespace InterractiveFloor.Unity
{
    public enum ScanLineVerticalDirection
    {
        Up,
        Down
    }
}