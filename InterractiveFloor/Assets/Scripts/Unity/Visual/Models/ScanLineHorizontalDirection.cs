﻿namespace InterractiveFloor.Unity
{
    public enum ScanLineHorizontalDirection
    {
        Left,
        Right
    }
}