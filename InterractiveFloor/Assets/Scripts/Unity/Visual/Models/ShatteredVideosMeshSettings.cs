﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class ShatteredVideosMeshSettings : Model
    {
        [Positive]
        public float Width { get; set; } = 1920.0f;

        [Positive]
        public float Height { get; set; } = 1080.0f;

        [Positive]
        public float Thickness { get; set; } = 80.0f;

        [LowerThan(nameof(Width))]
        public float CenterX { get; set; } = 960.0f;

        [LowerThan(nameof(Height))]
        public float CenterY { get; set; } = 540.0f;

        public float AngleOffset { get; set; }

        [GreaterThan(4)]
        public int Fragments { get; set; } = 12;

        [Positive]
        public int PointsPerEdge { get; set; }

        [InRange(0.0f, 100.0f)]
        public float AngleStandardDeviation { get; set; } = 20.0f;
    } 
}
