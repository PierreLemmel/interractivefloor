﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class ScannedImageSettings : Model
    {
        public Material Material { get; set; }

        [Positive]
        public float ColumnsThickness { get; set; } = 0.8f;

        [Positive]
        public float RowsThickness { get; set; } = 0.8f;

        [Positive]
        public float ColumnSpeed { get; set; } = 15.0f;

        [Positive]
        public float RowSpeed { get; set; } = 15.0f;
    }
}