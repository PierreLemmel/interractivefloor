﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class ShatteredVideosMovementSettings : Model
    {
        public bool AutoChangeDistance { get; set; } = true;

        [Positive]
        public float DistanceMean { get; set; } = 100.0f;
        
        [Positive]
        public float DistanceSigma { get; set; } = 10.0f;


        [Positive]
        public float FragmentSmoothTime { get; set; } = 1.0f;


        public bool AutoChangeVelocity { get; set; } = true;

        [Positive]
        public float InitialVelocityMean { get; set; } = 100.0f;

        [Positive]
        public float InitialVelocitySigma { get; set; } = 10.0f;
    } 
}