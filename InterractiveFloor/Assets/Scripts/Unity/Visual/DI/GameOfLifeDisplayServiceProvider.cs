﻿using InterractiveFloor.Unity.Maths;
using InterractiveFloor.Unity.MeshGen;

namespace InterractiveFloor.Unity
{
    public class GameOfLifeDisplayServiceProvider : SceneComponent,
        IServiceProvider<IGameOfLifeAutomaton>,
        IServiceProvider<IGameOfLifeGridGenerator>
    {
        IGameOfLifeAutomaton IServiceProvider<IGameOfLifeAutomaton>.GetInstance() => new GameOfLifeAutomaton();
        IGameOfLifeGridGenerator IServiceProvider<IGameOfLifeGridGenerator>.GetInstance() => new GameOfLifeGridGenerator();
    }
}