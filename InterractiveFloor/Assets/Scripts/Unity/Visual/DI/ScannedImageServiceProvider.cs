﻿using InterractiveFloor.Unity.MeshGen;

namespace InterractiveFloor.Unity
{
    public class ScannedImageServiceProvider : SceneComponent, IServiceProvider<IScannedSquareGenerator>
    {
        IScannedSquareGenerator IServiceProvider<IScannedSquareGenerator>.GetInstance() => new ScannedSquareGenerator();
    }
}