﻿using InterractiveFloor.Unity.MeshGen;

namespace InterractiveFloor.Unity
{
    public class ShatteredVideoServiceProvider : SceneComponent, IServiceProvider<IFragmentGenerator>
    {
        IFragmentGenerator IServiceProvider<IFragmentGenerator>.GetInstance() => new FragmentGenerator();
    }
}