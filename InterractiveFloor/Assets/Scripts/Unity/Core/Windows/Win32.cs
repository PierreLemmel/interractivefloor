﻿using System;
using System.Runtime.InteropServices;

namespace InterractiveFloor.Unity.Windows
{
    public static class Win32
    {
        private const string User32Dll = "user32.dll";

        [DllImport(User32Dll)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport(User32Dll)]
        public static extern int SendMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);

        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_CLOSE = 0xF060;
    }
}