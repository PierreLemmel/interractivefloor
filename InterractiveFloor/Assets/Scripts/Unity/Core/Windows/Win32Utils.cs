﻿using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace InterractiveFloor.Unity.Windows
{
    public static class Win32Utils
    {
        public static void RunCleanupOopsWindowsTask()
        {
            const int MaxNbOfAttempts = 20;
            int found = 0;

            Task.Run(() =>
            {
                Debug.Log("Cleanup 'oops' windows");
                for (int i = 0; i < MaxNbOfAttempts && found < 2; i++)
                {
                    Thread.Sleep(100);
                    IntPtr lHwnd = Win32.FindWindow(null, "Oops");
                    if (lHwnd != IntPtr.Zero)
                    {
                        Win32.SendMessage(lHwnd, Win32.WM_SYSCOMMAND, Win32.SC_CLOSE, 0);
                        Debug.Log("Found and closed 'oops' window");
                        found++;
                    }
                }
            });
        }
    }
}
