﻿namespace InterractiveFloor.Unity
{
    public interface ISettingsOwner<TSettings> where TSettings : Settings<TSettings>
    {
        TSettings Settings { get; set; }
    }
}