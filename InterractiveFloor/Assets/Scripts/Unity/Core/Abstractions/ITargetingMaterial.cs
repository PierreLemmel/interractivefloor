﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public interface ITargetingMaterial
    {
        Material TargetMaterial { get; set; }
    }
}
