﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public interface ISharingMesh
    {
        Mesh SharedMesh { get; set; }
    }
}