﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public interface ISharingMaterial
    {
        Material SharedMaterial { get; set; }
    }
}