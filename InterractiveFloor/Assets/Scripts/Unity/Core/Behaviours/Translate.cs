﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class Translate : SceneComponent, ISettingsOwner<TranslateSettings>
    {
        public TranslateSettings Settings { get; set; }


        public Vector3 Direction
        {
            get => Settings.Direction;
            set => Settings.Direction = value;
        }

        public float Speed
        {
            get => Settings.Speed;
            set => Settings.Speed = value;
        }

        public Space Space
        {
            get => Settings.Space;
            set => Settings.Space = value;
        }

        protected override void Tick() => transform.Translate(Speed * Time.deltaTime * Direction.normalized, Space);
    }
}
