﻿using InterractiveFloor.Unity;
using UnityEngine;

namespace InterractiveFloor
{
    public class Spawner : SceneComponent, ISettingsOwner<SpawnerSettings>
    {
        public SpawnerSettings Settings { get; set; }

        public GameObject ObjectToSpawn
        {
            get => Settings.ObjectToSpawn;
            set => Settings.ObjectToSpawn = value;
        }

        public float Interval
        {
            get => Settings.Interval;
            set => Settings.Interval = value;
        }

        private int index = 0;
        private float lastSpawntime;

        public void ResetTimer()
        {
            lastSpawntime = Time.time;
        }

        public void ForceSpawning() => SpawnItem();
        public void ForceSpawning(int count)
        {
            for (int i = 0; i < count; i++)
                CreateItem();
            lastSpawntime = Time.time;
        }

        protected override void Bootstrap(ISpyConfig spy)
        {
            lastSpawntime = Time.time;
        }

        protected override void Tick()
        {
            Guard.NotNull(ObjectToSpawn, nameof(ObjectToSpawn));

            if (Time.time - lastSpawntime > Interval)
            {
                SpawnItem();
            }
        }

        private void SpawnItem()
        {
            CreateItem();
            lastSpawntime = Time.time;
        }

        private void CreateItem()
        {
            GameObject child = GameObjects
                            .FromPrefab(ObjectToSpawn)
                            .AsChildOf(this);

            child.name += $" - {index++:000}";
        }
    }
}