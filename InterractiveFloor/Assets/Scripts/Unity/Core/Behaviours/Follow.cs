﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class Follow : SceneComponent
    {
        public GameObject Target { get; set; }

        protected override void Tick()
        {
            if (Target is null) return;

            transform.position = Target.transform.position;
            transform.rotation = Target.transform.rotation;
        }
    }
}
