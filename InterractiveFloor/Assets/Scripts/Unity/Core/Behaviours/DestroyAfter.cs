﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class DestroyAfter : SceneComponent, ISettingsOwner<DestroyAfterSettings>
    {
        public DestroyAfterSettings Settings { get; set; }

        public float LifeTime
        {
            get => Settings.LifeTime;
            set => Settings.LifeTime = value;
        }


        private float creationTime;

        protected override void Bootstrap(ISpyConfig spy)
        {
            creationTime = Time.time;
        }

        protected override void Tick()
        {
            if (Time.time - creationTime >= LifeTime)
            {
                Destroy(gameObject);
            }
        }
    }
}