﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class Rotate : SceneComponent, ISettingsOwner<RotateSettings>
    {
        public RotateSettings Settings { get; set; }

        public Vector3 Axis
        {
            get => Settings.Axis;
            set => Settings.Axis = value;
        }

        public float Speed
        {
            get => Settings.Speed;
            set => Settings.Speed = value;
        }

        public Space Space
        {
            get => Settings.Space;
            set => Settings.Space = value;
        }

        protected override void Tick() => transform.Rotate(Axis, Speed * Time.deltaTime, Space);
    }
}