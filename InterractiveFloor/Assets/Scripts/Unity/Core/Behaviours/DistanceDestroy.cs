﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class DistanceDestroy : SceneComponent, ISettingsOwner<DistanceDestroySettings>
    {
        public DistanceDestroySettings Settings { get; set; }

        public GameObject Target
        {
            get => Settings.Target;
            set => Settings.Target = value;
        }

        public float MaxDistance
        {
            get => Settings.Distance;
            set => Settings.Distance = value;
        }

        protected override void Tick()
        {
            float distance = (Target.transform.position - transform.position).magnitude;

            if (distance > MaxDistance)
                Destroy(gameObject);
        }
    }
}