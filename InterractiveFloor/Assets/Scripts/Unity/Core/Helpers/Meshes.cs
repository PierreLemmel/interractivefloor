﻿using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace InterractiveFloor.Unity.MeshGen
{
    public static class Meshes
    {
        public static MeshBuilder CreateNew() => new MeshBuilder();

        public static Mesh Quad(float width, float height)
        {
            Mesh mesh = new Mesh();

            Quad(width, height, mesh);

            return mesh;
        }

        public static void Quad(float width, float height, Mesh inMesh)
        {
            inMesh.Clear(keepVertexLayout: false);

            float halfWidth = width / 2.0f;
            float halfHeight = height / 2.0f;

            Vector3[] vertices = new Vector3[4];
            Vector2[] uvs = new Vector2[4];
            int[] triangles = new int[6];

            vertices[0] = new Vector3(-halfWidth, 0.0f, -halfHeight);
            vertices[1] = new Vector3(-halfWidth, 0.0f, halfHeight);
            vertices[2] = new Vector3(halfWidth, 0.0f, halfHeight);
            vertices[3] = new Vector3(halfWidth, 0.0f, -halfHeight);

            uvs[0] = new Vector2(0.0f, 0.0f);
            uvs[1] = new Vector2(0.0f, 1.0f);
            uvs[2] = new Vector2(1.0f, 1.0f);
            uvs[3] = new Vector2(1.0f, 0.0f);

            triangles[0] = 0;
            triangles[1] = 1;
            triangles[2] = 2;

            triangles[3] = 0;
            triangles[4] = 2;
            triangles[5] = 3;

            inMesh.vertices = vertices;
            inMesh.uv = uvs;
            inMesh.triangles = triangles;
        }

        /// <summary>
        /// Safely clears data mesh. Built-in <c>Mesh.Clear()</c> resets submesh count and index format and can lead to confusing errors.
        /// </summary>
        public static void ClearVertices(this Mesh mesh)
        {
            IndexFormat format = mesh.indexFormat;
            int submeshes = mesh.subMeshCount;

            mesh.Clear(keepVertexLayout: false);

            mesh.subMeshCount = submeshes;
            mesh.indexFormat = format;
        }

        public static Vector3 CalculateCenter(this Mesh mesh)
        {
            Vector3 total = Vector3.zero;

            Vector3[] vertices = mesh.vertices;
            foreach (Vector3 vertex in vertices)
            {
                total += vertex;
            }

            Vector3 center = total / vertices.Length;

            return center;
        }
    }
}