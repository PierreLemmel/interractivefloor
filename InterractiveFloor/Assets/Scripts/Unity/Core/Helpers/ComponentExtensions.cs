﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class ComponentExtensions
    {
        public static void SetAsChildOf(this Component component, Component other)
        {
            if (component is null)
                throw new ArgumentNullException(nameof(component));

            component.transform.SetParent(other.transform);
        }

        public static void SetAsChildOf(this Component component, GameObject other)
        {
            if (component is null)
                throw new ArgumentNullException(nameof(component));

            component.transform.SetParent(other.transform);
        }

        public static GameObject AddChild(this Component component, string name)
        {
            if (component is null)
                throw new ArgumentNullException(nameof(component));

            GameObject newGo = new GameObject(name);
            newGo.transform.SetParent(component.transform);

            return newGo;
        }

        public static void ResetLocalPosition(this Component component) => component.transform.localPosition = Vector3.zero;
        public static void ResetPosition(this Component component) => component.transform.position = Vector3.zero;

        public static void ResetLocalRotation(this Component component) => component.transform.localRotation = Quaternion.identity;
        public static void ResetRotation(this Component component) => component.transform.rotation = Quaternion.identity;

        public static void ResetScale(this Component component) => component.transform.localScale = Vector3.one;
    }
}