﻿namespace InterractiveFloor.Unity
{
    public static class Shaders
    {
        public static class Unlit
        {
            public static string Color = "Unlit/Color";
            public static string Texture = "Unlit/Texture";
        }
    }
}