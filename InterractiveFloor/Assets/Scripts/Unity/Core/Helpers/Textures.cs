﻿using System;
using UnityEngine;

namespace InterractiveFloor
{
    public static class Textures
    {
        public static bool CheckDimensions(this Texture texture, int width, int height) => texture.width == width && texture.height == height;
    }
}