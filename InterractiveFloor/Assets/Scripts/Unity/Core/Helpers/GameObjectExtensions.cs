﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using UObject = UnityEngine.Object;

namespace InterractiveFloor.Unity
{
    public static class GameObjectExtensions
    {
        public static void ClearChildren(this GameObject go)
        {
            Transform transform = go.transform;
            for (int i = transform.childCount - 1; i >= 0; i--)
                DestroyWithChildren(transform.GetChild(i).gameObject);
        }

        public static void DestroyWithChildren(this GameObject go)
        {
            Transform transform = go.transform;
            for (int i = transform.childCount - 1; i >= 0; i--)
                DestroyWithChildren(transform.GetChild(i).gameObject);

            UObject.DestroyImmediate(go);
        }

        public static void SetAsChildOf(this GameObject go, Component other)
        {
            Guard.NotNull(go, nameof(go));

            go.transform.SetParent(other.transform);
        }

        public static void SetAsChildOf(this GameObject go, GameObject other)
        {
            Guard.NotNull(go, nameof(go));

            go.transform.SetParent(other.transform);
        }

        public static GameObject AddChild(this GameObject go, string name)
        {
            Guard.NotNull(go, nameof(go));

            GameObject newGo = new GameObject(name);
            newGo.SetAsChildOf(go);

            return newGo;
        }

        public static bool HasComponent<TComponent>(this GameObject go) where TComponent : Component
        {
            TComponent component = go.GetComponent<TComponent>();
            return component != null;
        }

        public static bool HasComponentInChildren<TComponent>(this GameObject go) where TComponent : Component
        {
            TComponent component = go.GetComponentInChildren<TComponent>();
            return component != null;
        }

        public static IEnumerable<GameObject> GetChildren(this GameObject go)
        {
            Guard.NotNull(go, nameof(go));

            Transform transform = go.transform;
            return Enumerable.Range(0, transform.childCount)
                .Select(i => transform.GetChild(i).gameObject);
        }

        public static bool HasChildren(this GameObject go) => go.transform.childCount > 0;

        public static GameObject GetChild(this GameObject go) => go.GetChildren().Single();
    }
}