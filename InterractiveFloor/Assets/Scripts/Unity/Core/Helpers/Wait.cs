﻿using System;
using System.Collections;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class Wait
    {
        public static IEnumerator TillNextFrame
        {
            get
            {
                yield return null;
            }
        }

        public static IEnumerator ForFrames(int frames)
        {
            for (int i = 0; i < frames; i++)
                yield return null;
        }

        public static YieldInstruction ForSeconds(float seconds) => new WaitForSeconds(seconds);
        public static YieldInstruction ForMilliseconds(int milliseconds) => new WaitForSeconds(milliseconds / 1000.0f);

        /// <summary>
        /// Awaits that an operation is done and that its <c>completed</c> event has been called.
        /// </summary>
        public static IEnumerator ForAsyncOperation(AsyncOperation asyncOp)
        {
            if (asyncOp.isDone)
                throw new InvalidOperationException("Can't wait for an ulready finished async operation");

            bool done = false;
            asyncOp.completed += _ => done = true;

            yield return asyncOp;
            while (!done)
                yield return TillNextFrame;
        }
    }
}