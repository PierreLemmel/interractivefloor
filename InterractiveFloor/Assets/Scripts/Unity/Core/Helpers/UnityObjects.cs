﻿using System;
using UnityEngine;

using UObject = UnityEngine.Object;

namespace InterractiveFloor
{
    public static class UnityObjects
    {
        public static void Destroy(this UObject uobj) => UObject.Destroy(uobj);
    }
}