﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class GameObjects
    {
        public static GameObjectBuilder CreateNew() => new GameObjectBuilder();
        public static GameObjectBuilder CreatePrefab() => new GameObjectBuilder(activate: false);
        public static FromPrefabBuilder FromPrefab(GameObject prefab) => new FromPrefabBuilder(prefab);
    }
}