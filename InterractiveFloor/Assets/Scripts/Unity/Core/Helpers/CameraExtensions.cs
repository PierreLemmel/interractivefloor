﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class CameraExtensions
    {
        public static void LookAt(this Camera camera, Transform target) => camera.transform.LookAt(target);
        public static void LookAt(this Camera camera, GameObject target) => camera.transform.LookAt(target.transform);
        public static void LookAt<TComponent>(this Camera camera, TComponent target) where TComponent : Component => camera.transform.LookAt(target.transform);
    }
}