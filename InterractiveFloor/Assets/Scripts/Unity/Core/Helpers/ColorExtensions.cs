﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class ColorExtensions
    {
        public static bool HasTransparency(this Color32 color) => color.a != 0xff;
        public static bool HasTransparency(this Color color) => HasTransparency((Color32)color);

        public static string ToHexString(this Color32 color)
        {
            if (color.HasTransparency())
                return $"#{color.r:X2}{color.g:X2}{color.b:X2}{color.a:X2}";
            else
                return $"#{color.r:X2}{color.g:X2}{color.b:X2}";
        }
        public static string ToHexString(this Color color) => ToHexString((Color32)color);
    }
}