﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class Materials
    {
        public static Material FromShader(string shader) => new Material(Shader.Find(shader));
    }
}