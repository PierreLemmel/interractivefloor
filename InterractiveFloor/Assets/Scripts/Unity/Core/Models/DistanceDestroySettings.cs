﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class DistanceDestroySettings : Settings<DistanceDestroySettings>
    {
        public GameObject Target { get; set; }
        public float Distance { get; set; }
    }
}