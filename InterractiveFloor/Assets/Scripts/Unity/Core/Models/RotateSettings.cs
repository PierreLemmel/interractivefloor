﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class RotateSettings : Settings<RotateSettings>
    {
        public Vector3 Axis { get; set; } = Vector3.up;
        public float Speed { get; set; }
        public Space Space { get; set; } = Space.Self;
    }
}