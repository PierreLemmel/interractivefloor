﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class SpawnerSettings : Settings<SpawnerSettings>
    {
        public GameObject ObjectToSpawn { get; set; }
        public float Interval { get; set; } = 1.0f;
    }
}
