﻿namespace InterractiveFloor.Unity
{
    public enum StartStopState
    {
        Stopped = 0,
        Started = 1,
    }
}