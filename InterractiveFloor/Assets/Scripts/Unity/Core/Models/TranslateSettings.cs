﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class TranslateSettings : Settings<TranslateSettings>
    {
        public Vector3 Direction { get; set; } = Vector3.forward;
        public float Speed { get; set; }
        public Space Space { get; set; } = Space.Self;
    }
}