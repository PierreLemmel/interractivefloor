﻿using System;

namespace InterractiveFloor.Unity
{
    public class DestroyAfterSettings : Settings<DestroyAfterSettings>
    {
        public float LifeTime { get; set; }
    }
}