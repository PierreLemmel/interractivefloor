﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public abstract class Settings<TSettings> : ScriptableObject where TSettings : Settings<TSettings>
    {
        public static TSettings CreateDefault() => CreateInstance<TSettings>();

        public static TSettings CreateDefault(string name)
        {
            TSettings settings = CreateDefault();
            settings.name = name;
            return settings;
        }
    }
}