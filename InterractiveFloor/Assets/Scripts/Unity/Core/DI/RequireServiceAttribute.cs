﻿using System;

namespace InterractiveFloor
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public sealed class RequireServiceAttribute : Attribute
    {
        public Type Service { get; }

        public RequireServiceAttribute(Type service)
        {
            Service = service ?? throw new ArgumentNullException(nameof(service));
        }
    }
}