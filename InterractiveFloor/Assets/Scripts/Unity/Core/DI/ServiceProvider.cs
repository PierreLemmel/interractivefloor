﻿namespace InterractiveFloor.Unity
{
    public abstract class ServiceProvider<TService> : SceneComponent, IServiceProvider<TService>
    {
        public abstract TService GetInstance();
    }
}