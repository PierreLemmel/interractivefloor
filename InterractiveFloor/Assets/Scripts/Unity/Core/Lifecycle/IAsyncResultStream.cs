﻿namespace InterractiveFloor.Unity
{
    public interface IAsyncResultStream<TResult> where TResult : Result
    {
        bool TryGetNextResult(out TResult result);
    }
}