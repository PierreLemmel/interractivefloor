﻿namespace InterractiveFloor.Unity
{
    public interface IStopable
    {
        void Stop();
    }
}