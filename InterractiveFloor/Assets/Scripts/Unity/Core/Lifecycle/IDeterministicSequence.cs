﻿namespace InterractiveFloor.Unity
{
    public interface IDeterministicSequence<TValue>
    {
        TValue CurrentValue { get; }

        bool MoveNext();
    }
}