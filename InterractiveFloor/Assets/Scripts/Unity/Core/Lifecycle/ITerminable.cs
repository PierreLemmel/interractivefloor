﻿namespace InterractiveFloor.Unity
{
    public interface ITerminable
    {
        void Terminate();
    }
}