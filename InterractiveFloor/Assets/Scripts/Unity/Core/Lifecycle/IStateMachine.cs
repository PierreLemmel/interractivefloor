﻿namespace InterractiveFloor.Unity
{
    public interface IStateMachine<TState>
    {
        TState State { get; }
    }
}