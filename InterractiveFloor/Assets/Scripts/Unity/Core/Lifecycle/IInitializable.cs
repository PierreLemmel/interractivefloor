﻿namespace InterractiveFloor.Unity
{
    public interface IInitializable
    {
        void Initialize();
    }

    public interface IInitializable<TSettings> where TSettings : Model
    {
        void Initialize(TSettings settings);
    }
}
