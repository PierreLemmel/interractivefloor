﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class MeshFilterSetupExtensions
    {
        public static GameObjectBuilder.IComponentSetup<MeshFilter> SharingMesh
            (this GameObjectBuilder.IComponentSetup<MeshFilter> setup, Mesh mesh)
            => setup.Setup(filter => filter.sharedMesh = mesh);
    }
}