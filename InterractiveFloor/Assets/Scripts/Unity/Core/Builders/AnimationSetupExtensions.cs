﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class AnimationSetupExtensions
    {
        public static GameObjectBuilder.IComponentSetup<Animation> WithClip
            (this GameObjectBuilder.IComponentSetup<Animation> setup, AnimationClip clip)
            => setup.Setup(anim => anim.clip = clip);

        public static GameObjectBuilder.IComponentSetup<Animation> AutomaticallyPlaying
            (this GameObjectBuilder.IComponentSetup<Animation> setup)
            => setup.Setup(anim => anim.playAutomatically = true);

        public static GameObjectBuilder.IComponentSetup<Animation> NotAutomaticallyPlaying
            (this GameObjectBuilder.IComponentSetup<Animation> setup)
            => setup.Setup(anim => anim.playAutomatically = false);
    }
}