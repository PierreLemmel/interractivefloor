﻿using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class CameraSetupExtensions
    {
        public static GameObjectBuilder.IComponentSetup<Camera> BasedOn
            (this GameObjectBuilder.IComponentSetup<Camera> setup, Camera other)
            => setup.Setup(camera =>
            {
                Transform transform = camera.transform;

                Vector3 oldPosition = transform.position;
                Quaternion oldRotation = transform.rotation;
                Vector3 oldScale = transform.localScale;

                camera.CopyFrom(other);

                transform.position = oldPosition;
                transform.rotation = oldRotation;
                transform.localScale = oldScale;
            });
    }
}