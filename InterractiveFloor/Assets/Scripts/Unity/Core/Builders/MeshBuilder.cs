﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace InterractiveFloor.Unity.MeshGen
{
    public class MeshBuilder : Builder<Mesh>
    {
        private ICollection<Action<Mesh>> setupActions = new List<Action<Mesh>>();

        private MeshBuilder AddSetupAction(Action<Mesh> setupAction)
        {
            setupActions.Add(setupAction);
            return this;
        }

        public MeshBuilder Named(string name) => AddSetupAction(mesh => mesh.name = name);
        public MeshBuilder WithSubmeshes(int submeshes) => AddSetupAction(mesh =>
        {
            mesh.indexFormat = IndexFormat.UInt32;
            mesh.subMeshCount = submeshes;
        });
        public MeshBuilder UsingInt32() => AddSetupAction(mesh => mesh.indexFormat = IndexFormat.UInt32);

        public override Mesh Build()
        {
            Mesh mesh = new Mesh();

            foreach (Action<Mesh> setupAction in setupActions)
                setupAction.Invoke(mesh);

            return mesh;
        }
    }
}
