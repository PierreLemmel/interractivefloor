﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public sealed class GameObjectBuilder : Builder<GameObject>
    {
        private ICollection<Action<GameObject>> setupActions = new List<Action<GameObject>>();
        private ICollection<Action<GameObject>> postInitActions = new List<Action<GameObject>>();

        private readonly bool activate;

        public GameObjectBuilder(bool activate = true)
        {
            this.activate = activate;
        }

        public GameObjectBuilder AddSetupAction(Action<GameObject> setupAction)
        {
            setupActions.Add(setupAction);
            return this;
        }

        public GameObjectBuilder Named(string name) => AddSetupAction(go => go.name = name);
        public GameObjectBuilder AsChildOf(GameObject parent) => AddSetupAction(go => go.SetAsChildOf(parent));
        public GameObjectBuilder AsChildOf(Component parent) => AddSetupAction(go => go.SetAsChildOf(parent));

        public GameObjectBuilder RotatedBy(Quaternion rotation) => AddSetupAction(go => go.transform.localRotation *= rotation);
        public GameObjectBuilder RotatedBy(float x, float y, float z) => RotatedBy(Quaternion.Euler(x,y, z));

        public GameObjectBuilder TranslatedBy(Vector3 offset) => AddSetupAction(go => go.transform.localPosition += offset);
        public GameObjectBuilder TranslatedBy(float x, float y, float z) => TranslatedBy(new Vector3(x, y, z));

        public GameObjectBuilder WithComponent<TComponent>() where TComponent : Component
            => WithComponent(Actions.Empty<IComponentSetup<TComponent>>());
        public GameObjectBuilder WithComponent<TComponent>(Action<TComponent> componentSetup) where TComponent : Component
            => WithComponent<TComponent>(setup => setup.Setup(componentSetup));
        public GameObjectBuilder WithComponent<TComponent>(Action<IComponentSetup<TComponent>> setupAction) where TComponent : Component
        {
            ComponentSetup<TComponent> setup = new ComponentSetup<TComponent>(this);
            setupAction(setup);

            Action<TComponent> componentSetupAction = setup.SetupAction;
            Action<TComponent> componentPostInitAction = setup.PostInitAction;

            return AddSetupAction(go =>
            {
                TComponent component = go.AddComponent<TComponent>();

                componentSetupAction?.Invoke(component);

                if (componentPostInitAction != null)
                    postInitActions.Add(_ => componentPostInitAction.Invoke(component));
            });
        }

        public GameObjectBuilder WithComponents<TComponent1, TComponent2>()
            where TComponent1 : Component where TComponent2 : Component
            => WithComponent<TComponent1>()
            .WithComponent<TComponent2>();

        public GameObjectBuilder WithComponents<TComponent1, TComponent2, TComponent3>()
            where TComponent1 : Component where TComponent2 : Component where TComponent3 : Component
            => WithComponent<TComponent1>()
            .WithComponent<TComponent2>()
            .WithComponent<TComponent3>();

        public GameObjectBuilder WithComponents<TComponent1, TComponent2, TComponent3, TComponent4>()
            where TComponent1 : Component where TComponent2 : Component where TComponent3 : Component where TComponent4 : Component
            => WithComponent<TComponent1>()
            .WithComponent<TComponent2>()
            .WithComponent<TComponent3>()
            .WithComponent<TComponent4>();

        public GameObjectBuilder WithComponents<TComponent1, TComponent2, TComponent3, TComponent4, TComponent5>()
            where TComponent1 : Component where TComponent2 : Component where TComponent3 : Component where TComponent4 : Component where TComponent5 : Component
            => WithComponent<TComponent1>()
            .WithComponent<TComponent2>()
            .WithComponent<TComponent3>()
            .WithComponent<TComponent4>()
            .WithComponent<TComponent5>();

        public GameObjectBuilder WithComponents<TComponent1, TComponent2, TComponent3, TComponent4, TComponent5, TComponent6>()
            where TComponent1 : Component where TComponent2 : Component where TComponent3 : Component where TComponent4 : Component where TComponent5 : Component where TComponent6 : Component
            => WithComponent<TComponent1>()
            .WithComponent<TComponent2>()
            .WithComponent<TComponent3>()
            .WithComponent<TComponent4>()
            .WithComponent<TComponent5>()
            .WithComponent<TComponent6>();

        public GameObjectBuilder WithComponents<TComponent1, TComponent2, TComponent3, TComponent4, TComponent5, TComponent6, TComponent7>()
            where TComponent1 : Component where TComponent2 : Component where TComponent3 : Component where TComponent4 : Component where TComponent5 : Component where TComponent6 : Component where TComponent7 : Component
            => WithComponent<TComponent1>()
            .WithComponent<TComponent2>()
            .WithComponent<TComponent3>()
            .WithComponent<TComponent4>()
            .WithComponent<TComponent5>()
            .WithComponent<TComponent6>()
            .WithComponent<TComponent7>();
        public GameObjectBuilder WithComponents<TComponent1, TComponent2, TComponent3, TComponent4, TComponent5, TComponent6, TComponent7, TComponent8>()
            where TComponent1 : Component where TComponent2 : Component where TComponent3 : Component where TComponent4 : Component where TComponent5 : Component where TComponent6 : Component where TComponent7 : Component where TComponent8 : Component
            => WithComponent<TComponent1>()
            .WithComponent<TComponent2>()
            .WithComponent<TComponent3>()
            .WithComponent<TComponent4>()
            .WithComponent<TComponent5>()
            .WithComponent<TComponent6>()
            .WithComponent<TComponent7>()
            .WithComponent<TComponent8>();

        public GameObjectBuilder WithChild(Action<GameObjectBuilder> childSetup)
        {
            return AddSetupAction(go =>
            {
                GameObjectBuilder childBuilder = new GameObjectBuilder()
                    .AsChildOf(go);

                childSetup(childBuilder);

                childBuilder.Build();
            });
        }

        public override GameObject Build()
        {
            GameObject result = new GameObject();
            result.SetActive(false);

            foreach (Action<GameObject> setupAction in setupActions)
                setupAction(result);
            
            result.SetActive(activate);

            foreach (Action<GameObject> postInit in postInitActions)
                postInit(result);

            return result;
        }

        private class ComponentSetup<TComponent> :
            IComponentSetup<TComponent>
            where TComponent : Component
        {
            private readonly GameObjectBuilder parentBuilder;

            public Action<TComponent> SetupAction { get; private set; }
            public Action<TComponent> PostInitAction { get; private set; }

            public ComponentSetup(GameObjectBuilder parentBuilder)
            {
                this.parentBuilder = parentBuilder ?? throw new ArgumentNullException(nameof(parentBuilder));
            }

            public IComponentSetup<TComponent> Setup(Action<TComponent> setupAction)
            {
                SetupAction += setupAction;
                return this;
            }

            public IComponentSetup<TComponent> PostInit(Action<TComponent> postInit)
            {
                PostInitAction += postInit;
                return this;
            }
        }

        public interface IComponentSetup<TComponent> where TComponent : Component
        {
            IComponentSetup<TComponent> Setup(Action<TComponent> setupAction);
            IComponentSetup<TComponent> PostInit(Action<TComponent> postInit);
        }
    }
}