﻿using UnityEngine;
using UnityEngine.UI;

namespace InterractiveFloor.Unity
{
    using static InterractiveFloor.Unity.GameObjectBuilder;

    public static class UIComponentSetupExtension
    {
        public static IComponentSetup<TGraphic> FillParent<TGraphic>
            (this IComponentSetup<TGraphic> setup)
            where TGraphic : Graphic
        {
            return setup.PostInit(g =>
            {
                RectTransform rect = g.rectTransform;

                rect.anchorMin = Vector2.zero;
                rect.anchorMax = Vector2.one;

                rect.offsetMin = Vector2.zero;
                rect.offsetMax = Vector2.zero;
            });
        }

        public static IComponentSetup<Canvas> InScreenSpaceOverlay
            (this IComponentSetup<Canvas> setup)
            => setup.Setup(canvas => canvas.renderMode = RenderMode.ScreenSpaceOverlay);

        public static GameObjectBuilder.IComponentSetup<Canvas> InCameraSpace(this IComponentSetup<Canvas> setup, Camera camera)
            => setup.Setup(canvas =>
            {
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;
                canvas.worldCamera = camera;
            });

        public static IComponentSetup<TGraphic> WithColor<TGraphic>
            (this IComponentSetup<TGraphic> setup, Color color)
            where TGraphic : Graphic
            => setup.Setup(canvas => canvas.color = color);
    }
}
