﻿using InterractiveFloor.Unity.NDI;
using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    using TranslateSetup = GameObjectBuilder.IComponentSetup<Translate>;
    using RotateSetup = GameObjectBuilder.IComponentSetup<Rotate>;
    using SpawnerSetup = GameObjectBuilder.IComponentSetup<Spawner>;
    using DestroyAfterSetup = GameObjectBuilder.IComponentSetup<DestroyAfter>;
    using DistanceDestroySetup = GameObjectBuilder.IComponentSetup<DistanceDestroy>;

    public static class MoreSetup
    {
        #region Translate
        public static TranslateSetup InRelativeSpace(this TranslateSetup setup)
            => setup.Setup(translate => translate.Space = Space.Self);

        public static TranslateSetup InWorldSpace(this TranslateSetup setup)
            => setup.Setup(translate => translate.Space = Space.World);

        public static TranslateSetup AlongDirection(this TranslateSetup setup, Vector3 direction)
            => setup.Setup(translate => translate.Direction = direction);

        public static TranslateSetup AtSpeed(this TranslateSetup setup, float speed)
            => setup.Setup(translate => translate.Speed = speed);
        #endregion

        #region Rotate
        public static RotateSetup InRelativeSpace(this RotateSetup setup)
            => setup.Setup(rotate => rotate.Space = Space.Self);

        public static RotateSetup InWorldSpace(this RotateSetup setup)
            => setup.Setup(rotate => rotate.Space = Space.World);

        public static RotateSetup AroundAxis(this RotateSetup setup, Vector3 axis)
            => setup.Setup(rotate => rotate.Axis = axis);

        public static RotateSetup AtSpeed(this RotateSetup setup, float speed)
            => setup.Setup(rotate => rotate.Speed = speed);
        #endregion

        #region Spawner
        public static SpawnerSetup Spawning(this SpawnerSetup setup, GameObject model)
            => setup.Setup(spawner => spawner.ObjectToSpawn = model);

        public static SpawnerSetup AtInterval(this SpawnerSetup setup, float interval)
            => setup.Setup(spawner => spawner.Interval = interval);
        #endregion

        #region DestroyAfter
        public static DestroyAfterSetup LifeTime(this DestroyAfterSetup setup, float lifeTime)
            => setup.Setup(destroyAfter => destroyAfter.LifeTime = lifeTime);
        #endregion

        #region DistanceDestroy
        public static DistanceDestroySetup Targetting(this DistanceDestroySetup setup, GameObject target)
            => setup.Setup(distanceDestroy => distanceDestroy.Target = target);

        public static DistanceDestroySetup MaxDistance(this DistanceDestroySetup setup, float maxDistance)
            => setup.Setup(distanceDestroy => distanceDestroy.MaxDistance = maxDistance);
        #endregion

        #region Settings Owner
        public static GameObjectBuilder.IComponentSetup<TComponent> SharingSettings<TComponent, TSettings>(this GameObjectBuilder.IComponentSetup<TComponent> builder, TSettings sharedSettings)
            where TComponent : SceneComponent, ISettingsOwner<TSettings>
            where TSettings : Settings<TSettings>
            => builder.Setup(comp => comp.Settings = sharedSettings);

        public static GameObjectBuilder.IComponentSetup<TComponent> CreateSettings<TComponent, TSettings>(this GameObjectBuilder.IComponentSetup<TComponent> builder, Func<TSettings> createFunc)
            where TComponent : SceneComponent, ISettingsOwner<TSettings>
            where TSettings : Settings<TSettings>
            => builder.Setup(comp => comp.Settings = createFunc());
        #endregion

        #region ISharingMaterial
        public static GameObjectBuilder.IComponentSetup<TComponent> SharingMaterial<TComponent>(this GameObjectBuilder.IComponentSetup<TComponent> builder, Material material)
            where TComponent : SceneComponent, ISharingMaterial
            => builder.Setup(comp => comp.SharedMaterial = material);
        #endregion

        #region ISharingMesh
        public static GameObjectBuilder.IComponentSetup<TComponent> SharingMesh<TComponent>(this GameObjectBuilder.IComponentSetup<TComponent> builder, Mesh mesh)
            where TComponent : SceneComponent, ISharingMesh
            => builder.Setup(comp => comp.SharedMesh = mesh);
        #endregion

        #region ITargettingMaterial
        public static GameObjectBuilder.IComponentSetup<TComponent> TargetingMaterial<TComponent>(this GameObjectBuilder.IComponentSetup<TComponent> builder, Material target)
            where TComponent : SceneComponent, ITargetingMaterial
            => builder.Setup(tm => tm.TargetMaterial = target);
        #endregion
    }
}