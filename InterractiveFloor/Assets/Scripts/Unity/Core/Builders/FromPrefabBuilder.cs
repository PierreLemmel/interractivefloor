﻿
using UnityEngine;

using UObject = UnityEngine.Object;

namespace InterractiveFloor.Unity
{
    public class FromPrefabBuilder : Builder<GameObject>
    {
        private readonly GameObject original;
        private Transform parentTransform;

        public FromPrefabBuilder(GameObject prefab)
        {
            this.original = prefab;
        }

        public FromPrefabBuilder AsChildOf(GameObject parent)
        {
            parentTransform = parent.transform;
            return this;
        }

        public FromPrefabBuilder AsChildOf(Component component)
        {
            parentTransform = component.transform;
            return this;
        }

        public override GameObject Build()
        {
            GameObject go = UObject.Instantiate(original, parentTransform);
            go.SetActive(true);

            return go;
        }
    }
}