﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterractiveFloor.Unity
{
    using TranslateSetup = GameObjectBuilder.IComponentSetup<Translate>;
    using RotateSetup = GameObjectBuilder.IComponentSetup<Rotate>;
    using DestroyAfterSetup = GameObjectBuilder.IComponentSetup<DestroyAfter>;

    public static class GameObjectBuilderExtensions
    {
        public static GameObjectBuilder Rotating(this GameObjectBuilder builder, Action<Rotate> setup) => builder.WithComponent(setup);
        public static GameObjectBuilder Rotating(this GameObjectBuilder builder, Action<RotateSetup> setup) => builder.WithComponent(setup);
        public static GameObjectBuilder Translating(this GameObjectBuilder builder, Action<Translate> setup) => builder.WithComponent(setup);
        public static GameObjectBuilder Translating(this GameObjectBuilder builder, Action<TranslateSetup> setup) => builder.WithComponent(setup);
        public static GameObjectBuilder DestroyedAfter(this GameObjectBuilder builder, float lifeTime) => builder.WithComponent<DestroyAfter>(after => after.LifeTime(lifeTime));
    }
}