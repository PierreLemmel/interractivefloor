﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public static class MeshRendererSetupExtensions
    {
        public static GameObjectBuilder.IComponentSetup<MeshRenderer> SharingMaterial
            (this GameObjectBuilder.IComponentSetup<MeshRenderer> setup, Material material)
            => setup.Setup(renderer => renderer.sharedMaterial = material);

        public static GameObjectBuilder.IComponentSetup<MeshRenderer> SharingMaterials
            (this GameObjectBuilder.IComponentSetup<MeshRenderer> setup, params Material[] materials)
            => setup.Setup(renderer => renderer.sharedMaterials = materials);
    }
}