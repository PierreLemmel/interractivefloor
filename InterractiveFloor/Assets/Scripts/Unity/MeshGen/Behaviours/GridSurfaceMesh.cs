﻿using InterractiveFloor.Unity.Maths;
using System;
using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    [RequireService(typeof(IMathEngine))]
    [RequireService(typeof(ISurfaceGenerator))]
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class GridSurfaceMesh : SceneComponent
    {
        public GridSurfaceParameters SurfaceParameters { get; set; } = new GridSurfaceParameters();
        public MeshRendererParameters RendererParameters { get; set; } = new MeshRendererParameters();
        public FormulaModel Formula { get; set; } = new FormulaModel();

        public Vector2 Speed { get; set; } = new Vector2(0.0f, 0.0f);

        private Mesh mesh;
        private MeshRenderer meshRenderer;

        private ISurfaceGenerator surfaceGenerator;
        private IMathEngine mathEngine;

        private Vector3[] vertices;
        private Vector2[] uvs;
        private int[] points;

        private Vector2 accumulatedOffset = Vector2.zero;

        private Func<float, float, float, float> surfaceFunction = (x, y, t) => 0.0f;

        protected override void Bootstrap(ISpyConfig spy)
        {
            spy
                .When(SurfaceParameters)
                .HasChangesOn(
                    p => p.NbOfRows,
                    p => p.NbOfCols
                )
                .Do(SetupMesh);

            spy
                .When(RendererParameters)
                .HasChanged()
                .Do(SetupMaterial);

            spy
                .When(Formula)
                .HasChangesOn(p => p.Text)
                .Do(SetupFunction);

            MeshFilter meshFilter = GetComponent<MeshFilter>();
            mesh = meshFilter.mesh;
            meshRenderer = GetComponent<MeshRenderer>();

            surfaceGenerator = new SurfaceGenerator();
            mathEngine = new MathEngine();
        }

        protected override void Tick()
        {
            accumulatedOffset += Time.deltaTime * Speed;

            float width = SurfaceParameters.GridWidth;
            float height = SurfaceParameters.GridHeight;

            float halfWidth = width / 2.0f;
            float halfHeight = height / 2.0f;

            surfaceGenerator.GenerateSurfacePoints(vertices, SurfaceParameters, surfaceFunction, Time.time, accumulatedOffset);
            vertices.MapTo(uvs, vertex => new Vector2((vertex.x + halfWidth) / width, (vertex.z + halfHeight) / height));

            int nbOfCols = SurfaceParameters.NbOfCols;
            int nbOfRows = SurfaceParameters.NbOfRows;

            int pointsPerRow = nbOfCols + 2;
            int pointsPerCol = nbOfRows + 2;

            int quadsPerRow = nbOfCols + 1;
            int quadsPerCol = nbOfRows + 1;

            int k = 0;
            for (int row = 0; row < quadsPerCol; row++)
            {
                for (int col = 0; col < quadsPerRow; col++)
                {
                    points[k++] = row * pointsPerRow + col;
                    points[k++] = (row + 1) * pointsPerRow + col;
                    points[k++] = (row + 1) * pointsPerRow + col + 1;
                    points[k++] = row * pointsPerRow + col + 1;
                }
            }

            UpdateMesh();
        }

        private void SetupMesh()
        {
            mesh.Clear();

            int nbOfCols = SurfaceParameters.NbOfCols;
            int nbOfRows = SurfaceParameters.NbOfRows;

            int pointsPerRow = nbOfCols + 2;
            int pointsPerCol = nbOfRows + 2;

            int quadsPerRow = nbOfCols + 1;
            int quadsPerCol = nbOfRows + 1;

            vertices = new Vector3[pointsPerRow * pointsPerCol];
            uvs = new Vector2[pointsPerRow * pointsPerCol];
            points = new int[4 * quadsPerRow * quadsPerCol];

            UpdateMesh();
        }

        private void SetupMaterial()
        {
            meshRenderer.material = RendererParameters.MeshMaterial;
        }

        private void SetupFunction()
        {
            try
            {
                Func<float, float, float, float> result = mathEngine.Formula(Formula.Text)
                        .Parameter("x")
                        .Parameter("y")
                        .Parameter("t")
                        .Build();

                Formula.ErrorMessage = "";

                surfaceFunction = result;
            }
            catch (Exception ex)
            {
                Formula.ErrorMessage = ex.Message;
            }
        }

        private void UpdateMesh()
        {
            mesh.vertices = vertices;
            mesh.uv = uvs;
            mesh.SetIndices(points, MeshTopology.Quads, 0);
        }
    }
}
