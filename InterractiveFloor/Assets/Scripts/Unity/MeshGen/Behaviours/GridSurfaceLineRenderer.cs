﻿using InterractiveFloor.Unity.Maths;
using System;
using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public class GridSurfaceLineRenderer : SceneComponent
    {
        public GridSurfaceParameters SurfaceParameters { get; set; } = new GridSurfaceParameters();
        public LineRendererParameters LineRendererParameters { get; set; } = new LineRendererParameters();
        public FormulaModel Formula { get; set; } = new FormulaModel();

        public Vector2 Speed { get; set; } = new Vector2(0.0f, 0.0f);

        private ISurfaceGenerator surfaceGenerator;
        private IMathEngine mathEngine;

        private GameObject rowsRoot;
        private LineRenderer[] rowLines;

        private GameObject columnsRoot;
        private LineRenderer[] columnLines;

        private Vector3[] points;

        private Vector2 accumulatedOffset = Vector2.zero;

        private Func<float, float, float, float> surfaceFunction = (x, y, t) => 0.0f;

        protected override void Bootstrap(ISpyConfig spy)
        {
            rowsRoot = new GameObject("Rows");
            rowsRoot.SetAsChildOf(this);

            columnsRoot = new GameObject("Columns");
            columnsRoot.SetAsChildOf(this);

            spy
                .When(SurfaceParameters)
                .HasChangesOn(
                    p => p.NbOfRows,
                    p => p.NbOfCols
                )
                .Do(SetupPoints);

            spy
                .When(LineRendererParameters)
                .HasChanged()
                .Do(SetupPoints);

            spy
                .When(Formula)
                .HasChanged()
                .Do(SetupFunction);

            surfaceGenerator = new SurfaceGenerator();
            mathEngine = new MathEngine();
        }

        protected override void Initialize()
        {
            Camera camera = transform.parent.GetComponentInChildren<Camera>();
            camera.LookAt(transform);
        }

        protected override void Tick()
        {
            accumulatedOffset += Time.deltaTime * Speed;

            surfaceGenerator.GenerateSurfacePoints(points, SurfaceParameters, surfaceFunction, Time.time, accumulatedOffset);

            int nbOfCols = SurfaceParameters.NbOfCols;
            int nbOfRows = SurfaceParameters.NbOfRows;

            int pointsPerRow = nbOfCols + 2;
            int pointsPerCol = nbOfRows + 2;

            for (int row = 0; row < nbOfRows; row++)
            {
                int y = row + 1;
                Vector3[] rowPoints = new Vector3[pointsPerRow];
                for (int x = 0; x < pointsPerRow; x++)
                    rowPoints[x] = points[y * pointsPerRow + x];

                rowLines[row].SetPositions(rowPoints);
            }

            for (int col = 0; col < nbOfCols; col++)
            {
                int x = col + 1;
                Vector3[] colPoints = new Vector3[pointsPerCol];
                for (int y = 0; y < pointsPerCol; y++)
                    colPoints[y] = points[y * pointsPerRow + x];

                columnLines[col].SetPositions(colPoints);
            }
        }

        private void SetupPoints()
        {
            rowsRoot.ClearChildren();
            rowLines = new LineRenderer[SurfaceParameters.NbOfRows];
            int pointsPerRow = SurfaceParameters.NbOfCols + 2;
            for (int i = 0; i < SurfaceParameters.NbOfRows; i++)
            {
                GameObject rowObject = rowsRoot.AddChild($"Row-{i + 1:000}");
                rowLines[i] = CreateLine(rowObject, pointsPerRow);
            }

            columnsRoot.ClearChildren();
            columnLines = new LineRenderer[SurfaceParameters.NbOfCols];
            int pointsPerCol = SurfaceParameters.NbOfRows + 2;
            for (int i = 0; i < SurfaceParameters.NbOfCols; i++)
            {
                GameObject colObject = columnsRoot.AddChild($"Column-{i + 1:000}");
                columnLines[i] = CreateLine(colObject, pointsPerCol);
            }

            points = new Vector3[pointsPerRow * pointsPerCol];

            LineRenderer CreateLine(GameObject go, int positionCount)
            {
                LineRenderer lineRenderer = go.AddComponent<LineRenderer>();

                lineRenderer.sharedMaterial = LineRendererParameters.LinesMaterial;
                lineRenderer.startWidth = LineRendererParameters.LineWidth;
                lineRenderer.endWidth = LineRendererParameters.LineWidth;
                lineRenderer.positionCount = positionCount;

                return lineRenderer;
            }
        }

        private void SetupFunction()
        {
            try
            {
                Func<float, float, float, float> result = mathEngine.Formula(Formula.Text)
                        .Parameter("x")
                        .Parameter("y")
                        .Parameter("t")
                        .Build();

                Formula.ErrorMessage = "";

                surfaceFunction = result;
            }
            catch (Exception ex)
            {
                Formula.ErrorMessage = ex.Message;
            }
        }
    }
}