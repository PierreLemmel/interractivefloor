﻿namespace InterractiveFloor.Unity.MeshGen
{
    public class ScanImageColumnData : Model
    {
        public ScanLineHorizontalDirection Direction { get; set; } 
        public float Position { get; set; }
        public float Thickness { get; set; }

        public ScanImageColumnData(ScanLineHorizontalDirection direction, float position, float thickness)
        {
            Direction = direction;
            Position = position;
            Thickness = thickness;
        }
    }
}