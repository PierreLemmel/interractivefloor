﻿namespace InterractiveFloor.Unity.MeshGen
{
    public class ScannedSquareParameters : Model
    {
        [Positive]
        public float Width { get; set; } = 192.0f;

        [Positive]
        public float Height { get; set; } = 108.0f;
    }
}