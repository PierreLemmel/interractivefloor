﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public class MeshGenerationResult : Result
    {
        public MeshGenerationResult(Vector3[] vertices, Vector2[] uvs, int[] triangles)
        {
            Guard.NotNull(vertices, nameof(vertices));
            Guard.NotNull(uvs, nameof(uvs));
            Guard.NotNull(triangles, nameof(triangles));

            if (vertices.Length != uvs.Length)
                throw new InvalidOperationException($"Vertices and UVs must have the same length ({vertices.Length} / {uvs.Length})");

            Vertices = vertices;
            UVs = uvs;
            Triangles = triangles;
        }

        public Vector3[] Vertices { get; }
        public Vector2[] UVs { get; }
        public int[] Triangles { get; }
    }
}