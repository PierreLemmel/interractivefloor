﻿namespace InterractiveFloor.Unity.MeshGen
{
    public class HollowPolyhedronParameters : Model
    {
        [InRange(3, 80)]
        public int Sides { get; set; } = 6;

        [Positive]
        public float InnerRadius { get; set; } = 8.0f;

        [GreaterThan(nameof(InnerRadius), 0.02f)]
        public float OuterRadius { get; set; } = 10.0f;

        [GreaterThan(0.01f)]
        public float Thickness { get; set; } = 1.0f;
    }
}