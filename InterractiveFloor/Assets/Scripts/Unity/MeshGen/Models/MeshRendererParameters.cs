﻿using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public class MeshRendererParameters : Model
    {
        public Material MeshMaterial { get; set; }
    }
}