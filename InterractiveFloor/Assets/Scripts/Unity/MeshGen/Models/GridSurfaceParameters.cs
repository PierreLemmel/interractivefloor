﻿namespace InterractiveFloor.Unity.MeshGen
{
    public class GridSurfaceParameters : Model
    {
        [InRange(0.0f, 100.0f)]
        public float GridWidth { get; set; } = 15.0f;

        [InRange(0.0f, 100.0f)]
        public float GridHeight { get; set; } = 10.0f;

        [InRange(2, 100)]
        public int NbOfRows { get; set; } = 10;

        [InRange(2, 100)]
        public int NbOfCols { get; set; } = 15;
    }
}