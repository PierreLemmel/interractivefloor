﻿namespace InterractiveFloor.Unity.MeshGen
{
    public class ScanImageRowData : Model
    {
        public ScanLineVerticalDirection Direction { get; set; }
        public float Position { get; set; }
        public float Thickness { get; set; }

        public ScanImageRowData(ScanLineVerticalDirection direction, float position, float thickness)
        {
            Direction = direction;
            Position = position;
            Thickness = thickness;
        }
    }
}