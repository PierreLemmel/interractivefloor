﻿using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public class LineRendererParameters : Model
    {
        [InRange(0.0f, 1.0f)]
        public float LineWidth { get; set; } = 0.1f;

        public Material LinesMaterial { get; set; }
    }
}