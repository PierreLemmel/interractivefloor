﻿using System;

namespace InterractiveFloor.Unity.MeshGen
{
    public class HsvCircleSchemeSettings : Model
    {
        [InRange(0.0f, 1.0f)]
        public float Saturation { get; set; } = 0.75f;

        [InRange(0.0f, 1.0f)]
        public float Value { get; set; } = 0.75f;

        public float HueOffset { get; set; }
    }
}