﻿namespace InterractiveFloor.Unity.MeshGen
{
    public class FormulaModel : Model
    {
        public string Text { get; set; } = "0";

        [NotEditable]
        public string ErrorMessage { get; set; } = "";
    }
}