﻿using System;

namespace InterractiveFloor.Unity.MeshGen
{
    public interface IHollowPolyhedronGenerator
    {
        MeshGenerationResult GeneratePolyhedron(HollowPolyhedronParameters parameters);
    }
}