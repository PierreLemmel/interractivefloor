﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public class ScannedSquareGenerator : IScannedSquareGenerator
    {
        public MeshGenerationResult GenerateScannedSurface(ScannedSquareParameters parameters, IReadOnlyCollection<ScanImageRowData> rows, IReadOnlyCollection<ScanImageColumnData> columns)
        {
            float width = parameters.Width;
            float height = parameters.Height;

            int rowCount = rows.Count;
            int colCount = columns.Count;

            if (width < 0.0f) throw new InvalidOperationException("Can't generate a scanned surface with a negative width");
            if (height < 0.0f) throw new InvalidOperationException("Can't generate a scanned surface with a negative height");

            int verticesCount = 4 * rowCount + 4 * colCount;
            int uvCount = verticesCount;
            int pointsCount = 2 * 3 * rowCount + 2 * 3 * colCount;

            Vector3[] vertices = new Vector3[verticesCount];
            Vector2[] uvs = new Vector2[uvCount];
            int[] triangles = new int[pointsCount];

            float halfWidth = width / 2.0f;
            float halfHeight = height / 2.0f;

            int vertIndex = 0;
            int uvIndex = 0;
            int triIndex = 0;

            int colIndex = 0;
            foreach (ScanImageColumnData column in columns)
            {
                float halfThickness = column.Thickness / 2.0f;
                float position = column.Position;

                float xLeft = Mathf.Clamp(-halfWidth + position - halfThickness, -halfWidth, halfWidth);
                float xRight = Mathf.Clamp(-halfWidth + position + halfThickness, -halfWidth, halfWidth);

                vertices[vertIndex++] = new Vector3(xLeft, 0.0f, -halfHeight);
                vertices[vertIndex++] = new Vector3(xRight, 0.0f, -halfHeight);
                vertices[vertIndex++] = new Vector3(xLeft, 0.0f, halfHeight);
                vertices[vertIndex++] = new Vector3(xRight, 0.0f, halfHeight);

                float uLeft = Mathf.Clamp01((position - halfThickness) / width);
                float uRight = Mathf.Clamp01((position + halfThickness) / width);

                uvs[uvIndex++] = new Vector2(uLeft, 0.0f);
                uvs[uvIndex++] = new Vector2(uRight, 0.0f);
                uvs[uvIndex++] = new Vector2(uLeft, 1.0f);
                uvs[uvIndex++] = new Vector2(uRight, 1.0f);

                int baseIndex = 4 * colIndex;

                triangles[triIndex++] = baseIndex;
                triangles[triIndex++] = baseIndex + 2;
                triangles[triIndex++] = baseIndex + 1;

                triangles[triIndex++] = baseIndex + 2;
                triangles[triIndex++] = baseIndex + 3;
                triangles[triIndex++] = baseIndex + 1;

                colIndex++;
            }

            int rowIndex = 0;
            foreach (ScanImageRowData row in rows)
            {
                float halfThickness = row.Thickness / 2.0f;
                float position = row.Position;

                float zTop = Mathf.Clamp(-halfHeight + position + halfThickness, -halfHeight, halfHeight);
                float zBot = Mathf.Clamp(-halfHeight + position - halfThickness, -halfHeight, halfHeight);

                vertices[vertIndex++] = new Vector3(-halfWidth, 0.0f, zTop);
                vertices[vertIndex++] = new Vector3(-halfWidth, 0.0f, zBot);
                vertices[vertIndex++] = new Vector3(halfWidth, 0.0f, zTop);
                vertices[vertIndex++] = new Vector3(halfWidth, 0.0f, zBot);

                float vTop = Mathf.Clamp01((position + halfThickness) / height);
                float vBot = Mathf.Clamp01((position - halfThickness) / height);

                uvs[uvIndex++] = new Vector2(0.0f, vTop);
                uvs[uvIndex++] = new Vector2(0.0f, vBot);
                uvs[uvIndex++] = new Vector2(1.0f, vTop);
                uvs[uvIndex++] = new Vector2(1.0f, vBot);

                int baseIndex = 4 * colCount + 4 * rowIndex;

                triangles[triIndex++] = baseIndex;
                triangles[triIndex++] = baseIndex + 2;
                triangles[triIndex++] = baseIndex + 1;

                triangles[triIndex++] = baseIndex + 2;
                triangles[triIndex++] = baseIndex + 3;
                triangles[triIndex++] = baseIndex + 1;

                rowIndex++;
            }

            MeshGenerationResult result = new MeshGenerationResult(vertices, uvs, triangles);
            return result;
        }
    }
}