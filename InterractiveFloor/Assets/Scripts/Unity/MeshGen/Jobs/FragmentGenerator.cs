﻿using System;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public class FragmentGenerator : IFragmentGenerator
    {
        public void GenerateFragments(ShatteredVideosMeshSettings meshSettings, Mesh[] inMeshes, Vector3[] inDirections)
        {
            int meshCount = inMeshes.Length;

            float width = meshSettings.Width;
            float height = meshSettings.Height;
            float thickness = meshSettings.Thickness;
            float centerX = meshSettings.CenterX;
            float centerY = meshSettings.CenterY;
            int pointsPerEdge = meshSettings.PointsPerEdge;
            float angleOffset = meshSettings.AngleOffset;
            float angleStdDev = meshSettings.AngleStandardDeviation;

            if (meshCount < 4) throw new InvalidOperationException("Can't generate less than 4 fragments");
            if (width < 0.0f) throw new InvalidOperationException("Can't generate fragments with negative width");
            if (height < 0.0f) throw new InvalidOperationException("Can't generate fragments with negative height");
            if (pointsPerEdge < 0) throw new InvalidOperationException("Can't a have a negative number of points per edge");
            if (thickness < 0.0f) throw new InvalidOperationException("Can't generate fragments with negative thickness");
            if (!centerX.IsInRange(0.0f, width) || !centerY.IsInRange(0.0f, height)) throw new InvalidOperationException("Center must be in the (width, heihgt) bounds");

            (Vector3 intersectionCoord, Vector2 intersectionUV, Intersection segment)[] intersections = new (Vector3, Vector2, Intersection)[meshCount];

            float halfWidth = width / 2.0f;
            float halfHeight = height / 2.0f;
            float halfThickness = thickness / 2.0f;

            Vector3 centerCoordTop = new Vector3(centerX - halfWidth, halfThickness, centerY - halfHeight);
            Vector3 centerCoordBot = new Vector3(centerX - halfWidth, -halfThickness, centerY - halfHeight);
            Vector2 centerUV = new Vector2(centerX / width, centerY / height);

            Vector3 toTopCoord = new Vector3(0.0f, halfThickness, 0.0f);
            Vector3 toBotCoord = new Vector3(0.0f, -halfThickness, 0.0f);

            float angleMean = 360.0f / meshCount;
            float angleSigma = (angleStdDev / 100.0f) * angleMean;

            float[] angleValues = Sequences.NormalyDistributed(meshCount)
                .Mean(angleMean)
                .Sigma(angleSigma)
                .Positive()
                .EnsureTotalValue(360.0f);

            float a1 = Mathf.Rad2Deg * Mathf.Atan((height - centerY) / (width - centerX));
            float a2 = 180.0f - Mathf.Rad2Deg * Mathf.Atan((height - centerY) / centerX);
            float a3 = 270.0f - Mathf.Rad2Deg * Mathf.Atan(centerX / centerY);
            float a4 = 360.0f - Mathf.Rad2Deg * Mathf.Atan(centerY / (width - centerX));

            float currentAngle = angleOffset % 360.0f;

            for (int i = 0; i < meshCount; i++)
            {
                float radAngle = Mathf.Deg2Rad * currentAngle;
                float fragAngle = angleValues[i];

                Vector3 coord;
                Intersection segment;

                if (currentAngle.IsInRange(a1, a2))
                {
                    segment = Intersection.Top;
                    float x = centerCoordTop.x + Mathf.Tan(Mathf.PI/2 - radAngle) * (halfHeight - centerCoordTop.z);

                    coord = new Vector3(x, 0.0f, halfHeight);
                }
                else if (currentAngle.IsInRange(a2, a3))
                {
                    segment = Intersection.Left;
                    float z = centerCoordTop.z + Mathf.Tan(Mathf.PI - radAngle) * (halfWidth + centerCoordTop.x);

                    coord = new Vector3(-halfWidth, 0.0f, z);
                }
                else if (currentAngle.IsInRange(a3, a4))
                {
                    segment = Intersection.Bottom;
                    float x = centerCoordTop.x - Mathf.Tan(3.0f * Mathf.PI / 2.0f - radAngle) * (centerCoordTop.z + halfHeight);

                    coord = new Vector3(x, 0.0f, -halfHeight);
                }
                else
                {
                    segment = Intersection.Right;
                    float z = centerCoordTop.z + Mathf.Tan(radAngle) * (halfWidth - centerCoordTop.x);

                    if (z > halfHeight) Debug.LogWarning("&azeaze");

                    coord = new Vector3(halfWidth, 0.0f, z);
                }

                Vector2 uv = new Vector2((coord.x + halfWidth) / width, (coord.z + halfHeight) / height);
                intersections[i] = (intersectionCoord: coord, intersectionUV: uv, segment: segment);

                float directionAngle = currentAngle + fragAngle / 2.0f;
                Vector3 direction = new Vector3(Mathf.Cos(Mathf.Deg2Rad * directionAngle), 0.0f, Mathf.Sin(Mathf.Deg2Rad * directionAngle));
                inDirections[i] = direction;

                currentAngle = (currentAngle + fragAngle) % 360.0f;
            }

            for (int i = 0; i < meshCount - 1; i++)
            {
                SetFragment(i, i, i + 1);
            }
            SetFragment(meshCount - 1, meshCount - 1, 0);

            void SetFragment(int fragment, int i1, int i2)
            {
                Mesh mesh = inMeshes[fragment];
                mesh.ClearVertices();

                (Vector3 coord1, Vector2 uv1, Intersection inter1) = intersections[i1];
                (Vector3 coord2, Vector2 uv2, Intersection inter2) = intersections[i2];

                Vector3[] vertices;
                Vector2[] uvs;
                int[] triangles;

                if (inter1 == inter2)
                {
                    vertices = new Vector3[]
                    { 
                        centerCoordTop,
                        coord1 + toTopCoord,
                        coord2 + toTopCoord,

                        centerCoordBot,
                        coord1 + toBotCoord,
                        coord2 + toBotCoord,
                    };
                    uvs = new Vector2[]
                    { 
                        centerUV,
                        uv1,
                        uv2,

                        centerUV,
                        uv1,
                        uv2,
                    };
                    triangles = new int[]
                    { 
                        0, 2, 1,

                        3, 4, 5,

                        0, 4, 3,
                        0, 1, 4,

                        1, 2, 4,
                        2, 5, 4,

                        0, 5, 2,
                        0, 3, 5,
                    };
                }
                else
                {
                    Vector3 cornerCoord;
                    Vector2 cornerUV;

                    Intersection intersection = inter1 | inter2;
                    switch (intersection)
                    {
                        case Intersection.BottomLeft:
                            cornerCoord = new Vector3(-halfWidth, 0.0f, -halfHeight);
                            cornerUV = new Vector2(0.0f, 0.0f);
                            break;

                        case Intersection.BottomRight:
                            cornerCoord = new Vector3(halfWidth, 0.0f, -halfHeight);
                            cornerUV = new Vector2(1.0f, 0.0f);
                            break;

                        case Intersection.TopLeft:
                            cornerCoord = new Vector3(-halfWidth, 0.0f, halfHeight);
                            cornerUV = new Vector2(0.0f, 1.0f);
                            break;

                        case Intersection.TopRight:
                            cornerCoord = new Vector3(halfWidth, 0.0f, halfHeight);
                            cornerUV = new Vector2(1.0f, 1.0f);
                            break;

                        default:
                            throw new InvalidOperationException($"Unexpected intersection: {intersection}");
                    }


                    vertices = new Vector3[]
                    {
                        centerCoordTop,
                        coord1 + toTopCoord,
                        coord2 + toTopCoord,
                        cornerCoord + toTopCoord,

                        centerCoordBot,
                        coord1 + toBotCoord,
                        coord2 + toBotCoord,
                        cornerCoord + toBotCoord,
                    };
                    uvs = new Vector2[]
                    {
                        centerUV,
                        uv1,
                        uv2,
                        cornerUV,

                        centerUV,
                        uv1,
                        uv2,
                        cornerUV,
                    };
                    triangles = new int[]
                    {
                        0, 2, 1,
                        1, 2, 3,

                        4, 5, 6,
                        5, 7, 6,

                        0, 5, 4,
                        0, 1, 5,

                        0, 6, 2,
                        0, 4, 6,

                        1, 3, 7,
                        1, 7, 5,

                        3, 2, 6,
                        3, 6, 7,
                    };
                }

                mesh.vertices = vertices;
                mesh.uv = uvs;
                mesh.triangles = triangles;
            }
        }

        private enum Intersection
        {
            Right = 1,
            Top = 2,
            Left = 4,
            Bottom = 8,

            BottomLeft = Bottom | Left,
            BottomRight = Bottom | Right,
            TopLeft = Top | Left,
            TopRight = Top | Right,
        }
    }
}