﻿using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public interface IHollowPolyhedronTextureGenerator
    {
        Texture2D GenerateTexture(HsvCircleSchemeSettings colorScheme, int sides);
    }
}