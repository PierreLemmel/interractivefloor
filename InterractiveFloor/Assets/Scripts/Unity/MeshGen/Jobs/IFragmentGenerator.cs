﻿using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public interface IFragmentGenerator
    {
        void GenerateFragments(ShatteredVideosMeshSettings settings, Mesh[] inMeshes, Vector3[] inDirections);
    }
}