﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public class HollowPolyhedronGenerator : IHollowPolyhedronGenerator
    {
        // We use two distinct vertices when "closing" the shape to have a clean uv mapping
        private const int VerticesPerSlice = 5;
        private const int PointsPerSide = 2 * 3 * 4;

        public MeshGenerationResult GeneratePolyhedron(HollowPolyhedronParameters parameters)
        {
            int sides = parameters.Sides;
            float innerRadius = parameters.InnerRadius;
            float outerRadius = parameters.OuterRadius;
            float thickness = parameters.Thickness;

            if (sides < 2) throw new InvalidOperationException($"Can't generate a polyhedron with less than two sides");
            if (innerRadius > outerRadius) throw new InvalidOperationException($"Can't generate a polyhedron with innerradius greater than outerradius");
            if (innerRadius < 0.0f) throw new InvalidOperationException($"Can't generate a polyhedron with a negative innerradius");
            if (outerRadius < 0.0f) throw new InvalidOperationException($"Can't generate a polyhedron with a negative outerradius");
            if (thickness < 0.0f) throw new InvalidOperationException($"Can't generate a polyhedron with a negative thickness");

            float halfThickness = thickness / 2.0f;

            int verticesCount = VerticesPerSlice * (sides + 1);
            int uvCount = verticesCount;
            int pointsCount = PointsPerSide * sides;

            Vector3[] vertices = new Vector3[verticesCount];
            Vector2[] uvs = new Vector2[uvCount];
            int[] triangles = new int[pointsCount];

            int vertIndex = 0;
            int uvIndex = 0;
            for (int i = 0; i <= sides; i++)
            {
                float rel = (float)i / sides;
                float a = 2 * Mathf.PI * rel;

                float cosa = Mathf.Cos(a);
                float sina = Mathf.Sin(a);

                vertices[vertIndex++] = new Vector3(cosa * innerRadius, halfThickness, sina * innerRadius);
                vertices[vertIndex++] = new Vector3(cosa * outerRadius, halfThickness, sina * outerRadius);
                vertices[vertIndex++] = new Vector3(cosa * outerRadius, -halfThickness, sina * outerRadius);
                vertices[vertIndex++] = new Vector3(cosa * innerRadius, -halfThickness, sina * innerRadius);
                vertices[vertIndex++] = new Vector3(cosa * innerRadius, halfThickness, sina * innerRadius);

                uvs[uvIndex++] = new Vector2(rel, 0.0f);
                uvs[uvIndex++] = new Vector2(rel, 0.25f);
                uvs[uvIndex++] = new Vector2(rel, 0.50f);
                uvs[uvIndex++] = new Vector2(rel, 0.75f);
                uvs[uvIndex++] = new Vector2(rel, 1.0f);
            }




            int triangleIndex = 0;
            for (int i = 0; i < sides; i++)
            {
                int i1 = i + 1;

                //Top
                triangles[triangleIndex++] = VerticesPerSlice * i;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 1;
                triangles[triangleIndex++] = VerticesPerSlice * i + 1;

                triangles[triangleIndex++] = VerticesPerSlice * i;
                triangles[triangleIndex++] = VerticesPerSlice * i1;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 1;

                //Outer
                triangles[triangleIndex++] = VerticesPerSlice * i + 1;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 1;
                triangles[triangleIndex++] = VerticesPerSlice * i + 2;

                triangles[triangleIndex++] = VerticesPerSlice * i + 2;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 1;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 2;

                //Bottom
                triangles[triangleIndex++] = VerticesPerSlice * i + 2;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 2;
                triangles[triangleIndex++] = VerticesPerSlice * i + 3;

                triangles[triangleIndex++] = VerticesPerSlice * i + 3;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 2;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 3;

                //Inner
                triangles[triangleIndex++] = VerticesPerSlice * i + 3;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 3;
                triangles[triangleIndex++] = VerticesPerSlice * i + 4;

                triangles[triangleIndex++] = VerticesPerSlice * i + 4;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 3;
                triangles[triangleIndex++] = VerticesPerSlice * i1 + 4;
            }

            MeshGenerationResult result = new MeshGenerationResult(vertices, uvs, triangles);
            return result;
        }
    }
}
