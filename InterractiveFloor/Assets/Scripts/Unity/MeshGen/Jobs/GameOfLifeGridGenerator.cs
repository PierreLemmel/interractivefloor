﻿using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public class GameOfLifeGridGenerator : IGameOfLifeGridGenerator
    {
        private int[] triangles;

        public void UpdateVertices(Mesh gridMesh, int rows, int columns, float cellWidth, float cellSpacing)
        {
            float totalWidth = columns * cellWidth + (columns - 1) * cellSpacing;
            float totalHeight = rows * cellWidth + (rows - 1) * cellSpacing;


            int verticesCount = 4 * rows * columns;
            gridMesh.ClearVertices();

            Vector3[] vertices = new Vector3[verticesCount];
            Vector2[] uvs = new Vector2[verticesCount];

            int vertIndex = 0;
            int uvIndex = 0;

            float x0 = -totalWidth / 2.0f;
            float z0 = -totalHeight / 2.0f;

            float zBot = z0;
            for (int row = 0; row < rows; row++)
            {
                float xLeft = x0;
                for (int col = 0; col < columns; col++)
                {
                    float xRight = xLeft + cellWidth;

                    float zTop = zBot + cellWidth;

                    vertices[vertIndex++] = new Vector3(xLeft, 0.0f, zBot);
                    vertices[vertIndex++] = new Vector3(xLeft, 0.0f, zTop);
                    vertices[vertIndex++] = new Vector3(xRight, 0.0f, zTop);
                    vertices[vertIndex++] = new Vector3(xRight, 0.0f, zBot);

                    float uLeft = (xLeft - x0) / totalWidth;
                    float uRight = (xRight - x0) / totalWidth;
                    float vBot = (zBot - z0) / totalHeight;
                    float vTop = (zTop - z0) / totalHeight;

                    uvs[uvIndex++] = new Vector2(uLeft, vBot);
                    uvs[uvIndex++] = new Vector2(uLeft, vTop);
                    uvs[uvIndex++] = new Vector2(uRight, vTop);
                    uvs[uvIndex++] = new Vector2(uRight, vBot);

                    xLeft += cellWidth + cellSpacing;
                }

                zBot += cellWidth + cellSpacing;
            }

            gridMesh.vertices = vertices;
            gridMesh.uv = uvs;
            gridMesh.bounds = new Bounds(Vector3.zero, new Vector3(totalWidth, 0.0f, totalHeight));
            gridMesh.RecalculateNormals();
            gridMesh.RecalculateTangents();
        }

        public void UpdateSubmeshes(Mesh gridMesh, Grid<bool> state)
        {
            const int PointsPerCell = 6;

            Arrays.EnsureSize(ref triangles, PointsPerCell * state.CellCount);

            int rows = state.Height;
            int columns = state.Width;

            int aliveTriangles = 0;
            int deadTriangles = 0;

            int baseVertIndex = 0;

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < columns; col++)
                {
                    if (state[row, col])
                    {
                        int triIndex = aliveTriangles;

                        triangles[triIndex++] = baseVertIndex;
                        triangles[triIndex++] = baseVertIndex + 1;
                        triangles[triIndex++] = baseVertIndex + 2;

                        triangles[triIndex++] = baseVertIndex;
                        triangles[triIndex++] = baseVertIndex + 2;
                        triangles[triIndex++] = baseVertIndex + 3;

                        aliveTriangles += PointsPerCell;
                    }
                    else
                    {
                        int triIndex = triangles.Length - (deadTriangles + PointsPerCell);

                        triangles[triIndex++] = baseVertIndex;
                        triangles[triIndex++] = baseVertIndex + 1;
                        triangles[triIndex++] = baseVertIndex + 2;

                        triangles[triIndex++] = baseVertIndex;
                        triangles[triIndex++] = baseVertIndex + 2;
                        triangles[triIndex++] = baseVertIndex + 3;

                        deadTriangles += PointsPerCell;
                    }

                    baseVertIndex += 4;
                }
            }

            gridMesh.SetTriangles(triangles, 0, aliveTriangles, 0, calculateBounds: false);
            gridMesh.SetTriangles(triangles, aliveTriangles, triangles.Length - aliveTriangles, 1, calculateBounds: false);
        }
    }
}