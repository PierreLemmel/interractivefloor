﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public interface ISurfaceGenerator
    {
        void GenerateSurfacePoints(Vector3[] buffer, GridSurfaceParameters parameters, Func<float, float, float, float> f, float t, Vector2 offset);
    }
}