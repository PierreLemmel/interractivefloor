﻿using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public interface IGameOfLifeGridGenerator
    {
        void UpdateVertices(Mesh gridMesh, int rows, int columns, float cellWidth, float cellSpacing);
        void UpdateSubmeshes(Mesh gridMesh, Grid<bool> state);
    }
}