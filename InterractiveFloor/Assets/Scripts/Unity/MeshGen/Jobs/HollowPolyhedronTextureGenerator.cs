﻿using System;
using System.Linq;
using UnityEngine;

using URandom = UnityEngine.Random;

namespace InterractiveFloor.Unity.MeshGen
{
    public class HollowPolyhedronTextureGenerator : IHollowPolyhedronTextureGenerator
    {
        private const int PixelsPerColor = 60;

        public Texture2D GenerateTexture(HsvCircleSchemeSettings colorScheme, int sides)
        {
            Texture2D texture = new Texture2D(PixelsPerColor * sides, PixelsPerColor * 4);

            Color[] randomColors = Enumerable
                .Range(0, sides)
                .SelectMany(i => Enumerable.Repeat(ColorForSide(colorScheme, i, sides), PixelsPerColor))
                .ToArray();

            Color[] pixels = new Color[PixelsPerColor * 4 * PixelsPerColor * sides];

            int offset = 0;
            int lineLength = PixelsPerColor * sides;

            Color[] line = new Color[lineLength];
            for (int i = 0; i < 4; i++, offset += PixelsPerColor)
            {
                Array.Copy(randomColors, 0, line, offset, lineLength - offset);
                Array.Copy(randomColors, lineLength - offset, line, 0, offset);

                for (int j = 0; j < PixelsPerColor; j++)
                    Array.Copy(line, 0, pixels, (i * PixelsPerColor + j) * lineLength, lineLength);
            }


            texture.SetPixels(pixels);
            texture.Apply();

            return texture;
        }

        private static Color ColorForSide(HsvCircleSchemeSettings colorScheme, int index, int total)
        {
            float hue = ((colorScheme.HueOffset + 360.0f * index / total) % 360.0f) / 360.0f;
            float saturation = colorScheme.Saturation;
            float value = colorScheme.Value;

            return Color.HSVToRGB(hue, saturation, value);
        }
    }
}