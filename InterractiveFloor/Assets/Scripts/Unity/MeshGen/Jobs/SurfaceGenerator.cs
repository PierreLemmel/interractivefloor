﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity.MeshGen
{
    public class SurfaceGenerator : ISurfaceGenerator
    {
        public void GenerateSurfacePoints(Vector3[] points,  GridSurfaceParameters parameters, Func<float, float, float, float> f, float t, Vector2 offset)
        {
            int nbOfCols = parameters.NbOfCols;
            int nbOfRows = parameters.NbOfRows;

            float width = parameters.GridWidth;
            float height = parameters.GridHeight;

            int pointsPerRow = nbOfCols + 2;
            int pointsPerCol = nbOfRows + 2;

            float colMaxf = nbOfCols - 1.0f;
            float rowMaxf = nbOfRows - 1.0f;

            float halfWidth = width / 2.0f;
            float halfHeight = height / 2.0f;

            float spacingX = width / nbOfCols;
            float spacingZ = height / nbOfRows;

            float originX = offset.x;
            float originZ = offset.y;

            float relOffsetX = ((offset.x % spacingX / spacingX) + 0.5f) % 1.0f - 0.5f;
            float offsetX = -relOffsetX * spacingX;
            float relOffsetZ = ((offset.y % spacingZ / spacingZ) + 0.5f) % 1.0f - 0.5f;
            float offsetZ = -relOffsetZ * spacingZ;

            float rowRange = height * (nbOfRows - 1) / nbOfRows;
            float colRange = width * (nbOfCols - 1) / nbOfCols;

            for (int row = 0; row < nbOfRows; row++)
            {
                int y = row + 1;
                float zCoord = rowRange * (row / rowMaxf - 0.5f) + offsetZ;
                for (int col = 0; col < nbOfCols; col++)
                {
                    int x = col + 1;
                    float xCoord = colRange * (col / colMaxf - 0.5f) + offsetX;
                    SetPoint(x, y, xCoord, zCoord);
                }

                SetPoint(0, y, -halfWidth, zCoord);
                SetPoint(pointsPerRow - 1, y, halfWidth, zCoord);
            }

            for (int col = 0; col < nbOfCols; col++)
            {
                int x = col + 1;
                float xCoord = colRange * (col / colMaxf - 0.5f) + offsetX;
                SetPoint(x, 0, xCoord, -halfHeight);
                SetPoint(x, pointsPerCol - 1, xCoord, halfHeight);
            }

            SetPoint(0, 0, -halfWidth, -halfHeight);
            SetPoint(0, pointsPerCol - 1, -halfWidth, halfHeight);
            SetPoint(pointsPerRow - 1, 0, halfWidth, -halfHeight);
            SetPoint(pointsPerRow - 1, pointsPerCol - 1, halfWidth, halfHeight);

            void SetPoint(int x, int y, float xCoord, float zCoord)
            {
                float yCoord = f(xCoord + originX, zCoord + originZ, t);
                Vector3 point = new Vector3(xCoord, yCoord, zCoord);

                points[y * pointsPerRow + x] = point;
            }
        }
    }
}