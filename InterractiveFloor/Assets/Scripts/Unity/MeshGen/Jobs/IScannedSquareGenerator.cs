﻿using System.Collections.Generic;

namespace InterractiveFloor.Unity.MeshGen
{
    public interface IScannedSquareGenerator
    {
        MeshGenerationResult GenerateScannedSurface(ScannedSquareParameters parameters, IReadOnlyCollection<ScanImageRowData> rows, IReadOnlyCollection<ScanImageColumnData> columns);
    }
}