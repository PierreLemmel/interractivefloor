﻿using CSCore.CoreAudioAPI;
using CSCore.SoundIn;
using UnityEngine;

namespace InterractiveFloor.Unity.Audio
{
    public class PeakSource : IPeakSource
    {
        private AudioMeterInformation audioMeter;
        private MMDevice device;

        // If device is a capture device, he won't provide any information, unless some loopback is occuring
        private WasapiCapture dummyCapture;

        public PeakSource(MMDevice device)
        {
            audioMeter = AudioMeterInformation.FromDevice(device);
            this.device = device;
        }

        public void Start()
        {
            Debug.Log($"Starting PeakSource for device {device.FriendlyName} with id {device.DeviceID}");
            if (device.DataFlow == DataFlow.Capture)
            {
                dummyCapture = new WasapiCapture(true, AudioClientShareMode.Shared, 250)
                {
                    Device = device
                };
                dummyCapture.Initialize();
                dummyCapture.Start();
            }
            Debug.Log($"PeakSource started for device {device.FriendlyName} with id {device.DeviceID}");
        }

        public void Stop()
        {
            Debug.Log($"Stopping PeakSource for device {device.FriendlyName} with id {device.DeviceID}");
            if (dummyCapture != null)
            {
                dummyCapture.Stop();
                dummyCapture.Dispose();
                dummyCapture = null;
            }
            Debug.Log($"PeakSource stopped for device {device.FriendlyName} with id {device.DeviceID}");
        }

        public void Dispose() => device.Dispose();

        public bool TryGetNextResult(out PeakResult result)
        {
            float peak = audioMeter.GetPeakValue();
            result = new PeakResult(peak);

            return true;
        }
    }
}