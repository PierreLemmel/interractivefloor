﻿using CSCore.DSP;
using CSCore.SoundIn;
using System;

namespace InterractiveFloor.Unity.Audio
{
    public sealed class FftSource : IFftSource
    {
        private readonly ISoundIn soundIn;
        private FftProvider fftProvider;

        private float[] fftBuffer;
        private float[] inBuffer;

        private FftSettings fftSettings;
        private ISpy settingsSpy;

        private FftInfo fftInfo;

        private FftResult nextResult = null;

        public FftSource(ISoundIn soundIn)
        {
            Guard.NotNull(soundIn, nameof(soundIn));
            
            this.soundIn = soundIn;
        }

        public void Start(FftSettings settings)
        {
            fftSettings = settings;

            settingsSpy = Spy.CreateNew();

            settingsSpy
                .When(settings)
                .HasChanged()
                .Do(Setup);
        }

        private void Setup()
        {
            if (soundIn.RecordingState == RecordingState.Recording)
                soundIn.Stop();

            soundIn.Initialize();
            soundIn.DataAvailable += SoundIn_DataAvailable;

            int channels = soundIn.WaveFormat.Channels;

            fftProvider = new FftProvider(channels, fftSettings.FftSize);
            fftProvider.WindowFunction = GetWindowFunction(fftSettings.FftWindow);

            int fftSize = (int)fftSettings.FftSize;
            inBuffer = new float[fftSize];
            fftBuffer = new float[fftSize];

            int sampleRate = soundIn.WaveFormat.SampleRate;
            fftInfo = new FftInfo(fftSize, channels, sampleRate);

            int nbOfChannels = soundIn.WaveFormat.Channels;

            soundIn.Start();
        }

        public void Stop()
        {
            if (soundIn.RecordingState == RecordingState.Recording)
            {
                soundIn.Stop();
                soundIn.DataAvailable -= SoundIn_DataAvailable;
            }
        }

        public bool TryGetNextResult(out FftResult result)
        {
            settingsSpy.DetectChanges();

            if (soundIn.RecordingState != RecordingState.Recording)
            {
                result = null;
                return false;
            }

            result = nextResult;
            nextResult = null;

            return result != null;
        }

        private void SwapBuffers() => Utils.Swap(ref inBuffer, ref fftBuffer);

        private WindowFunction GetWindowFunction(FftWindow window)
        {
            switch (window)
            {
                case FftWindow.Hamming:
                    return WindowFunctions.Hamming;
                case FftWindow.HammingPeriodic:
                    return WindowFunctions.HammingPeriodic;
                case FftWindow.Hann:
                    return WindowFunctions.Hanning;
                case FftWindow.HannPeriodic:
                    return WindowFunctions.HanningPeriodic;
                default:
                    return WindowFunctions.None;
            }
        }

        private void SoundIn_DataAvailable(object sender, DataAvailableEventArgs e)
        {
            float[] data = CreateFloatArrayFromByteArray(e.Data, e.ByteCount);

            fftProvider.Add(data, data.Length);
            if (fftProvider.GetFftData(inBuffer))
            {
                SwapBuffers();

                FftResult result = new FftResult(fftBuffer, fftInfo);
                nextResult = result;
            }
        }

        private static float[] CreateFloatArrayFromByteArray(byte[] buffer, int nbOfBytes)
        {
            float[] fltArray = new float[nbOfBytes * sizeof(byte) / sizeof(float)];

            Buffer.BlockCopy(buffer, 0, fltArray, 0, nbOfBytes);
            return fltArray;
        }
    }
}