﻿namespace InterractiveFloor.Unity.Audio
{
    public interface IFftSource : IStartable<FftSettings>, IStopable, IAsyncResultStream<FftResult>
    {
        
    }
}