﻿using System;

namespace InterractiveFloor.Unity.Audio
{
    public interface IPeakSource : IAsyncResultStream<PeakResult>, IStartable, IStopable, IDisposable
    {
    }
}