﻿namespace InterractiveFloor.Unity.Audio
{
    public interface IDevicePeakSourceFactory : IFactory<IPeakSource, string>
    {
    }
}