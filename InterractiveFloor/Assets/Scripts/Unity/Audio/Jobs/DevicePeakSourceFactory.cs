﻿using CSCore.CoreAudioAPI;

namespace InterractiveFloor.Unity.Audio
{
    public class DevicePeakSourceFactory : IDevicePeakSourceFactory
    {
        public IPeakSource Create<TContext>(TContext context, string deviceId)
        {
            MMDevice mmDevice = MoreCSCore.GetMMDeviceById(deviceId);
            IPeakSource peakSource = new PeakSource(mmDevice);

            return peakSource;
        }
    }
}