﻿using CSCore.CoreAudioAPI;
using CSCore.SoundIn;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InterractiveFloor.Unity.Audio
{
    public static class MoreCSCore
    {
        public static void HandleAnyChange(this MMNotificationClient notificationClient, Action handler)
        {
            Guard.NotNull(notificationClient, nameof(notificationClient));
            Guard.NotNull(handler, nameof(handler));

            notificationClient.DeviceStateChanged += (sender, args) => handler();
            notificationClient.DeviceAdded += (sender, args) => handler();
            notificationClient.DeviceRemoved += (sender, args) => handler();
            notificationClient.DefaultDeviceChanged += (sender, args) => handler();
            notificationClient.DevicePropertyChanged += (sender, args) => handler();
        }

        public static MMDevice GetMMDeviceById(string id)
        {
            using (MMDeviceEnumerator enumerator = new MMDeviceEnumerator())
            {
                MMDevice device = enumerator
                    .EnumAudioEndpoints(DataFlow.All, DeviceState.Active)
                    .SingleOrDefault(d => d.DeviceID == id);

                return device;
            }
        }

        public static WaveInDevice GetWaveInDeviceByName(string name)
        {
            WaveInDevice device = WaveInDevice.EnumerateDevices().FirstOrDefault(d => d.Name == name);

            if (device is null)
            {
                string deviceList = string.Join(
                    Environment.NewLine,
                    WaveInDevice
                        .EnumerateDevices()
                        .Select(d => d.Name));
                string errorMsg = $"Can't find WaveInDevice named '{name}'. List of available devices: {deviceList}";

                throw new InvalidOperationException(errorMsg);
            }

            return device;
        }
    }
}
