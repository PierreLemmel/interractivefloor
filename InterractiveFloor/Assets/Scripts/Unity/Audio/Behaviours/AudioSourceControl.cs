﻿using UnityEngine;

namespace InterractiveFloor.Unity.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioSourceControl : SceneComponent
    {
        private AudioSource source;

        [Positive]
        public float Time { get; set; }

        private float lastTime = 0.0f;
        private float maxTime;

        protected override void Bootstrap(ISpyConfig spy)
        {
            source = GetComponent<AudioSource>();

            spy.When(source)
                .HasChangesOn(s => s.clip)
                .Do(SetupMaxTime);
        }

        private void SetupMaxTime() => maxTime = source.clip.length;

        protected override void Tick()
        {
            if (!source.isPlaying) return;

            if (Time != lastTime)
            {
                source.time = Mathf.Min(Time, maxTime);
            }

            lastTime = source.time;
            Time = lastTime;
        }

        [UserAction("Play")]
        public void Play()
        {
            source.Play();
            source.time = lastTime;
        }

        [UserAction("Pause")]
        public void Pause()
        {
            source.Stop();
            source.time = lastTime;
        }

        [UserAction("Stop")]
        public void Stop()
        {
            source.Stop();
            Time = 0.0f;
            lastTime = 0.0f;
            source.time = 0.0f;
        }
    }
}