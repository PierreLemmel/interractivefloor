﻿using UnityEngine;
using UnityEngine.UI;

namespace InterractiveFloor.Unity.Audio
{
    public class SimplePeakDisplay : SceneComponent
    {
        private IDevicePeakSourceFactory peakSourceFactory;
        private IPeakSource peakSource;

        private GameObject screenObject;
        private Image image;

        [AudioDeviceSource]
        public string Device { get; set; }

        public Gradient ColorGradient { get; set; }

        private float velocity = 0.0f;
        public FloatDampSettings PeakDampSettings { get; set; }
        private float targetPeak;
        private float currentPeak;

        protected override void Bootstrap(ISpyConfig spy)
        {
            spy
                .When(() => Device)
                .HasChanged()
                .Do(Setup);

            screenObject = GameObjects
                .CreateNew()
                .AsChildOf(this)
                .Named("screenObject")
                .WithChild(ui => ui
                    .Named("ui")
                    .WithComponent<Canvas>(canvas => canvas
                        .InScreenSpaceOverlay())
                    .WithChild(screen => screen
                        .Named("screen")
                        .WithComponent<Image>(img => img
                        .FillParent())
                    )
                );

            image = screenObject.GetComponentInChildren<Image>();

            peakSourceFactory = new DevicePeakSourceFactory();
        }

        protected override void Tick()
        {
            if (peakSource == null) return;

            if (peakSource.TryGetNextResult(out PeakResult result))
                targetPeak = result.PeakValue;

            currentPeak = Animations.SmoothDamp(currentPeak, targetPeak, ref velocity, PeakDampSettings);

            Color color = ColorGradient.Evaluate(currentPeak);
            image.color = color;
        }

        protected override void OnDisposed() => CleanupResources();

        private void Setup()
        {
            CleanupResources();

            if (Device.IsNullOrEmpty()) return;

            peakSource = peakSourceFactory.Create(this, Device);

            peakSource.Start();
        }

        private void CleanupResources()
        {
            peakSource?.Stop();
            peakSource?.Dispose();
            peakSource = null;
        }
    }
}