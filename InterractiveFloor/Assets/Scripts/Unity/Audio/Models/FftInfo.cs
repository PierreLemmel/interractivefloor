﻿namespace InterractiveFloor.Unity.Audio
{
    public class FftInfo
    {
        public int FftSize { get; }
        public int NbOfChannels { get; }
        public int SampleRate { get; }

        public FftInfo(int fftSize, int nbOfChannels, int sampleRate)
        {
            FftSize = fftSize;
            NbOfChannels = nbOfChannels;
            SampleRate = sampleRate;
        }
    }
}