﻿namespace InterractiveFloor.Unity.Audio
{
    public enum FftWindow
    {
        None = 0,
        Hamming = 1,
        HammingPeriodic = 2,
        Hann = 3,
        HannPeriodic = 4
    } 
}