﻿using System;

namespace InterractiveFloor.Unity.Audio
{
    public class FftResult : Result
    {
        public float[] FftData { get; }
        public FftInfo Info { get; }

        public FftResult(float[] fftData, FftInfo info)
        {
            FftData = fftData ?? throw new ArgumentNullException(nameof(fftData));
            Info = info ?? throw new ArgumentNullException(nameof(info));
        }
    }
}