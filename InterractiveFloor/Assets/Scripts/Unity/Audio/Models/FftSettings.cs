﻿using CSCore.DSP;

namespace InterractiveFloor.Unity.Audio
{
    public class FftSettings : Model
    {
        public FftWindow FftWindow { get; set; } = FftWindow.Hamming;
        public FftSize FftSize { get; set; } = FftSize.Fft1024;
    }
}