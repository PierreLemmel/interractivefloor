﻿namespace InterractiveFloor.Unity.Audio
{
    public class PeakResult : Result
    {
        public float PeakValue { get; }

        public PeakResult(float peakValue)
        {
            PeakValue = peakValue;
        }
    }
}