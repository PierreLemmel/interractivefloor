﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
	public class TypeReference : Model, ISerializationCallbackReceiver
	{
		public TypeReference() { }

		private TypeReference(string assemblyQualifiedName)
		{
			AssemblyQualifiedName = assemblyQualifiedName;
			_type = !string.IsNullOrEmpty(assemblyQualifiedName)
				? Type.GetType(assemblyQualifiedName)
				: null;
		}

		public TypeReference(Type type)
		{
			_type = type;
			AssemblyQualifiedName = type?.AssemblyQualifiedName;
		}

		public string AssemblyQualifiedName { get; set; }

		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (!string.IsNullOrEmpty(AssemblyQualifiedName))
			{
				_type = Type.GetType(AssemblyQualifiedName);

				if (_type == null)
					throw new InvalidOperationException($"Impossible to find type '{AssemblyQualifiedName}'.");
			}
			else
			{
				_type = null;
			}
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize() { }


		private Type _type;
		public Type Type => _type;

		public static implicit operator string(TypeReference typeReference) => typeReference.AssemblyQualifiedName;
		public static implicit operator Type(TypeReference typeReference) => typeReference.Type;
		public static implicit operator TypeReference(Type type) => new TypeReference(type);

		public static TypeReference FromAssemblyQualifiedName(string assemblyQualifiedName) => new TypeReference(assemblyQualifiedName);

		public override string ToString() => Type?.FullName ?? "(None)";

	}
}