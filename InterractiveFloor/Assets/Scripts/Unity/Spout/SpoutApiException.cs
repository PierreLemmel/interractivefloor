﻿using System;
namespace InterractiveFloor.Unity.Spout
{
    internal class SpoutApiException : Exception
    {
        public SpoutApiException(string message) : base(message) { }
    }
}