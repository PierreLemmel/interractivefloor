﻿using UnityEngine;

namespace InterractiveFloor.Unity.Spout
{
    public class SpoutSender : SceneComponent
    {
        public SpoutSenderParameters Parameters { get; set; } = new SpoutSenderParameters();

        private string currentSender = null;
        private RenderTexture texture;
        private GameObject spoutCameraObj = null;

        protected override void Bootstrap(ISpyConfig spy)
        {
            Guard.NotNull(Parameters.SenderName, nameof(Parameters.SenderName));

            SpoutApi.Initialize();
            if (Parameters.DebugInConsole)
                SpoutApi.InitDebug();
        }

        public void StartSender(string sceneName, Camera sceneCamera)
        {
            Guard.NotNull(sceneCamera, nameof(sceneCamera));

            currentSender = $"{Parameters.SenderName} - {sceneName}";

            spoutCameraObj = GameObjects
                .CreateNew()
                .AsChildOf(this)
                .Named("SpoutCameraRoot")
                .WithComponent<Follow>(follow =>
                    follow.Target = sceneCamera.gameObject)
                .WithChild(child => child
                    .Named("Camera")
                    .WithComponent<Camera>(camSetup => camSetup
                        .BasedOn(sceneCamera)
                        .Setup(cam => cam.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 180.0f))
                    )
                );

            Camera currentCamera = spoutCameraObj.GetComponentInChildren<Camera>();

            Vector2Int dimensions = Parameters.TargetDimensions;
            texture = new RenderTexture(dimensions.x, dimensions.y, 16);
            currentCamera.targetTexture = texture;

            SpoutApi.CreateSender(currentSender, texture, Parameters.TextureFormat);
        }

        public void StopCurrentSender()
        {
            if (currentSender is null) return;

            SpoutApi.CloseSender(currentSender);
            GameObject.Destroy(spoutCameraObj);
            currentSender = null;
            texture.Release();
        }

        protected override void Tick()
        {
            if (currentSender is null) return;

            SpoutApi.UpdateSender(currentSender, texture);
        }

        protected override void OnDisposed()
        {
            StopCurrentSender();
            SpoutApi.CleanUp();
        }
    }
}