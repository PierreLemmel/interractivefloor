﻿using UnityEngine;

namespace InterractiveFloor.Unity.Application.Content
{
    public interface IContentBundleStore
    {
        AssetBundleCreateRequest LoadBundle(string bundle);
        ContentManifest LoadManifest(AssetBundle assetBundle);
        void UnloadAllBundles();
        void UnloadBundle(string bundle);
    }
}