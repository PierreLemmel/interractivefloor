﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

using UApplication = UnityEngine.Application;

namespace InterractiveFloor.Unity.Application.Content
{
    public static class ContentBundles
    {
        private static readonly string[] specialBundles = new string[] { "settings" };
        public static string PathToContent => Path.Combine(UApplication.streamingAssetsPath, "content");

        public static IReadOnlyCollection<string> AllBundles => new DirectoryInfo(PathToContent)
            .EnumerateFiles()
            .Where(file => file.Extension.IsNullOrEmpty())
            .Select(file => file.Name)
            .Except(specialBundles)
            .ToList();
    }
}