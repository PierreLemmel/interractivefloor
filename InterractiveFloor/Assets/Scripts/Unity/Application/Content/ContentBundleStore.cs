﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.Unity.Application.Content
{
    public class ContentBundleStore : IContentBundleStore
    {
        private IDictionary<string, AssetBundle> loadedBundles = new Dictionary<string, AssetBundle>(StringComparer.InvariantCultureIgnoreCase);

        public AssetBundleCreateRequest LoadBundle(string bundle)
        {
            if (loadedBundles.ContainsKey(bundle))
                throw new InvalidOperationException($"Bundle already loaded: {bundle}");

            string uri = Path.Combine(ContentBundles.PathToContent, bundle);
            AssetBundleCreateRequest request = AssetBundle.LoadFromFileAsync(uri);
            request.completed += asyncOp =>
            {
                AssetBundleCreateRequest bundleRequest = (AssetBundleCreateRequest)asyncOp;
                loadedBundles.Add(bundle, bundleRequest.assetBundle);
            };

            return request;
        }

        public void UnloadBundle(string bundle)
        {
            if (loadedBundles.TryGetValue(bundle, out AssetBundle assetBundle))
            {
                assetBundle.Unload(true);
                loadedBundles.Remove(bundle);
            }
            else
                throw new InvalidOperationException($"Impossible to unload a non-loaded bundle: '{bundle}'");
        }

        public void UnloadAllBundles()
        {
            IReadOnlyCollection<string> bundlesDeepCopy = loadedBundles.Keys.ToList();
            foreach (string bundle in bundlesDeepCopy)
            {
                UnloadBundle(bundle);
            }
        }

        public ContentManifest LoadManifest(AssetBundle assetBundle)
        {
            TextAsset text = assetBundle.LoadAsset<TextAsset>("manifest.json");
            string json = text.text;

            ContentManifest manifest = JsonConvert.DeserializeObject<ContentManifest>(json);
            return manifest;
        }
    }
}