﻿namespace InterractiveFloor.Unity.Application
{
    public class ContentManifest
    {
        public string EntryPoint { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}