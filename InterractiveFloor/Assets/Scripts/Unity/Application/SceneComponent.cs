﻿using System;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public abstract class SceneComponent : MonoBehaviour
    {
        private readonly ISpy spy = Spy.CreateNew();

        public SceneRoot SceneRoot { get; private set; }
        public TService Resolve<TService>() where TService : class => GetComponents<SceneComponent>()
            .OfType<IServiceProvider<TService>>()
            .FirstOrDefault()
            ?.GetInstance()
            ?? throw new InvalidOperationException($"Impossible to resolve service");

        public TComponent GetComponentInScene<TComponent>() where TComponent : Component => SceneRoot.GetComponentInChildren<TComponent>();

        protected void ClearChildren() => gameObject.ClearChildren();

        private void Awake()
        {
            SceneRoot = GetComponentInParent<SceneRoot>();
            Bootstrap(spy);
        }

        private void Start() => Initialize();
        private void OnEnable() => UponActivation();
        private void OnDisable() => UponDeactivation();
        private void OnDestroy() => OnDisposed();

        private void Update()
        {
            spy.DetectChanges();
            Tick();
        }

        protected virtual void Bootstrap(ISpyConfig spy) { }

        protected virtual void Initialize() { }
        protected virtual void Tick() { }
        protected virtual void UponActivation() { }
        protected virtual void UponDeactivation() { }
        protected virtual void OnDisposed() { }
    }
}