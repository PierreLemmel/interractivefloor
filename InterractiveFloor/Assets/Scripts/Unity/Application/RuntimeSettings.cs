﻿using InterractiveFloor.Unity.Spout;

namespace InterractiveFloor.Unity.Application
{
    public class RuntimeSettings : Model
    {
        public bool EnableSpout { get; set; }

        [DisplayIf(nameof(EnableSpout), DisplayCondition.IsTrue)]
        public SpoutSenderParameters SpoutSettings { get; set; }
    }
}