﻿using InterractiveFloor.Unity.Application.Content;
using InterractiveFloor.Unity.Spout;
using InterractiveFloor.Unity.Windows;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InterractiveFloor.Unity.Application
{
    [RequireComponent(typeof(SceneRoot))]
    public class Runtime : SceneComponent
    {
        public RuntimeSettings Settings { get; set; } = new RuntimeSettings();

        [ContentBundleSource]
        public string CurrentScene { get; set; }
        private string lastScene = null;

        private GameObject scenesRoot;
        private SpoutSender spoutSender = null;

        private IContentBundleStore bundleStore;
        private readonly IDictionary<string, GameObject> scenes = new Dictionary<string, GameObject>() { };

        protected override void Bootstrap(ISpyConfig spy)
        {
            spy
                .When(() => CurrentScene)
                .HasChanged()
                .Do(() => StartCoroutine(SwitchToScene(CurrentScene)));

            spy
                .When(Settings)
                .HasChangesOn(settings => settings.EnableSpout)
                .Do(SetupSpout);

            scenesRoot = GameObjects
                .CreateNew()
                .Named("Scenes");

            bundleStore = new ContentBundleStore();

            AppDomain.CurrentDomain.UnhandledException += OnException;
        }

        protected override void OnDisposed()
        {
            Destroy(scenesRoot);
            AppDomain.CurrentDomain.UnhandledException -= OnException;
            bundleStore.UnloadAllBundles();
        }

        private IEnumerator SwitchToScene(string sceneName)
        {
            string sceneToLoad = sceneName;
            string sceneToUnload = lastScene;

            OnSceneChanged();

            yield return LoadScene(sceneToLoad);
            yield return UnloadScene(sceneToUnload);

            lastScene = sceneToLoad;
        }

        private IEnumerator LoadScene(string sceneToLoad)
        {
            if (sceneToLoad.IsNullOrEmpty()) yield break;

            Debug.Log($"Loading scene '{sceneToLoad}'");

            AssetBundleCreateRequest request = bundleStore.LoadBundle(sceneToLoad);
            yield return Wait.ForAsyncOperation(request);
            
            AssetBundle bundle = request.assetBundle;
            ContentManifest manifest = bundleStore.LoadManifest(bundle);

            string entryPoint = manifest.EntryPoint;

            GameObject contentPrefab = bundle.LoadAsset<GameObject>(entryPoint);

            string sceneObjName = GetSceneObjectNameForScene(sceneToLoad);
            GameObject sceneObject = GameObjects.CreateNew()
                .Named(sceneObjName)
                .AsChildOf(scenesRoot);

            scenes.Add(sceneObjName, sceneObject);

            GameObject content = GameObjects
                .FromPrefab(contentPrefab)
                .AsChildOf(sceneObject);

            if (Settings.EnableSpout)
            {
                Camera sceneCamera = content.GetComponentInChildren<Camera>();

                spoutSender.StopCurrentSender();
                spoutSender.StartSender(sceneToLoad, sceneCamera);
            }

            Debug.Log($"Scene loaded '{sceneToLoad}'");
        }

        private IEnumerator UnloadScene(string sceneToUnload)
        {
            if (sceneToUnload.IsNullOrEmpty()) yield break;

            Debug.Log($"Unloading scene '{sceneToUnload}'");

            string sceneObjName = GetSceneObjectNameForScene(sceneToUnload);
            GameObject sceneToDestroy = scenes[sceneObjName];
            scenes.Remove(sceneObjName);
            Destroy(sceneToDestroy);

            yield return Wait.TillNextFrame;

            bundleStore.UnloadBundle(sceneToUnload);

            Debug.Log($"Scene unloaded '{sceneToUnload}'");
        }

        private void OnSceneChanged()
        {
            Win32Utils.RunCleanupOopsWindowsTask();
        }

        private void OnException(object sender, UnhandledExceptionEventArgs e)
        {
            Debug.LogError(e.ExceptionObject);
        }

        private void SetupSpout()
        {
            if (spoutSender != null)
            {
                DestroyImmediate(spoutSender.gameObject);
                spoutSender = null;
            }

            if (Settings.EnableSpout)
            {
                GameObject spoutSenderObj = GameObjects.CreateNew()
                    .Named("SpoutSender")
                    .WithComponent<SpoutSender>(sender => sender.Parameters = Settings.SpoutSettings)
                    .AsChildOf(this);

                spoutSender = spoutSenderObj.GetComponent<SpoutSender>();
            }
        }

        private static string GetSceneObjectNameForScene(string scene) => $"scene - {scene}";
    }
}