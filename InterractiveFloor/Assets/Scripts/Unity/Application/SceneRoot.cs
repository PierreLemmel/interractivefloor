﻿using InterractiveFloor.Unity.Application;
using System;
using UnityEngine;

namespace InterractiveFloor.Unity
{
    public class SceneRoot : SceneComponent
    {
        public Camera SceneCamera => GetComponentInChildren<Camera>();
        public Runtime Runtime => FindObjectOfType<Runtime>() ?? throw new InvalidOperationException("There is no runtime in this scene");
    }
}