﻿namespace InterractiveFloor.Unity.Gcp
{
    public class GoogleApiSettings : Model
    {
        [Multiline]
        public string JsonCredentials { get; set; }
    }
}