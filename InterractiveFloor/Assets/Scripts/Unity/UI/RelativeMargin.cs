﻿using UnityEngine;

namespace InterractiveFloor.Unity.UI
{
    public class RelativeMargin : Model
    {
        [InRange(0.0f, 1.0f)]
        public float Left { get; set; }

        [InRange(0.0f, 1.0f)]
        public float Right { get; set; }

        [InRange(0.0f, 1.0f)]
        public float Top { get; set; }

        [InRange(0.0f, 1.0f)]
        public float Bottom { get; set; }

        public Vector2 AnchorMin => new Vector2(Left, Bottom);
        public Vector2 AnchorMax => new Vector2(Right, Top);

        public RelativeMargin() : this(0.0f, 0.0f, 0.0f, 0.0f) { }
        public RelativeMargin(float left, float right, float top, float bottom)
        {
            Left = left;
            Right = right;
            Top = top;
            Bottom = bottom;
        }

        public static RelativeMargin NoMargin => new RelativeMargin(0.0f, 1.0f, 1.0f, 0.0f);
    }
}