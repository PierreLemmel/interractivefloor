﻿using CSCore;
using CSCore.SoundIn;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Speech.V1;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Grpc.Auth;
using Grpc.Core;
using InterractiveFloor.Unity.Audio;
using InterractiveFloor.Unity.Gcp;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

using Watch = System.Diagnostics.Stopwatch;

namespace InterractiveFloor.Unity.Speech.Recognition
{
    public class GoogleSpeechRecognizer : ISpeechSource
    {
        private const double TimeoutFromWhichWeCanReset = 40.0;
        private const double GoogleSpeechApiTimeout = 60.0;

        private enum RecognizerState
        {
            Idle,
            Starting,
            Running,
            Stopping,
            Restarting
        }

        private readonly SpeechClient speech;
        private readonly ISoundIn soundIn;
        private readonly GoogleSpeechSettings speechSettings;

        private Watch watch;
        private TimeSpan streamingCallBeginning;

        private RecognizerState state = RecognizerState.Idle;
        private Guid? currentRecognitionId = null;
        private TimeSpan? currentRecognitionStartingTime = null;

        private SpeechClient.StreamingRecognizeStream streamingCall;

        private Task startingTask;
        private Task mainTask;
        private Task stoppingTask;
        private Task restartingTask;

        private object catchupLock = new object();
        private readonly ICollection<byte[]> catchupData = new List<byte[]>();
        private readonly ConcurrentQueue<SpeechSentence> resultQueue;

        private GoogleSpeechRecognizer(SpeechClient speech, ISoundIn waveIn, GoogleSpeechSettings speechSettings)
        {
            this.speech = speech ?? throw new ArgumentNullException(nameof(speech));
            this.soundIn = waveIn ?? throw new ArgumentNullException(nameof(waveIn));
            this.speechSettings = speechSettings ?? throw new ArgumentNullException(nameof(speechSettings));

            resultQueue = new ConcurrentQueue<SpeechSentence>();
        }

        public void Start()
        {
            Debug.Log("Initializing SpeechRecognizer...");
            if (state != RecognizerState.Idle)
                throw new InvalidOperationException($"Speech recognizer shouldn't be started when it's not Idle (state: {state})");

            soundIn.DataAvailable += OnDataAvailable;
            soundIn.Start();

            watch = Watch.StartNew();
            startingTask = Task.Run(CreateStartingTask);

            startingTask.ContinueWith(
                _ => RunMainTask(),
                TaskContinuationOptions.OnlyOnRanToCompletion);

            startingTask.ContinueWith(
                task => LogErrorsForTask(task, "starting task"),
                TaskContinuationOptions.OnlyOnFaulted);
        }

        public void Stop()
        {
            Debug.Log("Stopping SpeechRecognizer...");
            if (state != RecognizerState.Running)
                throw new InvalidOperationException("Speech recognizer not initialized");

            state = RecognizerState.Stopping;

            soundIn.Stop();
            soundIn.DataAvailable -= OnDataAvailable;

            Debug.Log("Marking streaming call as completed");
            stoppingTask = Task.Run(CreateStoppingTask);

            stoppingTask.ContinueWith(
                task => LogErrorsForTask(task, "stopping task"),
                TaskContinuationOptions.OnlyOnFaulted);

            stoppingTask.ContinueWith(
                task => Debug.Log("Streaming call marked as completed"),
                TaskContinuationOptions.OnlyOnRanToCompletion);

            stoppingTask.ContinueWith(_ => watch.Stop());
        }

        ~GoogleSpeechRecognizer()
        {
            soundIn.Dispose();
        }

        public bool TryGetNextResult(out SpeechSentence sentence) => resultQueue.TryDequeue(out sentence);

        private async Task CreateStartingTask()
        {
            Debug.Log("Performing asynchronous initialization");
            state = RecognizerState.Starting;
            await InitializeStreamingCall();

            Debug.Log("SpeechRecognizer started");
        }

        private async Task InitializeStreamingCall()
        {
            streamingCall = speech.StreamingRecognize();
            StreamingRecognizeRequest request = new StreamingRecognizeRequest
            {
                StreamingConfig = new StreamingRecognitionConfig
                {
                    Config = new RecognitionConfig
                    {
                        Encoding = RecognitionConfig.Types.AudioEncoding.Linear16,
                        SampleRateHertz = speechSettings.SampleRate,
                        LanguageCode = speechSettings.Langage,
                        EnableAutomaticPunctuation = true,
                        EnableWordTimeOffsets = true
                    },
                    InterimResults = true
                }
            };

            await streamingCall.WriteAsync(request);
        }

        private async Task CreateMainTask(CancellationTokenSource cancellationSource)
        {
            CancellationToken cancellationToken = cancellationSource.Token;

            Debug.Log("Beginning of speech Api streaming call");
            streamingCallBeginning = watch.Elapsed;

            SendCatchupRequestIfNeeded();

            while (await MoveToNextResponse())
            {
                cancellationToken.ThrowIfCancellationRequested();

                RepeatedField<StreamingRecognitionResult> results = streamingCall.ResponseStream.Current.Results;

                if (results.IsEmpty()) continue;

                bool isResultFinal = HandleApiResult(results);
                if (isResultFinal)
                    CancelTaskIfStreamingCallApproachesFromTimeout();
            }
            Debug.Log("Speech api streaming call ended ");

            async Task<bool> MoveToNextResponse()
            {
                try
                {
                    bool result = await streamingCall.ResponseStream.MoveNext(cancellationToken);
                    return result;
                }
                catch (RpcException ex) when (ex.StatusCode == StatusCode.Cancelled)
                {
                    throw new TaskCanceledException("Rpc call cancelled", ex);
                }
            }

            void CancelTaskIfStreamingCallApproachesFromTimeout()
            {
                TimeSpan timeSinceStreamingCallRunning = watch.Elapsed - streamingCallBeginning;
                if (timeSinceStreamingCallRunning > TimeSpan.FromSeconds(TimeoutFromWhichWeCanReset))
                    cancellationSource.Cancel();
            }

            void SendCatchupRequestIfNeeded()
            {
                lock (catchupLock)
                {
                    if (catchupData.Any())
                    {
                        byte[] data = Arrays.Merge(catchupData);
                        SendRequestToSpeechApi(data);
                        catchupData.Clear();
                    }
                }
            }
        }

        private async Task CreateStoppingTask()
        {
            if (state == RecognizerState.Restarting)
                await restartingTask;

            await streamingCall.WriteCompleteAsync();
            await mainTask;
        }

        private async Task RestartMainTask()
        {
            Debug.Log("Restarting SpeechRecognizer to bypass googlespeech api 1 minute limitation");
            state = RecognizerState.Restarting;
            
            await InitializeStreamingCall();
            RunMainTask();


            Debug.Log("SpeechRecognizer restarded");
        }

        private void RunMainTask()
        {
            CancellationTokenSource source = new CancellationTokenSource();

            mainTask = Task.Run(() => CreateMainTask(source));
            source.CancelAfter(TimeSpan.FromSeconds(GoogleSpeechApiTimeout));

            state = RecognizerState.Running;

            mainTask.ContinueWith(
                task => LogErrorsForTask(task, "mainTask"),
                TaskContinuationOptions.OnlyOnFaulted);

            mainTask.ContinueWith(
                _ => restartingTask = Task.Run(RestartMainTask),
                TaskContinuationOptions.OnlyOnCanceled);

            mainTask.ContinueWith(
                _ => Debug.Log("SpeechRecognizer main task ran to completion"),
                TaskContinuationOptions.OnlyOnRanToCompletion);
        }


        private void LogErrorsForTask(Task task, string taskName)
        {
            Debug.LogError($"Error on {taskName}");
            Debug.LogError(task.Exception.GetType().FullName);
            Debug.LogError(task.Exception.Message);

            foreach (Exception ex in task.Exception.InnerExceptions)
            {
                Debug.LogError($"\t{ex.GetType().FullName}");
                Debug.LogError($"\t{ex.Message}");
            }
        }

        private bool HandleApiResult(RepeatedField<StreamingRecognitionResult> results)
        {
            SpeechSentence result;
            bool isResultFinal;

            if (results.First().IsFinal)
            {
                SpeechRecognitionAlternative alternative = results
                    .Single()
                    .Alternatives
                    .Single();

                IReadOnlyCollection<Word> words = alternative
                    .Words
                    .Select(wordInfo =>
                    {
                        TimeSpan wordStartTime = streamingCallBeginning + wordInfo.StartTime.ToTimeSpan();
                        TimeSpan wordEndTime = streamingCallBeginning + wordInfo.EndTime.ToTimeSpan();
                        string word = wordInfo.Word;

                        return new Word(wordStartTime, wordEndTime, word);
                    })
                    .ToList();

                float confidence = alternative.Confidence;
                TimeSpan startTime = currentRecognitionStartingTime.Value;
                TimeSpan endTime = watch.Elapsed;
                result = new FinalizedSentence(words, confidence, currentRecognitionId.Value, startTime, endTime);
                isResultFinal = true;

                TerminateRecognitionSession();
            }
            else
            {
                if (!currentRecognitionId.HasValue)
                    InitRecognitionSession();

                IReadOnlyCollection<ElaboratingSentenceElement> recognitionResults = results
                    .Select(apiResult =>
                    {
                        SpeechRecognitionAlternative alternative = apiResult.Alternatives.Single();

                        string transcript = alternative.Transcript;
                        bool isStable = apiResult.Stability > 0.5f;

                        return new ElaboratingSentenceElement(transcript, isStable);
                    })
                    .ToList();

                result = new ElaboratingSentence(recognitionResults, currentRecognitionId.Value, currentRecognitionStartingTime.Value);
                isResultFinal = false;
            }

            resultQueue.Enqueue(result);
            return isResultFinal;

            void TerminateRecognitionSession()
            {
                currentRecognitionId = null;
                currentRecognitionStartingTime = null;
            }

            void InitRecognitionSession()
            {
                currentRecognitionId = Guid.NewGuid();
                currentRecognitionStartingTime = watch.Elapsed;
            }
        }

        private void OnDataAvailable(object sender, DataAvailableEventArgs weargs)
        {
            if (state == RecognizerState.Running)
            {
                SendRequestToSpeechApi(weargs.Data, weargs.ByteCount);
            }
            else if (state == RecognizerState.Restarting)
            {
                lock (catchupLock)
                {
                    if (weargs.Data.Length == weargs.ByteCount)
                        catchupData.Add(weargs.Data);
                    else
                    {
                        byte[] data = new byte[weargs.ByteCount];
                        Array.Copy(weargs.Data, data, weargs.ByteCount);
                    }
                }
            }
        }

        private const int WHOLE_ARRAY = -1;
        private void SendRequestToSpeechApi(byte[] data, int count = WHOLE_ARRAY)
        {
            ByteString audioContent = count == WHOLE_ARRAY ?
                ByteString.CopyFrom(data) :
                ByteString.CopyFrom(data, 0, count);
            StreamingRecognizeRequest writeRequest = new StreamingRecognizeRequest { AudioContent = audioContent };

            streamingCall.WriteAsync(writeRequest);
        }

        public static ISpeechSource Create(GoogleApiSettings apiSettings, GoogleSpeechSettings speechSettings, string deviceName)
        {
            GoogleCredential credential = GoogleCredential
                .FromJson(apiSettings.JsonCredentials);
            Channel channel = new Channel(
                SpeechClient.DefaultEndpoint.ToString(),
                credential.ToChannelCredentials());

            SpeechClient speech = SpeechClient.Create(channel);

            WaveFormat format = new WaveFormat(speechSettings.SampleRate, 16, 1);

            WaveInDevice device = deviceName.IsNullOrEmpty() ? WaveInDevice.DefaultDevice : MoreCSCore.GetWaveInDeviceByName(deviceName);
            ISoundIn soundIn = new WaveIn(format)
            {
                Device = device
            };

            soundIn.Initialize();

            GoogleSpeechRecognizer speechRecognizer = new GoogleSpeechRecognizer(speech, soundIn, speechSettings);
            return speechRecognizer;
        }
    }
}