﻿namespace InterractiveFloor.Unity.Speech.Recognition
{
    public class GoogleSpeechSettings : Model
    {
        public string Langage { get; set; } = "fr";

        [InRange(8000, 48000)]
        public int SampleRate { get; set; } = 16000;
    }
}