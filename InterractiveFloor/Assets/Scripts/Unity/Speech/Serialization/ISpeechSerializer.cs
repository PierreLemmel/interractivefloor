﻿namespace InterractiveFloor.Unity.Speech.Serialization
{
    public interface ISpeechSerializer
    {
        void Serialize(SpeechSession session);
    }
}