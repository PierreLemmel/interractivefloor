﻿using Newtonsoft.Json;
using System.IO;
using UnityEngine;
using UApplication = UnityEngine.Application;

namespace InterractiveFloor.Unity.Speech.Serialization
{
    public sealed class SpeechSerializer : ISpeechSerializer
    {
        public void Serialize(SpeechSession session)
        {
            string json = JsonConvert.SerializeObject(session, Formatting.Indented);

            string directory = Path.Combine(UApplication.persistentDataPath, "Speeches");
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            string filename = $"Speech-{session.SessionBeginning:yyyyMMdd-HHmmss}.json";
            string path = Path.Combine(directory, filename);

            File.WriteAllText(path, json);
            Debug.Log($"Speech file saved at: '{path}'");
        }
    }
}