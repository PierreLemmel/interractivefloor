﻿namespace InterractiveFloor.Unity.Speech.Display
{
    public enum SpeechDisplayMode
    {
        OnlyLast,
        SomeResults,
        All
    }
}