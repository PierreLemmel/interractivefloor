﻿namespace InterractiveFloor.Unity.Speech.Display
{
    public enum SpeechDisplaySeparator
    {
        WhiteSpace = 0,
        NewLine = 1
    }
}