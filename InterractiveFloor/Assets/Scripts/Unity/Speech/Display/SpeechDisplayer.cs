﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.Linq;
using System.Text;
using InterractiveFloor.Unity.Speech.Recognition;
using InterractiveFloor.Unity.Gcp;
using InterractiveFloor.Unity.Speech.Serialization;

namespace InterractiveFloor.Unity.Speech.Display
{
    [RequireService(typeof(ISpeechSource))]
    [RequireService(typeof(ISpeechSerializer))]
    public class SpeechDisplayer : SceneComponent
    {
        [AudioCaptureDeviceSource]
        public string InputDevice { get; set; }

        [EditTimeOnly]
        public GoogleApiSettings GoogleApiSettings { get; set; } = new GoogleApiSettings();

        [EditTimeOnly]
        public GoogleSpeechSettings GoogleSpeechSettings { get; set; } = new GoogleSpeechSettings();

        public SpeechDisplaySettings TextSettings { get; set; } = new SpeechDisplaySettings();

        public Sprite Background { get; set; }

        private ISpeechSource speechSource;
        private ISpeechSerializer speechSerializer;
        private TextMeshProUGUI textMesh;

        private readonly List<FinalizedSentence> finalResults = new List<FinalizedSentence>();
        private ElaboratingSentence buildingResult;
        private DateTime? startingTime;

        protected override void Bootstrap(ISpyConfig spy)
        {
            speechSource = GoogleSpeechRecognizer.Create(GoogleApiSettings, GoogleSpeechSettings, InputDevice);
            speechSerializer = new SpeechSerializer();

            GameObject textObject = GameObjects
                .CreateNew()
                .AsChildOf(this)
                .Named("textObject")
                .WithChild(ui => ui
                    .Named("ui")
                    .WithComponent<Canvas>(canvas => canvas.InCameraSpace(SceneRoot.SceneCamera))
                    .WithChild(tmpObj => tmpObj
                        .Named("textMesh")
                        .WithComponent<TextMeshProUGUI>(tmpSetup => tmpSetup
                            .PostInit(tmp =>
                            {
                                RectTransform rect = tmp.rectTransform;

                                rect.localScale = Vector3.one;
                                rect.localPosition = Vector3.zero;

                                rect.offsetMin = Vector2.zero;
                                rect.offsetMax = Vector2.zero;

                                tmp.alignment = TextAlignmentOptions.MidlineGeoAligned;
                                tmp.fontSize = TextSettings.TextSize;
                            })
                        )
                    )
                );
            
            textMesh = textObject.GetComponentInChildren<TextMeshProUGUI>();

            spy
                .When(TextSettings)
                .HasChangesOn(
                    s => s.AutoSize,
                    s => s.MinTextSize,
                    s => s.MaxTextSize,
                    s => s.TextSize,
                    s => s.Font
                )
                .Do(SetupTextSize);

            spy
                .When(TextSettings.Margin)
                .HasChanged()
                .Do(SetupLayout);

            spy
                .When(TextSettings)
                .HasChangesOn(
                    s => s.FinalTextColor,
                    s => s.UnstableTextColor,
                    s => s.StableTextColor,
                    s => s.DisplayMode,
                    s => s.SentencesToDisplay,
                    s => s.Separator
                )
                .Do(UpdateText);
        }

        protected override void Tick()
        {
            while (speechSource.TryGetNextResult(out SpeechSentence result))
            {
                DispatchResult(result);
                UpdateText();
            }
        }

        private void SetupTextSize()
        {
            bool autoSize = TextSettings.AutoSize;
            textMesh.enableAutoSizing = autoSize;

            if (autoSize)
            {
                textMesh.fontSizeMin = TextSettings.MinTextSize;
                textMesh.fontSizeMax = TextSettings.MaxTextSize;
            }
            else
            {
                textMesh.fontSize = TextSettings.TextSize;
            }

            textMesh.font = TextSettings.Font;
        }

        private void SetupLayout()
        {
            textMesh.rectTransform.anchorMin = TextSettings.Margin.AnchorMin;
            textMesh.rectTransform.anchorMax = TextSettings.Margin.AnchorMax;
        }

        private void DispatchResult(SpeechSentence result)
        {
            if (result is FinalizedSentence final)
            {
                finalResults.Add(final);
                buildingResult = null;
            }
            else if (result is ElaboratingSentence temporary)
            {
                buildingResult = temporary;
            }
        }

        private void UpdateText()
        {
            IEnumerable<string> stableTexts;
            IEnumerable<string> unstableTexts;

            bool isBuilding = buildingResult != null;
            if (isBuilding)
            {
                stableTexts = buildingResult.Elements.Where(elt => elt.IsStable).Select(elt => elt.Transcript);
                unstableTexts = buildingResult.Elements.Where(elt => !elt.IsStable).Select(elt => elt.Transcript);
            }
            else
            {
                stableTexts = Enumerable.Empty<string>();
                unstableTexts = Enumerable.Empty<string>();
            }

            int finalSentencesToDisplay;
            switch (TextSettings.DisplayMode)
            {
                case SpeechDisplayMode.OnlyLast:
                    finalSentencesToDisplay = isBuilding ? 0 : 1;
                    break;
                case SpeechDisplayMode.SomeResults:
                    finalSentencesToDisplay = isBuilding ? TextSettings.SentencesToDisplay - 1 : TextSettings.SentencesToDisplay;
                    break;
                case SpeechDisplayMode.All:
                    finalSentencesToDisplay = finalResults.Count;
                    break;
                default:
                    throw new InvalidOperationException($"Unexpected Display Mode: {TextSettings.DisplayMode}");
            }

            finalSentencesToDisplay = Math.Min(finalSentencesToDisplay, finalResults.Count);

            IEnumerable<string> finalTexts = finalResults.NLasts(finalSentencesToDisplay).Select(final => string.Join(" ", final.Words.Select(word => word.Transcript))); 
            DisplayTexts(finalTexts, stableTexts, unstableTexts);
        }

        private void DisplayTexts(IEnumerable<string> finalTexts, IEnumerable<string> stableTexts, IEnumerable<string> unstableTexts)
        {
            StringBuilder sb = new StringBuilder();

            Display(finalTexts, TextSettings.FinalTextColor);
            Display(stableTexts, TextSettings.FinalTextColor);
            Display(unstableTexts, TextSettings.FinalTextColor);

            void Display(IEnumerable<string> results, Color color)
            {
                if (results.IsEmpty()) return;

                IEnumerable<string> chunks = results;
                if (TextSettings.RemoveDiacritics)
                    chunks = chunks.Select(txt => txt.RemoveDiacritics());

                string separator = TextSettings.Separator == SpeechDisplaySeparator.NewLine ? Environment.NewLine : " ";

                sb.Append($"<color={color.ToHexString()}>");
                sb.Append(string.Join(separator, chunks));
                sb.Append("</color>");
            }

            textMesh.text = sb.ToString();
        }

        protected override void UponActivation()
        {
            speechSource.Start();
            startingTime = DateTime.Now;

            textMesh.GetComponent<RectTransform>().offsetMin = Vector2.zero;
            textMesh.GetComponent<RectTransform>().offsetMax = Vector2.zero;
        }

        protected override void UponDeactivation()
        {
            if (speechSource is null) return;

            DateTime endingTime = DateTime.Now;
            speechSource.Stop();

            SpeechSession session = new SpeechSession(startingTime.Value, endingTime, finalResults);

            speechSerializer.Serialize(session);

            startingTime = null;
        }
    }
}