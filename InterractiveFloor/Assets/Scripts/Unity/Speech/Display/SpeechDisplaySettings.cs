﻿using InterractiveFloor.Unity.UI;
using TMPro;
using UnityEngine;

namespace InterractiveFloor.Unity.Speech.Display
{
    public class SpeechDisplaySettings : Model
    {
        private const float MIN_SIZE = 1.0f;
        private const float MAX_SIZE = 200.0f;

        public Color FinalTextColor { get; set; } = Color.white;
        public Color StableTextColor { get; set; } = new Color(0.75f, 0.75f, 0.75f);
        public Color UnstableTextColor { get; set; } = Color.gray;

        public SpeechDisplayMode DisplayMode { get; set; }
        public SpeechDisplaySeparator Separator { get; set; }

        [InRange(2, 10)]
        [DisplayIf(nameof(DisplayMode), DisplayCondition.IsEqualTo, SpeechDisplayMode.SomeResults)]
        public int SentencesToDisplay { get; set; } = 4;

        public bool AutoSize { get; set; } = true;

        [InRange(MIN_SIZE, MAX_SIZE)]
        [DisplayIf(nameof(AutoSize), DisplayCondition.IsFalse)]
        public float TextSize { get; set; } = 75.0f;

        [InRange(MIN_SIZE, MAX_SIZE)]
        [DisplayIf(nameof(AutoSize), DisplayCondition.IsTrue)]
        public float MinTextSize { get; set; } = 25.0f;

        [InRange(MIN_SIZE, MAX_SIZE)]
        [DisplayIf(nameof(AutoSize), DisplayCondition.IsTrue)]
        public float MaxTextSize { get; set; } = 80.0f;

        public RelativeMargin Margin { get; set; } = RelativeMargin.NoMargin;

        public TMP_FontAsset Font { get; set; }

        public bool RemoveDiacritics { get; set; } = false;
    }
}