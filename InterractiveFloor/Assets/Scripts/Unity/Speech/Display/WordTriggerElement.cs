﻿using System.Collections.Generic;
using UnityEngine;

namespace InterractiveFloor.Unity.Speech.Display
{
    public class WordTriggerElement : Model
    {
        public List<string> Words { get; set; }
        public Sprite Image { get; set; }
    }
}
