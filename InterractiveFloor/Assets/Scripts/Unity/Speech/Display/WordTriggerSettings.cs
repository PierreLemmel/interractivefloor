﻿namespace InterractiveFloor.Unity.Speech.Display
{
    public class WordTriggerSettings : Model
    {
        public WordTriggerCondition Condition { get; set; } = WordTriggerCondition.Stable;
        [InRange(0.0f, 3.0f)]
        public float ReloadTime { get; set; } = 2.0f;
    }
}