﻿using InterractiveFloor;
using InterractiveFloor.Unity.Gcp;
using InterractiveFloor.Unity.Speech.Recognition;
using InterractiveFloor.Unity.Speech.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace InterractiveFloor.Unity.Speech.Display
{
    [RequireService(typeof(ISpeechSource))]
    [RequireService(typeof(ISpeechSerializer))]
    public class WordTriggerDisplayer : SceneComponent
    {
        [AudioCaptureDeviceSource]
        public string InputDevice { get; set; }

        [EditTimeOnly]
        public GoogleApiSettings GoogleApiSettings { get; set; } = new GoogleApiSettings();

        [EditTimeOnly]
        public GoogleSpeechSettings GoogleSpeechSettings { get; set; } = new GoogleSpeechSettings();

        [EditTimeOnly]
        public List<WordTriggerElement> Elements { get; set; }
        public WordTriggerSettings Settings { get; set; }

        public AnimationClip ImageAnimation { get; set; }

        private Image image;
        private Animation animationComponent;

        private ISpeechSource speechSource;
        private ISpeechSerializer speechSerializer;

        private IReadOnlyDictionary<string, Sprite> elementsDic;

        private readonly List<FinalizedSentence> finalResults = new List<FinalizedSentence>();

        private DateTime? startingTime;
        private DateTime lastTriggerTime = DateTime.MinValue;

        protected override void Bootstrap(ISpyConfig spy)
        {
            speechSource = GoogleSpeechRecognizer.Create(GoogleApiSettings, GoogleSpeechSettings, InputDevice);
            speechSerializer = new SpeechSerializer();

            GameObject screenObject = GameObjects
                .CreateNew()
                .AsChildOf(this)
                .Named("screenObject")
                .WithChild(ui => ui
                    .Named("ui")
                    .WithComponent<Canvas>(canvas => canvas
                        .InScreenSpaceOverlay())
                    .WithChild(screen => screen
                        .Named("screen")
                        .WithComponent<Image>(img => img
                            .FillParent()
                            .WithColor(new Color(1.0f, 1.0f, 1.0f, 0.0f))
                        )
                        .WithComponent<Animation>(anim => anim
                            .WithClip(ImageAnimation)
                            .NotAutomaticallyPlaying()
                        )
                    )
                );

            image = screenObject.GetComponentInChildren<Image>();
            animationComponent = screenObject.GetComponentInChildren<Animation>();

            elementsDic = Elements
                .SelectMany(elt => elt.Words.Select(word => (Word: word, Image: elt.Image)))
                .ToDictionary(wi => wi.Word, wi => wi.Image, StringComparer.InvariantCultureIgnoreCase);
        }

        private void DispatchResult(SpeechSentence result)
        {
            if (result is FinalizedSentence final)
            {
                finalResults.Add(final);

                IEnumerable<string> words = final.Words.Select(w => w.Transcript);
                CheckWords(words);
            }
            
            if (Settings.Condition == WordTriggerCondition.Stable)
            {
                if (result is ElaboratingSentence temporary)
                {
                    IEnumerable<string> words = temporary.Elements
                        .Where(elt => elt.IsStable)
                        .Select(elt => elt.Transcript);
                    CheckWords(words);
                }
            }
        }

        private void CheckWords(IEnumerable<string> words)
        {
            if ((DateTime.Now - lastTriggerTime).TotalSeconds < Settings.ReloadTime)
                return;

            foreach (string word in words)
            {
                string key = word.TrimPunctuation();

                if (elementsDic.TryGetValue(key, out Sprite sprite))
                {
                    image.sprite = sprite;
                    animationComponent.Play();

                    lastTriggerTime = DateTime.Now;

                    return;
                }
            }
        }

        protected override void UponActivation()
        {
            speechSource.Start();
            startingTime = DateTime.Now;
        }

        protected override void Tick()
        {
            while (speechSource.TryGetNextResult(out SpeechSentence result))
            {
                DispatchResult(result);
            }
        }

        protected override void UponDeactivation()
        {
            DateTime endingTime = DateTime.Now;
            speechSource.Stop();

            SpeechSession session = new SpeechSession(startingTime.Value, endingTime, finalResults);

            speechSerializer.Serialize(session);

            startingTime = null;
        }
    }
}