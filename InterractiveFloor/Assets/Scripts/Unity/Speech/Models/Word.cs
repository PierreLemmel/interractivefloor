﻿using System;

namespace InterractiveFloor.Unity.Speech
{
    public sealed class Word
    {
        public Word(TimeSpan startTime, TimeSpan endTime, string word)
        {
            StartTime = startTime;
            EndTime = endTime;
            Transcript = word ?? throw new ArgumentNullException(nameof(word));
        }

        public TimeSpan StartTime { get; }
        public TimeSpan EndTime { get; }
        public string Transcript { get; }

        public static implicit operator string(Word word) => word.Transcript;
    }
}