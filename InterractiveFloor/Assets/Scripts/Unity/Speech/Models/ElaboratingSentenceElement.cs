﻿using System;

namespace InterractiveFloor.Unity.Speech
{
    public sealed class ElaboratingSentenceElement
    {
        public string Transcript { get; }
        public bool IsStable { get; }

        public ElaboratingSentenceElement(string transcript, bool isStable)
        {
            Transcript = transcript ?? throw new ArgumentNullException(nameof(transcript));
            IsStable = isStable;
        }
    }
}