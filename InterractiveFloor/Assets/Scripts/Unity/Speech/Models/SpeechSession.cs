﻿using System;
using System.Collections.Generic;

namespace InterractiveFloor.Unity.Speech
{
    public class SpeechSession
    {
        public SpeechSession(DateTime sessionBeginning, DateTime sessionEnding, IReadOnlyCollection<FinalizedSentence> sentences)
        {
            SessionBeginning = sessionBeginning;
            SessionEnding = sessionEnding;
            Sentences = sentences ?? throw new ArgumentNullException(nameof(sentences));
        }

        public DateTime SessionBeginning { get; }
        public DateTime SessionEnding { get; }
        public IReadOnlyCollection<FinalizedSentence> Sentences { get; }
    }
}