﻿using System;
using System.Collections.Generic;

namespace InterractiveFloor.Unity.Speech
{
    public sealed class FinalizedSentence : SpeechSentence
    {
        public FinalizedSentence(IReadOnlyCollection<Word> words, float confidence, Guid recognitionId, TimeSpan startTime, TimeSpan endTime)
            : base(recognitionId, startTime)
        {
            EndTime = endTime;
            Words = words ?? throw new ArgumentNullException(nameof(words));
            Confidence = confidence;
        }

        public IReadOnlyCollection<Word> Words { get; }
        public TimeSpan EndTime { get; }
        public float Confidence { get; }
    }
}