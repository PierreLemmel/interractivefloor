﻿using System;
using System.Collections.Generic;

namespace InterractiveFloor.Unity.Speech
{
    public sealed class ElaboratingSentence : SpeechSentence
    {
        public ElaboratingSentence(IReadOnlyCollection<ElaboratingSentenceElement> elements, Guid recognitionId, TimeSpan startTime)
            : base(recognitionId, startTime)
        {
            Elements = elements ?? throw new ArgumentNullException(nameof(elements));
        }

        public IReadOnlyCollection<ElaboratingSentenceElement> Elements { get; }
    }
}