﻿using System;

namespace InterractiveFloor.Unity.Speech
{
    public abstract class SpeechSentence : Result
    {
        public Guid SentenceId { get; }
        public TimeSpan StartTime { get; }

        protected SpeechSentence(Guid sentenceId, TimeSpan startTime)
        {
            SentenceId = sentenceId;
            StartTime = startTime;
        }
    }
}