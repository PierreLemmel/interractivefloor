﻿namespace InterractiveFloor.Unity.Speech.Recognition
{
    public interface ISpeechSource : IStartable, IStopable, IAsyncResultStream<SpeechSentence>
    {
    }
}