﻿using Klak.Ndi;
using System;

namespace InterractiveFloor.Unity.NDI
{
    public static class KlakSetupExtensions
    {
        public static GameObjectBuilder.IComponentSetup<KlakReceiver> Source
            (this GameObjectBuilder.IComponentSetup<KlakReceiver> setup, string sourceName)
            => setup.Setup(kr => kr.sourceName = sourceName);
    }
}