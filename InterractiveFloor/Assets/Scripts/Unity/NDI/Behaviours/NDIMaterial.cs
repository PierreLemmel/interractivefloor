﻿using Klak.Ndi;
using UnityEngine;

namespace InterractiveFloor.Unity.NDI
{
	public class NDIMaterial : SceneComponent, ITargetingMaterial
	{
		private KlakReceiver klakReceiver;
		
		[NDISource]
		public string SourceName { get; set; }

		public Material TargetMaterial { get; set; }

		protected override void Bootstrap(ISpyConfig spy)
		{
			Guard.NotNull(TargetMaterial, nameof(TargetMaterial));

			GameObject klakReceiverObj = GameObjects.CreateNew()
				.Named("Klak Receiver")
				.AsChildOf(this)
				.WithComponent<KlakReceiver>(kr =>
					kr.Source(SourceName));

			klakReceiver = klakReceiverObj.GetComponent<KlakReceiver>();

			spy.When(klakReceiver)
				.HasChangesOn(kr => kr.receivedTexture)
				.Do(SetupScreenMaterial);

			spy.When(() => SourceName)
				.HasChanged()
				.Do(SetupSource);
		}

		private void SetupSource() => klakReceiver.sourceName = SourceName;
		private void SetupScreenMaterial() => TargetMaterial.mainTexture = klakReceiver.receivedTexture;
	} 
}