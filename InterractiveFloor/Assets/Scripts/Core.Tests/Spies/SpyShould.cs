﻿using AutoFixture;
using InterractiveFloor.Tests.Spies.Samples;
using NFluent;
using NUnit.Framework;
using System;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.Tests.Spies
{
    [TestFixture]
    public class SpyShould
    {
        private readonly IFixture fixture = new Fixture();

        private static void SomeAction() { }

        [Test]
        public void SimpleSpy_Should_Detect_Changes_On_First_Call()
        {
            SomeClass someObj = fixture.Create<SomeClass>();

            bool triggered = false;

            ISpy spy = Spy.CreateNew();
            spy
                .When(someObj)
                .HasChanged()
                .Do(() => triggered = true);

            spy.DetectChanges();

            Check.That(triggered).IsTrue();
        }

        [Test]
        public void MultiSpy_Should_Detect_All_Changes_On_First_Call()
        {
            SomeClass someObj = fixture.Create<SomeClass>();
            SomeOtherClass someOtherObj = fixture.Create<SomeOtherClass>();
            YetAnotherClass yetAnotherObj = fixture.Create<YetAnotherClass>();

            bool triggered1 = false;
            bool triggered2 = false;
            bool triggered3 = false;

            ISpy spy = Spy.CreateNew();
            spy
                .When(someObj)
                .HasChanged()
                .Do(() => triggered1 = true);

            spy
                .When(someOtherObj)
                .HasChanged()
                .Do(() => triggered2 = true);

            spy
                .When(yetAnotherObj)
                .HasChangesOn(yao => yao.Foo2)
                .Do(() => triggered3 = true);

            spy.DetectChanges();

            Check.That(triggered1).IsTrue();
            Check.That(triggered2).IsTrue();
            Check.That(triggered3).IsTrue();
        }

        [Test]
        public void SimpleSpy_Should_Not_Trigger_When_Nothing_Has_Changed()
        {
            SomeClass someObj = fixture.Create<SomeClass>();

            bool warmedUp = false;
            bool triggered1 = false;

            ISpy spy = Spy.CreateNew();
            spy
                .When(someObj)
                .HasChanged()
                .Do(() => triggered1 |= warmedUp);

            spy.DetectChanges();
            warmedUp = true;

            spy.DetectChanges();

            Check.That(triggered1).IsFalse();
        }

        [Test]
        public void SimpleSpy_Should_Trigger_When_Something_Has_Changed()
        {
            SomeClass someObj = fixture.Create<SomeClass>();

            bool warmedUp = false;
            bool triggered1 = false;

            ISpy spy = Spy.CreateNew();
            spy
                .When(someObj)
                .HasChanged()
                .Do(() => triggered1 |= warmedUp);

            spy.DetectChanges();
            warmedUp = true;

            someObj.IntProp = fixture.Create<int>();

            spy.DetectChanges();

            Check.That(triggered1).IsTrue();
        }

        [Test]
        public void MultiSpy_Should_Trigger_Only_The_Adequate_Action()
        {
            SomeClass someObj = fixture.Create<SomeClass>();
            SomeOtherClass someOtherObj = fixture.Create<SomeOtherClass>();

            bool warmedUp = false;
            bool triggered1 = false;
            bool triggered2 = false;

            ISpy spy = Spy.CreateNew();
            spy
                .When(someObj)
                .HasChanged()
                .Do(() => triggered1 |= warmedUp);

            spy
                .When(someOtherObj)
                .HasChangesOn(soo => soo.Foo)
                .Do(() => triggered2 |= warmedUp);

            spy.DetectChanges();
            warmedUp = true;

            someOtherObj.Foo = fixture.Create<int>();

            spy.DetectChanges();

            Check.That(triggered1).IsFalse();
            Check.That(triggered2).IsTrue();
        }

        [Test]
        public void MultiSpy_On_Same_Object_Should_Trigger_Only_The_Adequate_Action()
        {
            SomeClass someObj = fixture.Create<SomeClass>();

            bool warmedUp = false;
            bool triggered1 = false;
            bool triggered2 = false;
            bool triggered3 = false;

            ISpy spy = Spy.CreateNew();
            spy
                .When(someObj)
                .HasChangesOn(
                    so => so.IntProp,
                    so => so.OtherIntProp)
                .Do(() => triggered1 |= warmedUp);

            spy
                .When(someObj)
                .HasChangesOn(
                    so => so.FloatProp,
                    so => so.StringProp,
                    so => so.VectorProp)
                .Do(() => triggered2 |= warmedUp);

            spy
                .When(someObj)
                .HasChanged()
                .Do(() => triggered3 |= warmedUp);

            spy.DetectChanges();
            warmedUp = true;

            someObj.IntProp = fixture.Create<int>();

            spy.DetectChanges();

            Check.That(triggered1).IsTrue();
            Check.That(triggered2).IsFalse();
            Check.That(triggered3).IsTrue();
        }
        
        [Test]
        public void SimpleSpy_Should_Handle_Int32_Type()
        {
#pragma warning disable CS0219 // Variable is assigned but its value is never used
            bool triggered = false;
#pragma warning restore CS0219 // Variable is assigned but its value is never used
            string someString = fixture.Create<string>();

            Check.ThatCode(() =>
            {
                ISpy spy = Spy.CreateNew();
                spy
                    .When(() => someString)
                    .HasChanged()
                    .Do(() => triggered = true);
            }).DoesNotThrow();
        }

        [Test]
        public void SimpleSpy_Should_Handle_String_Type()
        {
#pragma warning disable CS0219 // Variable is assigned but its value is never used
            bool triggered = false;
#pragma warning restore CS0219 // Variable is assigned but its value is never used
            int someInt = fixture.Create<int>();

            Check.ThatCode(() =>
            {
                ISpy spy = Spy.CreateNew();
                spy
                    .When(() => someInt)
                    .HasChanged()
                    .Do(() => triggered = true);
            }).DoesNotThrow();
        }

        [Test]
        public void SimpleSpy_Should_Handle_Vector3_Type()
        {
#pragma warning disable CS0219 // Variable is assigned but its value is never used
            bool triggered = false;
#pragma warning restore CS0219 // Variable is assigned but its value is never used
            Vector3 someVect = fixture.Create<Vector3>();

            Check.ThatCode(() =>
            {
                ISpy spy = Spy.CreateNew();
                spy
                    .When(() => someVect)
                    .HasChanged()
                    .Do(() => triggered = true);
            }).DoesNotThrow();
        }

        [Test]
        public void SimpleBind_Should_Bind_A_Single_Property_Without_Throwing()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeOtherClass target = fixture.Create<SomeOtherClass>();

            Check.ThatCode(() =>
            {
                spy.Bind(target, t => t.Foo)
                    .To(source, s => s.IntProp);
            }).DoesNotThrow();
        }

        [Test]
        public void SimpleBind_Should_Ensure_That_Target_Value_Matches_Source_Value()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeOtherClass target = fixture.Create<SomeOtherClass>();

            spy.Bind(target, t => t.Foo)
                .To(source, s => s.IntProp);

            spy.DetectChanges();

            Check.That(target.Foo)
                .IsEqualTo(source.IntProp);
        }

        [Test]
        public void MultiBind_Should_Bind_2_Properties_Without_Throwing()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            Check.ThatCode(() =>
            {
                spy.Bind(target,
                        t => t.IntProp,
                        t => t.FloatProp)
                    .To(source,
                        s => s.IntProp,
                        s => s.FloatProp);
            }).DoesNotThrow();
        }

        [Test]
        public void MultiBind_Should_Bind_3_Properties_Without_Throwing()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            Check.ThatCode(() =>
            {
                spy.Bind(target,
                        t => t.IntProp,
                        t => t.FloatProp,
                        t => t.StringProp)
                    .To(source,
                        s => s.IntProp,
                        s => s.FloatProp,
                        s => s.StringProp);
            }).DoesNotThrow();
        }

        [Test]
        public void MultiBind_Should_Bind_4_Properties_Without_Throwing()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            Check.ThatCode(() =>
            {
                spy.Bind(target,
                        t => t.IntProp,
                        t => t.FloatProp,
                        t => t.StringProp,
                        t => t.VectorProp)
                    .To(source,
                        s => s.IntProp,
                        s => s.FloatProp,
                        s => s.StringProp,
                        s => s.VectorProp);
            }).DoesNotThrow();
        }

        [Test]
        public void MultiBind_Should_Bind_5_Properties_Without_Throwing()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            Check.ThatCode(() =>
            {
                spy.Bind(target,
                        t => t.IntProp,
                        t => t.FloatProp,
                        t => t.StringProp,
                        t => t.VectorProp,
                        t => t.IntArrayProp)
                    .To(source,
                        s => s.IntProp,
                        s => s.FloatProp,
                        s => s.StringProp,
                        s => s.VectorProp,
                        s => s.IntArrayProp);
            }).DoesNotThrow();
        }

        [Test]
        public void MultiBind_Should_Bind_6_Properties_Without_Throwing()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            Check.ThatCode(() =>
            {
                spy.Bind(target,
                        t => t.IntProp,
                        t => t.FloatProp,
                        t => t.StringProp,
                        t => t.VectorProp,
                        t => t.IntArrayProp,
                        t => t.SomeRefProp)
                    .To(source,
                        s => s.IntProp,
                        s => s.FloatProp,
                        s => s.StringProp,
                        s => s.VectorProp,
                        s => s.IntArrayProp,
                        s => s.SomeRefProp);
            }).DoesNotThrow();
        }

        [Test]
        public void MultiBind_Should_Bind_7_Properties_Without_Throwing()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            Check.ThatCode(() =>
            {
                spy.Bind(target,
                        t => t.IntProp,
                        t => t.FloatProp,
                        t => t.StringProp,
                        t => t.VectorProp,
                        t => t.IntArrayProp,
                        t => t.SomeRefProp,
                        t => t.OtherIntProp)
                    .To(source,
                        s => s.IntProp,
                        s => s.FloatProp,
                        s => s.StringProp,
                        s => s.VectorProp,
                        s => s.IntArrayProp,
                        s => s.SomeRefProp,
                        s => s.OtherIntProp);
            }).DoesNotThrow();
        }

        [Test]
        public void MultiBind_Should_Bind_8_Properties_Without_Throwing()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            Check.ThatCode(() =>
            {
                spy.Bind(target,
                        t => t.IntProp,
                        t => t.FloatProp,
                        t => t.StringProp,
                        t => t.VectorProp,
                        t => t.IntArrayProp,
                        t => t.SomeRefProp,
                        t => t.OtherIntProp,
                        t => t.OtherFloatProp)
                    .To(source,
                        s => s.IntProp,
                        s => s.FloatProp,
                        s => s.StringProp,
                        s => s.VectorProp,
                        s => s.IntArrayProp,
                        s => s.SomeRefProp,
                        s => s.OtherIntProp,
                        s => s.OtherFloatProp);
            }).DoesNotThrow();
        }

        [Test]
        public void MultiBind_2_Should_Ensure_That_Target_Values_Matches_Source_Values()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            spy.Bind(target,
                    t => t.IntProp,
                    t => t.FloatProp)
                .To(source,
                    s => s.IntProp,
                    s => s.FloatProp);

            spy.DetectChanges();

            Check.That(target.IntProp)
                .IsEqualTo(source.IntProp);

            Check.That(target.FloatProp)
                .IsEqualTo(source.FloatProp);
        }

        [Test]
        public void MultiBind_3_Should_Ensure_That_Target_Values_Matches_Source_Values()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            spy.Bind(target,
                    t => t.IntProp,
                    t => t.FloatProp,
                    t => t.StringProp)
                .To(source,
                    s => s.IntProp,
                    s => s.FloatProp,
                    s => s.StringProp);

            spy.DetectChanges();

            Check.That(target.IntProp)
                .IsEqualTo(source.IntProp);

            Check.That(target.FloatProp)
                .IsEqualTo(source.FloatProp);

            Check.That(target.StringProp)
                .IsEqualTo(source.StringProp);
        }

        [Test]
        public void MultiBind_4_Should_Ensure_That_Target_Values_Matches_Source_Values()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            spy.Bind(target,
                    t => t.IntProp,
                    t => t.FloatProp,
                    t => t.StringProp,
                    t => t.VectorProp)
                .To(source,
                    s => s.IntProp,
                    s => s.FloatProp,
                    s => s.StringProp,
                    s => s.VectorProp);

            spy.DetectChanges();

            Check.That(target.IntProp)
                .IsEqualTo(source.IntProp);

            Check.That(target.FloatProp)
                .IsEqualTo(source.FloatProp);

            Check.That(target.StringProp)
                .IsEqualTo(source.StringProp);

            Check.That(target.VectorProp)
                .IsEqualTo(source.VectorProp);
        }

        [Test]
        public void MultiBind_5_Should_Ensure_That_Target_Values_Matches_Source_Values()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            spy.Bind(target,
                    t => t.IntProp,
                    t => t.FloatProp,
                    t => t.StringProp,
                    t => t.VectorProp,
                    t => t.IntArrayProp,
                    t => t.SomeRefProp)
                .To(source,
                    s => s.IntProp,
                    s => s.FloatProp,
                    s => s.StringProp,
                    s => s.VectorProp,
                    s => s.IntArrayProp,
                    s => s.SomeRefProp);

            spy.DetectChanges();

            Check.That(target.IntProp)
                .IsEqualTo(source.IntProp);

            Check.That(target.FloatProp)
                .IsEqualTo(source.FloatProp);

            Check.That(target.StringProp)
                .IsEqualTo(source.StringProp);

            Check.That(target.VectorProp)
                .IsEqualTo(source.VectorProp);

            Check.That(target.IntArrayProp)
                .IsEqualTo(source.IntArrayProp);
        }

        [Test]
        public void MultiBind_6_Should_Ensure_That_Target_Values_Matches_Source_Values()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            spy.Bind(target,
                    t => t.IntProp,
                    t => t.FloatProp,
                    t => t.StringProp,
                    t => t.VectorProp,
                    t => t.IntArrayProp,
                    t => t.SomeRefProp)
                .To(source,
                    s => s.IntProp,
                    s => s.FloatProp,
                    s => s.StringProp,
                    s => s.VectorProp,
                    s => s.IntArrayProp,
                    s => s.SomeRefProp);

            spy.DetectChanges();

            Check.That(target.IntProp)
                .IsEqualTo(source.IntProp);

            Check.That(target.FloatProp)
                .IsEqualTo(source.FloatProp);

            Check.That(target.StringProp)
                .IsEqualTo(source.StringProp);

            Check.That(target.VectorProp)
                .IsEqualTo(source.VectorProp);

            Check.That(target.IntArrayProp)
                .IsEqualTo(source.IntArrayProp);

            Check.That(target.SomeRefProp)
                .IsEqualTo(source.SomeRefProp);
        }

        [Test]
        public void MultiBind_7_Should_Ensure_That_Target_Values_Matches_Source_Values()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            spy.Bind(target,
                    t => t.IntProp,
                    t => t.FloatProp,
                    t => t.StringProp,
                    t => t.VectorProp,
                    t => t.IntArrayProp,
                    t => t.SomeRefProp,
                    t => t.OtherIntProp)
                .To(source,
                    s => s.IntProp,
                    s => s.FloatProp,
                    s => s.StringProp,
                    s => s.VectorProp,
                    s => s.IntArrayProp,
                    s => s.SomeRefProp,
                    s => s.OtherIntProp);

            spy.DetectChanges();

            Check.That(target.IntProp)
                .IsEqualTo(source.IntProp);

            Check.That(target.FloatProp)
                .IsEqualTo(source.FloatProp);

            Check.That(target.StringProp)
                .IsEqualTo(source.StringProp);

            Check.That(target.VectorProp)
                .IsEqualTo(source.VectorProp);

            Check.That(target.IntArrayProp)
                .IsEqualTo(source.IntArrayProp);

            Check.That(target.SomeRefProp)
                .IsEqualTo(source.SomeRefProp);

            Check.That(target.OtherIntProp)
                .IsEqualTo(source.OtherIntProp);
        }

        [Test]
        public void MultiBind_8_Should_Ensure_That_Target_Values_Matches_Source_Values()
        {
            ISpy spy = Spy.CreateNew();

            SomeClass source = fixture.Create<SomeClass>();
            SomeClass target = fixture.Create<SomeClass>();

            spy.Bind(target,
                    t => t.IntProp,
                    t => t.FloatProp,
                    t => t.StringProp,
                    t => t.VectorProp,
                    t => t.IntArrayProp,
                    t => t.SomeRefProp,
                    t => t.OtherIntProp,
                    t => t.OtherFloatProp)
                .To(source,
                    s => s.IntProp,
                    s => s.FloatProp,
                    s => s.StringProp,
                    s => s.VectorProp,
                    s => s.IntArrayProp,
                    s => s.SomeRefProp,
                    s => s.OtherIntProp,
                    s => s.OtherFloatProp);

            spy.DetectChanges();

            Check.That(target.IntProp)
                .IsEqualTo(source.IntProp);

            Check.That(target.FloatProp)
                .IsEqualTo(source.FloatProp);

            Check.That(target.StringProp)
                .IsEqualTo(source.StringProp);

            Check.That(target.VectorProp)
                .IsEqualTo(source.VectorProp);

            Check.That(target.IntArrayProp)
                .IsEqualTo(source.IntArrayProp);

            Check.That(target.SomeRefProp)
                .IsEqualTo(source.SomeRefProp);

            Check.That(target.OtherIntProp)
                .IsEqualTo(source.OtherIntProp);

            Check.That(target.OtherFloatProp)
                .IsEqualTo(source.OtherFloatProp);
        }

        [Test]
        public void ValueArraySpy_Should_Bind_Without_Throwing()
        {
            Check.ThatCode(() =>
            {
                ISpy spy = Spy.CreateNew();

                int[] arr = fixture.CreateMany<int>().ToArray();

                spy.WhenAny(() => arr)
                    .HasChanged()
                    .Do(SomeAction);

            }).DoesNotThrow();
        }

        [Test]
        public void ValueArraySpy_Should_Trigger_On_First_Call()
        {
            ISpy spy = Spy.CreateNew();

            int[] arr = fixture.CreateMany<int>().ToArray();

            bool triggered = false;

            spy.WhenAny(() => arr)
                .HasChanged()
                .Do(() => triggered = true);

            spy.DetectChanges();

            Check.That(triggered)
                .IsTrue();
        }

        [Test]
        public void ValueArraySpy_Should_Not_Trigger_When_Nothing_Has_Changed()
        {
            ISpy spy = Spy.CreateNew();

            int[] arr = fixture.CreateMany<int>().ToArray();

            bool warmedUp = false;
            bool triggered = false;

            spy
                .WhenAny(arr)
                .HasChanged()
                .Do(() => triggered |= warmedUp);

            spy.DetectChanges();
            warmedUp = true;

            spy.DetectChanges();

            Check.That(triggered)
                .IsFalse();
        }

        [Test]
        public void ValueArraySpy_Should_Trigger_When_Something_Has_Changed()
        {
            Check.ThatCode(() =>
            {
                ISpy spy = Spy.CreateNew();

                int[] arr = fixture.CreateMany<int>().ToArray();

                spy.WhenAny(() => arr)
                    .HasChanged()
                    .Do(SomeAction);

            }).DoesNotThrow();
        }
    }
}