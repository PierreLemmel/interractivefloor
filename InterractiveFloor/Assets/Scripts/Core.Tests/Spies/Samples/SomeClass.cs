﻿using UnityEngine;

namespace InterractiveFloor.Tests.Spies.Samples
{
    public class SomeClass
    {
        public int IntProp { get; set; }
        public float FloatProp { get; set; }
        public string StringProp { get; set; }
        public Vector2 VectorProp { get; set; }
        public int[] IntArrayProp { get; set; }
        public SomeOtherClass SomeRefProp { get; set; }
        public int OtherIntProp { get; set; }
        public float OtherFloatProp { get; set; }

        public Vector3 SomePropertyThatISwearToGodThatIWontObserve { get; set; }
    }
}