﻿using UnityEngine;

namespace InterractiveFloor.Tests.Spies.Samples
{
    public class SomeClassWith3Properties
    {
        public int Prop1 { get; set; }
        public float Prop2 { get; set; }
        public string Prop3 { get; set; }
    }
}