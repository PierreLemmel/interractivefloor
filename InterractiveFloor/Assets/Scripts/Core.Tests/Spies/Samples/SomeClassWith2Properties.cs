﻿using UnityEngine;

namespace InterractiveFloor.Tests.Spies.Samples
{
    public class SomeClassWith2Properties
    {
        public int Prop1 { get; set; }
        public float Prop2 { get; set; }
    }
}