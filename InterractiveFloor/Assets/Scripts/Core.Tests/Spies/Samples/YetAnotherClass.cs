﻿using System;

namespace InterractiveFloor.Tests.Spies.Samples
{
    public class YetAnotherClass
    {
        public int Foo2 { get; set; }
        public string Bat { get; set; }
        public DateTime Baz { get; set; }
    }
}