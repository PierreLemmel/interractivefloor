﻿using UnityEngine;

namespace InterractiveFloor.Tests.Spies.Samples
{
    public class SomeClassWith6Properties
    {
        public int Prop1 { get; set; }
        public float Prop2 { get; set; }
        public string Prop3 { get; set; }
        public float Prop4 { get; set; }
        public int Prop5 { get; set; }
        public Vector2 Prop6 { get; set; }
    }
}