﻿namespace InterractiveFloor.Tests.Spies.Samples
{
    public class SomeOtherClass
    {
        public int Foo { get; set; }
        public float Bar { get; set; }
    }
}