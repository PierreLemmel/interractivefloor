﻿using AutoFixture;
using AutoFixture.Kernel;
using InterractiveFloor.Spies.Checkers;
using InterractiveFloor.Tests.Spies.Samples;
using NFluent;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.Tests.Spies
{
    [TestFixture]
    public class CheckersShould
    {
        private readonly IFixture fixture = new Fixture();

        #region 1 argument
        [Test]
        public void Checker1_Should_Handle_Null_Value()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.SomeRefProp, (SomeOtherClass)null)
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperty(m => m.SomeRefProp);

            Check.ThatCode(() =>
                {
                    bool firstCallResult = Checker.CheckForChanges(model);
                    bool hasChanged = Checker.CheckForChanges(model);
                })
                .DoesNotThrow();
        }

        [Test]
        public void Checker1_Should_Handle_Null_String_Value()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.StringProp, (string)null)
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperty(m => m.StringProp);

            Check.ThatCode(() => Checker.CheckForChanges(model))
                .DoesNotThrow();
        }

        [Test]
        public void Checker1_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperty(m => m.IntProp);

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker1_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperty(m => m.IntProp);

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker1_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Unobserved_Properties_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperty(m => m.IntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomePropertyThatISwearToGodThatIWontObserve = fixture.Create<Vector3>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker1_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperty(m => m.IntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker1_Should_Not_Return_True_On_Has_Changed_During_More_Than_One_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperty(m => m.IntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool firstSubsequentCall = Checker.CheckForChanges(model);
            bool secondSubsequentCall = Checker.CheckForChanges(model);

            Check.That(secondSubsequentCall).IsFalse();
        }
        #endregion

        #region 2 arguments
        [Test]
        public void Checker2_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp);

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker2_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker2_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Unobserved_Properties_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomePropertyThatISwearToGodThatIWontObserve = fixture.Create<Vector3>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker2_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_First_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker2_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Second_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.FloatProp = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker2_Should_Not_Return_True_On_Has_Changed_During_More_Than_One_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool firstSubsequentCall = Checker.CheckForChanges(model);
            bool secondSubsequentCall = Checker.CheckForChanges(model);

            Check.That(secondSubsequentCall).IsFalse();
        }
        #endregion

        #region 3 arguments
        [Test]
        public void Checker3_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp);

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker3_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp);

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker3_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Unobserved_Properties_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomePropertyThatISwearToGodThatIWontObserve = fixture.Create<Vector3>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker3_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_First_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker3_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Second_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.FloatProp = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker3_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Third_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.StringProp = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker3_Should_Not_Return_True_On_Has_Changed_During_More_Than_One_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool firstSubsequentCall = Checker.CheckForChanges(model);
            bool secondSubsequentCall = Checker.CheckForChanges(model);

            Check.That(secondSubsequentCall).IsFalse();
        }
        #endregion

        #region 4 arguments
        [Test]
        public void Checker4_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp);

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker4_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp);

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker4_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Unobserved_Properties_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomePropertyThatISwearToGodThatIWontObserve = fixture.Create<Vector3>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker4_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_First_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker4_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Second_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.FloatProp = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker4_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Third_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.StringProp = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker4_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Fourth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.VectorProp = fixture.Create<Vector2>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker4_Should_Not_Return_True_On_Has_Changed_During_More_Than_One_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool firstSubsequentCall = Checker.CheckForChanges(model);
            bool secondSubsequentCall = Checker.CheckForChanges(model);

            Check.That(secondSubsequentCall).IsFalse();
        }
        #endregion

        #region 5 arguments
        [Test]
        public void Checker5_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo);

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker5_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo);

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker5_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Unobserved_Properties_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomePropertyThatISwearToGodThatIWontObserve = fixture.Create<Vector3>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker5_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_First_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker5_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Second_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.FloatProp = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker5_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Third_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.StringProp = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker5_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Fourth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.VectorProp = fixture.Create<Vector2>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker5_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Fifth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomeRefProp.Foo = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker5_Should_Not_Return_True_On_Has_Changed_During_More_Than_One_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool firstSubsequentCall = Checker.CheckForChanges(model);
            bool secondSubsequentCall = Checker.CheckForChanges(model);

            Check.That(secondSubsequentCall).IsFalse();
        }
        #endregion

        #region 6 arguments
        [Test]
        public void Checker6_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp);

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker6_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp);

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker6_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Unobserved_Properties_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomePropertyThatISwearToGodThatIWontObserve = fixture.Create<Vector3>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker6_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_First_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker6_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Second_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.FloatProp = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker6_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Third_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.StringProp = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker6_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Fourth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.VectorProp = fixture.Create<Vector2>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker6_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Fifth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomeRefProp.Foo = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker6_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Sixth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.OtherIntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker6_Should_Not_Return_True_On_Has_Changed_During_More_Than_One_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool firstSubsequentCall = Checker.CheckForChanges(model);
            bool secondSubsequentCall = Checker.CheckForChanges(model);

            Check.That(secondSubsequentCall).IsFalse();
        }
        #endregion

        #region 7 arguments
        [Test]
        public void Checker7_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker7_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker7_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Unobserved_Properties_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomePropertyThatISwearToGodThatIWontObserve = fixture.Create<Vector3>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker7_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_First_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker7_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Second_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.FloatProp = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker7_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Third_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.StringProp = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker7_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Fourth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.VectorProp = fixture.Create<Vector2>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker7_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Fifth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomeRefProp.Foo = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker7_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Sixth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.OtherIntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker7_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Seventh_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.OtherFloatProp = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker7_Should_Not_Return_True_On_Has_Changed_During_More_Than_One_Call()
        {
            SomeClass model = fixture.Create<SomeClass>();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool firstSubsequentCall = Checker.CheckForChanges(model);
            bool secondSubsequentCall = Checker.CheckForChanges(model);

            Check.That(secondSubsequentCall).IsFalse();
        }
        #endregion

        #region 8 arguments
        [Test]
        public void Checker8_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker8_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker8_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Unobserved_Properties_Have_Changed()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomePropertyThatISwearToGodThatIWontObserve = fixture.Create<Vector3>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker8_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_First_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker8_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Second_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.FloatProp = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker8_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Third_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.StringProp = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker8_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Fourth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.VectorProp = fixture.Create<Vector2>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker8_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Fifth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.SomeRefProp.Foo = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker8_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Sixth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.OtherIntProp = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker8_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Seventh_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.OtherFloatProp = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker8_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Eighth_Observed_Property_Have_Changed()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntArrayProp[8] = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker8_Should_Not_Return_True_On_Has_Changed_During_More_Than_One_Call()
        {
            SomeClass model = fixture.Build<SomeClass>()
                .With(m => m.IntArrayProp, fixture.CreateMany<int>(12).ToArray())
                .Create();

            IChecker<SomeClass> Checker = CheckerFactory<SomeClass>
                .ForProperties(
                    m => m.IntProp,
                    m => m.FloatProp,
                    m => m.StringProp,
                    m => m.VectorProp,
                    m => m.SomeRefProp.Foo,
                    m => m.OtherIntProp,
                    m => m.OtherFloatProp,
                    m => m.IntArrayProp[8]);

            bool firstCallResult = Checker.CheckForChanges(model);

            model.IntProp = fixture.Create<int>();
            bool firstSubsequentCall = Checker.CheckForChanges(model);
            bool secondSubsequentCall = Checker.CheckForChanges(model);

            Check.That(secondSubsequentCall).IsFalse();
        }
        #endregion

        #region AllProperties
        #region Guards
        [Test]
        public void Checker_FromAllProperties_Should_Throw_InvalidOperationException_When_Provided_Type_Implements_IEquatable_Int32()
        {
            Check.ThatCode(() =>
            {
                IChecker<int> Checker = CheckerFactory<int>
                    .ForAllProperties();
            }).Throws<InvalidOperationException>();
        }

        [Test]
        public void Checker_FromAllProperties_Should_Throw_InvalidOperationException_When_Provided_Type_Implements_IEquatable_String()
        {
            Check.ThatCode(() =>
            {
                IChecker<string> Checker = CheckerFactory<string>
                    .ForAllProperties();
            }).Throws<InvalidOperationException>();
        }

        [Test]
        public void Checker_FromAllProperties_Should_Throw_InvalidOperationException_When_Provided_Type_Implements_IEquatable_Vector3()
        {
            Check.ThatCode(() =>
            {
                IChecker<Vector3> Checker = CheckerFactory<Vector3>
                    .ForAllProperties();
            }).Throws<InvalidOperationException>();
        }
        #endregion

        #region 1 Property
        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith1Property_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<SomeClassWith1Property> Checker = CheckerFactory<SomeClassWith1Property>
                    .ForAllProperties();
            }).DoesNotThrow();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith1Property_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClassWith1Property model = fixture.Create<SomeClassWith1Property>();

            IChecker<SomeClassWith1Property> Checker = CheckerFactory<SomeClassWith1Property>
                .ForAllProperties();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith1Property_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClassWith1Property model = fixture.Create<SomeClassWith1Property>();

            IChecker<SomeClassWith1Property> Checker = CheckerFactory<SomeClassWith1Property>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith1Property_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            SomeClassWith1Property model = fixture.Create<SomeClassWith1Property>();

            IChecker<SomeClassWith1Property> Checker = CheckerFactory<SomeClassWith1Property>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop1 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion

        #region 2 Properties
        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith2Properties_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<SomeClassWith2Properties> Checker = CheckerFactory<SomeClassWith2Properties>
                    .ForAllProperties();
            }).DoesNotThrow();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith2Properties_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClassWith2Properties model = fixture.Create<SomeClassWith2Properties>();

            IChecker<SomeClassWith2Properties> Checker = CheckerFactory<SomeClassWith2Properties>
                .ForAllProperties();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith2Properties_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClassWith2Properties model = fixture.Create<SomeClassWith2Properties>();

            IChecker<SomeClassWith2Properties> Checker = CheckerFactory<SomeClassWith2Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith2Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            SomeClassWith2Properties model = fixture.Create<SomeClassWith2Properties>();

            IChecker<SomeClassWith2Properties> Checker = CheckerFactory<SomeClassWith2Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop1 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith2Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property2_Have_Changed()
        {
            SomeClassWith2Properties model = fixture.Create<SomeClassWith2Properties>();

            IChecker<SomeClassWith2Properties> Checker = CheckerFactory<SomeClassWith2Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop2 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion

        #region 3 Properties
        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith3Properties_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<SomeClassWith3Properties> Checker = CheckerFactory<SomeClassWith3Properties>
                    .ForAllProperties();
            }).DoesNotThrow();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith3Properties_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClassWith3Properties model = fixture.Create<SomeClassWith3Properties>();

            IChecker<SomeClassWith3Properties> Checker = CheckerFactory<SomeClassWith3Properties>
                .ForAllProperties();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith3Properties_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClassWith3Properties model = fixture.Create<SomeClassWith3Properties>();

            IChecker<SomeClassWith3Properties> Checker = CheckerFactory<SomeClassWith3Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith3Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            SomeClassWith3Properties model = fixture.Create<SomeClassWith3Properties>();

            IChecker<SomeClassWith3Properties> Checker = CheckerFactory<SomeClassWith3Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop1 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith3Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property2_Have_Changed()
        {
            SomeClassWith3Properties model = fixture.Create<SomeClassWith3Properties>();

            IChecker<SomeClassWith3Properties> Checker = CheckerFactory<SomeClassWith3Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop2 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith3Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property3_Have_Changed()
        {
            SomeClassWith3Properties model = fixture.Create<SomeClassWith3Properties>();

            IChecker<SomeClassWith3Properties> Checker = CheckerFactory<SomeClassWith3Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop3 = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion

        #region 4 Properties
        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith4Properties_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<SomeClassWith4Properties> Checker = CheckerFactory<SomeClassWith4Properties>
                    .ForAllProperties();
            }).DoesNotThrow();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith4Properties_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClassWith4Properties model = fixture.Create<SomeClassWith4Properties>();

            IChecker<SomeClassWith4Properties> Checker = CheckerFactory<SomeClassWith4Properties>
                .ForAllProperties();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith4Properties_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClassWith4Properties model = fixture.Create<SomeClassWith4Properties>();

            IChecker<SomeClassWith4Properties> Checker = CheckerFactory<SomeClassWith4Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith4Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            SomeClassWith4Properties model = fixture.Create<SomeClassWith4Properties>();

            IChecker<SomeClassWith4Properties> Checker = CheckerFactory<SomeClassWith4Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop1 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith4Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property2_Have_Changed()
        {
            SomeClassWith4Properties model = fixture.Create<SomeClassWith4Properties>();

            IChecker<SomeClassWith4Properties> Checker = CheckerFactory<SomeClassWith4Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop2 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith4Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property3_Have_Changed()
        {
            SomeClassWith4Properties model = fixture.Create<SomeClassWith4Properties>();

            IChecker<SomeClassWith4Properties> Checker = CheckerFactory<SomeClassWith4Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop3 = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith4Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property4_Have_Changed()
        {
            SomeClassWith4Properties model = fixture.Create<SomeClassWith4Properties>();

            IChecker<SomeClassWith4Properties> Checker = CheckerFactory<SomeClassWith4Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop4 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion

        #region 5 Properties
        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith5Properties_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<SomeClassWith5Properties> Checker = CheckerFactory<SomeClassWith5Properties>
                    .ForAllProperties();
            }).DoesNotThrow();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith5Properties_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClassWith5Properties model = fixture.Create<SomeClassWith5Properties>();

            IChecker<SomeClassWith5Properties> Checker = CheckerFactory<SomeClassWith5Properties>
                .ForAllProperties();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith5Properties_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClassWith5Properties model = fixture.Create<SomeClassWith5Properties>();

            IChecker<SomeClassWith5Properties> Checker = CheckerFactory<SomeClassWith5Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith5Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            SomeClassWith5Properties model = fixture.Create<SomeClassWith5Properties>();

            IChecker<SomeClassWith5Properties> Checker = CheckerFactory<SomeClassWith5Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop1 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith5Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property2_Have_Changed()
        {
            SomeClassWith5Properties model = fixture.Create<SomeClassWith5Properties>();

            IChecker<SomeClassWith5Properties> Checker = CheckerFactory<SomeClassWith5Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop2 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith5Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property3_Have_Changed()
        {
            SomeClassWith5Properties model = fixture.Create<SomeClassWith5Properties>();

            IChecker<SomeClassWith5Properties> Checker = CheckerFactory<SomeClassWith5Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop3 = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith5Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property4_Have_Changed()
        {
            SomeClassWith5Properties model = fixture.Create<SomeClassWith5Properties>();

            IChecker<SomeClassWith5Properties> Checker = CheckerFactory<SomeClassWith5Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop4 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith5Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property5_Have_Changed()
        {
            SomeClassWith5Properties model = fixture.Create<SomeClassWith5Properties>();

            IChecker<SomeClassWith5Properties> Checker = CheckerFactory<SomeClassWith5Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop5 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion

        #region 6 Properties
        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith6Properties_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<SomeClassWith6Properties> Checker = CheckerFactory<SomeClassWith6Properties>
                    .ForAllProperties();
            })
            .DoesNotThrow();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith6Properties_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClassWith6Properties model = fixture.Create<SomeClassWith6Properties>();

            IChecker<SomeClassWith6Properties> Checker = CheckerFactory<SomeClassWith6Properties>
                .ForAllProperties();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith6Properties_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClassWith6Properties model = fixture.Create<SomeClassWith6Properties>();

            IChecker<SomeClassWith6Properties> Checker = CheckerFactory<SomeClassWith6Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith6Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            SomeClassWith6Properties model = fixture.Create<SomeClassWith6Properties>();

            IChecker<SomeClassWith6Properties> Checker = CheckerFactory<SomeClassWith6Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop1 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith6Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property2_Have_Changed()
        {
            SomeClassWith6Properties model = fixture.Create<SomeClassWith6Properties>();

            IChecker<SomeClassWith6Properties> Checker = CheckerFactory<SomeClassWith6Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop2 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith6Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property3_Have_Changed()
        {
            SomeClassWith6Properties model = fixture.Create<SomeClassWith6Properties>();

            IChecker<SomeClassWith6Properties> Checker = CheckerFactory<SomeClassWith6Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop3 = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith6Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property4_Have_Changed()
        {
            SomeClassWith6Properties model = fixture.Create<SomeClassWith6Properties>();

            IChecker<SomeClassWith6Properties> Checker = CheckerFactory<SomeClassWith6Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop4 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith6Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property5_Have_Changed()
        {
            SomeClassWith6Properties model = fixture.Create<SomeClassWith6Properties>();

            IChecker<SomeClassWith6Properties> Checker = CheckerFactory<SomeClassWith6Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop5 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith6Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property6_Have_Changed()
        {
            SomeClassWith6Properties model = fixture.Create<SomeClassWith6Properties>();

            IChecker<SomeClassWith6Properties> Checker = CheckerFactory<SomeClassWith6Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop6 = fixture.Create<Vector2>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion

        #region 7 Properties
        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith7Properties_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<SomeClassWith7Properties> Checker = CheckerFactory<SomeClassWith7Properties>
                    .ForAllProperties();
            })
            .DoesNotThrow();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith7Properties_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClassWith7Properties model = fixture.Create<SomeClassWith7Properties>();

            IChecker<SomeClassWith7Properties> Checker = CheckerFactory<SomeClassWith7Properties>
                .ForAllProperties();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith7Properties_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClassWith7Properties model = fixture.Create<SomeClassWith7Properties>();

            IChecker<SomeClassWith7Properties> Checker = CheckerFactory<SomeClassWith7Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith7Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            SomeClassWith7Properties model = fixture.Create<SomeClassWith7Properties>();

            IChecker<SomeClassWith7Properties> Checker = CheckerFactory<SomeClassWith7Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop1 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith7Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property2_Have_Changed()
        {
            SomeClassWith7Properties model = fixture.Create<SomeClassWith7Properties>();

            IChecker<SomeClassWith7Properties> Checker = CheckerFactory<SomeClassWith7Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop2 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith7Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property3_Have_Changed()
        {
            SomeClassWith7Properties model = fixture.Create<SomeClassWith7Properties>();

            IChecker<SomeClassWith7Properties> Checker = CheckerFactory<SomeClassWith7Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop3 = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith7Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property4_Have_Changed()
        {
            SomeClassWith7Properties model = fixture.Create<SomeClassWith7Properties>();

            IChecker<SomeClassWith7Properties> Checker = CheckerFactory<SomeClassWith7Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop4 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith7Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property5_Have_Changed()
        {
            SomeClassWith7Properties model = fixture.Create<SomeClassWith7Properties>();

            IChecker<SomeClassWith7Properties> Checker = CheckerFactory<SomeClassWith7Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop5 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith7Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property6_Have_Changed()
        {
            SomeClassWith7Properties model = fixture.Create<SomeClassWith7Properties>();

            IChecker<SomeClassWith7Properties> Checker = CheckerFactory<SomeClassWith7Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop6 = fixture.Create<Vector2>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith7Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property7_Have_Changed()
        {
            SomeClassWith7Properties model = fixture.Create<SomeClassWith7Properties>();

            IChecker<SomeClassWith7Properties> Checker = CheckerFactory<SomeClassWith7Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop7 = fixture.Create<bool>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion

        #region 8 Properties
        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                    .ForAllProperties();
            })
            .DoesNotThrow();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            SomeClassWith8Properties model = fixture.Create<SomeClassWith8Properties>();

            IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                .ForAllProperties();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            SomeClassWith8Properties model = fixture.Create<SomeClassWith8Properties>();

            IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            SomeClassWith8Properties model = fixture.Create<SomeClassWith8Properties>();

            IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop1 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property2_Have_Changed()
        {
            SomeClassWith8Properties model = fixture.Create<SomeClassWith8Properties>();

            IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop2 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property3_Have_Changed()
        {
            SomeClassWith8Properties model = fixture.Create<SomeClassWith8Properties>();

            IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop3 = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property4_Have_Changed()
        {
            SomeClassWith8Properties model = fixture.Create<SomeClassWith8Properties>();

            IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop4 = fixture.Create<float>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property5_Have_Changed()
        {
            SomeClassWith8Properties model = fixture.Create<SomeClassWith8Properties>();

            IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop5 = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property6_Have_Changed()
        {
            SomeClassWith8Properties model = fixture.Create<SomeClassWith8Properties>();

            IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop6 = fixture.Create<Vector2>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property7_Have_Changed()
        {
            SomeClassWith8Properties model = fixture.Create<SomeClassWith8Properties>();

            IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop7 = fixture.Create<bool>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_FromAllProperties_For_SomeClassWith8Properties_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property8_Have_Changed()
        {
            SomeClassWith8Properties model = fixture.Create<SomeClassWith8Properties>();

            IChecker<SomeClassWith8Properties> Checker = CheckerFactory<SomeClassWith8Properties>
                .ForAllProperties();

            bool firstCallResult = Checker.CheckForChanges(model);

            model.Prop8 = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion
        #endregion

        #region Equatable types
        #region Int
        [Test]
        public void Checker_ForEquatableType_For_Int_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<int> Checker = ValueCheckerFactory
                    .For<int>();
            }).DoesNotThrow();
        }

        [Test]
        public void Checker_ForEquatableType_For_Int_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            int model = fixture.Create<int>();

            IChecker<int> Checker = ValueCheckerFactory
                .For<int>();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_ForEquatableType_For_Int_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            int model = fixture.Create<int>();

            IChecker<int> Checker = ValueCheckerFactory
                .For<int>();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_ForEquatableType_For_Int_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            int model = fixture.Create<int>();

            IChecker<int> Checker = ValueCheckerFactory
                .For<int>();

            bool firstCallResult = Checker.CheckForChanges(model);

            model = fixture.Create<int>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion

        #region String
        [Test]
        public void Checker_ForEquatableType_For_String_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<string> Checker = ValueCheckerFactory
                    .For<string>();
            }).DoesNotThrow();
        }

        [Test]
        public void Checker_ForEquatableType_For_String_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            string model = fixture.Create<string>();

            IChecker<string> Checker = ValueCheckerFactory
                    .For<string>();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_ForEquatableType_For_String_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            string model = fixture.Create<string>();

            IChecker<string> Checker = ValueCheckerFactory
                    .For<string>();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_ForEquatableType_For_String_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            string model = fixture.Create<string>();

            IChecker<string> Checker = ValueCheckerFactory
                    .For<string>();

            bool firstCallResult = Checker.CheckForChanges(model);

            model = fixture.Create<string>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion

        #region Vector3
        [Test]
        public void Checker_ForEquatableType_For_Vector3_Should_Not_Throw()
        {
            Check.ThatCode(() =>
            {
                IChecker<Vector3> Checker = ValueCheckerFactory
                    .For<Vector3>();
            }).DoesNotThrow();
        }

        [Test]
        public void Checker_ForEquatableType_For_Vector3_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            Vector3 model = fixture.Create<Vector3>();

            IChecker<Vector3> Checker = ValueCheckerFactory
                    .For<Vector3>();

            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void Checker_ForEquatableType_For_Vector3_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            Vector3 model = fixture.Create<Vector3>();

            IChecker<Vector3> Checker = ValueCheckerFactory
                    .For<Vector3>();

            bool firstCallResult = Checker.CheckForChanges(model);
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void Checker_ForEquatableType_For_Vector3_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Property1_Have_Changed()
        {
            Vector3 model = fixture.Create<Vector3>();

            IChecker<Vector3> Checker = ValueCheckerFactory
                    .For<Vector3>();

            bool firstCallResult = Checker.CheckForChanges(model);

            model = fixture.Create<Vector3>();
            bool hasChanged = Checker.CheckForChanges(model);

            Check.That(hasChanged).IsTrue();
        }
        #endregion

        #region Non-Equatable types
        [Test]
        public void ValueChecker_For_Should_Throw_InvalidOperationException_When_Provided_Type_Implements_IEquatable()
        {
            Check.ThatCode(() =>
            {
                IChecker<SomeClass> Checker = ValueCheckerFactory
                    .For<SomeClass>();
            }).Throws<InvalidOperationException>();
        }
        #endregion
        #endregion

        #region ValueArrayChacker
        [Test]
        public void ValueArrayChecker_Should_Return_True_On_Has_Changed_On_First_Call()
        {
            const int length = 18;
            int[] array = fixture.CreateMany<int>(length).ToArray();

            IChecker<int[]> checker = CheckerFactory.ForValueArray<int>();

            bool hasChanged = checker.CheckForChanges(array);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void ValueArrayChecker_Should_Return_False_On_Has_Changed_On_Subsequent_Call_If_Model_Has_Not_Changed()
        {
            const int length = 18;
            int[] array = fixture.CreateMany<int>(length).ToArray();

            IChecker<int[]> checker = CheckerFactory.ForValueArray<int>();
            
            _ = checker.CheckForChanges(array);
            bool hasChanged = checker.CheckForChanges(array);

            Check.That(hasChanged).IsFalse();
        }

        [Test]
        public void ValueArrayChecker_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Array_Reference_Has_Changed()
        {
            const int length = 18;
            int[] array = fixture.CreateMany<int>(length).ToArray();

            IChecker<int[]> checker = CheckerFactory.ForValueArray<int>();
            
            _ = checker.CheckForChanges(array);

            array = array.ShallowCopy();

            bool hasChanged = checker.CheckForChanges(array);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        [TestCase(0)]
        [TestCase(8)]
        [TestCase(17)]
        public void ValueArrayChecker_Should_Return_True_On_Has_Changed_On_Subsequent_Call_If_Some_Array_Value_Has_Changed(int changedIndex)
        {
            const int length = 18;
            int[] array = fixture.CreateMany<int>(length).ToArray();

            IChecker<int[]> checker = CheckerFactory.ForValueArray<int>();
            
            _ = checker.CheckForChanges(array);

            array[changedIndex] = fixture.Create<int>();

            bool hasChanged = checker.CheckForChanges(array);

            Check.That(hasChanged).IsTrue();
        }

        [Test]
        public void ValueArrayChecker_Should_Not_Return_True_On_Has_Changed_During_More_Than_One_Call()
        {
            const int length = 18;
            int[] array = fixture.CreateMany<int>(length).ToArray();

            IChecker<int[]> checker = CheckerFactory.ForValueArray<int>();

            _ = checker.CheckForChanges(array);

            array[7] = fixture.Create<int>();

            bool firstSubsequentCall = checker.CheckForChanges(array);
            bool secondSubsequentCall = checker.CheckForChanges(array);

            Check.That(firstSubsequentCall).IsTrue();
            Check.That(secondSubsequentCall).IsFalse();
        }
        #endregion
    }
}