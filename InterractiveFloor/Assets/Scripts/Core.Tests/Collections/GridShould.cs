﻿using NFluent;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InterractiveFloor.Tests
{
    [TestFixture]
    public class GridShould
    {
        private const int GRID_WIDTH = 25;
        private const int GRID_HEIGHT = 42;

        #region Cells
        [Test]
        public void GridCell_Constructor_WhenGridIsNull_ThrowArgumentNullException()
        {
            Grid<ushort> grid = null;
            int row = 0;
            int column = 0;
            ushort value = 4;

            Check.ThatCode(() =>
            {
                GridCell<ushort> cell = new GridCell<ushort>(grid, row, column, value);
            }).Throws<ArgumentNullException>();
        }

        [Test]
        public void GridCell_Constructor_WhenEverythingCorrect_DoesNotThrow()
        {
            Grid<ushort> grid = new Grid<ushort>(12);

            int row = 8;
            int col = 10;

            Check.ThatCode(() =>
            {
                GridCell<ushort> cell = new GridCell<ushort>(grid, row, col);
            }).DoesNotThrow();
        }

        private static IEnumerable<int[]> GridCellNeighboursDatasource
        {
            get
            {
                yield return new[] { 2, 8 };
                yield return new[] { 8, 2 };
                yield return new[] { 0, 2 };
                yield return new[] { 8, 0 };
                yield return new[] { GRID_HEIGHT - 1, 2 };
                yield return new[] { 8, GRID_WIDTH - 1 };
                yield return new[] { 0, 0 };
                yield return new[] { 0, GRID_WIDTH - 1 };
                yield return new[] { GRID_HEIGHT - 1, 0 };
                yield return new[] { GRID_HEIGHT - 1, GRID_WIDTH - 1 };
            }
        }

        private static IEnumerable<int[]> GridCellNeighboursCountDatasource
        {
            get
            {
                yield return new[] { 2, 8, 8 };
                yield return new[] { 8, 2, 8 };
                yield return new[] { 0, 2, 5 };
                yield return new[] { 8, 0, 5 };
                yield return new[] { GRID_HEIGHT - 1, 2, 5 };
                yield return new[] { 8, GRID_WIDTH - 1, 5 };
                yield return new[] { 0, 0, 3 };
                yield return new[] { 0, GRID_WIDTH - 1, 3 };
                yield return new[] { GRID_HEIGHT - 1, 0, 3 };
                yield return new[] { GRID_HEIGHT - 1, GRID_WIDTH - 1, 3 };
            }
        }

        [Test]
        [TestCaseSource(nameof(GridCellNeighboursDatasource))]
        public void GridCell_Neighbours_EnumerateCells_DoesNotThrow(int row, int column)
        {
            Grid<long> grid = new Grid<long>(GRID_WIDTH, GRID_HEIGHT);

            GridCell<long> cell = grid[row, column];

            IEnumerable<GridCell<long>> neighbours = cell.Neighbours;
            foreach (GridCell<long> neighbour in neighbours) continue;
        }

        [Test]
        [TestCaseSource(nameof(GridCellNeighboursDatasource))]
        public void GridCell_Neighbours_AlwaysContainsNeighbours(int row, int column)
        {
            Grid<long> grid = new Grid<long>(GRID_WIDTH, GRID_HEIGHT);

            GridCell<long> cell = grid[row, column];

            IEnumerable<GridCell<long>> neighbours = cell.Neighbours;

            bool hasNeighbours = neighbours.Any();

            Check.That(hasNeighbours).IsTrue();
        }

        [Test]
        [TestCaseSource(nameof(GridCellNeighboursDatasource))]
        public void GridCell_Neighbours_AreAllNeighbour(int row, int column)
        {
            Grid<long> grid = new Grid<long>(GRID_WIDTH, GRID_HEIGHT);

            GridCell<long> cell = grid[row, column];

            IEnumerable<GridCell<long>> neighbours = cell.Neighbours;

            bool areAllNeighbours = neighbours.All(neighbour =>
            {
                int deltaRow = cell.RowIndex - neighbour.RowIndex;
                int deltaCol = cell.ColumnIndex - neighbour.ColumnIndex;

                return deltaRow >= -1 && deltaRow <= 1 && deltaCol >= -1 && deltaCol <= 1;
            });

            Check.That(areAllNeighbours).IsTrue();
        }

        [Test]
        [TestCaseSource(nameof(GridCellNeighboursDatasource))]
        public void GridCell_Neighbours_DoesNotContainSelf(int row, int column)
        {
            Grid<long> grid = new Grid<long>(GRID_WIDTH, GRID_HEIGHT);

            GridCell<long> cell = grid[row, column];

            IEnumerable<GridCell<long>> neighbours = cell.Neighbours;

            bool containsSelf = neighbours.Contains(cell);

            Check.That(containsSelf).IsFalse();
        }

        [Test]
        [TestCaseSource(nameof(GridCellNeighboursDatasource))]
        public void GridCell_Neighbours_Are_All_Distinct(int row, int column)
        {
            Grid<long> grid = new Grid<long>(GRID_WIDTH, GRID_HEIGHT);

            GridCell<long> cell = grid[row, column];

            bool distinct = cell.Neighbours.Select(c => (c.RowIndex, c.ColumnIndex)).AreAllDistinct();

            Check.That(distinct).IsTrue();
        }

        [Test]
        [TestCaseSource(nameof(GridCellNeighboursCountDatasource))]
        public void GridCell_Neighbours_Contains_Expected_Count(int row, int column, int count)
        {
            Grid<long> grid = new Grid<long>(GRID_WIDTH, GRID_HEIGHT);

            GridCell<long> cell = grid[row, column];

            IEnumerable<GridCell<long>> neighbours = cell.Neighbours;
            Check.That(neighbours).CountIs(count);
        }

        [Test]
        public void GridCell_Row_Index_Equals_RowIndex()
        {
            Grid<long> grid = new Grid<long>(GRID_WIDTH, GRID_HEIGHT);

            GridCell<long> cell = grid[4, 8];

            GridRow<long> row = cell.Row;

            Check.That(cell.RowIndex).IsEqualTo(row.Row);
        }

        [Test]
        public void GridCell_Column_Index_Equals_ColumnIndex()
        {
            Grid<long> grid = new Grid<long>(GRID_WIDTH, GRID_HEIGHT);

            GridCell<long> cell = grid[4, 8];

            GridColumn<long> column = cell.Column;

            Check.That(cell.ColumnIndex).IsEqualTo(column.Column);
        }
        #endregion

        #region Column Collection
        [Test]
        public void GridColumnCollection_Constructor_WhenArgumentIsCorrect_DoesNotThrow()
        {
            Grid<string> grid = new Grid<string>(GRID_WIDTH, GRID_HEIGHT, "My taylor is rich!");

            Check.ThatCode(() =>
            {
                GridColumnCollection<string> gcc = new GridColumnCollection<string>(grid);
            }).DoesNotThrow();
        }

        [Test]
        public void GridColumnCollection_EnumerateColumns_DoesNotThrow()
        {
            Grid<string> grid = new Grid<string>(GRID_WIDTH, GRID_HEIGHT, "Hello world!");

            GridColumnCollection<string> gcc = new GridColumnCollection<string>(grid);
            Check.ThatCode(() =>
            {
                foreach (GridColumn<string> gridCol in gcc) continue;
            }).DoesNotThrow();
        }

        [Test]
        [TestCase(-2)]
        [TestCase(GRID_WIDTH)]
        [TestCase(int.MaxValue)]
        public void GridColumnCollection_Index_WhenArgumentOutOfRange_ThrowIndexOutOfRangeException(int col)
        {
            Grid<char> grid = new Grid<char>(GRID_WIDTH, GRID_HEIGHT);

            GridColumnCollection<char> gcc = new GridColumnCollection<char>(grid);
            Check.ThatCode(() =>
            {
                GridColumn<char> column = gcc[col];
            }).Throws<IndexOutOfRangeException>();
        }

        [Test]
        [TestCase(0)]
        [TestCase(3)]
        [TestCase(GRID_WIDTH - 1)]
        public void GridColumnCollection_Index_WhenArgumentIsCorrect_DoesNotThrow(int col)
        {
            Grid<char> grid = new Grid<char>(GRID_WIDTH, GRID_HEIGHT);

            GridColumnCollection<char> gcc = new GridColumnCollection<char>(grid);
            Check.ThatCode(() =>
            {
                GridColumn<char> column = gcc[col];
            }).DoesNotThrow();
        }

        [Test]
        public void GridColumnCollection_Columns_Count_Equals_Grid_Width()
        {
            Grid<ushort> grid = new Grid<ushort>(GRID_WIDTH, GRID_HEIGHT);

            GridColumnCollection<ushort> gcc = new GridColumnCollection<ushort>(grid);

            int columnCount = gcc.Count();
            int gridWidth = grid.Width;

            Check.That(columnCount).IsEqualTo(gridWidth);
        }
        #endregion
        
        #region Column
        [Test]
        public void GridColumn_Constructor_WhenEverythingCorrect_DoesNotThrow()
        {
            Grid<ushort> grid = new Grid<ushort>(12);

            int col = 8;

            Check.ThatCode(() =>
            {
                GridColumn<ushort> gridCol = new GridColumn<ushort>(grid, col);
            }).DoesNotThrow();
        }

        [Test]
        public void GridColumn_EnumerateCells_DoesNotThrow()
        {
            Grid<ushort> grid = new Grid<ushort>(12);
            int col = 8;

            GridColumn<ushort> gridCol = new GridColumn<ushort>(grid, col);

            Check.ThatCode(() =>
            {
                foreach (GridCell<ushort> cell in gridCol) continue;
            }).DoesNotThrow();
        }

        [Test]
        [TestCase(-2)]
        [TestCase(GRID_HEIGHT)]
        [TestCase(GRID_HEIGHT + 4)]
        [TestCase(int.MaxValue)]
        public void GridColumn_Index_WhenArgumentOutOfRange_ThrowIndexOutOfRangeException(int row)
        {
            Grid<ushort> grid = new Grid<ushort>(GRID_WIDTH, GRID_HEIGHT);
            int col = 8;

            GridColumn<ushort> gridCol = new GridColumn<ushort>(grid, col);

            Check.ThatCode(() =>
            {
                GridCell<ushort> cell = gridCol[row];
            }).Throws<IndexOutOfRangeException>();
        }

        [Test]
        [TestCase(0)]
        [TestCase(2)]
        [TestCase(GRID_HEIGHT - 1)]
        public void GridColumn_Index_WhenArgumenIsCorrect_DoesNotThrow(int row)
        {
            Grid<long> grid = new Grid<long>(GRID_WIDTH, GRID_HEIGHT, 666L);
            int col = 8;

            GridColumn<long> gridCol = new GridColumn<long>(grid, col);

            Check.ThatCode(() =>
            {
                GridCell<long> cell = gridCol[row];
            }).DoesNotThrow();
        }

        [Test]
        public void GridColumn_Cells_Count_Equals_Grid_Height()
        {
            Grid<ushort> grid = new Grid<ushort>(GRID_WIDTH, GRID_HEIGHT);
            int col = 8;

            GridColumn<ushort> gridColumn = new GridColumn<ushort>(grid, col);

            int cellCount = gridColumn.Count();
            int gridHeight = grid.Height;

            Check.That(cellCount).IsEqualTo(gridHeight);
        }
        #endregion

        #region RowCollection
        [Test]
        public void GridRowCollection_Constructor_WhenArgumentIsCorrect_DoesNotThrow()
        {
            Grid<string> grid = new Grid<string>(GRID_WIDTH, GRID_HEIGHT, "My taylor is rich!");

            Assert.DoesNotThrow(() =>
            {
                GridRowCollection<string> grc = new GridRowCollection<string>(grid);
            });
        }

        [Test]
        public void GridRowCollection_EnumerateRows_DoesNotThrow()
        {
            Grid<string> grid = new Grid<string>(GRID_WIDTH, GRID_HEIGHT, "Hello world!");

            GridRowCollection<string> grc = new GridRowCollection<string>(grid);
            Assert.DoesNotThrow(() =>
            {
                foreach (GridRow<string> gridRow in grc) continue;
            });
        }

        [Test]
        [TestCase(-2)]
        [TestCase(GRID_HEIGHT)]
        [TestCase(int.MaxValue)]
        public void GridRowCollection_Index_WhenArgumentOutOfRange_ThrowIndexOutOfRangeException(int col)
        {
            Grid<char> grid = new Grid<char>(GRID_WIDTH, GRID_HEIGHT);

            GridRowCollection<char> grc = new GridRowCollection<char>(grid);
            Check.ThatCode(() =>
            {
                GridRow<char> row = grc[col];
            }).Throws<IndexOutOfRangeException>();
        }

        [Test]
        [TestCase(0)]
        [TestCase(3)]
        [TestCase(GRID_HEIGHT - 1)]
        public void GridRowCollection_Index_WhenArgumentIsCorrect_DoesNotThrow(int col)
        {
            Grid<char> grid = new Grid<char>(GRID_WIDTH, GRID_HEIGHT);

            GridRowCollection<char> grc = new GridRowCollection<char>(grid);
            Check.ThatCode(() =>
            {
                GridRow<char> row = grc[col];
            }).DoesNotThrow();
        }

        [Test]
        public void GridRowCollection_Rows_Count_Equals_Grid_Height()
        {
            Grid<ushort> grid = new Grid<ushort>(GRID_WIDTH, GRID_HEIGHT);

            GridRowCollection<ushort> grc = new GridRowCollection<ushort>(grid);

            int rowCount = grc.Count();
            int gridHeight = grid.Height;

            Check.That(rowCount).IsEqualTo(gridHeight);
        }
        #endregion

        #region Row
        [Test]
        public void GridRow_Constructor_WhenEverythingCorrect_DoesNotThrow()
        {
            Grid<ushort> grid = new Grid<ushort>(12);

            int row = 8;

            Check.ThatCode(() =>
            {
                GridRow<ushort> gridRow = new GridRow<ushort>(grid, row);
            }).DoesNotThrow();
        }

        [Test]
        public void GridRow_EnumerateCells_DoesNotThrow()
        {
            Grid<ushort> grid = new Grid<ushort>(12);
            int row = 8;

            GridRow<ushort> gridRow = new GridRow<ushort>(grid, row);

            Check.ThatCode(() =>
            {
                foreach (GridCell<ushort> cell in gridRow) continue;
            }).DoesNotThrow();
        }

        [Test]
        [TestCase(-2)]
        [TestCase(GRID_WIDTH)]
        [TestCase(GRID_WIDTH + 5)]
        [TestCase(int.MaxValue)]
        public void GridRow_Index_WhenArgumentOutOfRange_ThrowIndexOutOfRangeException(int col)
        {
            Grid<ushort> grid = new Grid<ushort>(GRID_WIDTH, GRID_HEIGHT);
            int row = 8;

            GridRow<ushort> gridRow = new GridRow<ushort>(grid, row);

            Check.ThatCode(() =>
            {
                GridCell<ushort> cell = gridRow[col];
            }).Throws<IndexOutOfRangeException>();
        }

        [Test]
        [TestCase(0)]
        [TestCase(2)]
        [TestCase(GRID_WIDTH - 1)]
        public void GridRow_Index_WhenArgumenIsCorrect_DoesNotThrow(int col)
        {
            Grid<ushort> grid = new Grid<ushort>(GRID_WIDTH, GRID_HEIGHT);
            int row = 8;

            GridRow<ushort> gridRow = new GridRow<ushort>(grid, row);

            Check.ThatCode(() =>
            {
                GridCell<ushort> cell = gridRow[col];
            }).DoesNotThrow();
        }

        [Test]
        public void GridRow_Cells_Count_Equals_Grid_Width()
        {
            Grid<ushort> grid = new Grid<ushort>(GRID_WIDTH, GRID_HEIGHT);
            int row = 8;

            GridRow<ushort> gridRow = new GridRow<ushort>(grid, row);

            int cellCount = gridRow.Count();
            int gridWidth = grid.Width;

            Check.That(cellCount).IsEqualTo(gridWidth);
        }
        #endregion

        #region Grid
        #region Array Constructor
        [Test]
        public void GridArrayConstructor_WhenCells_AreEmpty_Throw_ArgumentNullException()
        {
            int[,] cells = null;

            Check.ThatCode(() =>
            {
                Grid<int> grid = new Grid<int>(cells);
            }).Throws<ArgumentNullException>();
        }

        [Test]
        public void GridArrayConstructor_WhenCellsWidthIsZero_Throw_ArgumentException()
        {
            int[,] cells = new int[0, 18];

            Check.ThatCode(() =>
            {
                Grid<int> grid = new Grid<int>(cells);
            }).Throws<ArgumentException>();
        }

        [Test]
        public void GridArrayConstructor_WhenCellsHeightIsZero_Throw_ArgumentNullException()
        {
            int[,] cells = new int[,]
            {
                { },
                { },
                { },
                { },
                { }
            };

            Check.ThatCode(() =>
            {
                Grid<int> grid = new Grid<int>(cells);
            }).Throws<ArgumentException>();
        }

        [Test]
        public void GridArrayConstructor_WhenCellsIsCorrect_DoesNotThrow()
        {
            Check.ThatCode(() =>
            {
                int[,] cells = new int[,]
                {
                    { 1 , 2 , 3 , 4  },
                    { 5 , 6 , 7 , 8  },
                    { 9 , 10, 11, 12 },
                    { 13, 14, 15, 16 },
                    { 17, 18, 19, 20 }
                };

                Grid<int> grid = new Grid<int>(cells);
            }).DoesNotThrow();
        }

        [Test]
        public void GridArrayConstructor_GridDimensions_EqualsToArrays()
        {
            const int CELLS_WIDTH = 4;
            const int CELLS_HEIGHT = 5;

            int[,] cells = new int[CELLS_HEIGHT, CELLS_WIDTH]
            {
                { 1 , 2 , 3 , 4  },
                { 5 , 6 , 7 , 8  },
                { 9 , 10, 11, 12 },
                { 13, 14, 15, 16 },
                { 17, 18, 19, 20 }
            };

            Grid<int> grid = new Grid<int>(cells);

            int gridWidth = grid.Width;
            int gridHeight = grid.Height;

            Check.That(CELLS_WIDTH).IsEqualTo(gridWidth);
            Check.That(CELLS_HEIGHT).IsEqualTo(gridHeight);
        }

        [Test]
        public void GridArrayConstructor_GridValues_EqualsToArrays()
        {
            const int CELLS_WIDTH = 4;
            const int CELLS_HEIGHT = 5;

            int[,] cells = new int[CELLS_HEIGHT, CELLS_WIDTH]
            {
                { 1 , 2 , 3 , 4  },
                { 5 , 6 , 7 , 8  },
                { 9 , 10, 11, 12 },
                { 13, 14, 15, 16 },
                { 17, 18, 19, 20 }
            };

            Grid<int> grid = new Grid<int>(cells);

            for (int row = 0; row < CELLS_HEIGHT; row++)
            {
                for (int col = 0; col < CELLS_WIDTH; col++)
                {
                    int cellValue = cells[row, col];
                    int gridValue = grid[row, col].Value;

                    Check.That(gridValue).IsEqualTo(cellValue);
                }
            }
        }
        #endregion

        #region Width/Height Constructor
        [Test]
        [TestCase(0)]
        [TestCase(-2)]
        public void GridWidthHeightConstructor_WhenWidth_IsNegativeOrNull_ThrowArgumentException(int width)
        {
            int height = 10;

            Check.ThatCode(() =>
            {
                Grid<ushort> grid = new Grid<ushort>(width, height);
            }).Throws<ArgumentException>();
        }

        [Test]
        [TestCase(0)]
        [TestCase(-7)]
        public void GridWidthHeightConstructor_WhenHeight_IsNegativeOrNull_ThrowArgumentException(int height)
        {
            int width = 10;

            Check.ThatCode(() =>
            {
                Grid<ushort> grid = new Grid<ushort>(width, height);
            }).Throws<ArgumentException>();
        }

        [Test]
        public void GridWidthHeightConstructor_ValidValues_DoesNotThrow()
        {
            Check.ThatCode(() =>
            {
                int width = 4;
                int height = 5;

                Grid<string> grid = new Grid<string>(width, height);
            }).DoesNotThrow();
        }

        [Test]
        public void GridWidthHeightConstructor_ValuesEqualsToDefault_UShort()
        {
            int width = 4;
            int height = 5;

            Grid<ushort> grid = new Grid<ushort>(width, height);

            foreach (GridCell<ushort> cell in grid)
            {
                ushort cellValue = cell.Value;
                Check.That(cellValue).IsEqualTo(default(ushort));
            }
        }

        [Test]
        public void GridWidthHeightConstructor_ValuesEqualsToDefault_String()
        {
            int width = 4;
            int height = 5;

            Grid<string> grid = new Grid<string>(width, height);

            foreach (GridCell<string> cell in grid)
            {
                string cellValue = cell.Value;
                Check.That(cellValue).IsEqualTo(default(string));
            }
        }
        #endregion

        #region Width/Height/DefaultValue Constructor
        [Test]
        [TestCase(0)]
        [TestCase(-2)]
        public void GridWidthHeightDefaultValueConstructor_WhenWidth_IsNegativeOrNull_ThrowArgumentException(int width)
        {
            int height = 10;
            ushort defaultValue = 5;

            Check.ThatCode(() =>
            {
                Grid<ushort> grid = new Grid<ushort>(width, height, defaultValue);
            }).Throws<ArgumentException>();
        }

        [Test]
        [TestCase(0)]
        [TestCase(-7)]
        public void GridWidthHeightDefaultValueConstructor_WhenHeight_IsNegativeOrNull_ThrowArgumentException(int height)
        {
            int width = 10;
            ushort defaultValue = 5;

            Check.ThatCode(() =>
            {
                Grid<ushort> grid = new Grid<ushort>(width, height, defaultValue);
            }).Throws<ArgumentException>();
        }

        [Test]
        public void GridWidthHeightDefaultValueConstructor_ValidValues_DoesNotThrow()
        {
            Check.ThatCode(() =>
            {
                int width = 4;
                int height = 5;

                string defaultValue = "Hello!";

                Grid<string> grid = new Grid<string>(width, height, defaultValue);
            }).DoesNotThrow();
        }

        [Test]
        public void GridWidthHeightDefaultConstructor_ValuesEqualsToDefault_UShort()
        {
            int width = 4;
            int height = 5;

            ushort defaultValue = 13;

            Grid<ushort> grid = new Grid<ushort>(width, height, defaultValue);

            foreach (GridCell<ushort> cell in grid)
            {
                ushort cellValue = cell.Value;
                Check.That(cellValue).IsEqualTo(defaultValue);
            }
        }

        [Test]
        public void GridWidthHeightDefaultConstructor_ValuesEqualsToDefault_String()
        {
            int width = 4;
            int height = 5;

            string defaultValue = "Hello!";

            Grid<string> grid = new Grid<string>(width, height, defaultValue);

            foreach (GridCell<string> cell in grid)
            {
                string cellValue = cell.Value;
                Check.That(cellValue).IsEqualTo(defaultValue);
            }
        }
        #endregion

        #region Size Constructor
        [Test]
        [TestCase(0)]
        [TestCase(-2)]
        public void GridSizeConstructor_WhenSize_IsNegativeOrNull_ThrowArgumentException(int size)
        {
            Check.ThatCode(() =>
            {
                Grid<ushort> grid = new Grid<ushort>(size);
            }).Throws<ArgumentException>();
        }

        [Test]
        public void GridSizeConstructor_ValidValues_DoesNotThrow()
        {
            Check.ThatCode(() =>
            {
                int size = 10;

                Grid<string> grid = new Grid<string>(size);
            }).DoesNotThrow();
        }

        [Test]
        public void GridSizeConstructor_ValuesEqualsToDefault_UShort()
        {
            int size = 14;

            Grid<ushort> grid = new Grid<ushort>(size);

            foreach (GridCell<ushort> cell in grid)
            {
                ushort cellValue = cell.Value;
                Check.That(cellValue).IsEqualTo(default(ushort));
            }
        }

        [Test]
        public void GridSizeConstructor_ValuesEqualsToDefault_String()
        {
            int size = 14;

            Grid<string> grid = new Grid<string>(size);

            foreach (GridCell<string> cell in grid)
            {
                string cellValue = cell.Value;
                Check.That(cellValue).IsEqualTo(default(string));
            }
        }
        #endregion

        #region Size/DefaultValue Constructor
        [Test]
        [TestCase(0)]
        [TestCase(-2)]
        public void GridSizeDefaultValueConstructor_WhenSize_IsNegativeOrNull_ThrowArgumentException(int size)
        {
            ushort defaultValue = 18;

            Check.ThatCode(() =>
            {
                Grid<ushort> grid = new Grid<ushort>(size, defaultValue);
            }).Throws<ArgumentException>();
        }

        [Test]
        public void GridSizeDefaultValueConstructor_ValidValues_DoesNotThrow()
        {
            ushort defaultValue = 14;

            Check.ThatCode(() =>
            {
                int size = 10;

                Grid<string> grid = new Grid<string>(size, defaultValue);
            }).DoesNotThrow();
        }

        [Test]
        public void GridSizeDefaultValueConstructor_ValuesEqualsToDefault_UShort()
        {
            int size = 14;
            ushort defaultValue = 23;

            Grid<ushort> grid = new Grid<ushort>(size, defaultValue);

            foreach (GridCell<ushort> cell in grid)
            {
                ushort cellValue = cell.Value;
                Check.That(cellValue).IsEqualTo(defaultValue);
            }
        }

        [Test]
        public void GridSizeDefaultValueConstructor_ValuesEqualsToDefault_String()
        {
            int size = 14;
            string defaultValue = "World!";

            Grid<string> grid = new Grid<string>(size, defaultValue);

            foreach (GridCell<string> cell in grid)
            {
                string cellValue = cell.Value;
                Check.That(cellValue).IsEqualTo(defaultValue);
            }
        }
        #endregion

        #region Copy
        [Test]
        public void Grid_Copy_Copy_Should_Not_Be_Modified_When_Original_Is_Modified()
        {
            int[,] cells = new int[,]
            {
                { 1 , 2 , 3 , 4  },
                { 5 , 6 , 7 , 8  },
                { 9 , 10, 11, 12 },
                { 13, 14, 15, 16 },
                { 17, 18, 19, 20 }
            };

            Grid<int> original = new Grid<int>(cells);
            Grid<int> copy = original.Copy();

            original[1, 1].Value = 666;

            Check.That(copy[1, 1].Value).IsEqualTo(6);
        }

        [Test]
        public void Grid_Copy_Original_Should_Not_Be_Modified_When_Copy_Is_Modified()
        {
            int[,] cells = new int[,]
            {
                { 1 , 2 , 3 , 4  },
                { 5 , 6 , 7 , 8  },
                { 9 , 10, 11, 12 },
                { 13, 14, 15, 16 },
                { 17, 18, 19, 20 }
            };

            Grid<int> original = new Grid<int>(cells);
            Grid<int> copy = original.Copy();

            copy[1, 1].Value = 666;

            Check.That(original[1, 1].Value).IsEqualTo(6);
        }
        #endregion

        #region Enumerable
        [Test]
        public void Grid_EnumerateCells_DoesNotThrow()
        {
            const int CELLS_WIDTH = 4;
            const int CELLS_HEIGHT = 5;

            int[,] cells = new int[CELLS_HEIGHT, CELLS_WIDTH]
            {
                { 1 , 2 , 3 , 4  },
                { 5 , 6 , 7 , 8  },
                { 9 , 10, 11, 12 },
                { 13, 14, 15, 16 },
                { 17, 18, 19, 20 }
            };

            Grid<int> grid = new Grid<int>(cells);

            Check.ThatCode(() =>
            {
                foreach (GridCell<int> cell in grid) continue;
            }).DoesNotThrow();
        }
        #endregion

        #region Cells
        [Test]
        public void Grid_GridCells_IndexAreCorrect()
        {
            const int CELLS_WIDTH = 4;
            const int CELLS_HEIGHT = 5;

            int[,] cells = new int[CELLS_HEIGHT, CELLS_WIDTH]
            {
                { 1 , 2 , 3 , 4  },
                { 5 , 6 , 7 , 8  },
                { 9 , 10, 11, 12 },
                { 13, 14, 15, 16 },
                { 17, 18, 19, 20 }
            };

            Grid<int> grid = new Grid<int>(cells);

            for (int row = 0; row < CELLS_HEIGHT; row++)
            {
                for (int col = 0; col < CELLS_WIDTH; col++)
                {
                    GridCell<int> gridCell = grid[row, col];

                    int cellRow = gridCell.RowIndex;
                    int cellColumn = gridCell.ColumnIndex;

                    Check.That(row).IsEqualTo(cellRow);
                    Check.That(col).IsEqualTo(cellColumn);
                }
            }
        }

        [Test]
        [TestCase(-2, 4)]
        [TestCase(3, -8)]
        [TestCase(GRID_HEIGHT, 5)]
        [TestCase(0, GRID_WIDTH + 2)]
        public void Grid_Index_WhenArgumentOutOfRange_ThrowIndexOutOfRangeException(int row, int col)
        {
            Grid<decimal> grid = new Grid<decimal>(GRID_WIDTH, GRID_HEIGHT, -14.0m);

            Check.ThatCode(() =>
            {
                GridCell<decimal> cell = grid[row, col];
            }).Throws<IndexOutOfRangeException>();
        }

        [Test]
        [TestCaseSource(nameof(GridIndexCorrectValuesDataSource))]
        public void Grid_Index_WhenArgumenIsCorrect_DoesNotThrow(int row, int col)
        {
            Grid<decimal> grid = new Grid<decimal>(GRID_WIDTH, GRID_HEIGHT, 28.5m);

            Check.ThatCode(() =>
            {
                GridCell<decimal> cell = grid[row, col];
            }).DoesNotThrow();
        }

        [Test]
        [TestCaseSource(nameof(GridIndexCorrectValuesDataSource))]
        public void Grid_Index_MultipleAccess_ReturnsSameCell(int row, int col)
        {
            Grid<float> grid = new Grid<float>(GRID_WIDTH, GRID_HEIGHT, 3.14f);

            GridCell<float> cell1 = grid[row, col];
            GridCell<float> cell2 = grid[row, col];

            Check.That(cell1).IsEqualTo(cell2);
        }

        [Test]
        [TestCaseSource(nameof(GridCellsOnSameRowDataSource))]
        public void Grid_Cells_OnSameRow_Share_Row(int row, int col1, int col2)
        {
            Grid<object> grid = new Grid<object>(GRID_WIDTH, GRID_HEIGHT, "Why not?");

            GridCell<object> cell1 = grid[row, col1];
            GridRow<object> row1 = cell1.Row;

            GridCell<object> cell2 = grid[row, col2];
            GridRow<object> row2 = cell2.Row;

            Check.That(row1).IsEqualTo(row2);
        }

        [Test]
        [TestCaseSource(nameof(GridCellsOnSameColumnDataSource))]
        public void Grid_Cells_OnSameColumn_Share_Column(int column, int row1, int row2)
        {
            Grid<object> grid = new Grid<object>(GRID_WIDTH, GRID_HEIGHT, "Why not?");

            GridCell<object> cell1 = grid[row1, column];
            GridColumn<object> col1 = cell1.Column;

            GridCell<object> cell2 = grid[row2, column];
            GridColumn<object> col2 = cell2.Column;

            Check.That(col1).IsEqualTo(col2);
        }
        #endregion

        #region DataSources
        private static IEnumerable<int[]> GridIndexCorrectValuesDataSource
        {
            get
            {
                yield return new[] { 0, 0 };
                yield return new[] { 2, 8 };
                yield return new[] { 0, GRID_WIDTH - 1 };
                yield return new[] { GRID_HEIGHT - 1, 0 };
                yield return new[] { GRID_HEIGHT - 1, GRID_WIDTH - 1 };
            }
        }

        private static IEnumerable<int[]> GridCellsOnSameRowDataSource
        {
            get
            {
                yield return new int[] { 3, 2, 8 };
                yield return new int[] { 0, 0, GRID_WIDTH - 1 };
                yield return new int[] { GRID_HEIGHT - 1, 2, 8 };
                yield return new int[] { GRID_HEIGHT - 1, 0, GRID_WIDTH - 1 };
            }
        }

        private static IEnumerable<int[]> GridCellsOnSameColumnDataSource
        {
            get
            {
                yield return new int[] { 5, 4, 7 };
                yield return new int[] { 0, 0, GRID_HEIGHT - 1 };
                yield return new int[] { GRID_WIDTH - 1, 4, 7 };
                yield return new int[] { GRID_WIDTH - 1, 0, GRID_HEIGHT - 1 };
            }
        }
        #endregion
        #endregion
    }
}
