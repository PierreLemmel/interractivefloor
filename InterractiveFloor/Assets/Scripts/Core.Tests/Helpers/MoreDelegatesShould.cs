﻿using NFluent;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace InterractiveFloor.Tests
{
    [TestFixture]
    public class MoreDelegatesShould
    {
        [Test]
        public void Combine_Should_Execute_All_Inner_Delegates()
        {
            bool del1Executed = false;
            bool del2Executed = false;
            bool del3Executed = false;
            bool del4Executed = false;

            IEnumerable<Action> actions = new Action[]
            {
                () => del1Executed = true,
                () => del2Executed = true,
                () => del3Executed = true,
                () => del4Executed = true,
            };

            Action resultingAction = actions.Combine();
            resultingAction.Invoke();

            Check.That(del1Executed).IsTrue();
            Check.That(del2Executed).IsTrue();
            Check.That(del3Executed).IsTrue();
            Check.That(del4Executed).IsTrue();
        }
    }
}