﻿using NFluent;
using NUnit.Framework;

namespace InterractiveFloor.Tests
{
    [TestFixture]
    public class StringsShould
    {
        [Test]
        [TestCase("Hello", "hello")]
        [TestCase("world", "world")]
        public void UncapitalizeFirstLetter_Should_Uncapitalize_First_Letter(string input, string expected)
        {
            string result = input.UncapitalizeFirstLetter();

            Check.That(result).IsEqualTo(expected);
        }

        [Test]
        [TestCase("hello world", "hello world")]
        [TestCase("HeLlO W0rld!!", "HeLlO W0rld!!")]
        [TestCase("Crème brûlée", "Creme brulee")]
        [TestCase("Les Écorcés", "Les Ecorces")]
        [TestCase("Ça va ?", "Ca va ?")]
        public void RemoveDiacritics_Should_Remove_Diacritics(string input, string expected)
        {
            string result = input.RemoveDiacritics();

            Check.That(result).IsEqualTo(expected);
        }

        [Test]
        [TestCase("hello world", "hello world")]
        [TestCase("HeLlO W0rld!!", "HeLlO W0rld")]
        [TestCase("!!Coucou", "Coucou")]
        [TestCase("!!Coucou!!??!!", "Coucou")]
        [TestCase("Ça va ?", "Ça va")]
        [TestCase("? Ça va ?", "Ça va")]
        public void TrimPunctuation_Should_Trim_Punctuation(string input, string expected)
        {
            string result = input.TrimPunctuation();

            Check.That(result).IsEqualTo(expected);
        }
    }
}