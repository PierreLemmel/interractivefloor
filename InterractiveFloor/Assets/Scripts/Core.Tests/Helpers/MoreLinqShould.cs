﻿using NFluent;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace InterractiveFloor.Tests
{
    [TestFixture]
    public class MoreLinqShould
    {
        [Test]
        public void IsEmpty_Returns_True_If_Sequence_Is_Empty()
        {
            IEnumerable<int> empty = Enumerable.Empty<int>();
            bool isEmpty = empty.IsEmpty();

            Check.That(isEmpty).IsTrue();
        }

        [Test]
        public void IsEmpty_Returns_False_If_Sequence_Is_Not_Empty()
        {
            IEnumerable<int> notEmpty = Enumerable.Range(0, 18000);
            bool isEmpty = notEmpty.IsEmpty();

            Check.That(isEmpty).IsFalse();
        }

        [Test]
        public void IsEmpty_Should_Not_Iterate_Over_The_Whole_Sequence()
        {
            IEnumerable<int> sequence = new EnumerableThatThrowsAfterACertainNumberOfIterations<int>(5);
            Check.ThatCode(() => sequence.IsEmpty())
                .DoesNotThrow();
        }

        [Test]
        public void IsSingle_Returns_False_If_Sequence_Is_Empty()
        {
            IEnumerable<int> empty = Enumerable.Empty<int>();
            bool isSingle = empty.IsSingle();

            Check.That(isSingle).IsFalse();
        }

        [Test]
        public void IsSingle_Returns_True_If_Sequence_Has_Single_Element()
        {
            IEnumerable<int> single = new int[] { 42 };
            bool isSingle = single.IsSingle();

            Check.That(isSingle).IsTrue();
        }

        [Test]
        public void IsSingle_Returns_False_If_Sequence_Has_Multiple_Elements()
        {
            IEnumerable<int> multiple = Enumerable.Range(18, 42);
            bool isSingle = multiple.IsSingle();

            Check.That(isSingle).IsFalse();
        }

        [Test]
        public void IsSingle_Should_Not_Iterate_Over_The_Whole_Sequence()
        {
            IEnumerable<int> sequence = new EnumerableThatThrowsAfterACertainNumberOfIterations<int>(5);
            Check.ThatCode(() => sequence.IsSingle())
                .DoesNotThrow();
        }

        [Test]
        public void AreAllDistincts_Returns_True_When_All_Elements_Are_Distinct()
        {
            IEnumerable<int> sequence = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            Check.That(sequence.AreAllDistinct()).IsTrue();
        }

        [Test]
        public void AreAllDistincts_Returns_False_When_All_Elements_Are_Not_Distinct()
        {
            IEnumerable<int> sequence = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 3 };

            Check.That(sequence.AreAllDistinct()).IsFalse();
        }
    }
}