﻿using NFluent;
using NUnit.Framework;
using System.Collections.Generic;

using static InterractiveFloor.Tests.SomeEnum;

namespace InterractiveFloor.Tests
{
    [TestFixture]
    public class EnumsShould
    {
        [Test]
        [TestCaseSource(nameof(IsOneOfReturnsTrueTestCaseData))]
        public void IsOneOf_Returns_True_If_Input_Is_In_Provided_Values(SomeEnum input, SomeEnum[] values)
        {
            bool isOneOf = input.IsOneOf(values);
            Check.That(isOneOf).IsTrue();
        }
        public static IEnumerable<object[]> IsOneOfReturnsTrueTestCaseData
        {
            get
            {
                yield return new object[] { Value1, new SomeEnum[] { Value1 } };
                yield return new object[] { Value1, new SomeEnum[] { Value2, Value1 } };
                yield return new object[] { Value1, new SomeEnum[] { Value2, Value1, Value3, Value4, Value5 } };
            }
        }

        [Test]
        [TestCaseSource(nameof(IsOneOfReturnsFalseTestCaseData))]
        public void IsOneOf_Returns_False_If_Input_Is_In_Provided_Values(SomeEnum input, SomeEnum[] values)
        {
            bool isOneOf = input.IsOneOf(values);
            Check.That(isOneOf).IsFalse();
        }

        public static IEnumerable<object[]> IsOneOfReturnsFalseTestCaseData
        {
            get
            {
                yield return new object[] { Value1, new SomeEnum[] { } };
                yield return new object[] { Value1, new SomeEnum[] { Value2, Value3 } };
                yield return new object[] { Value1, new SomeEnum[] { Value1 | Value2 } };
            }
        }
    }
}
