﻿using AutoFixture;
using InterractiveFloor.Unity.Tests.SampleClasses;
using NFluent;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

using UObject = UnityEngine.Object;

namespace InterractiveFloor.Unity.Tests
{
    [TestFixture]
    public class GameObjectBuilderShould
    {
        private readonly IFixture fixture = new Fixture();

        [UnityTest]
        public IEnumerator Create_An_ActiveObject()
        {
            GameObject go = new GameObjectBuilder();

            Check.That(go.activeInHierarchy).IsTrue();

            UObject.Destroy(go);

            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator Create_An_Object_With_A_Name()
        {
            string name = fixture.Create<string>();

            GameObject go = new GameObjectBuilder()
                .Named(name);

            Check.That(go.name).IsEqualTo(name);

            UObject.Destroy(go);

            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator Create_An_Object_With_A_Parent()
        {
            GameObject parent = new GameObject("Parent");

            GameObject go = new GameObjectBuilder()
                .AsChildOf(parent);

            Check.That(go.transform.parent)
                .IsEqualTo(parent.transform);

            UObject.Destroy(parent);

            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator Create_An_Object_With_A_Component()
        {
            GameObject go = new GameObjectBuilder().WithComponent<SomeTestBehaviour>();

            Check.That(go).HasComponent<SomeTestBehaviour>();

            UObject.Destroy(go);

            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator Create_An_Object_With_A_Component_And_A_Simple_Setup()
        {
            Guid someId = fixture.Create<Guid>();

            GameObject go = new GameObjectBuilder()
                .WithComponent<SomeTestBehaviour>(behaviour => behaviour.Id = someId);

            Check.That(go).HasComponent<SomeTestBehaviour>();
            Check.That(go.GetComponent<SomeTestBehaviour>().Id).IsEqualTo(someId);

            UObject.Destroy(go);

            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator Create_An_Object_With_A_Component_And_A_Complex_Setup()
        {
            Guid someId = fixture.Create<Guid>();
            string someMessage = fixture.Create<string>();

            GameObject go = new GameObjectBuilder()
                .WithComponent<SomeTestBehaviour>(comp => comp
                    .Setup(behaviour => behaviour.Id = someId)
                    .PostInit(behaviour => behaviour.Message = someMessage));

            Check.That(go).HasComponent<SomeTestBehaviour>();
            Check.That(go.GetComponent<SomeTestBehaviour>().Id).IsEqualTo(someId);
            Check.That(go.GetComponent<SomeTestBehaviour>().Message).IsEqualTo(someMessage);

            UObject.Destroy(go);

            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator Setup_A_Component_Calls_Setup_Before_Object_Is_Enabled()
        {
            GameObject go = new GameObjectBuilder()
                .WithComponent<ThrowIfTriggerIsCalledAfterEnabled>(comp => comp
                    .Setup(behaviour => behaviour.PushTheTriggerAndHopeItDoesntBlowUp()));

            UObject.Destroy(go);

            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator Setup_A_Component_Calls_PostInit_After_Object_Is_Enabled()
        {
            GameObject go = new GameObjectBuilder()
                .WithComponent<ThrowIfTriggerIsCalledBeforeEnabled>(comp => comp
                    .PostInit(behaviour => behaviour.PushTheTriggerAndHopeItDoesntBlowUp()));

            UObject.Destroy(go);

            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator Create_A_Child()
        {
            GameObject go = new GameObjectBuilder()
                .Named("Parent")
                .WithChild(child => child
                    .Named("Child"));

            Check.That(go).HasChildren();

            UObject.Destroy(go);

            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator Create_Nested_Childs()
        {
            GameObject go = new GameObjectBuilder()
                .Named("Parent")
                .WithChild(child => child
                    .Named("Child")
                    .WithComponent<MeshRenderer>()
                    .WithChild(nested => nested
                        .Named("Nested")
                        .WithComponent<MeshCollider>()));

            Check.That(go).HasChildren();
            Check.That(go.GetChild()).HasChildren();
            Check.That(go.GetChild()).HasComponent<MeshRenderer>();
            Check.That(go.GetChild().GetChild()).HasComponent<MeshCollider>();

            UObject.Destroy(go);

            yield return Wait.TillNextFrame;
        }
    }
}