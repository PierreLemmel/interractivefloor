﻿using System;

namespace InterractiveFloor.Unity.Tests.SampleClasses
{
    public class TriggerException : Exception
    {
        public TriggerException(string message) : base(message) { }
    }
}