﻿namespace InterractiveFloor.Unity.Tests.SampleClasses
{
    public interface ITriggerable
    {
        void PushTheTriggerAndHopeItDoesntBlowUp();
    }
}