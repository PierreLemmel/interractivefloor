﻿using UnityEngine;

namespace InterractiveFloor.Unity.Tests.SampleClasses
{
    public class ThrowIfTriggerIsCalledBeforeEnabled : MonoBehaviour
    {
        public void PushTheTriggerAndHopeItDoesntBlowUp()
        {
            if (!isActiveAndEnabled)
                throw new TriggerException("Component is already enabled");
        }
    }
}