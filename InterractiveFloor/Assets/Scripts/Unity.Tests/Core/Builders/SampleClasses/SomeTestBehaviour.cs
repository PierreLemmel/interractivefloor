﻿using System;
using UnityEngine;

namespace InterractiveFloor.Unity.Tests.SampleClasses
{
    public class SomeTestBehaviour : MonoBehaviour
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
    }
}