﻿using UnityEngine;

namespace InterractiveFloor.Unity.Tests.SampleClasses
{
    public class ThrowIfTriggerIsCalledAfterEnabled : MonoBehaviour, ITriggerable
    {
        public void PushTheTriggerAndHopeItDoesntBlowUp()
        {
            if (isActiveAndEnabled)
                throw new TriggerException("Component is already enabled");
        }
    }
}