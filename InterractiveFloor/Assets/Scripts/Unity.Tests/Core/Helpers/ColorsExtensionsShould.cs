﻿using AutoFixture;
using NFluent;
using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;

namespace InterractiveFloor.Unity.Tests
{
    [TestFixture]
    public class ColorsExtensionsShould
    {
        private readonly IFixture fixture = new Fixture();

        [Test]
        public void Color_HasTransparency_Returns_True_On_Colors_With_Alpha_Not_Equal_To_One()
        {
            Color transparent = new Color(1.0f, 1.0f, 1.0f, 0.5f);

            Check.That(transparent.HasTransparency()).IsTrue();
        }

        [Test]
        public void Color32_HasTransparency_Returns_True_On_Colors_With_Alpha_Not_Equal_To_One()
        {
            Color transparent = new Color32(0xff, 0xff, 0xff, 0x80);

            Check.That(transparent.HasTransparency()).IsTrue();
        }

        [Test]
        public void Color_HasTransparency_Returns_False_On_Colors_With_Alpha_Not_Equal_To_One()
        {
            Color opaque = new Color(1.0f, 1.0f, 1.0f, 1.0f);

            Check.That(opaque.HasTransparency()).IsFalse();
        }

        [Test]
        public void Color32_HasTransparency_Returns_False_On_Colors_With_Alpha_Not_Equal_To_One()
        {
            Color opaque = new Color32(0xff, 0xff, 0xff, 0xff);

            Check.That(opaque.HasTransparency()).IsFalse();
        }

        [Test]
        [TestCaseSource(nameof(ColorToHexStringTestData))]
        public void Color_ToHexString_Return_Expected_Result(Color color, string expected)
        {
            string result = color.ToHexString();
            Check.That(result).IsEqualTo(expected);
        }

        [Test]
        [TestCaseSource(nameof(Color32ToHexStringTestData))]
        public void Color32_ToHexString_Return_Expected_Result(Color32 color, string expected)
        {
            string result = color.ToHexString();
            Check.That(result).IsEqualTo(expected);
        }

        public static IEnumerable<object[]> ColorToHexStringTestData
        {
            get
            {
                yield return new object[] { Color.black, "#000000" };
                yield return new object[] { Color.red, "#FF0000" };
                yield return new object[] { new Color(1.0f, 1.0f, 1.0f, 0.0f), "#FFFFFF00" };
            }
        }

        public static IEnumerable<object[]> Color32ToHexStringTestData
        {
            get
            {
                yield return new object[] { (Color32)Color.black, "#000000" };
                yield return new object[] { (Color32)Color.red, "#FF0000" };
                yield return new object[] { new Color32(0xff, 0xff, 0xff, 00), "#FFFFFF00" };
            }
        }
    }
}