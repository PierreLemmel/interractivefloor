﻿using InterractiveFloor.Unity.Application;
using InterractiveFloor.Unity.Application.Content;
using NFluent;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.TestTools;

using UObject = UnityEngine.Object;

namespace InterractiveFloor.Unity.Tests.Bundles
{
    [TestFixture]
    public class BundlesShould
    {
        [UnityTest]
        public IEnumerator LoadAndUnloadWithoutProblem([ValueSource(typeof(ContentBundles), nameof(ContentBundles.AllBundles))] string bundle)
        {
            ContentBundleStore store = new ContentBundleStore();

            AssetBundleCreateRequest request = store.LoadBundle(bundle);
            yield return Wait.ForAsyncOperation(request);

            store.UnloadBundle(bundle);
            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator ContainAValidManifest([ValueSource(typeof(ContentBundles), nameof(ContentBundles.AllBundles))] string bundle)
        {
            ContentBundleStore store = new ContentBundleStore();

            AssetBundleCreateRequest request = store.LoadBundle(bundle);
            yield return Wait.ForAsyncOperation(request);

            AssetBundle assetBundle = request.assetBundle;

            ContentManifest manifest = store.LoadManifest(assetBundle);

            Check.That(manifest).IsNotNull();
            Check.That(manifest.Name).IsNotNullOrWhiteSpace();
            Check.That(manifest.Description).IsNotNullOrWhiteSpace();
            Check.That(manifest.EntryPoint).IsNotNullOrWhiteSpace();

            store.UnloadBundle(bundle);
            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator CanInstantiateAndDestroyEntryPoint([ValueSource(typeof(ContentBundles), nameof(ContentBundles.AllBundles))] string bundle)
        {
            ContentBundleStore store = new ContentBundleStore();

            AssetBundleCreateRequest request = store.LoadBundle(bundle);
            yield return Wait.ForAsyncOperation(request);

            AssetBundle assetBundle = request.assetBundle;

            ContentManifest manifest = store.LoadManifest(assetBundle);

            string entryPoint = manifest.EntryPoint;
            GameObject prefab = assetBundle.LoadAsset<GameObject>(entryPoint);
            Check.That(prefab).IsNotNull();

            GameObject instance = GameObjects.FromPrefab(prefab);
            Check.That(instance).IsNotNull();

            yield return Wait.ForSeconds(0.5f);

            UObject.Destroy(instance);

            yield return Wait.TillNextFrame;

            store.UnloadBundle(bundle);
            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator EntryPointContainsASceneRootAndACamera([ValueSource(typeof(ContentBundles), nameof(ContentBundles.AllBundles))] string bundle)
        {
            ContentBundleStore store = new ContentBundleStore();

            AssetBundleCreateRequest request = store.LoadBundle(bundle);
            yield return Wait.ForAsyncOperation(request);

            AssetBundle assetBundle = request.assetBundle;

            ContentManifest manifest = store.LoadManifest(assetBundle);

            string entryPoint = manifest.EntryPoint;
            GameObject prefab = assetBundle.LoadAsset<GameObject>(entryPoint);
            Check
                .WithCustomMessage("Asset should be loadable")
                .That(prefab)
                .IsNotNull();

            GameObject instance = GameObjects.FromPrefab(prefab);
            Check
                .WithCustomMessage("Prefab should be possile to instantiate")
                .That(instance)
                .IsNotNull();

            Check
                .WithCustomMessage("Scenes should have a SceneRoot component on its root")
                .That(instance)
                .HasComponent<SceneRoot>();

            Check
                .WithCustomMessage("Instance should contain a camera within children")
                .That(instance)
                .HasComponentInChildren<Camera>();

            yield return Wait.ForSeconds(0.5f);

            UObject.Destroy(instance);

            yield return Wait.TillNextFrame;

            store.UnloadBundle(bundle);
            yield return Wait.TillNextFrame;
        }

        [UnityTest]
        public IEnumerator EntryPointCanResolveAllDependencies([ValueSource(typeof(ContentBundles), nameof(ContentBundles.AllBundles))] string bundle)
        {
            ContentBundleStore store = new ContentBundleStore();

            AssetBundleCreateRequest request = store.LoadBundle(bundle);
            yield return Wait.ForAsyncOperation(request);

            AssetBundle assetBundle = request.assetBundle;

            ContentManifest manifest = store.LoadManifest(assetBundle);

            string entryPoint = manifest.EntryPoint;
            GameObject prefab = assetBundle.LoadAsset<GameObject>(entryPoint);
            GameObject instance = GameObjects.FromPrefab(prefab);

            SceneRoot root = instance.GetComponent<SceneRoot>();

            IEnumerable<(SceneComponent Component, Type Service)> testCases = root.GetComponentsInChildren<SceneComponent>()
                .SelectMany(comp => comp
                    .GetType()
                    .GetAllAttributes<RequireServiceAttribute>()
                    .Select(rsa => (Component: comp, rsa.Service))
                );

            ICollection<string> errors = new List<string>();

            foreach ((SceneComponent component, Type service) in testCases)
            {
                Type serviceType = typeof(IServiceProvider<>).MakeGenericType(service);

                bool hasComponentprovidingService = component.GetComponents<SceneComponent>().Any(sc => sc.GetType().Implements(serviceType));
                if (!hasComponentprovidingService)
                {
                    string errorMsg = $"Component named '{component.name}' of type {component.GetType().Name} misses provider for service {service.Name}";
                    errors.Add(errorMsg);
                }
            }

            Check
                .That(errors)
                .IsEmpty();

            UObject.Destroy(instance);

            yield return Wait.TillNextFrame;

            store.UnloadBundle(bundle);
            yield return Wait.TillNextFrame;
        }
    }
}