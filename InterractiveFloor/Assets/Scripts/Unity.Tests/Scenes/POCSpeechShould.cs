﻿using InterractiveFloor.Unity.Application;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

namespace InterractiveFloor.Unity.Tests.Scenes
{
    [TestFixture]
    public class POCSpeechShould
    {
        private const string Bundle = "pocspeech";

        private const int SmallDuration = 5_000;
        private const int LongDuration = 90_000;
        private const int PrettyLongDuration = 150_000;

        [UnityTest]
        [Timeout(SmallDuration)]
        public IEnumerator Survive_Without_Error_To_A_Small_Duration() => TestForDuration(SmallDuration);

        [UnityTest]
        [Timeout(LongDuration)]
        public IEnumerator Survive_Without_Error_To_A_Long_Duration() => TestForDuration(LongDuration);

        [UnityTest]
        [Timeout(PrettyLongDuration)]
        public IEnumerator Survive_Without_Error_To_A_Pretty_Long_Duration() => TestForDuration(PrettyLongDuration);

        private IEnumerator TestForDuration(int duration)
        {
            GameObject runtimeObj = GameObjects.CreateNew()
                .Named("Runtime")
                .WithComponent<Runtime>(setup => setup
                    .PostInit(runtime => runtime.CurrentScene = Bundle));

            const int OffsetToAvoidTimeout = 2_000;
            yield return Wait.ForMilliseconds(duration - OffsetToAvoidTimeout);

            Object.Destroy(runtimeObj);

            yield return Wait.ForMilliseconds(200);
        }
    }
}