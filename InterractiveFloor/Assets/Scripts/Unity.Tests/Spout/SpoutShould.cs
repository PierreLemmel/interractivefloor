﻿using InterractiveFloor.Unity.Spout;
using NUnit.Framework;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

using UObject = UnityEngine.Object;
using TextureFormat = InterractiveFloor.Unity.Spout.TextureFormat;

namespace InterractiveFloor.Unity.Tests.Spout
{
    [TestFixture]
    public class SpoutShould
    {
        [UnityTest]
        public IEnumerator Initialize_And_Stop_Without_Crashing()
        {
            GameObject senderObject = GameObjects
                .CreateNew()
                .Named("Spout sender")
                .WithComponent<SpoutSender>(sender => sender.Parameters = new SpoutSenderParameters
                {
                    DebugInConsole = false,
                    SenderName = $"Test Sender-{Guid.NewGuid()}",
                    TargetDimensions = new Vector2Int(1920, 1080),
                    TextureFormat = TextureFormat.DXGI_FORMAT_R8G8B8A8_UNORM
                });

            yield return Wait.ForSeconds(10.0f);

            UObject.Destroy(senderObject);

            yield return Wait.TillNextFrame;
        }
    }
}