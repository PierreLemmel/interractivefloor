﻿using InterractiveFloor.Internal;
using NFluent;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

using UObject = UnityEngine.Object;
using URangeAttribute = UnityEngine.RangeAttribute;

namespace InterractiveFloor.Unity.Tests
{
    [TestFixture]
    public class ModelsShould
    {
        #region Sources
        private static readonly IReadOnlyCollection<Type> ProjectTypes = AppDomain
            .CurrentDomain
            .GetAssemblies()
            .Where(assembly => assembly.FullName.Contains("InterractiveFloor.")
                && !assembly.FullName.Contains(".Tests"))
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => !type.IsAbstract && type.IsClass)
            .ToList();

        public static IEnumerable<Type> ModelTypes => ProjectTypes
            .Where(type => type.IsSubclassOf(typeof(Model)));

        public static IEnumerable<Type> WeavableTypes => ProjectTypes
            .Where(type => type.IsSubclassOf(typeof(Model)) || type.IsSubclassOf(typeof(UObject)));

        public static IEnumerable<PropertyInfo> ModelReadWriteProperties => WeavableTypes
            .SelectMany(type => type.GetProperties())
            .Where(prop => prop.DeclaringType.Assembly.FullName.Contains("InterractiveFloor"))
            .Where(prop => prop.GetGetMethod() != null
                && prop.GetSetMethod() != null);

        public static IEnumerable<object[]> ModelReadWritePropertiesSource => ModelReadWriteProperties
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<PropertyInfo> InRangeProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<InRangeAttribute>());

        public static IEnumerable<object[]> ModelInRangeIntProperties => InRangeProperties
            .Where(prop => prop.PropertyType == typeof(int))
            .Select(prop => new object[] { prop.DeclaringType, prop, prop.GetCustomAttribute<InRangeAttribute>() });

        public static IEnumerable<object[]> ModelInRangeFloatProperties => InRangeProperties
            .Where(prop => prop.PropertyType == typeof(float))
            .Select(prop => new object[] { prop.DeclaringType, prop, prop.GetCustomAttribute<InRangeAttribute>() });

        public static IEnumerable<object[]> NotEditableProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<NotEditableAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<object[]> EditTimeOnlyProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<EditTimeOnlyAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<PropertyInfo> DisplayIfPropertiesSource => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<DisplayIfAttribute>());

        public static IEnumerable<object[]> DisplayIfProperties_IsTrue_Or_IsFalse => DisplayIfPropertiesSource
            .Where(prop => prop
                .GetCustomAttribute<DisplayIfAttribute>()
                .Condition.IsOneOf(DisplayCondition.IsTrue, DisplayCondition.IsFalse))
            .Select(prop => new object[] { prop.DeclaringType, prop, prop.GetCustomAttribute<DisplayIfAttribute>() });

        public static IEnumerable<object[]> DisplayIfProperties_IsEqualTo => DisplayIfPropertiesSource
            .Where(prop => prop.GetCustomAttribute<DisplayIfAttribute>().Condition == DisplayCondition.IsEqualTo)
            .Select(prop => new object[] { prop.DeclaringType, prop, prop.GetCustomAttribute<DisplayIfAttribute>() });

        public static IEnumerable<object[]> DisplayIfProperties => DisplayIfPropertiesSource
            .Select(prop => new object[] { prop.DeclaringType, prop, prop.GetCustomAttribute<DisplayIfAttribute>() });

        public static IEnumerable<object[]> AudioDeviceSourceProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<AudioDeviceSourceAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<object[]> AudioRenderDeviceSourceProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<AudioRenderDeviceSourceAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<object[]> AudioCaptureDeviceSourceProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<AudioCaptureDeviceSourceAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<object[]> IsClassProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<IsClassAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<object[]> IsInterfaceProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<IsInterfaceAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<object[]> ImplementsProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<ImplementsAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<object[]> ImplementsPropertiesWithInterfaceSource => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<ImplementsAttribute>()
                && !prop.GetCustomAttribute<ImplementsAttribute>().InterfaceSource.IsNullOrEmpty())
            .Select(prop => new object[] { prop.DeclaringType, prop });
        #endregion

        #region All properties
        [Test]
        [TestCaseSource(nameof(ModelTypes))]
        public void Have_A_Serializable_Attribute(Type type)
        {
            SerializableAttribute serializableAttr = type.GetCustomAttribute<SerializableAttribute>();

            Assert.That(serializableAttr, Is.Not.Null);
        }

        [Test]
        [TestCaseSource(nameof(ModelReadWritePropertiesSource))]
        public void Model_Properties_Should_Contain_A_Backing_Field_With_A_SerializeField_Attribute(Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            Check.That(backingField).IsNotNull();

            SerializeField serializeField = backingField.GetCustomAttribute<SerializeField>();
            Check.That(serializeField).IsNotNull();
        }
        #endregion

        #region InRange
        [Test]
        [TestCaseSource(nameof(ModelInRangeIntProperties))]
        public void Model_Int_Properties_With_InRangeAnnotation_Throw_ModelValidationException_When_Provided_Value_Lower_Than_Min
            (Type declaringType, PropertyInfo property, InRangeAttribute inRange)
        {
            object model = Activator.CreateInstance(declaringType);

            int valueLowerThanMin = (int)inRange.Min - 14;

            Check.ThatCode(() => property.SetValue(model, valueLowerThanMin))
                .Throws<TargetInvocationException>()
                .DueTo<ModelValidationException>();
        }

        [Test]
        [TestCaseSource(nameof(ModelInRangeIntProperties))]
        public void Model_Int_Properties_With_InRangeAnnotation_Throw_ModelValidationException_When_Provided_Value_Greater_Than_Max
            (Type declaringType, PropertyInfo property, InRangeAttribute inRange)
        {
            object model = Activator.CreateInstance(declaringType);

            int valueGreaterThanMax = (int)inRange.Max + 3;

            Check.ThatCode(() => property.SetValue(model, valueGreaterThanMax))
                .Throws<TargetInvocationException>()
                .DueTo<ModelValidationException>();
        }

        [Test]
        [TestCaseSource(nameof(ModelInRangeIntProperties))]
        public void Model_Int_Properties_With_InRangeAnnotation_Does_Not_Throw_ModelValidationException_When_Provided_Value_Between_Min_And_Max
            (Type declaringType, PropertyInfo property, InRangeAttribute inRange)
        {
            object model = Activator.CreateInstance(declaringType);

            int someValueBetweenMinAndMax = (3 * (int)inRange.Max + 2 * (int)inRange.Min) / 5;

            Check.ThatCode(() => property.SetValue(model, someValueBetweenMinAndMax))
                .DoesNotThrow();
        }

        [Test]
        [TestCaseSource(nameof(ModelInRangeIntProperties))]
        public void Model_Int_Properties_With_InRangeAnnotation_Has_A_Backing_Field_With_Unity_RangeAttribute
            (Type declaringType, PropertyInfo property, InRangeAttribute inRange)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            URangeAttribute unityRange = backingField.GetCustomAttribute<URangeAttribute>();
            Check.That(unityRange).IsNotNull();

            Check.That(unityRange.min).IsEqualTo(inRange.Min);
            Check.That(unityRange.max).IsEqualTo(inRange.Max);
        }

        [Test]
        [TestCaseSource(nameof(ModelInRangeFloatProperties))]
        public void Model_Float_Properties_With_InRangeAnnotation_Throw_ModelValidationException_When_Provided_Value_Lower_Than_Min
            (Type declaringType, PropertyInfo property, InRangeAttribute inRange)
        {
            object model = Activator.CreateInstance(declaringType);

            float valueLowerThanMin = inRange.Min - 3.14f;

            Check.ThatCode(() => property.SetValue(model, valueLowerThanMin))
                .Throws<TargetInvocationException>()
                .DueTo<ModelValidationException>();
        }

        [Test]
        [TestCaseSource(nameof(ModelInRangeFloatProperties))]
        public void Model_Float_Properties_With_InRangeAnnotation_Throw_ModelValidationException_When_Provided_Value_Greater_Than_Max
            (Type declaringType, PropertyInfo property, InRangeAttribute inRange)
        {
            object model = Activator.CreateInstance(declaringType);

            float valueGreaterThanMax = inRange.Max + 6.66f;

            Check.ThatCode(() => property.SetValue(model, valueGreaterThanMax))
                .Throws<TargetInvocationException>()
                .DueTo<ModelValidationException>();
        }

        [Test]
        [TestCaseSource(nameof(ModelInRangeFloatProperties))]
        public void Model_Float_Properties_With_InRangeAnnotation_Does_Not_Throw_ModelValidationException_When_Provided_Value_Between_Min_And_Max
            (Type declaringType, PropertyInfo property, InRangeAttribute inRange)
        {
            object model = Activator.CreateInstance(declaringType);

            float someValueBetweenMinAndMax = (2.0f * inRange.Max + 3.5f * inRange.Min) / 5.5f;

            Assert.DoesNotThrow(() => property.SetValue(model, someValueBetweenMinAndMax));
        }

        [Test]
        [TestCaseSource(nameof(ModelInRangeFloatProperties))]
        public void Model_Float_Properties_With_InRangeAnnotation_Has_A_Backing_Field_With_Unity_RangeAttribute
            (Type declaringType, PropertyInfo property, InRangeAttribute inRange)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            URangeAttribute unityRange = backingField.GetCustomAttribute<URangeAttribute>();
            Check.That(unityRange).IsNotNull();

            Check.That(unityRange.min).IsEqualTo(inRange.Min);
            Check.That(unityRange.max).IsEqualTo(inRange.Max);
        }
        #endregion

        #region NotEditable
        [Test]
        [TestCaseSource(nameof(NotEditableProperties))]
        public void Model_Properties_With_NotEditableAttribute_Has_A_Backing_Field_With_ReadOnlyAttribute
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            ReadOnlyAttribute readOnlyAttribute = backingField.GetCustomAttribute<ReadOnlyAttribute>();
            Check.That(readOnlyAttribute).IsNotNull();
        }
        #endregion

        #region EditTimeOnly
        [Test]
        [TestCaseSource(nameof(EditTimeOnlyProperties))]
        public void Model_Properties_With_EditTimeOnlyAttribute_Has_A_Backing_Field_With_EditTimeOnlyAttribute
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __EditTimeOnlyAttribute editTimeOnlyAttribute = backingField.GetCustomAttribute<__EditTimeOnlyAttribute>();
            Check.That(editTimeOnlyAttribute).IsNotNull();
        }
        #endregion

        #region DisplayIf
        [Test]
        [TestCaseSource(nameof(DisplayIfProperties))]
        public void Model_Properties_With_DisplayIfAnnotation_Has_A_Backing_Field_With_DisplayIfAttribute_With_Same_Values
            (Type declaringType, PropertyInfo property, DisplayIfAttribute displayIfAnnotation)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __DisplayIfAttribute propertyAttribute = backingField.GetCustomAttribute<__DisplayIfAttribute>();

            Check.That(propertyAttribute).IsNotNull();
            Check.That(propertyAttribute.OtherProperty).IsEqualTo(displayIfAnnotation.OtherProperty);
            Check.That(propertyAttribute.Condition).IsEqualTo(displayIfAnnotation.Condition);
            Check.That(propertyAttribute.Parameters).IsEquivalentTo(displayIfAnnotation.Parameters);
        }

        [Test]
        [TestCaseSource(nameof(DisplayIfProperties))]
        public void Model_Properties_With_DisplayIfAnnotation_OtherProperty_References_An_Existing_Property
            (Type declaringType, PropertyInfo property, DisplayIfAttribute displayIfAnnotation)
        {
            string otherPropName = displayIfAnnotation.OtherProperty;
            PropertyInfo otherProperty = declaringType.GetProperty(otherPropName);

            Check.WithCustomMessage($"{declaringType.Name} doesn't possess a '{otherPropName}' property")
                .That(otherProperty)
                .IsNotNull();
        }

        [Test]
        [TestCaseSource(nameof(DisplayIfProperties_IsTrue_Or_IsFalse))]
        public void Model_Properties_With_DisplayIfAnnotation_Which_IsTrue_Or_IsFalse_Has_No_Parameter
            (Type declaringType, PropertyInfo property, DisplayIfAttribute displayIfAnnotation)
        {
            IReadOnlyCollection<object> parameters = displayIfAnnotation.Parameters;
            Check.WithCustomMessage($"Property '{property.Name}' on type '{declaringType.Name}' has a {displayIfAnnotation.Condition} condition and should not have parameters")
                .That(parameters)
                .IsEmpty();
        }

        [Test]
        [TestCaseSource(nameof(DisplayIfProperties_IsEqualTo))]
        public void Model_Properties_With_DisplayIfAnnotation_Which_IsEqualTo_Has_A_Single_Parameter
            (Type declaringType, PropertyInfo property, DisplayIfAttribute displayIfAnnotation)
        {
            IReadOnlyCollection<object> parameters = displayIfAnnotation.Parameters;
            Check.WithCustomMessage($"Property '{property.Name}' on type '{declaringType.Name}' has a {displayIfAnnotation.Condition} condition and should a single parameter")
                .That(parameters)
                .CountIs(1);
        }

        [Test]
        [TestCaseSource(nameof(DisplayIfProperties_IsTrue_Or_IsFalse))]
        public void Model_Properties_With_DisplayIfAnnotation_Which_IsTrue_Or_IsFalse_OtherProperty_References_A_Bool_Property
            (Type declaringType, PropertyInfo property, DisplayIfAttribute displayIfAnnotation)
        {
            string otherPropName = displayIfAnnotation.OtherProperty;
            PropertyInfo otherProperty = declaringType.GetProperty(otherPropName);

            Check.WithCustomMessage($"Property '{property.Name}' on type '{declaringType.Name}' has a {displayIfAnnotation.Condition} and should reference a boolean property")
                .That(otherProperty.PropertyType)
                .IsEqualTo(typeof(bool));
        }
        #endregion

        #region AudioDevices
        [Test]
        [TestCaseSource(nameof(AudioDeviceSourceProperties))]
        public void Model_Properties_With_AudioDeviceSourceAttribute_Have_A_Backing_Field_With_AudioCaptureSourceAttribute
           (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __AudioDeviceSourceAttribute audioDeviceSourceAttr = backingField.GetCustomAttribute<__AudioDeviceSourceAttribute>();
            Check.That(audioDeviceSourceAttr).IsNotNull();
        }

        [Test]
        [TestCaseSource(nameof(AudioDeviceSourceProperties))]
        public void Model_Properties_With_AudioDeviceSourceAttribute_Be_String_Properties
            (Type declaringType, PropertyInfo property)
        {
            Check.That(property.PropertyType).IsEqualTo(typeof(string));
        }

        [Test]
        [TestCaseSource(nameof(AudioCaptureDeviceSourceProperties))]
        public void Model_Properties_With_AudioCaptureDeviceSourceAttribute_Have_A_Backing_Field_With_AudioCaptureSourceAttribute
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __AudioCaptureDeviceSourceAttribute audioCaptureDeviceSourceAttr = backingField.GetCustomAttribute<__AudioCaptureDeviceSourceAttribute>();
            Check.That(audioCaptureDeviceSourceAttr).IsNotNull();
        }

        [Test]
        [TestCaseSource(nameof(AudioCaptureDeviceSourceProperties))]
        public void Model_Properties_With_AudioCaptureDeviceSourceProperties_Be_String_Properties
            (Type declaringType, PropertyInfo property)
        {
            Check.That(property.PropertyType).IsEqualTo(typeof(string));
        }

        [Test]
        [TestCaseSource(nameof(AudioRenderDeviceSourceProperties))]
        public void Model_Properties_With_AudioRenderDeviceSourceAttribute_Have_A_Backing_Field_With_AudioRenderSourceAttribute
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __AudioRenderDeviceSourceAttribute audioRenderDeviceSourceAttr = backingField.GetCustomAttribute<__AudioRenderDeviceSourceAttribute>();
            Check.That(audioRenderDeviceSourceAttr).IsNotNull();
        }

        [Test]
        [TestCaseSource(nameof(AudioRenderDeviceSourceProperties))]
        public void Model_Properties_With_AudioRenderDeviceSourceProperties_Be_String_Properties
            (Type declaringType, PropertyInfo property)
        {
            Check.That(property.PropertyType).IsEqualTo(typeof(string));
        }
        #endregion

        #region IsClass
        [Test]
        [TestCaseSource(nameof(IsClassProperties))]
        public void Model_Properties_With_IsClassAttribute_Have_A_Backing_Field_With_IsClassAttribute
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __IsClassAttribute isClassAttr = backingField.GetCustomAttribute<__IsClassAttribute>();
            Check.That(isClassAttr).IsNotNull();
        }

        [Test]
        [TestCaseSource(nameof(IsClassProperties))]
        public void Model_Properties_With_IsClassAttribute_Be_TypeReference_Properties
            (Type declaringType, PropertyInfo property)
        {
            Check.That(property.PropertyType).IsEqualTo(typeof(TypeReference));
        }
        #endregion

        #region IsInterface
        [Test]
        [TestCaseSource(nameof(IsInterfaceProperties))]
        public void Model_Properties_With_IsInterfaceAttribute_Have_A_Backing_Field_With_IsInterfaceAttribute
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __IsInterfaceAttribute isInterfaceAttr = backingField.GetCustomAttribute<__IsInterfaceAttribute>();
            Check.That(isInterfaceAttr).IsNotNull();
        }

        [Test]
        [TestCaseSource(nameof(IsInterfaceProperties))]
        public void Model_Properties_With_IsInterfaceAttribute_Be_TypeReference_Properties
            (Type declaringType, PropertyInfo property)
        {
            Check.That(property.PropertyType).IsEqualTo(typeof(TypeReference));
        }
        #endregion

        #region Implements
        [Test]
        [TestCaseSource(nameof(ImplementsProperties))]
        public void Model_Properties_With_ImplementsAttribute_Have_A_Backing_Field_With_ImplementsAttribute
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __ImplementsAttribute implementsAttr = backingField.GetCustomAttribute<__ImplementsAttribute>();
            Check.That(implementsAttr).IsNotNull();
        }

        [Test]
        [TestCaseSource(nameof(ImplementsProperties))]
        public void Model_Properties_With_ImplementsAttribute_Be_TypeReference_Properties
            (Type declaringType, PropertyInfo property)
        {
            Check.That(property.PropertyType).IsEqualTo(typeof(TypeReference));
        }

        [Test]
        [TestCaseSource(nameof(ImplementsPropertiesWithInterfaceSource))]
        public void Model_Properties_With_ImplementsAttribute_And_Interface_Source_Must_Reference_A_Type_Reference_Property
            (Type declaringType, PropertyInfo property)
        {
            ImplementsAttribute implementsAttribute = property.GetCustomAttribute<ImplementsAttribute>();
            string interfaceSource = implementsAttribute.InterfaceSource;

            PropertyInfo sourceProperty = declaringType.GetProperty(interfaceSource);

            Check.WithCustomMessage("Source property should exist")
                .That(sourceProperty)
                .IsNotNull();

            Check.WithCustomMessage("Source property must be a TypeReference")
                .That(sourceProperty.PropertyType)
                .IsEqualTo(typeof(TypeReference));
        }
        #endregion

        #region Greater Than
        public static IEnumerable<object[]> GreaterThanProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<GreaterThanAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<object[]> GreaterThanFloatProperties => GreaterThanProperties
            .Where(objarr => ((PropertyInfo)objarr[1]).PropertyType == typeof(float));

        public static IEnumerable<object[]> GreaterThanIntProperties => GreaterThanProperties
            .Where(objarr => ((PropertyInfo)objarr[1]).PropertyType == typeof(int));

        [Test]
        [TestCaseSource(nameof(GreaterThanProperties))]
        public void Model_Properties_With_GreaterThanAttribute_Have_A_Backing_Field_With_GreaterThanAttribute
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __GreaterThanAttribute greaterThanAttribute = backingField.GetCustomAttribute<__GreaterThanAttribute>();
            Check.That(greaterThanAttribute).IsNotNull();
        }

        [Test]
        [TestCaseSource(nameof(GreaterThanFloatProperties))]
        public void Model_Properties_Of_Type_Float_With_GreaterThanAttribute_Either_Have_A_Value_Or_Reference_An_Existing_Float_Property
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __GreaterThanAttribute greaterThanAttribute = backingField.GetCustomAttribute<__GreaterThanAttribute>();

            bool haveValue = greaterThanAttribute.Value.HasValue;
            if (!haveValue)
            {
                string valueProperty = greaterThanAttribute.ValueProperty;
                Check.WithCustomMessage("GreaterThanProperties wih no value should reference a property")
                    .That(valueProperty)
                    .IsNotNullOrEmpty();

                PropertyInfo otherProperty = declaringType.GetProperty(valueProperty, BindingFlags.Instance | BindingFlags.Public);

                Check.WithCustomMessage("OtherProperty must be a float property")
                    .That(otherProperty.PropertyType)
                    .IsEqualTo(typeof(float));
            }
        }

        [Test]
        [TestCaseSource(nameof(GreaterThanIntProperties))]
        public void Model_Properties_Of_Type_Int_With_GreaterThanAttribute_Either_Have_A_Value_Or_Reference_An_Existing_Int_Property
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __GreaterThanAttribute greaterThanAttribute = backingField.GetCustomAttribute<__GreaterThanAttribute>();

            bool haveValue = greaterThanAttribute.Value.HasValue;
            if (!haveValue)
            {
                string valueProperty = greaterThanAttribute.ValueProperty;
                Check.WithCustomMessage("GreaterThanProperties wih no value should reference a property")
                    .That(valueProperty)
                    .IsNotNullOrEmpty();

                PropertyInfo otherProperty = declaringType.GetProperty(valueProperty, BindingFlags.Instance | BindingFlags.Public);

                Check.WithCustomMessage("OtherProperty must be an int property")
                    .That(otherProperty.PropertyType)
                    .IsEqualTo(typeof(int));
            }
        }
        #endregion

        #region Lower Than
        public static IEnumerable<object[]> LowerThanProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<LowerThanAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        public static IEnumerable<object[]> LowerThanFloatProperties => LowerThanProperties
            .Where(objarr => ((PropertyInfo)objarr[1]).PropertyType == typeof(float));

        public static IEnumerable<object[]> LowerThanIntProperties => LowerThanProperties
            .Where(objarr => ((PropertyInfo)objarr[1]).PropertyType == typeof(int));

        [Test]
        [TestCaseSource(nameof(LowerThanProperties))]
        public void Model_Properties_With_LowerThanAttribute_Have_A_Backing_Field_With_LowerThanAttribute
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __LowerThanAttribute lowerThanAttribute = backingField.GetCustomAttribute<__LowerThanAttribute>();
            Check.That(lowerThanAttribute).IsNotNull();
        }

        [Test]
        [TestCaseSource(nameof(LowerThanFloatProperties))]
        public void Model_Properties_Of_Type_Float_With_LowerThanAttribute_Either_Have_A_Value_Or_Reference_An_Existing_Float_Property
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __LowerThanAttribute greaterThanAttribute = backingField.GetCustomAttribute<__LowerThanAttribute>();

            bool haveValue = greaterThanAttribute.Value.HasValue;
            if (!haveValue)
            {
                string valueProperty = greaterThanAttribute.ValueProperty;
                Check.WithCustomMessage("LowerThanProperties wih no value should reference a property")
                    .That(valueProperty)
                    .IsNotNullOrEmpty();

                PropertyInfo otherProperty = declaringType.GetProperty(valueProperty, BindingFlags.Instance | BindingFlags.Public);

                Check.WithCustomMessage("OtherProperty must be a float property")
                    .That(otherProperty.PropertyType)
                    .IsEqualTo(typeof(float));
            }
        }

        [Test]
        [TestCaseSource(nameof(LowerThanIntProperties))]
        public void Model_Properties_Of_Type_Int_With_LowerThanAttribute_Either_Have_A_Value_Or_Reference_An_Existing_Int_Property
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __LowerThanAttribute greaterThanAttribute = backingField.GetCustomAttribute<__LowerThanAttribute>();

            bool haveValue = greaterThanAttribute.Value.HasValue;
            if (!haveValue)
            {
                string valueProperty = greaterThanAttribute.ValueProperty;
                Check.WithCustomMessage("LowerThanProperties wih no value should reference a property")
                    .That(valueProperty)
                    .IsNotNullOrEmpty();

                PropertyInfo otherProperty = declaringType.GetProperty(valueProperty, BindingFlags.Instance | BindingFlags.Public);

                Check.WithCustomMessage("OtherProperty must be an int property")
                    .That(otherProperty.PropertyType)
                    .IsEqualTo(typeof(int));
            }
        }
        #endregion

        #region Positive
        public static IEnumerable<object[]> PositiveProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<PositiveAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        [Test]
        [TestCaseSource(nameof(PositiveProperties))]
        public void Model_Properties_With_PositiveAttribute_Have_A_Backing_Field_With_PositiveAttribute
            (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __PositiveAttribute positiveAttribute = backingField.GetCustomAttribute<__PositiveAttribute>();
            Check.That(positiveAttribute).IsNotNull();
        }
        #endregion

        #region NDISource
        public static IEnumerable<object[]> NDISourceProperties => ModelReadWriteProperties
            .Where(prop => prop.HasCustomAttribute<NDISourceAttribute>())
            .Select(prop => new object[] { prop.DeclaringType, prop });

        [Test]
        [TestCaseSource(nameof(NDISourceProperties))]
        public void Model_Properties_With_NDISourceAttribute_Have_A_Backing_Field_With_NDISourceAttribute
           (Type declaringType, PropertyInfo property)
        {
            string expectedFieldName = "_" + property.Name.UncapitalizeFirstLetter();
            MemberInfo backingField = declaringType
                .GetMember(expectedFieldName, BindingFlags.Instance | BindingFlags.NonPublic)
                .Single();

            __NDISourceAttribute ndiSourceAttr = backingField.GetCustomAttribute<__NDISourceAttribute>();
            Check.That(ndiSourceAttr).IsNotNull();
        }

        [Test]
        [TestCaseSource(nameof(NDISourceProperties))]
        public void Model_Properties_With_NDISourceAttribute_Be_String_Properties
            (Type declaringType, PropertyInfo property)
        {
            Check.That(property.PropertyType).IsEqualTo(typeof(string));
        }
        #endregion
    }
}