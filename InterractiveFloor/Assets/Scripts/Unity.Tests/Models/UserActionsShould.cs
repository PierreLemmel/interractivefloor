﻿using InterractiveFloor.Internal;
using NFluent;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

using UObject = UnityEngine.Object;

namespace InterractiveFloor.Unity.Tests
{
    [TestFixture]
    public class UserActionsShould
    {
        private static readonly IReadOnlyCollection<Type> SceneComponentsType = AppDomain
            .CurrentDomain
            .GetAssemblies()
            .Where(assembly => assembly.FullName.Contains("InterractiveFloor.")
                && !assembly.FullName.Contains(".Tests"))
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => !type.IsAbstract && type.IsClass && type.InheritsFrom<SceneComponent>())
            .ToList();

        private static IEnumerable<MethodInfo> UserActions => SceneComponentsType
            .SelectMany(sctype => sctype.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic))
            .Where(method => method.HasCustomAttribute<UserActionAttribute>());

        private static IEnumerable<MethodInfo> EnabledIfMethods => SceneComponentsType
            .SelectMany(sctype => sctype.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic))
            .Where(method => method.HasCustomAttribute<EnabledIfAttribute>());

        [Test]
        [TestCaseSource(nameof(UserActions))]
        public void UserActions_Should_Have_No_Parameter_And_No_Return_Type(MethodInfo userAction)
        {
            Type returnType = userAction.ReturnType;
            Check.WithCustomMessage("UserActions should have no return type")
                .That(returnType)
                .IsEqualTo(typeof(void));

            ParameterInfo[] parameters = userAction.GetParameters();
            Check.WithCustomMessage("UserActions should have no parameters")
                .That(parameters)
                .IsEmpty();
        }

        [Test]
        [TestCaseSource(nameof(EnabledIfMethods))]
        public void EnabledIf_Methods_Should_Target_A_Method_Which_Has_No_Parameter_And_Returns_Bool(MethodInfo enabledIf)
        {
            string otherMethodName = enabledIf.GetCustomAttribute<EnabledIfAttribute>().OtherMethod;

            MethodInfo targetedMethod = enabledIf.DeclaringType.GetMethod(otherMethodName, BindingFlags.Instance | BindingFlags.NonPublic);
            Check.WithCustomMessage("EnabledIf methods should target a valid mathod")
                .That(targetedMethod)
                .IsNotNull();

            Type returnType = targetedMethod.ReturnType;
            Check.WithCustomMessage("EnabledIf targeted methods should return a boolean")
                .That(returnType)
                .IsEqualTo(typeof(bool));

            ParameterInfo[] parameters = targetedMethod.GetParameters();
            Check.WithCustomMessage("EnabledIf targeted methods should have no parameters")
                .That(parameters)
                .IsEmpty();
        }
    }
}