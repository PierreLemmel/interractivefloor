﻿using NFluent;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace InterractiveFloor.Unity.Tests
{
    [TestFixture]
    public class SceneComponentsShould
    {
        private static readonly IReadOnlyCollection<Type> ProjectTypes = AppDomain
            .CurrentDomain
            .GetAssemblies()
            .Where(assembly => assembly.FullName.Contains("InterractiveFloor.")
                && !assembly.FullName.Contains(".Tests"))
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => !type.IsAbstract && type.IsClass)
            .ToList();

        public static IEnumerable<Type> BehaviourTypes => ProjectTypes
            .Where(type => type.InheritsFrom<MonoBehaviour>());

        [Test]
        [TestCaseSource(nameof(BehaviourTypes))]
        public void AllBehaviours_Type_Should_Inherit_From_Scene_Component(Type behaviourType)
        {
            bool inheritsFromSceneComponent = behaviourType.InheritsFrom<SceneComponent>();
            Check.WithCustomMessage($"'{behaviourType.Name}' should inherit from '{nameof(SceneComponent)}'")
                .That(inheritsFromSceneComponent)
                .IsTrue();
        }

        [Test]
        [TestCaseSource(nameof(BehaviourTypes))]
        public void AllBehaviours_Type_Should_Not_Have_An_Awake_Method(Type behaviourType)
        {
            MethodInfo method =  behaviourType.GetMethod("Awake", BindingFlags.NonPublic | BindingFlags.Instance);

            Check
                .WithCustomMessage("SceneComponent should not have an Awake method, override 'BootStrap' method instead")
                .That(method)
                .IsNull();
        }

        [Test]
        [TestCaseSource(nameof(BehaviourTypes))]
        public void AllBehaviours_Type_Should_Not_Have_A_Start_Method(Type behaviourType)
        {
            MethodInfo method = behaviourType.GetMethod("Start", BindingFlags.NonPublic | BindingFlags.Instance);

            Check
                .WithCustomMessage("SceneComponent should not have a Start method, override 'Initialize' method instead")
                .That(method)
                .IsNull();
        }

        [Test]
        [TestCaseSource(nameof(BehaviourTypes))]
        public void AllBehaviours_Type_Should_Not_Have_An_OnEnable_Method(Type behaviourType)
        {
            MethodInfo method = behaviourType.GetMethod("OnEnable", BindingFlags.NonPublic | BindingFlags.Instance);

            Check
                .WithCustomMessage("SceneComponent should not have an OnEnable method, override 'UponActivation' method instead")
                .That(method)
                .IsNull();
        }

        [Test]
        [TestCaseSource(nameof(BehaviourTypes))]
        public void AllBehaviours_Type_Should_Not_Have_An_OnDisable_Method(Type behaviourType)
        {
            MethodInfo method = behaviourType.GetMethod("OnDisable", BindingFlags.NonPublic | BindingFlags.Instance);

            Check
                .WithCustomMessage("SceneComponent should not have a OnDisabled method, override 'UponDeactivation' method instead")
                .That(method)
                .IsNull();
        }

        [Test]
        [TestCaseSource(nameof(BehaviourTypes))]
        public void AllBehaviours_Type_Should_Not_Have_An_Update_Method(Type behaviourType)
        {
            MethodInfo method = behaviourType.GetMethod("Update", BindingFlags.NonPublic | BindingFlags.Instance);

            Check
                .WithCustomMessage("SceneComponent should not have an Update method, override 'Tick' method instead")
                .That(method)
                .IsNull();
        }

        [Test]
        [TestCaseSource(nameof(BehaviourTypes))]
        public void AllBehaviours_Type_Should_Not_Have_An_OnDestroy_Method(Type behaviourType)
        {
            MethodInfo method = behaviourType.GetMethod("OnDestroy", BindingFlags.NonPublic | BindingFlags.Instance);

            Check
                .WithCustomMessage("SceneComponent should not have a OnDisabled method, override 'OnDisposed' method instead")
                .That(method)
                .IsNull();
        }
    }
}
