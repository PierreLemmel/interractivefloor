﻿using NFluent;
using System;

namespace InterractiveFloor.Unity.Tests
{
    public static class NFluentExtensions
    {
        public static ICheckLink<ICheck<string>> IsNotNullOrEmpty(this ICheck<string> check) => check.Not.IsNullOrEmpty();
        public static ICheckLink<ICheck<string>> IsNotNullOrWhiteSpace(this ICheck<string> check) => check.Not.IsNullOrWhiteSpace();
    }
}