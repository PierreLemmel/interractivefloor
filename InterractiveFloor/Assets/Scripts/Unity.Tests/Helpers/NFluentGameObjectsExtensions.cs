﻿using NFluent;
using NFluent.Extensibility;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace InterractiveFloor.Unity.Tests
{
    public static class NFluentGameObjectsExtensions
    {
        public static ICheckLink<ICheck<GameObject>> HasComponent<TComponent>(this ICheck<GameObject> check) where TComponent : Component
        {
            var checker = ExtensibilityHelper.ExtractChecker(check);

            return checker.ExecuteCheck(
                () =>
                {
                    if (!checker.Value.HasComponent<TComponent>())
                        throw new FluentCheckException($"The object doesn't have a {typeof(TComponent).Name} component");
                },
                $"The object have a {typeof(TComponent).Name} component whereas it should not");
        }

        public static ICheckLink<ICheck<GameObject>> HasComponentInChildren<TComponent>(this ICheck<GameObject> check) where TComponent : Component
        {
            var checker = ExtensibilityHelper.ExtractChecker(check);

            return checker.ExecuteCheck(
                () =>
                {
                    if (!checker.Value.HasComponentInChildren<TComponent>())
                        throw new FluentCheckException($"The object doesn't have a {typeof(TComponent).Name} component in its children");
                },
                $"The object have a {typeof(TComponent).Name} component in its children whereas it should not");
        }

        public static ICheckLink<ICheck<GameObject>> HasChildren(this ICheck<GameObject> check)
        {
            var checker = ExtensibilityHelper.ExtractChecker(check);

            return checker.ExecuteCheck(
                () =>
                {
                    if (!checker.Value.HasChildren())
                        throw new FluentCheckException("The object doesn't have a child");
                },
                "The object have a child whereas it should not");
        }
    }
}